Code imported and adapted from
- https://github.com/catboost/catboost/blob/v1.2.1/catboost/libs/standalone_evaluator
- https://github.com/catboost/catboost/tree/v1.2.1/catboost/libs/model/flatbuffers
- https://github.com/catboost/catboost/blob/v1.2.1/catboost/libs/helpers/flatbuffers

See https://github.com/catboost/catboost/blob/v1.2.1/README.md for copyright and license
