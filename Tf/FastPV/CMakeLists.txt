###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Tf/FastPV
---------
#]=======================================================================]

gaudi_add_module(FastPV
    SOURCES
        src/FastPVFinder.cpp
        src/FastVertex.cpp
        src/TrackForPV.cpp
    LINK
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::DetDescLib
        LHCb::VPDetLib
        LHCb::MCEvent
        LHCb::RecEvent
        LHCb::TrackEvent
)
