
# Event Size Variables
#NumRich1Hits  ; I ; NumRich1Hits
#NumRich2Hits  ; I ; NumRich2Hits
#NumSPDHits    ; I ; NumSPDHits
#NumLongTracks ; I ; NumLongTracks
#NumMuonTracks ; I ; NumMuonTracks
#NumTTracks    ; I ; NumTTracks
#NumPVs        ; I ; NumPVs

TrackP ; F ; TrackP
TrackPt ; F ; TrackPt
TrackChi2PerDof ; F ; TrackChi2PerDof
TrackNumDof ; I ; TrackNumDof
TrackGhostProb ; F ; TrackGhostProbability
TrackFitVeloChi2 ; F ; TrackFitVeloChi2
TrackFitVeloNDoF ; I ; TrackFitVeloNDoF
RichUsedR1Gas ; I ; RichUsedR1Gas
RichAbovePiThres ; I ; RichAbovePiThres
RichAboveKaThres ; I ; RichAboveKaThres
RichDLLe ; F ; RichDLLe
RichDLLmu ; F ; RichDLLmu
RichDLLk ; F ; RichDLLk
RichDLLp ; F ; RichDLLp
RichDLLbt ; F ; RichDLLbt
InAccBrem ; I ; InAccBrem
BremPIDe ; F ; BremPIDe
