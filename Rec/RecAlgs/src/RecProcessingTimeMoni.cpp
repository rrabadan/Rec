/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include <algorithm>
//-----------------------------------------------------------------------------
// Implementation file for class : RecProcessingTimeMoni
//
// 2010-07-15 : Chris Jones
//-----------------------------------------------------------------------------

/** @class RecProcessingTimeMoni RecProcessingTimeMoni.h
 *
 *  Simple monitor making basic processing time plots for the Reconstruction
 *
 *  @author Chris Jones
 *  @date   2010-07-15
 */

class RecProcessingTimeMoni final : public GaudiHistoAlg {

public:
  /// Standard constructor
  RecProcessingTimeMoni( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  /// Definition of algorithm name list
  typedef std::vector<std::string> AlgorithmNames;

  AIDA::IHistogram1D* m_hist = nullptr; ///< Pointer to processing time histogram

  Gaudi::Property<AlgorithmNames> m_algNames{this, "Algorithms"}; ///< List of algorithm(s) to include in timing
  Gaudi::Property<double>         m_logMaxTime{this, "LogMaxEventTime",
                                       8.0}; ///< Job Option for log10(maximum overall processing time) for plots
  Gaudi::Property<double>         m_logMinTime{this, "LogMinEventTime",
                                       -2.0}; ///< Job Option for log10(minimum overall processing time) for plots
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RecProcessingTimeMoni )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RecProcessingTimeMoni::RecProcessingTimeMoni( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiHistoAlg( name, pSvcLocator ) {
  setProperty( "HistoTopDir", "Timing/" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode RecProcessingTimeMoni::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) return sc;

  // are we properly configured
  if ( m_algNames.empty() ) { sc = Warning( "No algorithms to time !" ); }

  // book the histogram at initialization time
  m_hist = book( "overallTime", "log10(Event Processing Time / ms)", m_logMinTime, m_logMaxTime, 100 );

  // return
  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode RecProcessingTimeMoni::execute() {

  // Loop over algorithms to include in the timing and add them up
  double time = std::accumulate(
      m_algNames.begin(), m_algNames.end(), double{0}, [&, chrono = chronoSvc()]( double t, const auto& name ) {
        const auto alg_time = chrono->chronoDelta( name + ":Execute", IChronoStatSvc::ELAPSED ) / 1000;
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << name << " " << alg_time << endmsg;
        return t + alg_time;
      } );

  // only fill if algorithm(s) ran (time>0)
  if ( time > 0 ) {
    // Take the base 10 log of the time (helps show the large tails)
    fill( m_hist, std::log10( time ), 1.0 );
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
