/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IProfile1D.h"
#include "Event/ProcStatus.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiUtils/HistoLabels.h"
#include "LHCbAlgs/Consumer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ProcStatAbortMoni
//
// 2010-07-16 : Chris Jones
//-----------------------------------------------------------------------------

/** @class ProcStatAbortMoni ProcStatAbortMoni.h
 *
 *  Monitor for abort rates in ProcStat
 *
 *  @author Chris Jones
 *  @date   2010-07-16
 */

class ProcStatAbortMoni final
    : public LHCb::Algorithm::Consumer<void( const LHCb::ProcStatus& ),
                                       Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  /// Standard constructor
  ProcStatAbortMoni( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;                                ///< Algorithm initialization
  void       operator()( const LHCb::ProcStatus& ) const override; ///< Algorithm execution

private:
  /// List of subsystems
  Gaudi::Property<std::vector<std::string>> m_subSystems{
      this,
      "SubSystems",
      {"Overall", "Hlt", "VELO", "TT", "IT", "OT", "Tracking", "Vertex", "RICH", "CALO", "MUON", "PROTO"}};

  /// cache the histogram pointer
  AIDA::IProfile1D* m_h = nullptr;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ProcStatAbortMoni )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ProcStatAbortMoni::ProcStatAbortMoni( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, {"ProcStatusLocation", LHCb::ProcStatusLocation::Default} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode ProcStatAbortMoni::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;

  // book the histo
  m_h = bookProfile1D( "aborts", "Processing Abort Rates (%)", -0.5, m_subSystems.size() - 0.5, m_subSystems.size() );
  // Set the bin labels
  const bool ok = Gaudi::Utils::Histos::setBinLabels( m_h, m_subSystems );

  // return
  return ok ? sc : StatusCode::FAILURE;
}

//=============================================================================
// Main execution
//=============================================================================
void ProcStatAbortMoni::operator()( const LHCb::ProcStatus& proc ) const {
  // Loop over sub-systems and fill plot
  int index( 0 );
  for ( const auto& sys : m_subSystems ) {
    ++index;
    if ( sys == "Overall" ) {
      m_h->fill( index, proc.aborted() ? 100.0 : 0.0 );
    } else {
      m_h->fill( index, proc.subSystemAbort( sys ) ? 100.0 : 0.0 );
    }
  }
  // Debug printout if aborted
  if ( proc.aborted() && msgLevel( MSG::DEBUG ) ) { debug() << proc << endmsg; }
}

//=============================================================================
