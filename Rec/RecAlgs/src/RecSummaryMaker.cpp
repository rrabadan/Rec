/***************************************************************************** \
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/RecSummary.h"
#include "LHCbAlgs/Consumer.h"
#include "LHCbAlgs/EmptyProducer.h"
#include "LHCbAlgs/Transformer.h"

// All neccessary objects
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDigits_v2.h"
#include "Event/FTLiteCluster.h"
#include "Event/PrHits.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "Event/VPFullCluster.h"
#include "Kernel/RichSmartID.h"
#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichUtils/RichSIMDTypes.h"

namespace {
  using Tracks        = LHCb::Track::Range;
  using PVs           = LHCb::RecVertices;
  using SciFiClusters = LHCb::FTLiteCluster::FTLiteClusters;
  using VeloHits      = LHCb::Pr::Hits<LHCb::Pr::HitType::VP>;
  using Digits        = LHCb::Event::Calo::Digits;
  using ecalClusters  = LHCb::Event::Calo::Clusters;
  using RichPixels    = Rich::Future::DAQ::DecodedData;
} // namespace

class RecSummaryMaker
    : public LHCb::Algorithm::Transformer<LHCb::RecSummary(
          Tracks const&, Tracks const&, Tracks const&, Tracks const&, Tracks const&, PVs const&, SciFiClusters const&,
          VeloHits const&, ecalClusters const&, Digits const&, Digits const&, RichPixels const& )> {
public:
  RecSummaryMaker( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"LongTracks", ""}, KeyValue{"VeloTracks", ""}, KeyValue{"UpstreamTracks", ""},
                      KeyValue{"DownstreamTracks", ""}, KeyValue{"Ttracks", ""}, KeyValue{"PVs", ""},
                      KeyValue{"SciFiClusters", ""}, KeyValue{"VeloClusters", ""}, KeyValue{"eCalClusters", ""},
                      KeyValue{"EDigits", ""}, KeyValue{"HDigits", ""}, KeyValue{"RichPixels", ""}},
                     KeyValue{"Output", ""} ) {}

  LHCb::RecSummary operator()( Tracks const& longtracks, Tracks const& velotracks, Tracks const& uptracks,
                               Tracks const& downtracks, Tracks const& ttracks, PVs const& pvs,
                               SciFiClusters const& scifi_clusters, VeloHits const& velohits,
                               ecalClusters const& ecal_clusters, Digits const& edigits, Digits const& hdigits,
                               RichPixels const& richhits ) const override {
    LHCb::RecSummary summary{};

    auto energy = []( auto init, const auto& digit ) { return init + digit.energy(); };

    const auto ecaltot = std::accumulate( edigits.begin(), edigits.end(), 0., energy );
    const auto hcaltot = std::accumulate( hdigits.begin(), hdigits.end(), 0., energy );

    auto       isVelo = []( const auto& track ) { return track->checkType( LHCb::Track::Types::Velo ); };
    const auto nVelo  = std::count_if( velotracks.begin(), velotracks.end(), isVelo );

    auto       isVeloBack = []( const auto& track ) { return track->checkType( LHCb::Track::Types::VeloBackward ); };
    const auto nBack      = std::count_if( velotracks.begin(), velotracks.end(), isVeloBack );

    summary.addInfo( LHCb::RecSummary::DataTypes::nPVs, pvs.size() );
    summary.addInfo( LHCb::RecSummary::DataTypes::nFTClusters, scifi_clusters.size() );
    summary.addInfo( LHCb::RecSummary::DataTypes::nVPClusters, velohits.size() );
    summary.addInfo( LHCb::RecSummary::DataTypes::nEcalClusters, ecal_clusters.size() );
    summary.addInfo( LHCb::RecSummary::DataTypes::eCalTot, ecaltot );
    summary.addInfo( LHCb::RecSummary::DataTypes::hCalTot, hcaltot );

    summary.addInfo( LHCb::RecSummary::DataTypes::nLongTracks, longtracks.size() );
    summary.addInfo( LHCb::RecSummary::DataTypes::nDownstreamTracks, downtracks.size() );
    summary.addInfo( LHCb::RecSummary::DataTypes::nUpstreamTracks, uptracks.size() );
    summary.addInfo( LHCb::RecSummary::DataTypes::nTTracks, ttracks.size() );
    summary.addInfo( LHCb::RecSummary::DataTypes::nVeloTracks, nVelo );
    summary.addInfo( LHCb::RecSummary::DataTypes::nBackTracks, nBack );

    summary.addInfo( LHCb::RecSummary::DataTypes::nRich1Hits, richhits.nTotalHits( Rich::Rich1 ) );
    summary.addInfo( LHCb::RecSummary::DataTypes::nRich2Hits, richhits.nTotalHits( Rich::Rich2 ) );

    return summary;
  }
};

DECLARE_COMPONENT( RecSummaryMaker )

class RecSummaryPrinter : public LHCb::Algorithm::Consumer<void( LHCb::RecSummary const& )> {
public:
  RecSummaryPrinter( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, {KeyValue{"Input", ""}} ){};

  void operator()( LHCb::RecSummary const& summary ) const override { always() << summary << endmsg; };
};

DECLARE_COMPONENT( RecSummaryPrinter )

// Algorithm to create an empty RecSummary
DECLARE_COMPONENT_WITH_ID( EmptyProducer<LHCb::RecSummary>, "FakeRecSummaryMaker" )
