###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rec/RecInterfaces
-----------------
#]=======================================================================]

gaudi_add_header_only_library(RecInterfacesLib
    LINK
        Gaudi::GaudiKernel
        LHCb::DetDescLib
        LHCb::PhysEvent
        LHCb::RecEvent
)

gaudi_add_dictionary(RecInterfacesDict
    HEADERFILES dict/RecInterfacesDict.h
    SELECTION dict/RecInterfacesDict.xml
    LINK RecInterfacesLib
    OPTIONS ${REC_DICT_GEN_DEFAULT_OPTS}
)
