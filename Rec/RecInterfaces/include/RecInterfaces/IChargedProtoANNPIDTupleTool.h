/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Particle.h"
#include "Event/ProtoParticle.h"

#include "GaudiKernel/IAlgTool.h"

#include <string>

#include "DetDesc/IGeometryInfo.h"
namespace Tuples {
  class Tuple;
}

namespace ANNGlobalPID {

  /**
   *  Interface to tool to fill the ANN PID variables into a tuple
   *
   *  @author Chris Jones
   *  @date   2011-02-04
   */
  struct IChargedProtoANNPIDTupleTool : extend_interfaces<IAlgTool> {

    DeclareInterfaceID( IChargedProtoANNPIDTupleTool, 3, 0 );

    /** Fill the tuple tool with information for the given ProtoParticle
     *  @param proto Pointer to the ProtoParticle to fill into the tuple
     *  @param pdgCode The PID to assume for this ProtoParticle
     *  @return StatusCode indicating if the ProtoParticle information was successfully filled
     */
    virtual StatusCode fill( Tuples::Tuple& tuple, LHCb::ProtoParticle const* proto, IGeometryInfo const& geometry,
                             LHCb::ParticleID pid = LHCb::ParticleID() ) const = 0;

    /** Fill the tuple tool with information for the given Particle
     *  @param part Pointer to the Particle to fill into the tuple
     *  @return StatusCode indicating if the Particle information was successfully filled
     */
    StatusCode fill( Tuples::Tuple& tuple, LHCb::Particle const* part, IGeometryInfo const& geometry ) const {
      return ( part ? fill( tuple, part->proto(), geometry, part->particleID() ) : StatusCode::SUCCESS );
    }
  };

} // namespace ANNGlobalPID
