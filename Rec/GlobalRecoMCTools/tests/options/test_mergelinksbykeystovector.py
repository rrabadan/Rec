###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration .         #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test the MergeLinksByKeysToVector algorithm.

Asserts that the list of input objects is converted to an output vector of
pointers to those objects.
"""
from __future__ import print_function

from Configurables import (
    ApplicationMgr,
    LHCbApp,
    MergeLinksByKeysToVector,
)
import GaudiPython as GP
from PRConfig.TestFileDB import test_file_db
from DDDB.CheckDD4Hep import UseDD4Hep


def compare(links_vec, original_link_paths):
    assert len(links_vec) == len(original_link_paths), "Size mismatch"
    for idx, link_path in enumerate(original_link_paths):
        element = links_vec.at(idx)
        expected = TES[link_path]
        assert element == expected, "Index {} in output vector is not equal to object at {}".format(
            idx, link_path)
        # Couple more checks out of paranoia (in case for example the equality method does very little)
        assert element.linkMgr().size() == expected.linkMgr().size(
        ), "Index {} in output vector has different LinkMgr".format(idx)
        assert element.linkReference().size() == expected.linkReference().size(
        ), "Index {} in output vector has different linkReference".format(idx)


app = LHCbApp(EvtMax=100)
test_file_db["upgrade_minbias_hlt1_filtered"].run(configurable=app)
if not UseDD4Hep:
    from Configurables import CondDB
    CondDB(Upgrade=True)

charged_merger = MergeLinksByKeysToVector(
    "ChargedMergeLinksByKeysToVector",
    InputLinksByKeys=["Link/Rec/Track/Best"],
    Output="Link/MergedTrack2MCP",
)

hypo_locs = [
    "Rec/Calo/Photons", "Rec/Calo/SplitPhotons", "Rec/Calo/MergedPi0s"
]
neutral_merger = MergeLinksByKeysToVector(
    "NeutralMergeLinksByKeysToVector",
    InputLinksByKeys=[str("Link/" + loc) for loc in hypo_locs],
    Output="Link/MergedCaloHypo2MCP",
)

ApplicationMgr(TopAlg=[
    charged_merger,
    neutral_merger,
])

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

i = 0
while TES["/Event"] and i < app.EvtMax:
    charged_link_paths = charged_merger.getProp("InputLinksByKeys")
    charged_links_vec = TES[str(charged_merger.getProp("Output"))].getData()
    compare(charged_links_vec, charged_link_paths)

    neutral_link_paths = neutral_merger.getProp("InputLinksByKeys")
    neutral_links_vec = TES[str(neutral_merger.getProp("Output"))].getData()
    compare(neutral_links_vec, neutral_link_paths)

    appMgr.run(1)
    i += 1

print("SUCCESS")
