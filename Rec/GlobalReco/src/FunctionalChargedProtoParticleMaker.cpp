/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Functors/with_functors.h"
#include "Interfaces/IProtoParticleTool.h"

#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include <DetDesc/DetectorElement.h>

#include "LHCbAlgs/MergingTransformer.h"

#include <memory>
#include <string>

/**
 * Algorithm to build charged ProtoParticles from charged Tracks.
 * A selection can be applied to the tracks via a TrackSelector tool.
 *
 *
 * @author Sascha Stahl   Sascha.Stahl@cern.ch
 * @date 03/03/2020
 */

namespace {
  struct TrackPredicate {
    using Signature                    = bool( const LHCb::Track& );
    static constexpr auto PropertyName = "Code";
  };
} // namespace

class FunctionalChargedProtoParticleMaker final
    : public with_functors<
          LHCb::Algorithm::MergingTransformer<
              LHCb::ProtoParticles( const Gaudi::Functional::vector_of_const_<LHCb::Track::Range>& ranges ),
              LHCb::Algorithm::Traits::usesConditions<>>,
          TrackPredicate> {

public:
  /// Standard constructor
  FunctionalChargedProtoParticleMaker( const std::string& name, ISvcLocator* pSvcLocator )
      : with_functors( name, pSvcLocator, //
                       {"Inputs", {LHCb::TrackLocation::Default}},
                       KeyValue{"Output", LHCb::ProtoParticleLocation::Charged} ) {}

  LHCb::ProtoParticles
  operator()( const Gaudi::Functional::vector_of_const_<LHCb::Track::Range>& ranges ) const override {
    if ( !m_det.get().geometry() ) { throw GaudiException( "Could not load geometry", name(), StatusCode::FAILURE ); }
    auto& geometry = *m_det.get().geometry();
    // make output container
    LHCb::ProtoParticles protos;
    auto const&          track_pred = getFunctor<TrackPredicate>();
    const bool           useTkKey   = ( ranges.size() == 1 );
    // Loop over tracks container
    for ( const auto& tracks : ranges ) {
      protos.reserve( protos.size() + tracks.size() );
      // Loop over tracks
      for ( const auto* tk : tracks ) {
        // Select tracks
        if ( !tk || !track_pred( *tk ) ) { continue; }
        // Make a proto-particle
        auto proto = std::make_unique<LHCb::ProtoParticle>();
        // Set track reference
        proto->setTrack( tk );
        // Add minimal track info
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackChi2PerDof, tk->chi2PerDoF() );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackNumDof, tk->nDoF() );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackHistory, static_cast<int>( tk->history() ) );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackType, static_cast<int>( tk->type() ) );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackP, tk->p() );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackPt, tk->pt() );
        // Save in output container
        // If more than one Track container, cannot use Track key
        if ( useTkKey ) {
          protos.insert( proto.release(), tk->key() );
        } else {
          protos.insert( proto.release() );
        }
        ++m_count;
      }
    }
    for ( const auto& addInfo : m_addInfo ) ( *addInfo )( protos, geometry ).ignore();
    return protos;
  }

private:
  Gaudi::Property<std::string> m_standardGeometry_address{this, "StandardGeometryTop", LHCb::standard_geometry_top};
  // A merging transformer does not easily allow this to be passed in the operator(), so we just do it manually and pass
  // it as a member variable.
  LHCb::DetDesc::ConditionAccessor<LHCb::Detector::DeLHCb> m_det{this, "DeLHCb", m_standardGeometry_address};

  mutable Gaudi::Accumulators::Counter<>                  m_count{this, "CreatedProtos"};
  ToolHandleArray<LHCb::Rec::Interfaces::IProtoParticles> m_addInfo{this, "AddInfo", {}};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FunctionalChargedProtoParticleMaker )
