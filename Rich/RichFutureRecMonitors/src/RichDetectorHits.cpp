/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichUtils/RichDAQDefinitions.h"

// STD
#include <cstdint>
#include <mutex>

namespace Rich::Future::Rec::Moni {

  /** @class DetectorHits RichDetectorHits.h
   *
   *  Monitors the data sizes in the RICH detectors.
   *
   *  @author Chris Jones
   *  @date   2016-12-06
   */

  class DetectorHits final : public LHCb::Algorithm::Consumer<
                                 void( const DAQ::DecodedData&,            //
                                       const Rich::Utils::RichSmartIDs& ), //
                                 LHCb::DetDesc::usesBaseAndConditions<HistoAlgBase, Rich::Utils::RichSmartIDs>> {

  public:
    /// Standard constructor
    DetectorHits( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"DecodedDataLocation", DAQ::DecodedDataLocation::Default},
                     // input conditions data
                     KeyValue{"RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins2DHistos", 50 ).ignore();
    }

    /// Initialize
    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] { Rich::Utils::RichSmartIDs::addConditionDerivation( this ); } );
    }

  public:
    /// Functional operator
    void operator()( const DAQ::DecodedData& data, const Rich::Utils::RichSmartIDs& smartIDsHelper ) const override {

      // Count active PDs in each RICH
      DetectorArray<std::uint32_t> activePDs = {{}};
      // Count hits in each RICH
      DetectorArray<std::uint32_t> richHits = {{}};
      // Count active modules
      DetectorArray<std::uint32_t> richModules = {{}};

      // the lock
      std::lock_guard lock( m_updateLock );

      // Loop over RICHes
      for ( const auto rich : activeDetectors() ) {

        // data for this RICH
        const auto& rD = data[rich];

        // sides per RICH
        for ( const auto& pD : rD ) {

          // PD modules per side
          richModules[rich] += pD.size();
          for ( const auto& mD : pD ) {

            // PDs per module
            for ( const auto& PD : mD ) {

              // PD ID
              const auto pdID = PD.pdID();
              if ( pdID.isValid() ) {

                // Vector of SmartIDs
                const auto& rawIDs = PD.smartIDs();

                // Do we have any hits
                if ( !rawIDs.empty() ) {

                  // Side
                  const auto side = pdID.panel();

                  // Fill average HPD occ plot
                  fillHisto( h_nTotalPixsPerPD[rich], rawIDs.size() );

                  // count active PDs
                  ++activePDs[rich];

                  // count hits
                  richHits[rich] += rawIDs.size();

                  // loop over hits
                  for ( const auto id : rawIDs ) {
                    // get global/local coords
                    const auto gPos = smartIDsHelper.globalPosition( id );
                    const auto lPos = smartIDsHelper.globalToPDPanel( gPos );
                    // fill plots
                    fillHisto( h_pixXYGlo[rich][side], gPos.X(), gPos.Y() );
                    fillHisto( h_pixXGlo[rich][side], gPos.X() );
                    fillHisto( h_pixYGlo[rich][side], gPos.Y() );
                    fillHisto( h_pixXYLoc[rich], lPos.X(), lPos.Y() );
                    fillHisto( h_pixXLoc[rich], lPos.X() );
                    fillHisto( h_pixYLoc[rich], lPos.Y() );
                  }

                } // PD has hits

              } // PDID is valid

            } // PDs

          } // modules

        } // panels

      } // RICHes

      // Loop over RICHes
      for ( const auto rich : activeDetectors() ) {
        // Fill active PD plots
        fillHisto( h_nActivePDs[rich], activePDs[rich] );
        // Fill RICH hits plots
        if ( richHits[rich] > 0 ) {
          fillHisto( h_nTotalPixs[rich], richHits[rich] );
          fillHisto( h_nActiveModules[rich], richModules[rich] );
        }
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;

      const DetectorArray<double> panelXsizes{{650, 800}};
      const DetectorArray<double> panelYsizes{{680, 750}};
      const DetectorArray<double> gloXsizes{{750, 4100}};
      const DetectorArray<double> gloYsizes{{1600, 800}};
      const DetectorArray<double> pSF{{0.6, 0.5}}; // reduced (x,y) size in global projection

      for ( const auto rich : activeDetectors() ) {
        ok &= saveAndCheck( h_nTotalPixsPerPD[rich],                                   //
                            richHisto1D( Rich::HistogramID( "nTotalPixsPerPD", rich ), //
                                         "Average overall PD occupancy (nHits>0)", 0.5, 64.5, 65 ) );
        ok &= saveAndCheck( h_nTotalPixs[rich],                                   //
                            richHisto1D( Rich::HistogramID( "nTotalPixs", rich ), //
                                         "Overall occupancy (nHits>0)", 0, m_maxPixels, nBins1D() ) );
        ok &= saveAndCheck( h_nActivePDs[rich],                                   //
                            richHisto1D( Rich::HistogramID( "nActivePDs", rich ), //
                                         "# Active PDs (nHits>0)", -0.5, 2000.5, 2001 ) );
        ok &= saveAndCheck( h_nActiveModules[rich],                                   //
                            richHisto1D( Rich::HistogramID( "nActiveModules", rich ), //
                                         "# Active PD Modules (nHits>0)", -0.5, 150.5, 151 ) );
        // hit points (global)
        for ( const auto side : Rich::sides() ) {
          const auto minX =
              ( rich == Rich::Rich1 ? -gloXsizes[rich]
                                    : side == Rich::right ? -gloXsizes[rich] //
                                                          : gloXsizes[rich] - ( pSF[rich] * panelXsizes[rich] ) );
          const auto maxX =
              ( rich == Rich::Rich1 ? gloXsizes[rich]
                                    : side == Rich::right ? ( pSF[rich] * panelXsizes[rich] ) - gloXsizes[rich] //
                                                          : gloXsizes[rich] );
          const auto minY =
              ( rich == Rich::Rich2 ? -gloYsizes[rich]
                                    : side == Rich::top ? gloYsizes[rich] - ( pSF[rich] * panelYsizes[rich] ) //
                                                        : -gloYsizes[rich] );
          const auto maxY =
              ( rich == Rich::Rich2 ? gloYsizes[rich]
                                    : side == Rich::top ? gloYsizes[rich] //
                                                        : -gloYsizes[rich] + ( pSF[rich] * panelYsizes[rich] ) );
          ok &= saveAndCheck( h_pixXYGlo[rich][side],                     //
                              richHisto2D( HID( "pixXYGlo", rich, side ), //
                                           "Global (X,Y) hits",           //
                                           minX, maxX, nBins2D(),         //
                                           minY, maxY, nBins2D(),         //
                                           "Global X / mm", "Global Y / mm" ) );
          ok &= saveAndCheck( h_pixXGlo[rich][side],                     //
                              richHisto1D( HID( "pixXGlo", rich, side ), //
                                           "Global X hits",              //
                                           minX, maxX, nBins2D(),        //
                                           "Global X / mm" ) );
          ok &= saveAndCheck( h_pixYGlo[rich][side],                     //
                              richHisto1D( HID( "pixYGlo", rich, side ), //
                                           "Global Y hits",              //
                                           minY, maxY, nBins2D(),        //
                                           "Global Y / mm" ) );
        }
        // hit points (local)
        ok &= saveAndCheck( h_pixXYLoc[rich],                                              //
                            richHisto2D( HID( "pixXYLoc", rich ),                          //
                                         "Local (X,Y) hits",                               //
                                         -panelXsizes[rich], panelXsizes[rich], nBins2D(), //
                                         -panelYsizes[rich], panelYsizes[rich], nBins2D(), //
                                         "Local X / mm", "Local Y / mm" ) );
        ok &= saveAndCheck( h_pixXLoc[rich],                                               //
                            richHisto1D( HID( "pixXLoc", rich ),                           //
                                         "Local X hits",                                   //
                                         -panelXsizes[rich], panelXsizes[rich], nBins2D(), //
                                         "Local X / mm" ) );
        ok &= saveAndCheck( h_pixYLoc[rich],                                               //
                            richHisto1D( HID( "pixYLoc", rich ),                           //
                                         "Local Y hits",                                   //
                                         -panelYsizes[rich], panelYsizes[rich], nBins2D(), //
                                         "Local Y / mm" ) );
      }

      return StatusCode{ok};
    }

  private:
    /// Maximum pixels
    Gaudi::Property<unsigned int> m_maxPixels{this, "MaxPixels", 10000u};

  private:
    // data

    /// mutex lock
    mutable std::mutex m_updateLock;

  private:
    // cached histograms

    /// Pixels per PD histograms
    DetectorArray<AIDA::IHistogram1D*> h_nTotalPixsPerPD = {{}};
    /// Pixels per detector histograms
    DetectorArray<AIDA::IHistogram1D*> h_nTotalPixs = {{}};
    /// Number active PDs histograms
    DetectorArray<AIDA::IHistogram1D*> h_nActivePDs = {{}};
    /// Number active module histograms
    DetectorArray<AIDA::IHistogram1D*> h_nActiveModules = {{}};

    DetectorArray<PanelArray<AIDA::IHistogram2D*>> h_pixXYGlo = {{}};
    DetectorArray<PanelArray<AIDA::IHistogram1D*>> h_pixXGlo  = {{}};
    DetectorArray<PanelArray<AIDA::IHistogram1D*>> h_pixYGlo  = {{}};

    DetectorArray<AIDA::IHistogram2D*> h_pixXYLoc = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_pixXLoc  = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_pixYLoc  = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DetectorHits )

} // namespace Rich::Future::Rec::Moni
