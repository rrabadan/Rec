/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecBase.icpp
 *
 *  Implementation file for RICH reconstruction base class : Rich::Rec::CommonBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005-08-26
 */
//-----------------------------------------------------------------------------

#pragma once

// local
#include "RichFutureRecBase/RichRecBase.h"

//=============================================================================
// Initialisation
//=============================================================================
template <class PBASE>
StatusCode Rich::Future::Rec::CommonBase<PBASE>::initialiseRichReco() {

  // load tools
  auto sc = m_richPartProp.retrieve();
  if ( !sc ) return sc;

  // consistency checks
  if ( richIsActive( Rich::Rich1 ) != radiatorIsActive( Rich::Rich1Gas ) ||
       richIsActive( Rich::Rich2 ) != radiatorIsActive( Rich::Rich2Gas ) ) {
    base()->error() << "Inconsistent RICH " << m_richIsActive //
                    << " and radiator " << m_radIsActive << " options" << endmsg;
    return StatusCode::FAILURE;
  }

  // Get the list of PID types to consider
  m_pidTypes = m_richPartProp->particleTypes();

  // cache active hypos excluding 'below threshold'
  m_pidTypesNoBT  = m_pidTypes;
  const auto btIt = std::find( m_pidTypesNoBT.begin(), m_pidTypesNoBT.end(), Rich::BelowThreshold );
  if ( btIt != m_pidTypesNoBT.end() ) { m_pidTypesNoBT.erase( btIt ); }

  // Initialise the active detectors and radiators
  for ( const auto rich : Rich::detectors() ) {
    if ( richIsActive( rich ) ) { m_activeRiches.push_back( rich ); }
  }
  for ( const auto rad : Rich::radiators() ) {
    if ( radiatorIsActive( rad ) ) { m_activeRadiators.push_back( rad ); }
  }

  // return
  return sc;
}
//=============================================================================

//=============================================================================
// Finalisation
//=============================================================================
template <class PBASE>
StatusCode Rich::Future::Rec::CommonBase<PBASE>::finaliseRichReco() {
  return StatusCode::SUCCESS;
}
//=============================================================================
