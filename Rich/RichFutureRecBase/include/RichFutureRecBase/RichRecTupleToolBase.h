/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecTupleToolBase.h
 *
 * Header file for reconstruction tool base class : Rich::Future::Rec::TupleToolBase
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date   2005/01/13
 */
//-----------------------------------------------------------------------------

#pragma once

// Base class
#include "RichFutureKernel/RichTupleToolBase.h"
#include "RichFutureRecBase/RichRecBase.h"

namespace Rich::Future::Rec {

  //-----------------------------------------------------------------------------
  /** @class TupleToolBase RichRecTupleToolBase.h RichRecBase/RichRecTupleToolBase.h
   *
   *  Abstract base class for RICH reconstruction tools providing
   *  some basic functionality (identical to RichRecToolBase) but with additional
   *  histogram and ntuple functionality provided by GaudiTupleTool base class.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2005/01/13
   */
  //-----------------------------------------------------------------------------

  class TupleToolBase : public Rich::Future::TupleToolBase,
                        public Rich::Future::Rec::CommonBase<Rich::Future::TupleToolBase> {

  public:
    /// Standard constructor
    TupleToolBase( const std::string& type, const std::string& name, const IInterface* parent );

    /** Initialization of the tool after creation
     *
     * @return The status of the initialization
     * @retval StatusCode::SUCCESS Initialization was successful
     * @retval StatusCode::FAILURE Initialization failed
     */
    StatusCode initialize() override;

    /** Finalization of the tool before deletion
     *
     * @return The status of the finalization
     * @retval StatusCode::SUCCESS Finalization was successful
     * @retval StatusCode::FAILURE Finalization failed
     */
    StatusCode finalize() override;
  };

} // namespace Rich::Future::Rec
