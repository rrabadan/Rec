/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Event Model
#include "Event/MCRichHit.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// Utils
#include "RichFutureUtils/RichSmartIDs.h"

// STD
#include <map>

namespace Rich::Future::Rec::MC {

  /** @class RichPixelUseMCInfo RichPixelUseMCInfo.h
   *
   *  Use MC truth to cheat RICH pixel positions and/or time
   *
   *  @author Chris Jones
   *  @date   2023-09-20
   */

  class RichPixelUseMCInfo final : public LHCb::Algorithm::Transformer<
                                       SIMDPixelSummaries( SIMDPixelSummaries const&, //
                                                           LHCb::MCRichHits const&,   //
                                                           Rich::Utils::RichSmartIDs const& ),
                                       LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Rich::Utils::RichSmartIDs>> {

  public:
    /// Standard constructor
    RichPixelUseMCInfo( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       {KeyValue{"InRichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                        KeyValue{"MCRichHitsLocation", LHCb::MCRichHitLocation::Default},
                        // input conditions data
                        KeyValue{"RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey}},
                       // output data
                       {KeyValue{"OutRichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default + "Out"}} ) {}

    /// Initialize
    StatusCode initialize() override {
      // base class initialise followed by conditions
      return Transformer::initialize().andThen( [&] {
        // create the RICH smartID helper instance
        Rich::Utils::RichSmartIDs::addConditionDerivation( this );
        // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
      } );
    }

  public:
    /// Functional operator
    SIMDPixelSummaries operator()( SIMDPixelSummaries const&        pixels, //
                                   LHCb::MCRichHits const&          mchits, //
                                   Rich::Utils::RichSmartIDs const& smartIDsHelper ) const override {

      // Collect all the (signal) hits in the same pixel
      std::map<LHCb::RichSmartID, std::vector<LHCb::MCRichHit*>> hitsPerPix;
      for ( const auto mchit : mchits ) {
        auto id = mchit->sensDetID().channelDataOnly();
#ifdef USE_DD4HEP
        if ( m_detdescMCinput ) {
          // If built for DD4HEP apply correction to PMT module numbers to account
          // for different numbering scheme between DD4HEP and DetDesc.
          // Option needs to be explicitly activated only when input is known to
          // be DetDesc based MC.
          // ***** To eventually be removed when DetDesc finally dies completely *****
          const Rich::DetectorArray<Rich::PanelArray<LHCb::RichSmartID::DataType>> mod_corr{{{0, 0}, {6, 18}}};
          const auto pdMod      = id.pdMod() + mod_corr[id.rich()][id.panel()];
          const auto pdNumInMod = id.pdNumInMod();
          id.setPD( pdMod, pdNumInMod );
        }
#endif
        if ( id.isValid() ) { hitsPerPix[id].push_back( mchit ); }
      }

      // Clone the pixels to update them
      SIMDPixelSummaries out_pixs = pixels;

      // Loop over summary data and update positions
      for ( auto& pix : out_pixs ) {

        // flag to indicate if positions have been updated
        bool hasPosUpdate = false;

        // scalar loop
        for ( std::size_t i = 0; i < SIMDPixel::SIMDFP::Size; ++i ) {
          if ( pix.validMask()[i] ) {

            // SmartID for this hit
            const auto id = pix.smartID()[i].channelDataOnly();
            _ri_verbo << id << endmsg;
            if ( !id.isValid() ) { continue; }

            // Do we have any MChits for this ID
            const auto& pix_mchits = hitsPerPix[id];
            if ( !pix_mchits.empty() ) {
              // Find best MCHit to use. Either first labelled as single, otherwise
              // just the first in the container
              const auto* mch = pix_mchits.front();
              for ( const auto* h : pix_mchits ) {
                if ( h->isSignal() ) {
                  mch = h;
                  break;
                }
              }
              if ( m_useMCPos ) {
                // Update hit position using MC
                const auto mc_gpos = mch->entry();
                _ri_verbo << " -> original GPos  " << i << " " << pix.gloPos( i ) << endmsg;
                _ri_verbo << " -> MC update GPos " << i << " " << mc_gpos << endmsg;
                // Update position for this scalar
                pix.gloPos().X()[i] = mc_gpos.X();
                pix.gloPos().Y()[i] = mc_gpos.Y();
                pix.gloPos().Z()[i] = mc_gpos.Z();
                hasPosUpdate        = true;
              }
              if ( m_useMCTime ) {
                // Set the hit time using MC
                _ri_verbo << " -> original time  " << i << " " << pix.hitTime()[i] << endmsg;
                pix.hitTime()[i] = mch->timeOfFlight();
                _ri_verbo << " -> MC update time " << i << " " << pix.hitTime()[i] << endmsg;
              }
            } else {
              _ri_verbo << " -> No MC signal info" << endmsg;
            }

          } // valid mask
        }   // scalar loop

        // finally update SIMD local positions if needed
        if ( hasPosUpdate ) { pix.locPos() = smartIDsHelper.globalToPDPanel( pix.rich(), pix.side(), pix.gloPos() ); }

      } // SIMD pixel loop

      return out_pixs;
    }

  private:
    // properties

    /** Temporary workaround for processing DetDesc MC as input.
     *  When active, applies corrections to the data to make compatible
     *  with the dd4hep builds. */
    Gaudi::Property<bool> m_detdescMCinput{this, "IsDetDescMC", true};

    /// Enable use of MC position info
    Gaudi::Property<bool> m_useMCPos{this, "UseMCPosition", true};

    /// Enable use of MC time info
    Gaudi::Property<bool> m_useMCTime{this, "UseMCTime", false};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( RichPixelUseMCInfo )

} // namespace Rich::Future::Rec::MC
