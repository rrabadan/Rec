/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Array properties
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Gaudi
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IMagneticFieldSvc.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/ToolHandle.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Event Model
#include "Event/LinksByKey.h"
#include "Event/MCRichSegment.h"
#include "Event/MCRichTrack.h"
#include "Event/State.h"
#include "Event/Track.h"

// Rich Utils
#include "RichUtils/BoostArray.h"
#include "RichUtils/RichRayTracingUtils.h"
#include "RichUtils/RichTrackSegment.h"

// Rich Detector
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"

// LHCbKernel
#include "Kernel/RichSmartID.h"

// Rec event model
#include "RichFutureRecEvent/RichRecRelations.h"

// GSL
#include "gsl/gsl_math.h"

// STL
#include <array>
#include <iostream>
#include <limits>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

// pull in methods from Rich::RayTracingUtils
using namespace Rich::RayTracingUtils;

namespace Rich::Future::Rec::MC {

  namespace {

    /// The output data
    using OutData = std::tuple<LHCb::RichTrackSegment::Vector,     //
                               Relations::TrackToSegments::Vector, //
                               Relations::SegmentToTrackVector,    //
                               LHCb::Tracks,                       //
                               LHCb::LinksByKey>;

    /// cached Detector information
    class SegMakerDetInfo {
    public:
      /// Type for pointers to RICH radiator detector elements
      using Radiators = std::vector<const Rich::Detector::Radiator*>;

    public:
      /// Rich1 and Rich2 detector elements
      DetectorArray<const Rich::Detector::RichBase*> riches = {{}};
      /// Pointers to RICH radiator detector elements
      Radiators radiators;

    public:
      /// Construct from detector elements
      SegMakerDetInfo( const Rich::Detector::Rich1&     rich1, //
                       const Rich::Detector::Rich2&     rich2, //
                       const Rich::RadiatorArray<bool>& usedRads ) {
        riches = {&rich1, &rich2};
        radiators.reserve( Rich::NRiches );
        if ( usedRads[Rich::Rich1Gas] ) { radiators.push_back( &rich1.radiator() ); }
        if ( usedRads[Rich::Rich2Gas] ) { radiators.push_back( &rich2.radiator() ); }
      }
    };

  } // namespace

  /** @class TrSegMakerFromMCRichTracks
   *
   *  Builds RichTrackSegments from MCRichTracks
   *
   *  @author Chris Jones
   *  @date   2023-06-29
   */
  class TrSegMakerFromMCRichTracks final
      : public LHCb::Algorithm::MultiTransformer<OutData( LHCb::MCRichTracks const&, SegMakerDetInfo const& ),
                                                 LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, SegMakerDetInfo>> {

  public:
    /// Standard constructor
    TrSegMakerFromMCRichTracks( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            // data inputs
                            {KeyValue{"MCRichTracksLocation", LHCb::MCRichTrackLocation::Default},
                             // conditions input
                             KeyValue{"DetectorCache", DeRichLocations::derivedCondition( name + "-DetectorCache" )}},
                            // data outputs
                            {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                             KeyValue{"TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Initial},
                             KeyValue{"SegmentToTrackLocation", Relations::SegmentToTrackLocation::Default},
                             KeyValue{"OutputTracksLocation", LHCb::TrackLocation::Default + "RICH"},
                             KeyValue{"MCParticlesLinkLocation", "Link/" + LHCb::TrackLocation::Default + "RICH"}} ) {}

    /// Initialization after creation
    StatusCode initialize() override {
      auto sc = MultiTransformer::initialize();
      if ( !sc ) { return sc; }

      // Force debug messages
      // sc = setProperty( "OutputLevel", MSG::VERBOSE );

      if ( radiatorIsActive( Rich::Aerogel ) ) {
        error() << "Aerogel not supported in the 'RichFuture' framework" << endmsg;
        return StatusCode::FAILURE;
      }
      if ( !radiatorIsActive( Rich::Rich1Gas ) ) { _ri_debug << "Track segments for Rich1Gas are disabled" << endmsg; }
      if ( !radiatorIsActive( Rich::Rich2Gas ) ) { _ri_debug << "Track segments for Rich2Gas are disabled" << endmsg; }
      // derived condition data
      Detector::Rich1::addConditionDerivation( this );
      Detector::Rich2::addConditionDerivation( this );
      addConditionDerivation( {Detector::Rich1::DefaultConditionKey,                         // inputs
                               Detector::Rich2::DefaultConditionKey},                        //
                              inputLocation<SegMakerDetInfo>(),                              // output
                              [usedRads = radiatorIsActive()]( const Detector::Rich1& rich1, //
                                                               const Detector::Rich2& rich2 ) {
                                return SegMakerDetInfo{rich1, rich2, usedRads};
                              } );

      _ri_verbo << m_minP << " " << m_minPt << endmsg;

      // return
      return sc;
    }

  public:
    /// Algorithm execution via transform
    OutData operator()( LHCb::MCRichTracks const& mcRichtracks, //
                        SegMakerDetInfo const&    detInfo ) const override {

      _ri_debug << "Found " << mcRichtracks.size() << " MCRichTracks" << endmsg;

      // container to return
      OutData data;
      // shortcuts to tuple contents
      auto& segments    = std::get<LHCb::RichTrackSegment::Vector>( data );
      auto& tkToSegsRel = std::get<Relations::TrackToSegments::Vector>( data );
      auto& segToTkRel  = std::get<Relations::SegmentToTrackVector>( data );
      auto& tks         = std::get<LHCb::Tracks>( data );
      auto& tkLinks     = std::get<LHCb::LinksByKey>( data );

      LHCb::Tracks::size_type tkIndex( 0 );

      // cache info for Track->MCParticle relations
      using TkToMCPs = std::unordered_map<const LHCb::MCParticle*, std::vector<const LHCb::Track*>>;
      TkToMCPs tkToMCPs;

      // Loop over MC Rich Tracks
      for ( const auto mcRichTk : mcRichtracks ) {
        // sanity check
        if ( !mcRichTk ) { continue; }
        _ri_verbo << "Considering MCRichTrack " << mcRichTk->key() << endmsg;

        // associated MCParticle
        const auto mcp = mcRichTk->mcParticle();
        if ( !mcp ) { continue; }

        const auto charge = mcp->particleID().threeCharge() / 3;
        if ( 1 != abs( charge ) ) { continue; } // for some reason sometimes happens ...

        // Check we have an origin vertex
        const auto mc_orig_v = mcp->primaryVertex();
        if ( !mc_orig_v ) { continue; }

        // Check MCParticle momentum vector
        const auto mcp_vect = mcp->momentum().Vect();
        if ( mcp->momentum().Pt() < m_minPt ) { continue; }

        // Create a fake 'reco' track for this MC track
        auto tk = std::make_unique<LHCb::Track>();

        // lambda func to create track states
        auto makeState = [&]( const auto pnt, const auto mom, const LHCb::State::Location loc ) {
          const auto        x    = pnt.x();
          const auto        y    = pnt.y();
          const auto        z    = pnt.z();
          const auto        tx   = ( mom.z() > 0 ? mom.x() / mom.z() : 0.0 );
          const auto        ty   = ( mom.z() > 0 ? mom.y() / mom.z() : 0.0 );
          const auto        p2   = mom.mag2();
          const auto        qOvP = ( p2 > 0 ? charge / std::sqrt( p2 ) : 999 );
          const LHCb::State state( Gaudi::TrackVector{x, y, tx, ty, qOvP}, z, loc );
          _ri_verbo << "Created State : " << state << endmsg;
          tk->addToStates( state );
        };

        // Create 'first' state
        makeState( mc_orig_v->position(), mcp_vect, LHCb::State::Location::FirstMeasurement );

        // Fake some parameters
        tk->setNDoF( 10 ); // ??
        tk->setChi2PerDoF( 1 );
        tk->setGhostProbability( 0 );
        tk->setLikelihood( 1 ); // ??
        tk->setHistory( LHCb::Track::History::TrackIdealPR );
        tk->setType( LHCb::Track::Types::Long );

        // Track errors... ??
        LHCb::RichTrackSegment::StateErrors stateErrs{};

        // temporary container for segment indices
        Relations::SegmentIndices segList;

        // Loop over all radiators
        for ( const auto radiator : detInfo.radiators ) {
          // which radiator
          const auto rad = radiator->radiatorID();
          _ri_verbo << " -> Considering radiator " << rad << endmsg;

          // Get MC segment for this track and radiator
          const auto mcSeg = mcRichTk->segmentInRad( rad );
          if ( !mcSeg ) { continue; }
          _ri_verbo << "  -> Found MCRichSegment " << mcSeg->key() << endmsg;
          _ri_verbo << "  -> " << *mcSeg << endmsg;

          // Apply selection cuts
          if ( mcSeg->pathLength() < m_minPathL[rad] ) { continue; }
          if ( mcSeg->mcRichOpticalPhotons().size() < m_minPhots[rad] ) { continue; }
          _ri_verbo << "   -> Passed selection cuts " << endmsg;

          // Have an MC segment so make a reco segment

          // Get entry information
          const auto& entryPoint         = mcSeg->entryPoint();
          const auto& entryStateMomentum = mcSeg->entryMomentum();
          // Get exit information
          const auto& exitPoint         = mcSeg->exitPoint();
          const auto& exitStateMomentum = mcSeg->exitMomentum();
          // Get middle point information
          const auto midPoint         = mcSeg->bestPoint( 0.5 );
          const auto midStateMomentum = mcSeg->bestMomentum( 0.5 );

          _ri_verbo << "Rad Points  | entry=" << entryPoint << " mid=" << midPoint << " exit=" << exitPoint << endmsg;
          _ri_verbo << "Rad Momenta | entry=" << entryStateMomentum << " mid=" << midStateMomentum
                    << " exit=" << exitStateMomentum << endmsg;

          // Sanity checks on entry/exit points
          if ( entryPoint.Z() < m_minZ[rad] || midPoint.Z() < m_minZ[rad] || exitPoint.Z() < m_minZ[rad] ) { continue; }

          // check min/max momentum on segment
          if ( sqrt( entryStateMomentum.mag2() ) < m_minP[rad] || //
               sqrt( midStateMomentum.mag2() ) < m_minP[rad] ||   //
               sqrt( exitStateMomentum.mag2() ) < m_minP[rad] ) {
            continue;
          }
          if ( sqrt( entryStateMomentum.mag2() ) > m_maxP[rad] || //
               sqrt( midStateMomentum.mag2() ) > m_maxP[rad] ||   //
               sqrt( exitStateMomentum.mag2() ) > m_maxP[rad] ) {
            continue;
          }

          // if get here segment will be saved so save relations
          segList.push_back( segments.size() ); // this gives the index for the next entry ...
          segToTkRel.push_back( tkIndex );

          // make track states for this radiator
          makeState( entryPoint, entryStateMomentum,
                     ( Rich::Rich1Gas == rad ? LHCb::State::Location::BegRich1 : LHCb::State::Location::BegRich2 ) );
          makeState( exitPoint, exitStateMomentum,
                     ( Rich::Rich1Gas == rad ? LHCb::State::Location::EndRich1 : LHCb::State::Location::EndRich2 ) );

          // Create intersection info
          Rich::RadIntersection::Vector intersects;
          intersects.emplace_back( entryPoint, entryStateMomentum, exitPoint, exitStateMomentum, radiator );

          // Finally create a segment in the container
          segments.emplace_back( std::move( intersects ), midPoint, midStateMomentum, //
                                 rad, radiator->rich(),                               //
                                 stateErrs, stateErrs, stateErrs,                     //
                                 mc_orig_v->position4vector() );

          // Set mean photon energy
          segments.back().setAvPhotonEnergy( richPartProps()->meanPhotonEnergy( rad ) );

          _ri_verbo << "Created RichTrackSegment : " << segments.back() << endmsg;
        }

        // If we made at least one segment, save track
        if ( !segList.empty() ) {
          // Just use index as key ...
          const auto tkKey = tkIndex;
          // relations stuff...
          tkToSegsRel.emplace_back( tkKey, tkIndex );
          auto& tkRels          = tkToSegsRel.back();
          tkRels.segmentIndices = std::move( segList );
          // fill Track <-> MCParticle link table
          tkToMCPs[mcp].push_back( tk.get() );
          // Finally pass ownership to container
          _ri_verbo << "Created Track : " << *tk << endmsg;
          tks.insert( tk.release(), tkKey );
          // finally increment tk index last for next one
          ++tkIndex;
        }

      } // MC track loop

      // initialise the linker
      tkLinks = LHCb::LinksByKey{std::in_place_type<LHCb::Track>, std::in_place_type<LHCb::MCParticle>,
                                 LHCb::LinksByKey::Order::decreasingWeight};
      for ( const auto& [mcp, tks] : tkToMCPs ) {
        if ( !tks.empty() ) {
          const double weight = 1.0 / tks.size();
          for ( const auto tk : tks ) {
            _ri_verbo << "Creating MCP->TK link Tk=" << tk->key() << " MCP=" << mcp->key() << " weight=" << weight
                      << endmsg;
            tkLinks.link( tk->key(), mcp, weight );
          }
        }
      }

      // return the final data
      _ri_debug << "Created " << segments.size() << " track segments" << endmsg;
      return data;
    }

  private:
    // properties

    /// Min path length for each radiator
    Gaudi::Property<RadiatorArray<double>> m_minPathL{
        this, "MinPathLengths", {10 * Gaudi::Units::mm, 500 * Gaudi::Units::mm, 1500 * Gaudi::Units::mm}};

    /// Min number of photons for each radiator
    Gaudi::Property<RadiatorArray<double>> m_minPhots{this, "MinNumPhotons", {3, 5, 5}};

    /// Overall Min monentum cut
    Gaudi::Property<RadiatorArray<double>> m_minP{
        this,
        "MinP",
        {0.0 * Gaudi::Units::GeV, 0.0 * Gaudi::Units::GeV, 0.0 * Gaudi::Units::GeV},
        "Minimum momentum (GeV/c)"};

    /// Overall Max monentum cut
    Gaudi::Property<RadiatorArray<double>> m_maxP{
        this,
        "MaxP",
        {9e9 * Gaudi::Units::GeV, 9e9 * Gaudi::Units::GeV, 9e9 * Gaudi::Units::GeV},
        "Maximum momentum (GeV/c)"};

    /// Minimum track transerve momentum
    Gaudi::Property<double> m_minPt{this, "MinPt", 0.0 * Gaudi::Units::GeV, "Minimum transerve momentum (GeV/c)"};

    /// Minimum z position for states in each radiator (mm)
    Gaudi::Property<RadiatorArray<double>> m_minZ{this, "MinStateZ", {800, 800, 9000}};

  }; // namespace Rich::Future::Rec::MC

  //=============================================================================

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( TrSegMakerFromMCRichTracks )

} // namespace Rich::Future::Rec::MC
