#!/usr/bin/env python3
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import subprocess, os, argparse

parser = argparse.ArgumentParser(
    description="Submit condor jobs for RICH reco",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument(
    "-l",
    "--lumis",
    help="luminosity value(s)",
    default="1.5e34,1.2e34,1.0e34,3.0e33,2.0e33,2.0e32",
    type=str)
parser.add_argument("-4d", "--enable4D", help="Enable 4D", action='store_true')
parser.add_argument(
    "--pixwins",
    help="Pixel time window(s) (only for 4D)",
    default="3.0,2.0,1.0",
    type=str)
parser.add_argument(
    "--photwins",
    help="Photon time window(s) (only for 4D)",
    default="1.0,0.5,0.25,0.1,0.05",
    type=str)
args = parser.parse_args()
config = vars(args)
print(config)

enable4D = config["enable4D"]
os.environ["ENABLE4D"] = str(enable4D)

optsroot = "/usera/jonesc/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/"

# To Do. Pass these as parameters below.
#taskOpts = optsroot+"/RichFuture.py"
#dataOpts = optsroot+"/data/PMTs/UpgradeII/WithSpill/DIGI/30000000.py"

rootDir = os.getcwd()

for Lumi in config["lumis"].split(","):

    os.environ["LUMI"] = Lumi

    # Known number of input files for each lumi data set
    nFiles = (40 if float(Lumi) < 1.0e34 else 100)
    # files per subjob. Less for the higher lumi (10^34) samples
    filesPerJob = (4 if float(Lumi) < 1.0e34 else 2)

    if enable4D:

        for PixWin in config["pixwins"].split(","):
            for PhotWin in config["photwins"].split(","):
                os.environ["PIXWIN"] = PixWin
                os.environ["PHOTWIN"] = PhotWin
                jobName = "4D/lumi-" + Lumi + "/PixWin-" + "{:.3f}".format(
                    float(PixWin)) + "/PhotWin-" + "{:.3f}".format(
                        float(PhotWin))
                print("Submitting jobs for", jobName)
                subprocess.run([
                    optsroot + "jobs/submit.sh", jobName,
                    str(nFiles),
                    str(filesPerJob)
                ])

    else:

        jobName = "3D/lumi-" + Lumi
        print("Submitting jobs for", jobName)
        subprocess.run([
            optsroot + "jobs/submit.sh", jobName,
            str(nFiles),
            str(filesPerJob)
        ])
