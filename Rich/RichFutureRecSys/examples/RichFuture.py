###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# --------------------------------------------------------------------------------------

from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from GaudiKernel.SystemOfUnits import GeV
from Configurables import LHCbApp, GaudiSequencer, DDDBConf
from Configurables import UnpackMCParticle, UnpackMCVertex
from Configurables import FPEAuditor
import os, socket

# --------------------------------------------------------------------------------------


# Add PID information
def addInfo(alg, typ, name):
    t = alg.addTool(typ, name)
    alg.AddInfo += [t]
    return t


# --------------------------------------------------------------------------------------

# Print hostname
print("Host", socket.gethostname())

# Use own DB clones
#os.environ['GITCONDDBPATH'] = '/usera/jonesc/NFS/GitDB'

# Are we running in a batch job ?
batchMode = "CONDOR_FILE_BATCH" in os.environ

# histograms
#histos = "OfflineFull"
histos = "Expert"

# ROOT persistency
ApplicationMgr().HistogramPersistency = "ROOT"
from Configurables import RootHistCnv__PersSvc

RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True
myBuild = os.getenv('User_release_area', 'None').split('/')[-1]
myConf = os.getenv('BINARY_TAG', 'None')
rootFileBaseName = "RichFuture-" + myBuild + "-" + myConf + "-" + histos
HistogramPersistencySvc().OutputFile = rootFileBaseName + "-Histos.root"

# Event numbers
nEvents = (100 if not batchMode else 99999999)
EventSelector().PrintFreq = (10 if not batchMode else 250)
#LHCbApp().SkipEvents      = 1184

# Just to initialise
DDDBConf()
LHCbApp()

# Timestamps in messages
#LHCbApp().TimeStamp = True

msgSvc = getConfigurable("MessageSvc")
#msgSvc.OutputLevel = 1
msgSvc.Format = "% F%40W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 40

# Auditors
AuditorSvc().Auditors += ["FPEAuditor"]
FPEAuditor().ActivateAt = ["Execute"]
#AuditorSvc().Auditors += [ "NameAuditor" ]

# The overall sequence. Filled below.
all = GaudiSequencer("All", MeasureTime=True)

# --------------------------------------------------------------------------------------

# Finally set up the application
app = ApplicationMgr(
    TopAlg=[all],
    EvtMax=nEvents,  # events to be processed
    ExtSvc=['ToolSvc', 'AuditorSvc'],
    AuditAlgorithms=True)

# Are we using DD4Hep ?
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hep = DD4hepSvc(DetectorList=["/world", "Magnet", "Rich1", "Rich2"])
    #dd4hep.DumpConditions = True
    app.ExtSvc += [dd4hep]

#ApplicationMgr().ExtSvc += ["Gaudi::MetaDataSvc"]

# --------------------------------------------------------------------------------------

# Reco options
useMCTracks = True
useMCHits = False

# Get time windows from environment vars if set
# otherwise use default values as passed.
PixWin = float(os.getenv("PIXWIN", "3.0"))
PhotWin = float(os.getenv("PHOTWIN", "0.25"))

# Enable 4D reco in each RICH
is4D = bool(os.getenv("ENABLE4D", "1"))
enable4D = (is4D, is4D)
#enable4D = (False, False)
# Average expected time for signal
avSignalTime = (13.03, 52.94)
# Pixel time window size in each RICH (in ns)
pixTimeWindow = (PixWin, PixWin)
# Photon reco time window size in each RICH (ns)
photTimeWindow = (PhotWin, PhotWin)

# Min Momentum cuts
minP = (1.0 * GeV, 1.0 * GeV, 1.0 * GeV)
minPt = 0.2 * GeV

# Add time to reco tracks using MC
# Only used if useMCTracks=False and 4D enabled
tkSegAddTimeFromMC = ((enable4D[0] or enable4D[1]) and not useMCTracks)

# Add time info to Rich hits
# Only if useMCHits=False and 4D enabled
usePixelMCTime = ((enable4D[0] or enable4D[1]) and not useMCHits)

# Cheat pixel positions using MC info
usePixelMCPos = False

# Unpacking sequence before reco is run
preUnpackSeq = GaudiSequencer("PreUnpackSeq", MeasureTime=True)
all.Members += [preUnpackSeq]

# Fetch required data from file
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
fetcher = FetchDataFromFile('FetchDSTData')
fetcher.DataKeys = ['Trigger/RawEvent']
preUnpackSeq.Members += [fetcher]

# Unpack the ODIN raw event
odinSeq = GaudiSequencer("ODINSeq", MeasureTime=True)
all.Members += [odinSeq]
from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent
odinSeq.Members += [
    UnpackRawEvent(
        'UnpackODIN',
        BankTypes=['ODIN'],
        RawEventLocation='Trigger/RawEvent',
        RawBankLocations=['DAQ/RawBanks/ODIN'])
]
# Create the ODIN object from its raw bank
from Configurables import createODIN
decodeODIN = createODIN("ODINDecode")
odinSeq.Members += [decodeODIN]

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
    all.Members += [IOVProducer("ReserveIOVDD4hep", ODIN=decodeODIN.ODIN)]

if tkSegAddTimeFromMC or useMCHits or useMCTracks:
    preUnpackSeq.Members += [UnpackMCVertex(), UnpackMCParticle()]

if usePixelMCPos or usePixelMCTime or useMCHits:
    fetcher.DataKeys += ['pSim/Rich/Hits']
    from Configurables import MCRichHitUnpacker
    preUnpackSeq.Members += [MCRichHitUnpacker()]
    for spill in ["PrevPrev", "Prev", "Next", "NextNext", "LHCBackground"]:
        preUnpackSeq.Members += [
            MCRichHitUnpacker(
                spill + "MCRichHitUnpack",
                InputName="/Event/" + spill + "/pSim/Rich/Hits",
                OutputName="/Event/" + spill + "/MC/Rich/Hits")
        ]

if not useMCHits:
    # Run regular rawEvent decoding
    fetcher.DataKeys += ['Rich/RawEvent']
    from Configurables import Rich__Future__RawBankDecoder as RichDecoder
    unpack_rich = UnpackRawEvent(
        'UnpackRich',
        BankTypes=['Rich'],
        RawEventLocation='Rich/RawEvent',
        RawBankLocations=['DAQ/RawBanks/Rich'])
    richDecode = RichDecoder("RichDecodeRawBanks")
    preUnpackSeq.Members += [unpack_rich]
    all.Members += [richDecode]
    # If asked for update decoded data with time from MC
    if usePixelMCTime:
        richDecode.DecodedDataLocation = str(
            richDecode.DecodedDataLocation) + "BeforeMCCheat"
        from Configurables import Rich__Future__MC__DecodedDataAddMCInfo as DecodedDataAddMCInfo
        all.Members += [
            DecodedDataAddMCInfo(
                "RichDecodedDataAddMCInfo",
                InDecodedDataLocation=richDecode.DecodedDataLocation)
        ]
else:
    # Emulate RICH decoded data from extended MC info from Gauss
    from Configurables import Rich__Future__MC__DecodedDataFromMCRichHits as RichMCDecoder
    richMCDecode = RichMCDecoder("RichDecodeFromMC")
    richMCDecode.IsDetDescMC = True
    richMCDecode.RejectBackground = False
    richMCDecode.IncludeTimeInfo = True
    all.Members += [richMCDecode]

# Now get the RICH sequence

if not useMCTracks:

    # Explicitly unpack the Tracks
    fetcher.DataKeys += ['pRec/Track/Best']
    from Configurables import UnpackTrack
    tkUnpack = UnpackTrack("UnpackTracks")
    preUnpackSeq.Members += [tkUnpack]

    # Filter the tracks by type
    from Configurables import TracksSharedSplitterPerType as TrackFilter
    tkFilt = TrackFilter("TrackTypeFilter")
    tkFilt.InputTracks = 'Rec/Track/Best'
    tkFilt.LongTracks = 'Rec/Track/BestLong'
    tkFilt.DownstreamTracks = 'Rec/Track/BestDownstream'
    tkFilt.UpstreamTracks = 'Rec/Track/BestUpstream'
    tkFilt.Ttracks = 'Rec/Track/BestTtrack'
    tkFilt.VeloTracks = 'Rec/Track/BestVelo'
    all.Members += [tkFilt]

    # Input tracks
    #tkLocs = {
    #    "Long": tkFilt.LongTracks,
    #    "Down": tkFilt.DownstreamTracks,
    #    "Up": tkFilt.UpstreamTracks,
    #    "Seed": tkFilt.Ttracks
    #}
    #tkLocs = { "All"  : tkFilt.InputTracks  }
    tkLocs = {"Long": tkFilt.LongTracks}
    #tkLocs = { "Up" : tkFilt.UpstreamTracks }
    #tkLocs = { "Down" : tkFilt.DownstreamTracks }

    # Output PID
    #pidLocs = {
    #    "Long": "Rec/Rich/LongPIDs",
    #    "Down": "Rec/Rich/DownPIDs",
    #    "Up": "Rec/Rich/UpPIDs",
    #    "Seed": "Rec/Rich/SeedPIDs"
    #}
    #pidLocs = { "All" : "Rec/Rich/PIDs" }
    pidLocs = {"Long": "Rec/Rich/LongPIDs"}
    #pidLocs = { "Up" : "Rec/Rich/UpPIDs" }
    #pidLocs = { "Down" : "Rec/Rich/DownPIDs" }

    # Input tracks
    inTracks = tkFilt.InputTracks

else:

    # Build track info from RICH extended MC info
    from Configurables import (MCRichSegmentUnpacker, MCRichTrackUnpacker)
    fetcher.DataKeys += [
        'pSim/Rich/Segments', 'pSim/Rich/Tracks', 'pSim/MCVertices',
        'pSim/MCParticles'
    ]
    preUnpackSeq.Members += [MCRichSegmentUnpacker(), MCRichTrackUnpacker()]
    # Fake Input tracks
    inTracks = "Rec/Track/FromRichMCTracks"
    # Use RichMCTracks as input
    tkLocs = {"MC": inTracks}
    pidLocs = {"MC": "Rec/Rich/MCPIDs"}

# track extrapolator type
#tkExtrap = "TrackRungeKuttaExtrapolator/TrackExtrapolator"
tkExtrap = "TrackSTEPExtrapolator/TrackExtrapolator"

# RICH radiators to use
rads = ["Rich1Gas", "Rich2Gas"]
#rads = ["Rich1Gas"]
#rads = ["Rich2Gas"]

# PID types to process
parts = [
    "electron", "muon", "pion", "kaon", "proton", "deuteron", "belowThreshold"
]
#parts = ["pion","kaon","proton","deuteron","belowThreshold"]

photonRecoType = "Quartic"
#photonRecoType = "FastQuartic"
#photonRecoType = "CKEstiFromRadius"

photonSel = "Nominal"
#photonSel = "Online"
#photonSel = "None"

# Number ring points for ray tracing (scaled by CK theta)
ringPointsMin = (16, 16, 16)
ringPointsMax = (96, 96, 96)

# Compute the ring share CK theta valuesRichFuture-Feature-x86_64_v3-centos7-gcc12-opt-Expert-Histos.root
#rSTol = 0.075  # as fraction of sat. CK theta
#newCKRingTol = (rSTol, rSTol, rSTol)
newCKRingTol = (0.0, 0.05, 0.1)

# Detectable Yield calculation
detYieldPrecision = "Average"
#detYieldPrecision = "Full"

# Track CK resolution treatment
#tkCKResTreatment = "Functional"
tkCKResTreatment = "Parameterised"

# CK resolutions
from RichFutureRecSys.ConfiguredRichReco import defaultNSigmaCuts

ckResSigma = defaultNSigmaCuts()
#S = 4.5
#for tk in tkLocs.keys():
#    # Override presel cuts
#    ckResSigma[photonSel][tkCKResTreatment][tk][0] = (S, S, S)
#    # Override sel cuts
#    ckResSigma[photonSel][tkCKResTreatment][tk][1] = (99,99,99)

# Track CK res scale factors
tkCKResScaleF = None  # Use nominal values
#scF = 1.2
#tkCKResScaleF = (scF, scF, scF)

# Merged output
finalPIDLoc = "Rec/Rich/PIDs"

# DataType
#dType = "2016"
dType = "Upgrade"

# Online Brunel mode.
#online = True
online = False

# PD Grouping for backgrounds
pdGroupSize = (4, 4)

# Create missing track states
createStates = False

# Perform clustering ?
pixelClustering = (False, False)
#pixelClustering = ( True, True )

# Truncate CK angles ?
#truncateAngles = (False, False, False)
truncateAngles = (True, True, True)

# Minimum photon probability
minPhotonProb = None  # Nominal

# Likelihood Settings
# Number Iterations
nIts = 2
# likelihood thresholds
likeThres = [-1e-2, -1e-3, -1e-4, -1e-5]  # nominal
#likeThres = [-1e-3, -1e-3, -1e-4, -1e-5]

# OutputLevel (-1 means no setting)
outLevel = -1

# --------------------------------------------------------------------------------------
# The reconstruction
from RichFutureRecSys.ConfiguredRichReco import RichRecoSequence
RichRec = RichRecoSequence(
    OutputLevel=outLevel,
    dataType=dType,
    onlineBrunelMode=online,
    enable4DReco=enable4D,
    tkSegAddTimeFromMC=tkSegAddTimeFromMC,
    pixUsePosFromMC=usePixelMCPos,
    averageHitTime=avSignalTime,
    pixelTimeWindow=pixTimeWindow,
    photonTimeWindow=photTimeWindow,
    photonReco=photonRecoType,
    photonSelection=photonSel,
    radiators=rads,
    particles=parts,
    createMissingStates=createStates,
    PDGroupSize=pdGroupSize,
    MinP=minP,
    MinPt=minPt,
    NRingPointsMax=ringPointsMax,
    NRingPointsMin=ringPointsMin,
    NewCKRingTol=newCKRingTol,
    minPhotonProbability=minPhotonProb,
    detYieldPrecision=detYieldPrecision,
    tkCKResTreatment=tkCKResTreatment,
    tkCKResScaleFactors=tkCKResScaleF,
    nSigmaCuts=ckResSigma,
    applyPixelClustering=pixelClustering,
    truncateCKAngles=truncateAngles,
    trackExtrapolator=tkExtrap,
    nIterations=nIts,
    LikelihoodThreshold=likeThres,
    inputTrackLocations=tkLocs,
    outputPIDLocations=pidLocs,
    mergedOutputPIDLocation=finalPIDLoc)

# The final sequence to run
all.Members += [RichRec]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Simple debugging / testing
enableDebug = False
if enableDebug:
    from Configurables import Rich__Future__TestDerivedDetObjects as TestDerivedDetObjects
    from Configurables import Rich__Future__TestDecodeAndIDs as TestDecodeAndIDs
    all.Members += [
        TestDerivedDetObjects("TestDerivedDets", OutputLevel=1),
        TestDecodeAndIDs("TestDecodeAndIDs")
    ]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Data Monitoring
enableDataMoni = True
if enableDataMoni:
    from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecMonitors
    all.Members += [
        RichRecMonitors(
            enable4DReco=enable4D,
            inputTrackLocations=tkLocs,
            radiators=rads,
            applyPixelClustering=pixelClustering,
            onlineBrunelMode=online,
            outputPIDLocations=pidLocs,
            histograms=histos)
    ]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# MC Checking and PID tupling

enableMCChecks = True
enablePIDTuples = True

if enableMCChecks or enablePIDTuples:
    from Configurables import MCRichDigitSummaryUnpacker
    from Configurables import MCRichHitUnpacker, MCRichOpticalPhotonUnpacker
    fetcher.DataKeys += [
        'pSim/MCVertices', 'pSim/MCParticles', 'pSim/Rich/Hits',
        'pSim/Rich/OpticalPhotons'
    ]
    # Unpacking sequence after reco is run
    postUnpackSeq = GaudiSequencer("PostUnpackSeq", MeasureTime=True)
    all.Members += [postUnpackSeq]
    postUnpackSeq.Members += [
        UnpackMCVertex(),
        UnpackMCParticle(),
        MCRichHitUnpacker(),
        MCRichOpticalPhotonUnpacker()
    ]
    if not useMCHits:
        postUnpackSeq.Members += [MCRichDigitSummaryUnpacker()]

if enableMCChecks:
    from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecCheckers
    all.Members += [
        RichRecCheckers(
            enable4DReco=enable4D,
            inputTrackLocations=tkLocs,
            radiators=rads,
            applyPixelClustering=pixelClustering,
            onlineBrunelMode=online,
            outputPIDLocations=pidLocs,
            histograms=histos)
    ]

if enablePIDTuples:
    from Configurables import NTupleSvc
    app.ExtSvc.append(NTupleSvc())
    # ProtoParticle monitoring. For detailed PID monitoring.
    protoMoniSeq = GaudiSequencer("ProtoMonitoring", MeasureTime=True)
    all.Members += [protoMoniSeq]
    # Remake the protos with the new Rich PID objects
    from Configurables import (ChargedProtoParticleAddRichInfo,
                               ChargedProtoParticleAddCombineDLLs,
                               FunctionalChargedProtoParticleMaker,
                               DelegatingTrackSelector, TrackSelector)
    makecproto = FunctionalChargedProtoParticleMaker("ProtoPMaker", AddInfo=[])
    makecproto.Inputs = [inTracks]
    makecproto.addTool(DelegatingTrackSelector, name="TrackSelector")
    tracktypes = ['Long', 'Upstream', 'Downstream', 'Ttrack']
    makecproto.TrackSelector.TrackTypes = tracktypes
    selector = makecproto.TrackSelector
    for tsname in tracktypes:
        selector.addTool(TrackSelector, name=tsname)
        ts = getattr(selector, tsname)
        ts.TrackTypes = [tsname]
    rich = addInfo(makecproto, ChargedProtoParticleAddRichInfo, "AddRich")
    comb = addInfo(makecproto, ChargedProtoParticleAddCombineDLLs, "CombDLLs")
    protoMoniSeq.Members += [makecproto]
    # monitoring config
    from Configurables import GlobalRecoChecks
    GlobalRecoChecks().Sequencer = protoMoniSeq
    GlobalRecoChecks().ProtoTupleName = rootFileBaseName + "-ProtoTuple.root"
    GlobalRecoChecks().ANNTupleName = rootFileBaseName + "-ANNPIDTuple.root"
    # ANNPID monitoring
    GlobalRecoChecks().AddANNPIDInfo = False
    #GlobalRecoChecks().AddANNPIDInfo = True
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Example command lines
# --------------------------------------------------------------------------------------

# Normal running (old data format)
# gaudirun.py ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/MCFormat/MCXDstFiles.py} 2>&1 | tee RichFuture-${User_release_area##/*/}-${CMTCONFIG}.log

# Normal running (realistic data format)
# gaudirun.py ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/RealTel40Format/MCLDstFiles.py} 2>&1 | tee RichFuture-${User_release_area##/*/}-${CMTCONFIG}.log

# Run on XSIM files
# gaudirun.py ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/MCFormat/MCXSimFiles.py} 2>&1 | tee RichFuture-${User_release_area##/*/}-${CMTCONFIG}.log

# DD4HEP tests
# GITCONDDBPATH='/usera/jonesc/NFS/GitDB' gaudirun.py ....

# Upgrade II Studies
# gaudirun.py ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/UpgradeII/NoSpill/DIGI/30000000.py} 2>&1 | tee RichFuture-${User_release_area##/*/}-${CMTCONFIG}.log
# gaudirun.py ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/UpgradeII/WithSpill/DIGI/30000000.py} 2>&1 | tee RichFuture-${User_release_area##/*/}-${CMTCONFIG}.log

# --------------------------------------------------------------------------------------
