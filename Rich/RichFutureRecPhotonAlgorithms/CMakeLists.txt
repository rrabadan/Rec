###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichFutureRecPhotonAlgorithms
----------------------------------
#]=======================================================================]

gaudi_add_module(RichFutureRecPhotonAlgorithms
    SOURCES
        src/RichBasePhotonReco.cpp
        src/RichCommonQuarticPhotonReco.cpp
        src/RichCreateScalarPhotons.cpp
        src/RichSIMDCKEstiFromRadiusPhotonReco.cpp
        src/RichSIMDPhotonPredictedPixelSignal.cpp
        src/RichSIMDQuarticPhotonReco.cpp
    LINK
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::DetDescLib
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::RichDetectorsLib
        LHCb::RichDetLib
        LHCb::RichFutureUtils
        LHCb::RichUtils
        Rec::RichFutureRecBase
        Rec::RichFutureRecEvent
        Rec::RichRecUtils
        VDT::vdt
)

# Fixes for GCC7.
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU" AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "7.0")
    target_compile_options(RichFutureRecPhotonAlgorithms PRIVATE -faligned-new)
endif()
