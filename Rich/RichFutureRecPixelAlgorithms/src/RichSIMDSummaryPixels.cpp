
/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// Utils
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichUtils/ZipRange.h"

// STL
#include <algorithm>
#include <cstdint>

namespace Rich::Future::Rec {

  /** @class SIMDSummaryPixels
   *
   *  Forms SIMD summary objects for the pixel information
   *
   *  @author Chris Jones
   *  @date   2017-10-16
   */
  class SIMDSummaryPixels final : public LHCb::Algorithm::Transformer<
                                      SIMDPixelSummaries( const Rich::PDPixelCluster::Vector&, //
                                                          const Rich::Utils::RichSmartIDs& ),
                                      LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Rich::Utils::RichSmartIDs>> {

  public:
    /// Standard constructor
    SIMDSummaryPixels( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // input data
                       {KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default},
                        // input conditions data
                        KeyValue{"RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey}},
                       // output data
                       {KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default}} ) {}

    /// Initialize
    StatusCode initialize() override {
      // base class initialise followed by conditions
      return Transformer::initialize().andThen( [&] {
        // create the RICH smartID helper instance
        Rich::Utils::RichSmartIDs::addConditionDerivation( this );
        // loop over RICHes
        for ( const auto rich : Rich::detectors() ) {
          if ( m_enable4D[rich] ) {
            info() << "4D pixels enabled for " << rich << " | Window = "   //
                   << ( m_avHitTime[rich] - m_timeWindow[rich] ) << " to " //
                   << ( m_avHitTime[rich] + m_timeWindow[rich] ) << " ns" << endmsg;
          }
        }
        // force debugging output
        // return setProperty( "OutputLevel", MSG::VERBOSE );
      } );
    }

  public:
    /// Operator for each space point
    SIMDPixelSummaries operator()( const Rich::PDPixelCluster::Vector& clusters,
                                   const Rich::Utils::RichSmartIDs&    smartIDsHelper ) const override;

  private:
    /// Shortcut incase clustering is disabled
    Gaudi::Property<bool> m_noClustering{this, "NoClustering", true};

    /// Enabled 4D reconstruction
    Gaudi::Property<DetectorArray<bool>> m_enable4D{this, "Enable4D", {false, false}};

    /// Average expected hit time for signal in each RICH
    Gaudi::Property<DetectorArray<float>> m_avHitTime{
        this, "AvHitTime", {13.03 * Gaudi::Units::ns, 52.94 * Gaudi::Units::ns}};

    /// Time window for each RICH
    Gaudi::Property<DetectorArray<float>> m_timeWindow{
        this, "TimeWindow", {3.0 * Gaudi::Units::ns, 3.0 * Gaudi::Units::ns}};
  };

} // namespace Rich::Future::Rec

using namespace Rich::Future::Rec;

//=============================================================================

namespace {
  /// SIMD (x,y,z) position, default initialised to (0,0,0)
  struct SIMDXYZ {
    using FP = SIMDPixel::SIMDFP;
    FP x{FP::Zero()};
    FP y{FP::Zero()};
    FP z{FP::Zero()};
  };
} // namespace

//=============================================================================

SIMDPixelSummaries                                                           //
SIMDSummaryPixels::operator()( const Rich::PDPixelCluster::Vector& clusters, //
                               const Rich::Utils::RichSmartIDs&    smartIDsHelper ) const {

  // Pixel Summaries
  SIMDPixelSummaries summaries;

  // Reserve size based on # pixels and SIMD address size.
  // Add 4 for padding in each RICH panel.
  summaries.reserve( ( clusters.size() / SIMDPixel::SIMDFP::Size ) + 4 );

  // last RICH
  Rich::DetectorType lastRich{Rich::InvalidDetector};
  // last side
  Rich::Side lastSide{Rich::InvalidSide};

  // Working SIMD values
  SIMDXYZ            gXYZ;                               // global position
  SIMDPixel::SIMDFP  effArea{SIMDPixel::SIMDFP::Zero()}; // effective area
  SIMDPixel::ScIndex scClusIn( -1 );                     // indices to original clusters
  SIMDPixel::Mask    mask{SIMDPixel::Mask::Zero()};      // selection mask

  // SIMD index
  std::uint32_t index = 0;

  // Working Smart IDs
  SIMDPixel::SmartIDs smartIDs;

  // Functor to save a SIMD pixel
  auto savePix = [&]() {
    // get local and global position data
    const auto gPos = SIMDPixel::Point( gXYZ.x, gXYZ.y, gXYZ.z );
    const auto lPos = smartIDsHelper.globalToPDPanel( lastRich, lastSide, gPos );
    // save SIMD pixel
    summaries.emplace_back( lastRich, lastSide, smartIDs, //
                            gPos, lPos,                   //
                            effArea, scClusIn, mask );
    // count
    summaries.addHit( lastRich, lastSide );
    // reset
    index = 0;
  };

  // Functor to add padding info at the end of an incomplete SIMD pixel
  auto addPadding = [&]() {
    // set the OK mask for a reduced range based on index
    mask                = SIMDPixel::SIMDFP::IndexesFromZero() < index;
    const auto not_mask = !mask;
    // area and position info
    effArea( not_mask ) = SIMDPixel::SIMDFP::Zero();
    gXYZ.x( not_mask )  = SIMDPixel::SIMDFP::Zero();
    gXYZ.y( not_mask )  = SIMDPixel::SIMDFP::Zero();
    // Set default z to values roughly right for each RICH, to
    // avoid precision issues in the photon reco later on.
    gXYZ.z( not_mask ) = SIMDPixel::SIMDFP( Rich::Rich1 == lastRich ? 1600 : 10500 );
    // cast mask for int types...
    const auto not_im  = LHCb::SIMD::simd_cast<SIMDPixel::ScIndex::mask_type>( not_mask );
    scClusIn( not_im ) = -SIMDPixel::ScIndex::One();
    // SmartIDs cannot be done with mask.
    GAUDI_LOOP_UNROLL( SIMDPixel::SIMDFP::Size )
    for ( auto i = index; i < SIMDPixel::SIMDFP::Size; ++i ) { smartIDs[i] = LHCb::RichSmartID(); }
  };

  // make the global positions from the clusters
  const auto gPoints = smartIDsHelper.globalPositions( clusters, m_noClustering );

  // Scalar cluster index
  std::size_t clusIn = 0;

  // Note relying on fact they are sorted by panel and rich here.
  for ( const auto&& [cluster, gloPos] : Ranges::ConstZip( clusters, gPoints ) ) {

    // which RICH and side
    const auto rich = cluster.rich();
    const auto side = cluster.panel();

    // Up front selections
    bool clus_selected = true;
    // Apply 4D time selection window
    if ( m_enable4D[rich] ) {
      // Only apply window if time is actually set
      if ( cluster.primaryID().adcTimeIsSet() ) {
        const auto hitT = cluster.primaryID().time();
        if ( fabs( hitT - m_avHitTime[rich] ) > m_timeWindow[rich] ) {
          _ri_verbo << "Rejected " << cluster.primaryID() << endmsg;
          clus_selected = false;
        }
      }
    }
    if ( clus_selected ) {

      // If different RICH or side ?
      if ( rich != lastRich || side != lastSide ) {
        // Skip saving anything if index is 0.
        // Also covers not saving first time in loop.
        if ( 0 != index ) {
          // add padding if needed
          addPadding();
          // Save the current info
          savePix();
        }
        // update RICH and panel
        lastRich = rich;
        lastSide = side;
      }

      // Update global position
      gXYZ.x[index] = gloPos.X();
      gXYZ.y[index] = gloPos.Y();
      gXYZ.z[index] = gloPos.Z();
      // effective area * cluster size
      effArea[index] = cluster.size() * cluster.dePD()->effectivePixelArea();
      // (primary) SmartID
      smartIDs[index] = cluster.primaryID();
      // set scalar cluster index
      scClusIn[index] = clusIn;

      // If this is the last index, push to container and start again
      if ( SIMDPixel::SIMDFP::Size - 1 == index ) {
        // Set the selection mask as all OK
        mask = SIMDPixel::Mask( true );
        // save an entry. No need to handle padding here as SIMD data is full.
        savePix();
      } else {
        // increment index for next scalar pixel
        ++index;
      }

    } // cluster is selected

    // must increment cluster index for every cluster, used or not
    ++clusIn;

  } // cluster loop

  // Save last one if needed
  if ( 0 != index ) {
    // add padding if needed
    addPadding();
    // Save the current info
    savePix();
  }

  // Initialise the summaries, sets ranges etc.
  summaries.init();

  // -------------------------------------------------------------------------------------------

  // Some verbose bugging printout
  if ( msgLevel( MSG::DEBUG ) ) {

    // explicitly count number in each region to compare to ranges
    DetectorArray<PanelArray<unsigned int>> count = {{}};
    for ( const auto& p : summaries ) { ++count[p.rich()][p.side()]; }

    std::size_t rangeSum = 0;
    bool        OK       = true;
    // Loop over each range and test against full container
    for ( const auto rich : {Rich::Rich1, Rich::Rich2} ) {
      for ( const auto side : {Rich::firstSide, Rich::secondSide} ) {
        // get the range for this rich and side
        const auto rPixs = summaries.range( rich, side );
        for ( const auto& p : rPixs ) {
          // make sure pixel agrees with range
          const auto pixOK = ( rich == p.rich() && side == p.side() );
          OK &= pixOK;
        }
        // count sum of range sizes
        rangeSum += rPixs.size();
        // does range have same size as explicit count ?
        if ( count[rich][side] != rPixs.size() ) {
          OK = false;
          error() << "Problem with " << rich << " " << Rich::text( rich, side ) << " range. Explicit count "
                  << count[rich][side] << " != " << rPixs.size() << endmsg;
        }
      }
    }

    // test final size
    OK &= ( summaries.size() == rangeSum );
    if ( !OK || msgLevel( MSG::VERBOSE ) ) {
      verbose() << "Total SIMD pixels = " << summaries.size() << " == " << rangeSum << endmsg;
      for ( const auto& p : summaries ) { verbose() << " " << p << endmsg; }
    }
  }

  // -------------------------------------------------------------------------------------------

  // return
  return summaries;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDSummaryPixels )

//=============================================================================
