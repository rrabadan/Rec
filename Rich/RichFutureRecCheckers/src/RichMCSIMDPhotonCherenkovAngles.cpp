/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <algorithm>
#include <cassert>
#include <mutex>
#include <utility>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "Event/MCRichDigitSummary.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

namespace Rich::Future::Rec::MC::Moni {

  /** @class SIMDPhotonCherenkovAngles RichSIMDPhotonCherenkovAngles.h
   *
   *  MC checking of the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class SIMDPhotonCherenkovAngles final
      : public LHCb::Algorithm::Consumer<void( const Summary::Track::Vector&,                   //
                                               const LHCb::Track::Range&,                       //
                                               const SIMDPixelSummaries&,                       //
                                               const Rich::PDPixelCluster::Vector&,             //
                                               const Relations::PhotonToParents::Vector&,       //
                                               const LHCb::RichTrackSegment::Vector&,           //
                                               const CherenkovAngles::Vector&,                  //
                                               const SIMDCherenkovPhoton::Vector&,              //
                                               const Rich::Future::MC::Relations::TkToMCPRels&, //
                                               const LHCb::MCRichDigitSummarys& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    SIMDPhotonCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                     KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                     KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default},
                     KeyValue{"TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles},
                     KeyValue{"RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins1DHistos", 100 ).ignore();
      // debugging
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,     //
                     const LHCb::Track::Range&                       tracks,        //
                     const SIMDPixelSummaries&                       pixels,        //
                     const Rich::PDPixelCluster::Vector&             clusters,      //
                     const Relations::PhotonToParents::Vector&       photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&           segments,      //
                     const CherenkovAngles::Vector&                  expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&              photons,       //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                     const LHCb::MCRichDigitSummarys&                digitSums ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    // properties

    /// minimum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_minBeta{this, "MinBeta", {0.9999f, 0.9999f, 0.9999f}};

    /// maximum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_maxBeta{this, "MaxBeta", {999.99f, 999.99f, 999.99f}};

    /// Min theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMin{this, "ChThetaRecHistoLimitMin", {0.150f, 0.010f, 0.010f}};

    /// Max theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMax{this, "ChThetaRecHistoLimitMax", {0.325f, 0.056f, 0.033f}};

    /// Histogram ranges for CK resolution plots
    Gaudi::Property<RadiatorArray<float>> m_ckResRange{this, "CKResHistoRange", {0.025f, 0.0026f, 0.002f}};

    /// Min momentum by radiator (MeV/c)
    Gaudi::Property<RadiatorArray<double>> m_minP{
        this, "MinP", {0.5 * Gaudi::Units::GeV, 0.5 * Gaudi::Units::GeV, 0.5 * Gaudi::Units::GeV}};

    /// Max momentum by radiator (MeV/c)
    Gaudi::Property<RadiatorArray<double>> m_maxP{
        this, "MaxP", {120.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV}};

    /// Option to skip electrons
    Gaudi::Property<bool> m_skipElectrons{this, "SkipElectrons", false, "Skip electrons from monitoring"};

  private:
    // cached data

    /// mutex lock
    mutable std::mutex m_updateLock;

    // histograms
    RadiatorArray<AIDA::IHistogram1D*> h_ckResAll         = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResTrue        = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResFake        = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResAllBetaCut  = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResTrueBetaCut = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResFakeBetaCut = {{}};

    RadiatorArray<AIDA::IHistogram1D*> h_thetaRecTrue = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_phiRecTrue   = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_thetaRecFake = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_phiRecFake   = {{}};

    RadiatorArray<AIDA::IProfile1D*> h_ckResTrueVP = {{}};

    RadiatorArray<AIDA::IHistogram1D*> h_ckResAllPion         = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResTruePion        = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResFakePion        = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResAllPionBetaCut  = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResTruePionBetaCut = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_ckResFakePionBetaCut = {{}};

    AIDA::IHistogram1D* h_ckResTrueRich2RType = nullptr;
    AIDA::IHistogram1D* h_ckResTrueRich2HType = nullptr;

    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_ckexpVptot = {{}};
  };

} // namespace Rich::Future::Rec::MC::Moni

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------

StatusCode SIMDPhotonCherenkovAngles::prebookHistograms() {

  bool ok = true;

  // Loop over radiators
  for ( const auto rad : activeRadiators() ) {

    // beta cut string
    std::string betaS = "beta";
    if ( m_minBeta[rad] > 0.0 ) { betaS = std::to_string( m_minBeta[rad] ) + "<" + betaS; }
    if ( m_maxBeta[rad] < 1.0 ) { betaS = betaS + "<" + std::to_string( m_maxBeta[rad] ); }

    // Plots for MC true type
    ok &= saveAndCheck( h_ckResAll[rad],                                               //
                        richHisto1D( HID( "ckResAll", rad ),                           //
                                     "Rec-Exp CKTheta - True Type",                    //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov Theta) / rad" ) );
    ok &= saveAndCheck( h_ckResTrue[rad],                                              //
                        richHisto1D( HID( "ckResTrue", rad ),                          //
                                     "Rec-Exp CKTheta - MC true photons - True Type",  //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov Theta) / rad" ) );
    ok &= saveAndCheck( h_ckResFake[rad],                                              //
                        richHisto1D( HID( "ckResFake", rad ),                          //
                                     "Rec-Exp CKTheta - MC fake photons - True Type",  //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov Theta) / rad" ) );

    ok &= saveAndCheck( h_ckResAllBetaCut[rad],                                        //
                        richHisto1D( HID( "ckResAllBetaCut", rad ),                    //
                                     "Rec-Exp CKTheta - True Type - " + betaS,         //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov Theta) / rad" ) );
    ok &= saveAndCheck( h_ckResTrueBetaCut[rad],
                        richHisto1D( HID( "ckResTrueBetaCut", rad ),                             //
                                     "Rec-Exp CKTheta - MC true photons - True Type - " + betaS, //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),           //
                                     "delta(Cherenkov Theta) / rad" ) );
    ok &= saveAndCheck( h_ckResFakeBetaCut[rad],                                                 //
                        richHisto1D( HID( "ckResFakeBetaCut", rad ),                             //
                                     "Rec-Exp CKTheta - MC fake photons - True Type - " + betaS, //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),           //
                                     "delta(Cherenkov Theta) / rad" ) );

    ok &= saveAndCheck( h_thetaRecTrue[rad],                                          //
                        richHisto1D( HID( "thetaRecTrue", rad ),                      //
                                     "Reconstructed CKTheta - MC true photons",       //
                                     m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(), //
                                     "Cherenkov Theta / rad" ) );
    ok &= saveAndCheck( h_phiRecTrue[rad],                                    //
                        richHisto1D( HID( "phiRecTrue", rad ),                //
                                     "Reconstructed CKPhi - MC true photons", //
                                     0.0, 2.0 * Gaudi::Units::pi, nBins1D(),  //
                                     "Cherenkov Phi / rad" ) );
    ok &= saveAndCheck( h_thetaRecFake[rad],                                          //
                        richHisto1D( HID( "thetaRecFake", rad ),                      //
                                     "Reconstructed CKTheta - MC fake photons",       //
                                     m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(), //
                                     "Cherenkov Theta / rad" ) );
    ok &= saveAndCheck( h_phiRecFake[rad],                                    //
                        richHisto1D( HID( "phiRecFake", rad ),                //
                                     "Reconstructed CKPhi - MC fake photons", //
                                     0.0, 2.0 * Gaudi::Units::pi, nBins1D(),  //
                                     "Cherenkov Phi / rad" ) );
    ok &= saveAndCheck( h_ckResTrueVP[rad],                                                   //
                        richProfile1D( HID( "ckResTrueVP", rad ),                             //
                                       "<Rec-Exp CKTheta> V P - MC true photons - True Type", //
                                       m_minP[rad], m_maxP[rad], nBins1D(),                   //
                                       "Track Momentum (MeV/c)", "<Rec-Exp CKTheta> / rad" ) );

    // Plots assuming pion hypo

    ok &= saveAndCheck( h_ckResAllPion[rad],                                           //
                        richHisto1D( HID( "ckResAllPion", rad ),                       //
                                     "Rec-Exp CKTheta - Pion Type",                    //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov Theta) / rad" ) );
    ok &= saveAndCheck( h_ckResTruePion[rad],                                          //
                        richHisto1D( HID( "ckResTruePion", rad ),                      //
                                     "Rec-Exp CKTheta - MC true photons - Pion Type",  //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov Theta) / rad" ) );
    ok &= saveAndCheck( h_ckResFakePion[rad],                                          //
                        richHisto1D( HID( "ckResFakePion", rad ),                      //
                                     "Rec-Exp CKTheta - MC fake photons - Pion Type",  //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov Theta) / rad" ) );

    ok &= saveAndCheck( h_ckResAllPionBetaCut[rad],                                    //
                        richHisto1D( HID( "ckResAllPionBetaCut", rad ),                //
                                     "Rec-Exp CKTheta - Pion Type - " + betaS,         //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov Theta) / rad" ) );
    ok &= saveAndCheck( h_ckResTruePionBetaCut[rad],                                             //
                        richHisto1D( HID( "ckResTruePionBetaCut", rad ),                         //
                                     "Rec-Exp CKTheta - MC true photons - Pion Type - " + betaS, //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),           //
                                     "delta(Cherenkov Theta) / rad" ) );
    ok &= saveAndCheck( h_ckResFakePionBetaCut[rad],                                             //
                        richHisto1D( HID( "ckResFakePionBetaCut", rad ),                         //
                                     "Rec-Exp CKTheta - MC fake photons - Pion Type - " + betaS, //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),           //
                                     "delta(Cherenkov Theta) / rad" ) );
    // loop over particle types
    for ( const auto hypo : activeParticlesNoBT() ) {
      ok &= saveAndCheck( h_ckexpVptot[rad][hypo],                             //
                          richProfile1D( HID( "ckexpVptot", rad, hypo ),       //
                                         "Expected CK theta V track momentum", //
                                         m_minP[rad], m_maxP[rad], nBins1D(),  //
                                         "Track Momentum (GeV/c)" ) );
    }

    // For RICH2 only make plots for R and H type PMTs seperately
    if ( Rich::Rich2Gas == rad ) {
      ok &= saveAndCheck( h_ckResTrueRich2RType,                                         //
                          richHisto1D( HID( "ckResTrueRTypePD", rad ),                   //
                                       "Rec-Exp CKTheta - MC true photons - R-Type PDs", //
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                       "delta(Cherenkov theta) / rad" ) );
      ok &= saveAndCheck( h_ckResTrueRich2HType,                                         //
                          richHisto1D( HID( "ckResTrueHTypePD", rad ),                   //
                                       "Rec-Exp CKTheta - MC true photons - H-Type PDs", //
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                       "delta(Cherenkov theta) / rad" ) );
    }

  } // radiators

  return StatusCode{ok};
}

//-----------------------------------------------------------------------------

void SIMDPhotonCherenkovAngles::operator()( const Summary::Track::Vector&                   sumTracks,     //
                                            const LHCb::Track::Range&                       tracks,        //
                                            const SIMDPixelSummaries&                       pixels,        //
                                            const Rich::PDPixelCluster::Vector&             clusters,      //
                                            const Relations::PhotonToParents::Vector&       photToSegPix,  //
                                            const LHCb::RichTrackSegment::Vector&           segments,      //
                                            const CherenkovAngles::Vector&                  expTkCKThetas, //
                                            const SIMDCherenkovPhoton::Vector&              photons,       //
                                            const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                                            const LHCb::MCRichDigitSummarys&                digitSums ) const {

  // Make a local MC helper object
  const Helper mcHelper( tkrels, digitSums );

  // the lock
  std::lock_guard lock( m_updateLock );

  // loop over segments
  for ( const auto&& [seg, ckexp] : Ranges::ConstZip( segments, expTkCKThetas ) ) {
    // radiator
    const auto rad = seg.radiator();
    if ( !radiatorIsActive( rad ) ) continue;
    // momentum
    const auto pTot = seg.bestMomentumMag();
    // loop over PID types
    for ( const auto hypo : activeParticlesNoBT() ) { fillHisto( h_ckexpVptot[rad][hypo], pTot, ckexp[hypo] ); }
  }

  // loop over the photon info
  for ( const auto&& [sumTk, tk] : Ranges::ConstZip( sumTracks, tracks ) ) {

    // Get the MCParticles for this track
    const auto mcPs = mcHelper.mcParticles( *tk, true, 0.5 );
    _ri_verbo << "Found " << mcPs.size() << " MCPs for Track " << tk->key() << endmsg;

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {

      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];

      // Get the SIMD summary pixel
      const auto& simdPix = pixels[rels.pixelIndex()];

      // the segment for this photon
      _ri_verbo << " -> Segment " << rels.segmentIndex() << endmsg;
      const auto& seg = segments[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !radiatorIsActive( rad ) ) { continue; }

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // Weight per MCP
      const auto mcPW = ( !mcPs.empty() ? 1.0 / (double)mcPs.size() : 1.0 );

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( !phot.validityMask()[i] ) { continue; }

        // SmartID
        const auto id = phot.smartID()[i];
        _ri_verbo << " -> " << id << endmsg;

        // scalar cluster
        const auto& clus = clusters[simdPix.scClusIndex()[i]];

        // reconstructed theta
        const auto thetaRec = phot.CherenkovTheta()[i];

        // reconstructed phi
        const auto phiRec = phot.CherenkovPhi()[i];

        // do we have an true MC Cherenkov photon
        const auto trueCKMCPs = mcHelper.trueCherenkovPhoton( *tk, rad, clus );
        _ri_verbo << " -> Found " << trueCKMCPs.size() << " trueCK MCPs" << endmsg;

        // loop over MCPs
        for ( const auto mcP : mcPs ) {

          // The True MCParticle type
          auto pid = mcHelper.mcParticleType( mcP );
          // If MC type not known, assume Pion (as in real data)
          if ( Rich::Unknown == pid ) { pid = Rich::Pion; }
          // skip electrons which are reconstructed badly..
          if ( m_skipElectrons && Rich::Electron == pid ) { continue; }

          // beta cut for true MC type
          const auto mcbeta = richPartProps()->beta( pTot, pid );
          const auto betaC  = ( mcbeta >= m_minBeta[rad] && mcbeta <= m_maxBeta[rad] );

          // true Cherenkov signal ?
          const auto trueCKSig =
              ( mcP ? std::find( trueCKMCPs.begin(), trueCKMCPs.end(), mcP ) != trueCKMCPs.end() : false );
          _ri_verbo << "  -> Matched MCP " << ( mcP ? mcP->key() : -1 ) << " = " << trueCKSig << endmsg;

          // expected CK theta ( for true type )
          const auto thetaExp = expCKangles[pid];

          // delta theta
          const auto deltaTheta = thetaRec - thetaExp;

          // fill some plots
          fillHisto( h_ckResAll[rad], deltaTheta, mcPW );
          if ( betaC ) { fillHisto( h_ckResAllBetaCut[rad], deltaTheta, mcPW ); }
          if ( !trueCKSig ) {
            fillHisto( h_ckResFake[rad], deltaTheta, mcPW );
            if ( betaC ) { fillHisto( h_ckResFakeBetaCut[rad], deltaTheta, mcPW ); }
            fillHisto( h_thetaRecFake[rad], thetaRec, mcPW );
            fillHisto( h_phiRecFake[rad], phiRec, mcPW );
          } else {
            fillHisto( h_ckResTrue[rad], deltaTheta, mcPW );
            if ( betaC ) { fillHisto( h_ckResTrueBetaCut[rad], deltaTheta, mcPW ); }
            fillHisto( h_thetaRecTrue[rad], thetaRec, mcPW );
            fillHisto( h_phiRecTrue[rad], phiRec, mcPW );
            fillHisto( h_ckResTrueVP[rad], pTot, deltaTheta, mcPW );
            if ( Rich::Rich2Gas == rad ) {
              fillHisto( ( id.isHTypePMT() ? h_ckResTrueRich2HType : h_ckResTrueRich2RType ), deltaTheta, mcPW );
            }
          }

        } // loop over associated MCPs

        // Now plots when assuming all tracks are pions ( as in real data )
        {
          // beta for pion hypo
          const auto pionbeta  = richPartProps()->beta( pTot, Rich::Pion );
          const auto pionBetaC = ( pionbeta >= m_minBeta[rad] && pionbeta <= m_maxBeta[rad] );

          // delta theta (w.r.t. pion hypothesis)
          const auto deltaTheta = thetaRec - expCKangles[Rich::Pion];

          // fill some plots
          fillHisto( h_ckResAllPion[rad], deltaTheta );
          if ( trueCKMCPs.empty() ) {
            fillHisto( h_ckResFakePion[rad], deltaTheta );
          } else {
            fillHisto( h_ckResTruePion[rad], deltaTheta );
          }
          if ( pionBetaC ) {
            fillHisto( h_ckResAllPionBetaCut[rad], deltaTheta );
            if ( trueCKMCPs.empty() ) {
              fillHisto( h_ckResFakePionBetaCut[rad], deltaTheta );
            } else {
              fillHisto( h_ckResTruePionBetaCut[rad], deltaTheta );
            }
          }
        } // pion plots

      } // SIMD scalar loop
    }   // photon loop
  }     // track loop
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPhotonCherenkovAngles )

//-----------------------------------------------------------------------------
