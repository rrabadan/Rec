/***************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureMoniAlg.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloDigits_v2.h"
#include "Event/ODIN.h"
#include "LHCbAlgs/Consumer.h"

/** @class CaloFutureDigitMonitor CaloFutureTimeAlignment.cpp
 *
 *   The algorithm for trivial monitoring of the "CaloDigits" as a function of
 *   the bunch crossing ID.
 *   The algorithm produces the following histograms:
 *    1. CaloDigit Energy per BXID
 *    2. CaloDigit ADCs per BXID
 *    3. One histogram per cell, accumulated adc deposits per TAE window,
 *       for the two subchannels of the cells, if flag Spectrum is True.
 *    4. One histogram per crate, acumulating adc deposits per BXID, if flag
 *       PerCrate is True.
 *   The same set of histograms, but with cut on ADC is produced if specified.
 *
 *   Histograms reside in the directory @p /stat/"Name" , where
 *   @p "Name" is the name of the algorithm
 *
 *   @see   CaloFutureMoniAlg
 *
 *   @author Nuria Valls Canudas nuria.valls.canudas@cern.ch
 *   @date   25/11/2021
 */

using Input = LHCb::Event::Calo::Digits;

class CaloFutureTimeAlignment final
    : public LHCb::Algorithm::Consumer<void( const Input&, const LHCb::ODIN&, const DeCalorimeter& ),
                                       LHCb::DetDesc::usesBaseAndConditions<CaloFutureMoniAlg, DeCalorimeter>> {
public:
  StatusCode initialize() override;
  void       operator()( const Input&, const LHCb::ODIN&, const DeCalorimeter& ) const override;

  CaloFutureTimeAlignment( const std::string& name, ISvcLocator* pSvcLocator );

private:
  Gaudi::Property<bool>             m_spectrum{this, "Spectrum", false, "activate spectrum per channel histogramming"};
  Gaudi::Property<bool>             m_perCrate{this, "PerCrate", true, "activate spectrum per crate"};
  Gaudi::Property<std::vector<int>> m_TAE_BXIDs{this, "TAE_BXIDs"};
};

DECLARE_COMPONENT( CaloFutureTimeAlignment )

CaloFutureTimeAlignment::CaloFutureTimeAlignment( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"Input", LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( name )},
                 KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                 KeyValue{"Detector", LHCb::Calo::Utilities::DeCaloFutureLocation( name )}} ) {}

StatusCode CaloFutureTimeAlignment::initialize() {
  return Consumer::initialize().andThen( [&] {
    // Bunch crossing axis
    m_xMin = 0;
    m_xMax = 3600;
    m_xBin = m_xMax - m_xMin;

    hBook2( "TA_E", "Energy per BX " + inputLocation(), m_xMin, m_xMax, m_xBin, m_energyMin, m_energyMax, m_yBinTA );
    hBook2( "TA_ADC", "ADC per BX  " + inputLocation(), m_xMin, m_xMax, m_xBin, m_adcMin, m_adcMax, m_yBinTA );
    info() << "Using digits from " << inputLocation() << endmsg;
    return StatusCode::SUCCESS;
  } );
}

void CaloFutureTimeAlignment::operator()( const Input& digits, const LHCb::ODIN& odin,
                                          const DeCalorimeter& calo ) const {
  if ( digits.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found empty container in " << inputLocation() << endmsg;
    return;
  }

  const auto bcID = odin.bunchId();
  for ( const auto& digit : digits ) {
    if ( digit.adc() < m_adcFilter ) continue;
    hFill2( "TA_E", bcID, digit.energy() );
    hFill2( "TA_ADC", bcID, digit.adc() );

    // Fill per crate histograms (TAE mode)
    if ( m_perCrate.value() ) {
      const auto crate = calo.cardCrate( calo.cardNumber( digit.cellID() ) );
      plot2D( bcID, digit.adc(), fmt::format( "TAE_crate{}", crate ), 0, 3600, m_adcMin, m_adcMax, 3600, m_yBinTA );
    }
    // Fill cell by cell histograms (TAE mode)
    if ( m_spectrum.value() ) {
      const auto id        = digit.cellID();
      int        TAEwindow = -9999;
      for ( auto TAEbx : m_TAE_BXIDs ) {
        if ( ( bcID < TAEbx + 5 ) & ( bcID > TAEbx - 5 ) ) { TAEwindow = bcID - TAEbx; }
        plot1D( TAEbx, "TAE BXIDs", "TAE BXIDs", 0, 4000, 4000, 1. ); // safety check on which BX we read on
      }
      if ( TAEwindow > -9999 ) {
        std::string tit      = fmt::format( "{} channel : {}", detData(), Gaudi::Utils::toString( id ) );
        std::string name_tae = fmt::format( "{}Cells/{}/{}_{}_tae", detData(), id.areaName(), id.row(), id.col() );
        plot1D( TAEwindow, name_tae, tit, -5, 6, 11, digit.adc() );
      }
    }
  }
}
