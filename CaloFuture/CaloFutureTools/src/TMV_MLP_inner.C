/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Class: ReadMLPInner

/* configuration options =====================================================

#GEN -*-*-*-*-*-*-*-*-*-*-*- general info -*-*-*-*-*-*-*-*-*-*-*-

Method         : MLP::MLP
TMVA Release   : 4.2.1         [262657]
ROOT Release   : 6.24/00       [399360]
Creator        : calvom
Date           : Fri Jun 18 16:01:59 2021
Host           : Linux centos7-docker 4.18.0-193.19.1.el8_2.x86_64 #1 SMP Mon Sep 14 14:37:00 UTC 2020 x86_64 x86_64
x86_64 GNU/Linux Training events: 39000 Analysis type  : [Classification]


#OPT -*-*-*-*-*-*-*-*-*-*-*-*- options -*-*-*-*-*-*-*-*-*-*-*-*-

# Set by User:
NCycles: "600" [Number of training cycles]
HiddenLayers: "N" [Specification of hidden layer architecture]
NeuronType: "tanh" [Neuron activation function type]
V: "False" [Verbose output (short form of "VerbosityLevel" below - overrides the latter one)]
VarTransform: "N" [List of variable transformations performed before training, e.g.,
"D_Background,P_Signal,G,N_AllClasses" for: "Decorrelation, PCA-transformation, Gaussianisation, Normalisation, each for
the given class of events ('Al$ H: "True" [Print method-specific help message] TestRate: "5" [Test for overtraining
performed at each #th epochs] UseRegulator: "False" [Use regulator to avoid over-training] # Default: RandomSeed: "1"
[Random seed for initial synapse weights (0 means unique seed for each run; default value '1')] EstimatorType: "CE" [MSE
(Mean Square Estimator) for Gaussian Likelihood or CE(Cross-Entropy) for Bernoulli Likelihood] NeuronInputType: "sum"
[Neuron input function type] VerbosityLevel: "Default" [Verbosity level] CreateMVAPdfs: "False" [Create PDFs for
classifier outputs (signal and background)] IgnoreNegWeightsInTraining: "False" [Events with negative weights are
ignored in the training (but are included for testing and performance evaluation)] TrainingMethod: "BP" [Train with
Back-Propagation (BP), BFGS Algorithm (BFGS), or Genetic Algorithm (GA - slower and worse)] LearningRate: "2.000000e-02"
[ANN learning rate parameter] DecayRate: "1.000000e-02" [Decay rate for learning parameter] EpochMonitoring: "False"
[Provide epoch-wise monitoring plots according to TestRate (caution: causes big ROOT output file!)] Sampling:
"1.000000e+00" [Only 'Sampling' (randomly selected) events are trained each epoch] SamplingEpoch: "1.000000e+00"
[Sampling is used for the first 'SamplingEpoch' epochs, afterwards, all events are taken for training]
SamplingImportance: "1.000000e+00" [ The sampling weights of events in epochs which successful (worse estimator than
before) are multiplied with SamplingImportance, else they are divided.] SamplingTraining: "True" [The training sample is
sampled] SamplingTesting: "False" [The testing sample is sampled] ResetStep: "50" [How often BFGS should reset history]
Tau: "3.000000e+00" [LineSearch "size step"]
BPMode: "sequential" [Back-propagation learning mode: sequential or batch]
BatchSize: "-1" [Batch size: number of events/batch, only set if in Batch Mode, -1 for BatchSize=number_of_events]
ConvergenceImprove: "1.000000e-30" [Minimum improvement which counts as improvement (<0 means automatic convergence
check is turned off)] ConvergenceTests: "-1" [Number of steps (without improvement) required for convergence (<0 means
automatic convergence check is turned off)] UpdateLimit: "10000" [Maximum times of regulator update] CalculateErrors:
"False" [Calculates inverse Hessian matrix at the end of the training to be able to calculate the uncertainties of an
MVA value] WeightRange: "1.000000e+00" [Take the events for the estimator calculations from small deviations from the
desired value to large deviations only over the weight range]
##


#VAR -*-*-*-*-*-*-*-*-*-*-*-* variables *-*-*-*-*-*-*-*-*-*-*-*-

NVar 10
fracE1             fracE1           fracE1             fracE1 'F'
[-0.00198796624318,0.429448157549] fracE2             fracE2           fracE2
fracE2                                               'F'    [-0.00215498171747,0.465184569359]
fracE3             fracE3           fracE3             fracE3 'F'
[-0.00254167243838,0.444466114044] fracE4            fracE4          fracE4
fracE4                                              'F'    [-0.00208954326808,0.469378530979]
fracEseed            fracEseed          fracEseed            fracEseed 'F'
[0.034051630646,0.953236103058] fracE6            fracE6          fracE6
fracE6                                              'F'    [-0.00280335964635,0.47236764431]
fracE7            fracE7          fracE7            fracE7 'F'
[-0.00228969682939,0.42861238122] fracE8            fracE8          fracE8
fracE8                                              'F'    [-0.00277676782571,0.461210012436]
fracE9            fracE9          fracE9            fracE9 'F'
[-0.00385475391522,0.440069496632] Et                            Et                            Et Et 'F'
[2.00060749054,29.4433078766] NSpec 0

============================================================================ */

#include "Kernel/STLExtensions.h"
#include "Kernel/TMV_utils.h"
#include <array>
#include <cmath>
#include <string_view>

namespace Data::ReadMLPInner {
  namespace {
    constexpr auto ActivationFnc = []( double x ) {
      // fast hyperbolic tan approximation
      if ( x > 4.97 ) return 1.f;
      if ( x < -4.97 ) return -1.f;
      float x2 = x * x;
      float a  = x * ( 135135.0f + x2 * ( 17325.0f + x2 * ( 378.0f + x2 ) ) );
      float b  = 135135.0f + x2 * ( 62370.0f + x2 * ( 3150.0f + x2 * 28.0f ) );
      return a / b;
    };
    constexpr auto OutputActivationFnc = []( double x ) {
      // sigmoid
      return 1.0 / ( 1.0 + exp( -x ) );
    };

    // Normalization transformation, initialisation
    constexpr auto fMin_1 = std::array{
        std::array{-0.00198796624318, -0.00215498171747, -0.00177178543527, -0.00208954326808, 0.034051630646,
                   -0.00280335964635, -0.00194630539045, -0.00277676782571, -0.00385475391522, 2.00066828728},
        std::array{-0.00176436058246, -0.00200005155057, -0.00254167243838, -0.0011355348397, 0.197875663638,
                   -0.00198318064213, -0.00228969682939, -0.00194063456729, -0.00159838388208, 2.00060749054},
        std::array{-0.00198796624318, -0.00215498171747, -0.00254167243838, -0.00208954326808, 0.034051630646,
                   -0.00280335964635, -0.00228969682939, -0.00277676782571, -0.00385475391522, 2.00060749054}};

    constexpr auto fMax_1 =
        std::array{std::array{0.298671633005, 0.465184569359, 0.291880369186, 0.451889395714, 0.953236103058,
                              0.456907629967, 0.306165158749, 0.461210012436, 0.293805390596, 24.0820713043},
                   std::array{0.429448157549, 0.453826248646, 0.444466114044, 0.469378530979, 0.906697630882,
                              0.47236764431, 0.42861238122, 0.447018384933, 0.440069496632, 29.4433078766},
                   std::array{0.429448157549, 0.465184569359, 0.444466114044, 0.469378530979, 0.953236103058,
                              0.47236764431, 0.42861238122, 0.461210012436, 0.440069496632, 29.4433078766}};
    // weight matrix from layer 0 to 1
    constexpr auto fWeightMatrix0to1 =
        std::array{std::array{-0.985335160973618, -6.67931433815913, 6.82392026173755, 1.69582275021297,
                              3.57129073967505, 1.12514901339897, 0.266869927912525, -6.28067789358299,
                              6.95205392613507, -0.904656806287974, -1.4603929623076},
                   std::array{2.10748984930235, -0.134929184921827, -0.627965176721353, 1.56502759657526,
                              0.836321581003512, 1.34383839413578, 0.479273654820681, -1.32513672506628,
                              0.775164730469395, 0.0493650571682389, -1.80415067884722},
                   std::array{-0.702448067411557, -7.98681307016533, 5.02709422770136, 0.143345741246158,
                              1.25502079472121, -10.8872546208244, 2.19910353060669, -0.454684683858646,
                              -0.669888637810434, -0.0281551044163271, -12.461611756555},
                   std::array{-0.48990539792607, -0.970523967468077, -0.298617487101007, -0.615556688859541,
                              1.33901456042377, -0.889110874027364, -0.460871611867257, -0.803392995020684,
                              -0.337322688406954, -4.5690973816318, -8.6408121007897},
                   std::array{-2.77671532935746, -1.53204760061346, 0.0829827703148823, -1.46341108252287,
                              -3.7675817599428, 6.30327290663336, -0.583928890032717, 6.32509322934298,
                              -5.00616934013949, -0.113805421374926, 3.64367890360636},
                   std::array{-6.86876782199281, 4.76027257543677, 0.788595537608121, -2.52799185478243,
                              -6.72852622761777, -3.13609699546236, -7.23682125218895, 5.98061015940384,
                              2.94618075009842, 0.581576344098958, 1.13210681178618},
                   std::array{-1.00703118749707, 0.131461920829255, 1.91571634346451, -9.30909464522452,
                              1.30351083593439, 0.142350578760852, 4.00647900652347, -8.08358601746906,
                              -1.18266066035196, -0.0785651990914711, -12.7661302379648},
                   std::array{-4.36501648542364, 6.828416997626, -0.192316444156694, 7.18723939192428,
                              -2.92983968871285, -1.1487584480626, -0.212193423813941, -0.494125390666021,
                              -3.40114865037688, 0.0421927933808669, 5.69266329140161},
                   std::array{-5.13435448460134, -0.803118108904071, -4.85927436859816, 8.0781338250635,
                              -2.86703402520859, 7.60548727815092, -5.11794185379839, -0.935045766560244,
                              -5.01457080175552, 0.625638641392796, -2.93183336616653},
                   std::array{-1.339327559491, 2.72660655745387, -1.31175274042089, 1.58168254322843, -2.14067189861168,
                              1.26490374580979, -0.847318811706012, -1.86094298725024, -0.630394109056789,
                              -0.0236486664775165, 2.04682618362147}};
    // weight matrix from layer 1 to 2
    constexpr auto fWeightMatrix1to2 = std::array{
        -2.72007944285266, -1.29152034820886, 1.88879727185053, 2.19028814752033,  -2.11712335078242, 2.64299195918365,
        2.08403354167435,  -1.99648308705913, 3.09280621282882, -1.69176171948825, -0.643618082110088};

    // the training input variables
    constexpr auto validator =
        TMV::Utils::Validator{"ReadMLPInner", std::tuple{"fracE1", "fracE2", "fracE3", "fracE4", "fracEseed", "fracE6",
                                                         "fracE7", "fracE8", "fracE9", "Et"}};

    // Normalization transformation
    constexpr auto transformer = TMV::Utils::Transformer{fMin_1, fMax_1};
    constexpr auto l0To1       = TMV::Utils::Layer{fWeightMatrix0to1, ActivationFnc};
    constexpr auto l1To2       = TMV::Utils::Layer{fWeightMatrix1to2, OutputActivationFnc};
    constexpr auto MVA         = TMV::Utils::MVA{validator, transformer, 2, l0To1, l1To2};
  } // namespace
} // namespace Data::ReadMLPInner

struct ReadMLPInner final {

  // constructor
  ReadMLPInner( LHCb::span<const std::string_view, 10> theInputVars ) {
    Data::ReadMLPInner::MVA.validate( theInputVars );
  }

  // the classifier response
  // "inputValues" is a vector of input values in the same order as the
  // variables given to the constructor
  static constexpr double GetMvaValue( LHCb::span<const double, 10> inputValues ) {
    return Data::ReadMLPInner::MVA( inputValues );
  }
};
