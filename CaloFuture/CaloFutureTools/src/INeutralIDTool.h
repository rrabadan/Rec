/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/CaloHypo.h"
#include "GaudiKernel/IAlgTool.h"

/** @class INeutralIDTool INeutralIDTool.h
 *  @author Miriam Calvo
 *  @date   2021-07-05
 */

namespace LHCb::Calo::Interfaces {
  struct INeutralID : extend_interfaces<IAlgTool> {

    // Return the interface ID
    DeclareInterfaceID( INeutralID, 1, 0 );

    struct Observables {
      double e1frac    = std::numeric_limits<double>::quiet_NaN();
      double e2frac    = std::numeric_limits<double>::quiet_NaN();
      double e3frac    = std::numeric_limits<double>::quiet_NaN();
      double e4frac    = std::numeric_limits<double>::quiet_NaN();
      double eseedfrac = std::numeric_limits<double>::quiet_NaN();
      double e6frac    = std::numeric_limits<double>::quiet_NaN();
      double e7frac    = std::numeric_limits<double>::quiet_NaN();
      double e8frac    = std::numeric_limits<double>::quiet_NaN();
      double e9frac    = std::numeric_limits<double>::quiet_NaN();
      int    area      = std::numeric_limits<int>::quiet_NaN();
    };

    virtual std::optional<double> isNotH( const CaloHypo& hypo, const Observables& v ) const = 0;

    virtual double isNotH( const Observables& v ) const = 0;
  };
} // namespace LHCb::Calo::Interfaces
