/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloClusters_v2.h"
#include "GaudiKernel/IAlgTool.h"

/** @class ICaloFutureClusterTool ICaloFutureClusterTool.h
 *           CaloFutureInterfaces/ICaloFutureClusterTool.h
 *
 *  The generic interface for "CalorimeterFuture tools" , which deals with
 *  CaloCluster objects, the potential candidates are:
 *
 *    - cluster parameters calculation  for whole cluster
 *    - cluster parameters calculations for maximum 4x4 submatrix
 *    - cluster parameters calculations from 3x3 submatrix
 *    - cluster parameters calculations from "swiss-cross" sub-cluster
 *
 *  @author Ivan Belyaev
 *  @date   30/10/2001
 */

namespace LHCb::Calo::Interfaces {
  struct IClusterTool : extend_interfaces<IAlgTool> {

    DeclareInterfaceID( IClusterTool, 1, 0 );

    /** The main processing method (functor interface)
     *  @param reference to collection of CaloClusters to be processed
     *  @return status code
     */
    virtual StatusCode
    operator()( const DeCalorimeter& detector,
                LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>
                    clusters ) const = 0;
  };
} // namespace LHCb::Calo::Interfaces

// ============================================================================
