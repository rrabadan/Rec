/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "MuonInterfaces/MuonHit.h"
#include "MuonDet/IMuonFastPosTool.h"
#include "MuonInterfaces/MuonLogHit.h"
#include <cmath>

namespace {
  double recomputePos( const std::vector<double>& data, double& dpos, int& clsize, double step ) {
    int    np  = 0;
    double sum = 0., sum2 = 0.;
    for ( auto ip = data.begin(); ip != data.end(); ++ip ) {
      // check that this position is not already the same of a previous pad
      bool prendila = std::none_of( data.begin(), ip, [&]( double p ) { return std::abs( *ip - p ) < 0.5 * step; } );
      if ( prendila ) {
        ++np;
        sum += *ip;
        sum2 += std::pow( *ip, 2 );
      }
    }
    if ( np > 1 ) {
      const float tmp = ( sum2 - sum * sum / np ) / ( np * np - np );
      dpos            = ( tmp > 0 ? std::sqrt( tmp ) : 0.0f );
    }
    clsize = np;
    return ( np > 0 ? sum / np : 0.0 );
  }
} // namespace

MuonHit::MuonHit() = default;

MuonHit::MuonHit( IMuonFastPosTool const* posTool ) : m_posTool( posTool ) {}

//=============================================================================
// Constructor from a MuonLogPad
//=============================================================================

MuonHit::MuonHit( const MuonLogPad* mp, IMuonFastPosTool const* posTool )
    : ROOT::Math::XYZPoint( 0., 0., 0. ), m_posTool( posTool ) {
  createFromPad( mp );
}

void MuonHit::createFromPad( const MuonLogPad* mp ) {
  m_pads.clear();
  if ( mp->type() == MuonLogPad::UNPAIRED ) return;
  auto pos = m_posTool->calcTilePos( mp->tile() );
  if ( pos ) {
    m_pads.push_back( mp );
    m_padx.push_back( pos->x() );
    m_pady.push_back( pos->y() );
    m_padz.push_back( pos->z() );
    static const auto isqrt_12 = 1.0 / std::sqrt( 12. );
    m_dx                       = isqrt_12 * pos->dX();
    m_dy                       = isqrt_12 * pos->dY();
    m_dz                       = isqrt_12 * pos->dZ();
    m_hit_minx                 = pos->x() - pos->dX();
    m_hit_maxx                 = pos->x() + pos->dX();
    m_hit_miny                 = pos->y() - pos->dY();
    m_hit_maxy                 = pos->y() + pos->dY();
    m_hit_minz                 = pos->z() - pos->dZ();
    m_hit_maxz                 = pos->z() + pos->dZ();
    SetXYZ( pos->x(), pos->y(), pos->z() );
    m_xsize = m_ysize = 1;

    recomputeTime();
  }
}

//=============================================================================
// public member functions
//

void MuonHit::addPad( const MuonLogPad* mp ) {
  if ( mp->type() == MuonLogPad::UNPAIRED ) return;
  if ( m_pads.empty() ) {
    createFromPad( mp );
  } else {
    auto pos = m_posTool->calcTilePos( mp->tile() );
    if ( pos ) {
      m_pads.push_back( mp );
      m_padx.push_back( pos->x() );
      m_pady.push_back( pos->y() );
      m_padz.push_back( pos->z() );
      if ( ( pos->x() - pos->dX() ) < m_hit_minx ) m_hit_minx = pos->x() - pos->dX();
      if ( ( pos->x() + pos->dX() ) > m_hit_maxx ) m_hit_maxx = pos->x() + pos->dX();
      if ( ( pos->y() - pos->dY() ) < m_hit_miny ) m_hit_miny = pos->y() - pos->dY();
      if ( ( pos->y() + pos->dY() ) > m_hit_maxy ) m_hit_maxy = pos->y() + pos->dY();
      if ( ( pos->z() - pos->dZ() ) < m_hit_minz ) m_hit_minz = pos->z() - pos->dZ();
      if ( ( pos->z() + pos->dZ() ) > m_hit_maxz ) m_hit_maxz = pos->z() + pos->dZ();
      auto x = recomputePos( m_padx, m_dx, m_xsize, pos->dX() );
      auto y = recomputePos( m_pady, m_dy, m_ysize, pos->dY() );
      auto z = recomputePos( m_padz, m_dz, m_zsize, 10 * pos->dZ() );
      SetXYZ( x, y, z );

      recomputeTime();
    }
  }
}

void MuonHit::recomputeTime() {
  int   np  = 0;
  float sum = 0., sum2 = 0.;
  m_mintime = 999.;
  m_maxtime = -999.;

  auto accumulate = [&]( float time ) {
    sum += time;
    sum2 += time * time;
    np++;
    if ( time < m_mintime ) m_mintime = time;
    if ( time > m_maxtime ) m_maxtime = time;
  };

  for ( const auto& pad : m_pads ) {
    if ( pad->type() == MuonLogPad::XTWOFE ) { // consider the two measurements as independent
      accumulate( pad->timeX() );
      accumulate( pad->timeY() );
    } else {
      accumulate( pad->time() );
    }
  }

  m_time = ( np > 0 ? sum / np : 0.0f );
  if ( np > 1 ) {
    const float tmp = ( sum2 - sum * sum / np ) / ( np * np - np );
    m_dtime         = ( tmp > 0 ? std::sqrt( tmp ) : 0.0f );
  } else {
    m_dtime = m_pads[0]->dtime();
  }
}

/// return tile sizes
std::array<double, 3> MuonHit::hitTile_Size() const {
  return {( m_hit_maxx - m_hit_minx ) / 2., ( m_hit_maxy - m_hit_miny ) / 2., ( m_hit_maxz - m_hit_minz ) / 2.};
}

/// store a progressive hit number for debugging
void MuonHit::setHitID( int id ) {
  if ( id != 0 ) m_hit_ID = id;
}

std::vector<const MuonLogHit*> MuonHit::getHits() const {
  std::vector<const MuonLogHit*> out;
  for ( const auto& pad : m_pads ) {
    auto padhits = pad->getHits();
    out.insert( out.end(), padhits.begin(), padhits.end() );
  }
  return out;
}

std::vector<LHCb::Detector::Muon::TileID> MuonHit::getTiles() const {
  std::vector<LHCb::Detector::Muon::TileID> tiles;
  for ( const auto& pad : m_pads ) {
    auto padhits = pad->getHits();
    std::transform( padhits.begin(), padhits.end(), std::back_inserter( tiles ),
                    []( const MuonLogHit* mlh ) { return mlh->tile(); } );
  }
  return tiles;
}

std::vector<LHCb::Detector::Muon::TileID> MuonHit::getLogPadTiles() const {
  std::vector<LHCb::Detector::Muon::TileID> tiles;
  tiles.reserve( m_pads.size() );
  std::transform( m_pads.begin(), m_pads.end(), std::back_inserter( tiles ),
                  []( const MuonLogPad* mlp ) { return mlp->tile(); } );
  return tiles;
}

std::vector<float> MuonHit::getTimes() const {
  auto               hits = getHits();
  std::vector<float> times;
  times.reserve( hits.size() );
  std::transform( hits.begin(), hits.end(), std::back_inserter( times ),
                  []( const MuonLogHit* mlh ) { return mlh->time(); } );
  return times;
}

LHCb::Detector::Muon::TileID MuonHit::centerTile() const {
  if ( m_pads.size() == 1 ) return m_pads.front()->tile();
  double                       d2min = 9999999.;
  LHCb::Detector::Muon::TileID out;
  for ( unsigned int ip = 0; ip < m_pads.size(); ip++ ) {
    auto d2 = std::pow( m_padx[ip] - X(), 2 ) + std::pow( m_pady[ip] - Y(), 2 ) + std::pow( m_padz[ip] - Z(), 2 );
    if ( d2 < d2min ) {
      d2min = d2;
      out   = m_pads[ip]->tile();
    }
  }
  if ( !out.isDefined() ) {
    for ( unsigned int ip = 0; ip < m_pads.size(); ip++ ) {
      std::cout << m_padx[ip] << " " << m_pady[ip] << " " << m_padz[ip] << std::endl;
    }
  }
  return out;
}
