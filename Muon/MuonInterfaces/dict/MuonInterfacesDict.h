/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DICT_MUONINTERFACESDICT_H
#define DICT_MUONINTERFACESDICT_H 1

#include "MuonInterfaces/IMuonClusterRec.h"
#include "MuonInterfaces/IMuonHitDecode.h"
#include "MuonInterfaces/IMuonPadRec.h"
#include "MuonInterfaces/IMuonTrackMomRec.h"
#include "MuonInterfaces/IMuonTrackRec.h"
#include "MuonInterfaces/MuonHit.h"
#include "MuonInterfaces/MuonLogHit.h"
#include "MuonInterfaces/MuonLogPad.h"
#include "MuonInterfaces/MuonNeuron.h"
#include "MuonInterfaces/MuonTrack.h"

MuonLogHit               l1;
MuonLogPad               x1;
MuonHit                  m1;
MuonTrack                t1;
MuonNeuron               n1( nullptr, nullptr );
std::vector<MuonLogHit*> k1;
std::vector<MuonLogPad*> z1;
std::vector<MuonHit*>    v1;
std::vector<MuonTrack*>  h1;
std::vector<MuonNeuron*> w1;

#endif // DICT_MUONINTERFACESDICT_H
