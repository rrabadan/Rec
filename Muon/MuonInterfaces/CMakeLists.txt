###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Muon/MuonInterfaces
-------------------
#]=======================================================================]

gaudi_add_library(MuonInterfacesLib
    SOURCES
        src/Lib/MuonHit.cpp
        src/Lib/MuonNeuron.cpp
        src/Lib/MuonTrack.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::DAQEventLib
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::MuonDetLib
            LHCb::TrackEvent
            ROOT::GenVector
            ROOT::MathCore
)

gaudi_add_dictionary(MuonInterfacesDict
    HEADERFILES dict/MuonInterfacesDict.h
    SELECTION dict/MuonInterfacesDict.xml
    LINK MuonInterfacesLib
    OPTIONS ${REC_DICT_GEN_DEFAULT_OPTS}
)
