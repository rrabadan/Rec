/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "MuonDet/DeMuonDetector.h"
#include "MuonInterfaces/MuonLogPad.h"

#include "GaudiKernel/IAlgTool.h"

#include <vector>

class MuonLogHit;

/**
 *  @date   2008-01-25
 */
struct IMuonPadRec : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IMuonPadRec, 2, 0 );

  virtual std::vector<MuonLogPad> pads( std::vector<MuonLogHit>& myhits, DeMuonDetector const& ) const = 0;
};
