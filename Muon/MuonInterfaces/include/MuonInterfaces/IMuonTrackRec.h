/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/RawEvent.h"
#include "MuonDet/DeMuonDetector.h"

#include "GaudiKernel/IAlgTool.h"

#include <vector>

class MuonHit;
class MuonTrack;
class MuonNeuron;

/**
 *  @author Giovanni Passaleva / Giacomo Graziani
 *  @date   2008-04-11
 */
struct IMuonTrackRec : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IMuonTrackRec, 3, 0 );

  virtual std::tuple<std::vector<MuonHit>, std::vector<MuonTrack>, bool> const
  tracks( LHCb::RawBank::View const&, const DeMuonDetector& ) const = 0;

  virtual void setZref( double Zref )                 = 0;
  virtual void setPhysicsTiming( bool PhysTiming )    = 0;
  virtual void setAssumeCosmics( bool AssumeCosmics ) = 0;
  virtual void setAssumePhysics( bool AssumePhysics ) = 0;
};
