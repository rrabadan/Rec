/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "MuonDet/DeMuonDetector.h"

#include "GaudiKernel/IAlgTool.h"

#include <vector>

class MuonHit;
class MuonLogPad;
class IMuonFastPosTool;

/**
 *  Interface to clustering algorithm for standalone muon reconstruction
 *  @author Giacomo GRAZIANI
 *  @date   2009-10-15
 */
struct IMuonClusterRec : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IMuonClusterRec, 4, 0 );

  virtual std::vector<MuonHit> clusters( const std::vector<MuonLogPad>& pads,
                                         DeMuonDetector const&          muonDetector ) const = 0;
};
