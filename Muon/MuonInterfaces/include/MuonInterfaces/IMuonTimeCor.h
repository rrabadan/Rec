/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONTOOLS_IMUONTIMECOR_H
#define MUONTOOLS_IMUONTIMECOR_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "Detector/Muon/TileID.h"
#include "GaudiKernel/IAlgTool.h"

/** @class IMuonTimeCor IMuonTimeCor.h MuonTools/IMuonTimeCor.h
 *
 *
 *  @author Alessia Satta
 *  @date   2009-12-22
 */
struct IMuonTimeCor : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IMuonTimeCor, 2, 0 );
  virtual StatusCode getCorrection( LHCb::Detector::Muon::TileID tile, int& cor )    = 0;
  virtual StatusCode getOutCorrection( LHCb::Detector::Muon::TileID tile, int& cor ) = 0;
  virtual StatusCode setOutCorrection( LHCb::Detector::Muon::TileID tile, int cor )  = 0;
  virtual StatusCode writeOutCorrection()                                            = 0;
  virtual StatusCode writeCorrection()                                               = 0;
};
#endif // MUONTOOLS_IMUONTIMECOR_H
