/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/IMuonFastPosTool.h"
#include "MuonDet/MuonNamespace.h"

#include "MuonInterfaces/IMuonClusterRec.h"
#include "MuonInterfaces/MuonHit.h"
#include "MuonInterfaces/MuonLogPad.h"

#include "Detector/Muon/TileID.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

namespace LHCb {

  /**
   *  clustering tool for the muon detector
   *  @author Giacomo GRAZIANI
   *  @date   2009-10-15
   */
  class MuonClusterRec : public extends<GaudiTool, IMuonClusterRec> {
  public:
    MuonClusterRec( const std::string& type, const std::string& name, const IInterface* parent );
    std::vector<MuonHit> clusters( const std::vector<MuonLogPad>& pads,
                                   DeMuonDetector const&          muonDetector ) const override;

  private:
    Gaudi::Property<unsigned int> m_maxPadsPerStation{this, "MaxPadsPerStation", 1500};
    ToolHandle<IMuonFastPosTool>  m_posTool{this, "PosTool", "MuonDetPosTool"};
  };

  DECLARE_COMPONENT_WITH_ID( MuonClusterRec, "MuonClusterRec" )
} // namespace LHCb

namespace {
  int regX( LHCb::Detector::Muon::TileID tile ) { return ( ( tile.quarter() > 1 ? -1 : 1 ) * tile.nX() ); }
  int regY( LHCb::Detector::Muon::TileID tile ) {
    return ( ( ( tile.quarter() > 0 && tile.quarter() < 3 ) ? -1 : 1 ) * tile.nY() );
  }
} // namespace

LHCb::MuonClusterRec::MuonClusterRec( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  declareInterface<IMuonClusterRec>( this );
}

std::vector<MuonHit> LHCb::MuonClusterRec::clusters( const std::vector<MuonLogPad>& pads,
                                                     DeMuonDetector const&          muonDetector ) const {
  static const std::array<int, 5>   factor = {3, 1, 1, 1, 1};
  int                               nhits  = 0;
  std::map<const MuonLogPad*, bool> usedPad;
  bool                              searchNeighbours = true;

  // group pads by station
  debug() << "Log. pads before clustering:" << endmsg;
  // make it a vector of vectors, to allow dynamic sizing
  std::vector<std::vector<const MuonLogPad*>> stationPads{static_cast<size_t>( muonDetector.stations() )};
  for ( auto& isP : stationPads ) isP.reserve( pads.size() );

  if ( msgLevel( MSG::DEBUG ) ) {
    for ( const auto& pad : pads ) {
      if ( !pad.truepad() ) continue;
      debug() << "LOGPAD Q" << ( pad.tile().quarter() + 1 ) << "M" << ( pad.tile().station() + 1 ) << "R"
              << ( pad.tile().region() + 1 ) << " nX=" << pad.tile().nX() << " nY=" << pad.tile().nY()
              << " time=" << pad.time() << " +/-" << pad.dtime() << endmsg;
    }
  }
  for ( const auto& pad : pads ) {
    if ( pad.truepad() ) stationPads[pad.tile().station()].push_back( &pad );
  }

  std::vector<MuonHit> clusters;
  for ( int station = 0; station < muonDetector.stations(); station++ ) {
    if ( stationPads[station].size() > factor[station] * m_maxPadsPerStation ) {
      info() << "skipping station M" << station + 1 << " with too many pads:" << stationPads[station].size() << endmsg;
      continue;
    }
    for ( auto ipad = stationPads[station].begin(); ipad != stationPads[station].end(); ipad++ ) {
      if ( !usedPad.count( *ipad ) ) {
        // cluster seed
        usedPad[*ipad] = true;
        clusters.emplace_back( *ipad, m_posTool.get() );
        MuonHit& muon_Hit = clusters.back();
        // store a progressive hit number for debugging purposes
        muon_Hit.setHitID( ++nhits );
        // now search for adjacent pads
        searchNeighbours = true;
        while ( searchNeighbours ) {
          searchNeighbours = false;
          for ( auto jpad = std::next( ipad ); jpad != stationPads[station].end(); ++jpad ) {
            if ( usedPad.count( *jpad ) ) continue;
            bool takeit      = false;
            int  deltaRegion = abs( (int)( ( *ipad )->tile().region() - ( *jpad )->tile().region() ) );
            if ( deltaRegion > 1 ) continue;
            if ( deltaRegion == 0 ) { // same region: use logical position
              takeit =
                  std::any_of( muon_Hit.logPads().begin(), muon_Hit.logPads().end(), [&]( const MuonLogPad* clpad ) {
                    if ( clpad->tile().region() != ( *jpad )->tile().region() ) return false;
                    int deltaX = std::abs( regX( clpad->tile() ) - regX( ( *jpad )->tile() ) );
                    int deltaY = std::abs( regY( clpad->tile() ) - regY( ( *jpad )->tile() ) );
                    return ( ( deltaX == 0 && deltaY == 1 ) || ( deltaX == 1 && deltaY == 0 ) );
                  } );
            } else { // adjacent regions: use absolute position
              auto pos = m_posTool->calcTilePos( ( *jpad )->tile() );

              bool Xinside = ( pos->x() > muon_Hit.minX() && pos->x() < muon_Hit.maxX() );
              bool Xadj = ( ( pos->x() > muon_Hit.maxX() && pos->x() - pos->dX() - muon_Hit.maxX() < pos->dX() / 2 ) ||
                            ( pos->x() < muon_Hit.minX() && muon_Hit.minX() - pos->x() - pos->dX() < pos->dX() / 2 ) );
              bool Yinside = ( pos->y() > muon_Hit.minY() && pos->y() < muon_Hit.maxY() );
              bool Yadj = ( ( pos->y() > muon_Hit.maxY() && pos->y() - pos->dY() - muon_Hit.maxY() < pos->dY() / 2 ) ||
                            ( pos->y() < muon_Hit.minY() && muon_Hit.minY() - pos->y() - pos->dY() < pos->dY() / 2 ) );
              takeit    = ( ( Xinside || Xadj ) && ( Yinside || Yadj ) );
            }
            if ( takeit ) { // it's a neighbour, add it to the cluster
              usedPad[*jpad] = true;
              muon_Hit.addPad( *jpad );
              searchNeighbours = true; // we exit the loop and restart from ipad+1 ith the larger cluster
              break;
            }
          }
        } // end of neighbour search
      }   // end of unused pad request
    }     // end of loop on log. pads of a given station
  }       // end of loop on stations
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "\n OBTAINED CLUSTERS:" << endmsg;
    for ( const auto& c : clusters ) {
      debug() << "Cluster #" << c.hitID() << " in M" << ( c.station() + 1 ) << " with " << c.npads()
              << " pads   X=" << c.minX() << " - " << c.maxX() << "  Y=" << c.minY() << " - " << c.maxY()
              << " first tile has Nx/Ny=" << c.tile().nX() << "/" << c.tile().nY() << endmsg;
    }
  } // end of clustering algorithm
  return clusters;
}
