/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/IMuonFastPosTool.h"
#include "MuonDet/MuonNamespace.h"

#include "Detector/Muon/TileID.h"

#include "DetDesc/GenericConditionAccessorHolder.h"

#include "GaudiAlg/GaudiTool.h"

namespace LHCb {

  /**
   *  @author Giacomo GRAZIANI
   *  @date   2009-03-17
   */
  class MuonDetPosTool : public extends<DetDesc::ConditionAccessorHolder<GaudiTool>, IMuonFastPosTool> {
  public:
    using extends::extends;

    std::optional<DeMuonDetector::TilePosition> calcTilePos( const Detector::Muon::TileID& tile ) const override {
      return m_muonDetector.get().position( tile );
    }

    std::optional<DeMuonDetector::TilePosition> calcStripXPos( const Detector::Muon::TileID& tile ) const override {
      return m_muonDetector.get().position( tile );
    }

    std::optional<DeMuonDetector::TilePosition> calcStripYPos( const Detector::Muon::TileID& tile ) const override {
      return m_muonDetector.get().position( tile );
    }

  private:
    ConditionAccessor<DeMuonDetector> m_muonDetector{this, DeMuonLocation::Default};
  };

  // Declaration of the Tool Factory
  DECLARE_COMPONENT_WITH_ID( MuonDetPosTool, "MuonDetPosTool" )

} // namespace LHCb
