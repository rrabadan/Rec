/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonInterfaces/IMuonPadRec.h"
#include "MuonInterfaces/MuonLogHit.h"
#include "MuonInterfaces/MuonLogPad.h"

#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#include "Detector/Muon/Layout.h"
#include "Detector/Muon/TileID.h"

#include "GaudiAlg/GaudiTool.h"

#include <memory>
#include <set>
#include <vector>

namespace LHCb {

  using MuonLayout = Detector::Muon::Layout;

  /**
   *  special reconstruction tool for Monitoring
   *  code based on MuonRec by A.Satta et al.
   *
   *  @author G.Graziani
   *  @date   2008-01-25
   */

  class MuonPadRec : public GaudiTool, virtual public IMuonPadRec {
  public:
    MuonPadRec( const std::string& type, const std::string& name, const IInterface* parent );
    std::vector<MuonLogPad> pads( std::vector<MuonLogHit>&, DeMuonDetector const& ) const override;

    StatusCode initialize() override;

  private:
    Gaudi::Property<std::vector<long int>> m_TileVeto{this, "TileVeto"};
    Gaudi::Property<bool>                  m_getfirsthit{this, "FirstComeFirstServed", false};
    std::set<long int>                     m_TileVetoed;

    void       addCoordsNoMap( std::vector<MuonLogPad>&, std::vector<MuonLogHit*>&, int, int ) const;
    StatusCode addCoordsCrossingMap( std::vector<MuonLogPad>&, std::vector<MuonLogHit*>&, int, int,
                                     DeMuonDetector const& ) const;
    StatusCode makeStripLayouts( int, int, MuonLayout&, MuonLayout&, DeMuonDetector const& ) const;
    void       removeDoubleHits( std::vector<MuonLogHit*>& hits ) const;
  };

  DECLARE_COMPONENT_WITH_ID( MuonPadRec, "MuonPadRec" )
} // namespace LHCb

LHCb::MuonPadRec::MuonPadRec( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ), m_TileVeto( 0 ) {
  declareInterface<IMuonPadRec>( this );
}

StatusCode LHCb::MuonPadRec::initialize() {
  return GaudiTool::initialize().andThen( [&]() {
    for ( const auto& iv : m_TileVeto ) m_TileVetoed.insert( iv );
    return StatusCode::SUCCESS;
  } );
}

//-------------------------------------------------------------------------------//
// logical pad reconstruction, based on MuonRec algorithm from MuonDAQ package   //
//-------------------------------------------------------------------------------//
std::vector<MuonLogPad> LHCb::MuonPadRec::pads( std::vector<MuonLogHit>& myhits,
                                                DeMuonDetector const&    muonDetector ) const {
  std::vector<MuonLogPad> pads;
  pads.reserve( 1000 );
  // save locally current hits
  std::vector<MuonLogHit*> hits;
  std::transform( myhits.begin(), myhits.end(), std::back_inserter( hits ),
                  []( MuonLogHit& mlh ) { return &mlh; } ); //@FIXME
  if ( msgLevel( MSG::DEBUG ) ) debug() << "buildLogicalPads: raw hits are " << hits.size() << endmsg;
  removeDoubleHits( hits );
  if ( msgLevel( MSG::DEBUG ) ) debug() << "                  after cleaning doubles " << hits.size() << endmsg;
  if ( !hits.empty() ) {
    int station;
    for ( station = 0; station < muonDetector.stations(); station++ ) {
      int region;
      for ( region = 0; region < muonDetector.regions(); region++ ) {

        // get mapping of input to output from region
        // in fact we are reversing the conversion done in the digitisation
        int NLogicalMap = muonDetector.getLogMapInRegion( station, region );
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << " station and region " << station << " " << region << " maps " << NLogicalMap << endmsg;

        if ( 1 == NLogicalMap ) {
          // straight copy of the input + making SmartRefs to the MuonDigits
          addCoordsNoMap( pads, hits, station, region );
        } else {
          // need to cross the input strips to get output strips
          StatusCode sc = addCoordsCrossingMap( pads, hits, station, region, muonDetector );
          if ( !sc.isSuccess() ) {
            throw GaudiException( "Failed to map digits to coords by crossing strips", "MuonPadRec::pads", sc );
          }
        }
      }
    }
  }
  return pads;
}

// Adding entries to coords 1 to 1 from digits, need to make the references
void LHCb::MuonPadRec::addCoordsNoMap( std::vector<MuonLogPad>& pads, std::vector<MuonLogHit*>& hits, int station,
                                       int region ) const {
  for ( const auto& hit : hits ) {
    if ( hit->tile().station() == static_cast<unsigned int>( station ) &&
         hit->tile().region() == static_cast<unsigned int>( region ) ) {
      // make the coordinate to be added to coords
      if ( msgLevel( MSG::DEBUG ) )
        debug() << " LOGPAD OK nomap ODE " << hit->odeName() << " ch " << hit->odeChannel() << " tile " << hit->tile()
                << " time)=" << hit->time() << endmsg;

      if ( m_TileVetoed.find( (long int)hit->tile() ) == m_TileVetoed.end() ) {
        pads.emplace_back( hit );
        pads.back().settruepad();
      } else {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "applied veto on pad " << ( (long int)hit->tile() ) << endmsg;
      }
    }
  }
}

StatusCode LHCb::MuonPadRec::addCoordsCrossingMap( std::vector<MuonLogPad>& pads, std::vector<MuonLogHit*>& hits,
                                                   int station, int region, DeMuonDetector const& muonDetector ) const {
  // get local MuonLayouts for strips
  MuonLayout layoutOne, layoutTwo;
  StatusCode sc = makeStripLayouts( station, region, layoutOne, layoutTwo, muonDetector );
  if ( !sc.isSuccess() ) { return sc; }

  // seperate the two types of logical channel, flag if used with the pair
  std::vector<std::pair<MuonLogHit*, bool>> typeOnes;
  std::vector<std::pair<MuonLogHit*, bool>> typeTwos;
  for ( const auto& hit : hits ) {
    if ( ( hit->tile() ).station() == static_cast<unsigned int>( station ) &&
         ( hit->tile() ).region() == static_cast<unsigned int>( region ) ) {
      if ( hit->tile().layout() == layoutOne ) {
        typeOnes.emplace_back( hit, false );
      } else if ( hit->tile().layout() == layoutTwo ) {
        typeTwos.emplace_back( hit, false );
      } else {
        Warning( "MuonDigits in list are not compatible with expected shapes" ).ignore();
      }
    }
  }
  // now cross the two sets of channels
  // sorry about this std::pair stuff but it is the easiest way of matching
  // a bool to each digit

  for ( auto& iOne : typeOnes ) {
    for ( auto& iTwo : typeTwos ) {
      // who said C++ did not make lovely transparent code?
      auto pad = iOne.first->tile().intercept( iTwo.first->tile() );
      if ( pad.isValid() ) {
        if ( m_TileVetoed.find( (long int)pad ) == m_TileVetoed.end() ) {
          pads.emplace_back( iOne.first, iTwo.first );
          pads.back().settruepad();
          if ( msgLevel( MSG::DEBUG ) )
            debug() << " LOGPAD OK crossed ODE " << iOne.first->odeName() << " ch " << iOne.first->odeChannel()
                    << " and " << iTwo.first->odeChannel() << " tiles " << iOne.first->tile() << " and "
                    << iTwo.first->tile() << " times=" << iOne.first->time() << " and " << iTwo.first->time() << endmsg;

        } else {
          if ( msgLevel( MSG::DEBUG ) ) debug() << "applied veto on pad " << ( (long int)pad ) << endmsg;
        }
        // set flags to used on iOne and iTwo
        iOne.second = true;
        iTwo.second = true;
      }
    }
  }

  // now copy across directly all strips that have not yet been used
  for ( const auto& iOne : typeOnes ) {
    if ( !( iOne.second ) ) pads.emplace_back( iOne.first );
  }
  for ( const auto& iTwo : typeTwos ) {
    if ( !( iTwo.second ) ) pads.emplace_back( iTwo.first );
  }
  return StatusCode::SUCCESS;
}

StatusCode LHCb::MuonPadRec::makeStripLayouts( int station, int region, MuonLayout& layout1, MuonLayout& layout2,
                                               DeMuonDetector const& muonDetector ) const {
  unsigned int x1 = muonDetector.getLayoutX( 0, station, region );
  unsigned int y1 = muonDetector.getLayoutY( 0, station, region );
  unsigned int x2 = muonDetector.getLayoutX( 1, station, region );
  unsigned int y2 = muonDetector.getLayoutY( 1, station, region );
  layout1         = MuonLayout{x1, y1};
  layout2         = MuonLayout{x2, y2};
  return StatusCode::SUCCESS;
}

void LHCb::MuonPadRec::removeDoubleHits( std::vector<MuonLogHit*>& hits ) const {
  auto ih1 = hits.begin();
  while ( ih1 != hits.end() ) {
    auto ih2 = std::find_if( hits.begin(), ih1, [&]( MuonLogHit const* i ) { return i->tile() == ( *ih1 )->tile(); } );
    if ( ih2 != ih1 ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Found double hit in Tile " << ( *ih1 )->tile() << endmsg;
      bool chooseone = m_getfirsthit
                           ? ( ( *ih1 )->time() < ( *ih2 )->time() ) // take the first one
                           : ( std::abs( ( *ih1 )->time() - 7.5 ) <
                               std::abs( ( *ih2 )->time() - 7.5 ) ); // or take time closer to 0 , i.e. 7.5 bits
      if ( chooseone ) {
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << "  choose time " << ( *ih1 )->time() << " rather than " << ( *ih2 )->time() << endmsg;
        ( *ih2 )->setTime( ( *ih1 )->rawtime() );
      } else {
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << "  choose time " << ( *ih2 )->time() << " rather than " << ( *ih1 )->time() << endmsg;
      }
      ih1 = hits.erase( ih1 );
    } else {
      ++ih1;
    }
  }
}
