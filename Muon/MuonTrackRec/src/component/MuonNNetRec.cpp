/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonInterfaces/IMuonClusterRec.h"
#include "MuonInterfaces/IMuonHitDecode.h"
#include "MuonInterfaces/IMuonPadRec.h"
#include "MuonInterfaces/IMuonTrackMomRec.h"
#include "MuonInterfaces/IMuonTrackRec.h"
#include "MuonInterfaces/MuonHit.h"
#include "MuonInterfaces/MuonLogHit.h"
#include "MuonInterfaces/MuonLogPad.h"
#include "MuonInterfaces/MuonNeuron.h"
#include "MuonInterfaces/MuonTrack.h"

#include "Event/State.h"
#include "Event/StateVector.h"
#include "Event/Track.h"
#include "Kernel/STLExtensions.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/ISequencerTimerTool.h"

#include "boost/container/static_vector.hpp"

#include <list>
#include <numeric>
#include <string>
#include <vector>

using namespace LHCb;
using namespace std;

/**
 *  @author Giovanni Passaleva
 *  @date   2008-04-11
 */
class MuonNNetRec : public GaudiTool, virtual public IMuonTrackRec {
public:
  MuonNNetRec( const std::string& type, const std::string& name, const IInterface* parent );
  StatusCode initialize() override;

  /**
   * builds and returns muon tracks as well as tooManyHits flag
   * @return tuple with vector of hits, vector of tracks and whether too many hits were found
   * Note that the tracks have pointers to hits in the vector of hits
   */
  std::tuple<std::vector<MuonHit>, std::vector<MuonTrack>, bool> const tracks( LHCb::RawBank::View const&,
                                                                               const DeMuonDetector& ) const override;
  void setZref( double Zref ) override { MuonTrackRec::Zref = Zref; }
  void setPhysicsTiming( bool PhysTiming ) override { MuonTrackRec::PhysTiming = PhysTiming; }
  void setAssumeCosmics( bool AssumeCosmics ) override {
    MuonTrackRec::IsCosmic = AssumeCosmics;
    if ( AssumeCosmics ) MuonTrackRec::IsPhysics = false;
  }
  void setAssumePhysics( bool AssumePhysics ) override {
    MuonTrackRec::IsPhysics = AssumePhysics;
    if ( AssumePhysics ) MuonTrackRec::IsCosmic = false;
  }

private:
  StatusCode muonNNetMon();
  StatusCode trackFit( std::vector<MuonTrack>& tracks ) const;

  // job options
  Gaudi::Property<float>  m_aa{this, "PosScaleFactor", 10., "head-tail weight scale factor"};
  Gaudi::Property<float>  m_bb{this, "NegScaleFactorB", 1., "head-head weight scale factor"};
  Gaudi::Property<float>  m_cc{this, "NegScaleFactorC", 1., "tail-tail weight scale factor"};
  Gaudi::Property<float>  m_slamb{this, "SlamFactorB", 5., "penalty for TT connections"};
  Gaudi::Property<float>  m_slamc{this, "SlamFactorC", 5., "penalty for HH connections"};
  Gaudi::Property<double> m_xesp1{this, "ExponentXZ", 10., "exponent for (1-sin(thxz)) weight factor"};
  Gaudi::Property<double> m_xesp2{this, "ExponentYZ", 10., "exponent for (1-sin(thyz)) weight factor"};
  Gaudi::Property<float>  m_dd{this, "Stimulus", 0., "stimulation weight term"};
  Gaudi::Property<float>  m_temp{this, "Temperature", 1., "temperature"};
  Gaudi::Property<float>  m_dsum{this, "Convergence", 1e-5, "convergence factor"};
  Gaudi::Property<float>  m_scut{this, "NeuronThreshold", 0.7, "neuron activation cut"};
  Gaudi::Property<float>  m_acut{this, "DoubleKillAngCut", 0.1, "angular cut for double length neurons killing"};
  Gaudi::Property<int>    m_span_cut{this, "TrackSpanCut", 2, "cut on span for selected tracks"};
  Gaudi::Property<int>    m_firing_cut{this, "StationFiringCut", 2, "min # of stations firing in the track"};
  Gaudi::Property<int>    m_maxNeurons{this, "MaxNeurons", 3000, "max number of possible track segments"};
  Gaudi::Property<int>    m_maxIterations{this, "MaxIterations", 100, "max number of NN iterations"};

  Gaudi::Property<int> m_skipStation{this, "SkipStation", -1,
                                     "station to be skipped (e.g. for eff. study): 0-4; -1 = keep all stns (default)"};

  Gaudi::Property<bool> m_allowHoles{this, "AllowHoles", true,
                                     "if <true> (default) holes of 1 station are allowed in track reconstruction"};

  Gaudi::Property<bool> m_physicsTiming{
      this, "PhysicsTiming", true,
      "if true, we assume that timing is the final one (accounting for TOF from primary vx)"};

  Gaudi::Property<bool> m_assumeCosmics{
      this, "AssumeCosmics", true, "if true (default) we assume that tracks are of cosmic origin (can be backward)"};

  Gaudi::Property<bool> m_assumePhysics{this, "AssumePhysics", false,
                                        "if true we assume that tracks have the 'right' direction (pz>0)"};

  // TOOLS
  ToolHandle<IMuonHitDecode> m_decTool{
      this, "DecodingTool", "MuonHitDecode",
      "Decoding tool (MuonHitDecode for offline, MuonMonHitDecode for online monitoring)"};
  ToolHandle<IMuonPadRec> m_padTool{this, "PadRecTool", "MuonPadRec", "Pad rec tool (MuonPadRec only option so far)"};
  ToolHandle<IMuonClusterRec>  m_clusterTool{this, "ClusterTool", "MuonClusterRec", "Clustering tool"};
  ToolHandle<IMuonTrackMomRec> m_momentumTool{this, "MomemtumTool", "MuonTrackMomRec",
                                              "Tool for calculating the transverse momentum"};

  Gaudi::Property<bool>  m_XTalk{this, "AddXTalk", false};
  Gaudi::Property<float> m_XtalkRadius{this, "XtalkRadius", 1.5};

  /// LHCb tracks output location in TES
  Gaudi::Property<std::string> m_trackOutputLoc{this, "TracksOutputLocation", "Rec/Muon/Track"};
};

DECLARE_COMPONENT( MuonNNetRec )

MuonNNetRec::MuonNNetRec( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<IMuonTrackRec>( this );
}

StatusCode MuonNNetRec::initialize() {
  StatusCode sc = GaudiTool::initialize();

  if ( !sc ) return sc;

  // switch off xtalk code if we're doing real clustering
  // if (m_clusterTool->name() == "MuonClusterRec") m_XTalk=false;

  // set tool options
  setPhysicsTiming( m_physicsTiming );
  setAssumeCosmics( m_assumeCosmics );
  setAssumePhysics( m_assumePhysics );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main reconstruction routine
//=============================================================================
std::tuple<std::vector<MuonHit>, std::vector<MuonTrack>, bool> const
MuonNNetRec::tracks( LHCb::RawBank::View const& raw, const DeMuonDetector& muonDetector ) const {
  // call decoding and pad reconstruction
  std::vector<MuonLogHit> myhits = m_decTool->hits( raw, muonDetector );
  std::vector<MuonLogPad> coords = m_padTool->pads( myhits, muonDetector );

  // call clustering algorithm
  std::vector<MuonHit> trackhits = m_clusterTool->clusters( coords, muonDetector );

  debug() << "\n running MuonNNetMon" << endmsg;
  unsigned nStations = muonDetector.stations();
  // preliminary cuts based on hits number in each station
  boost::container::static_vector<unsigned int, 5> hit_per_station( nStations, 0 );
  // check on maximum number of hit combinations giving a neuron
  for ( const auto& coord : trackhits ) hit_per_station[coord.station()]++;

  int ncomb = 0, nFiringS = 0;
  int minFiringS = 99;
  int maxFiringS = -99;
  for ( auto [i, nhits] : LHCb::range::enumerate( hit_per_station, 0u ) ) {
    if ( nhits > 0 ) {
      ++nFiringS;
      if ( minFiringS == 99 ) minFiringS = i;
      maxFiringS = i;
    }
    for ( unsigned j = i + 1; j < nStations && j < ( i + 3 ); j++ ) {
      if ( int( i ) != m_skipStation ) // stefania
        ncomb += hit_per_station[i] * hit_per_station[j];
    }
  }
  if ( nFiringS <= m_firing_cut || ( maxFiringS - minFiringS ) < m_span_cut ) {
    debug() << "not enough firing stations to get tracks" << endmsg;
    return {{}, {}, false};
  }

  if ( ncomb > m_maxNeurons ) {
    info() << "Too many hits to proceed with cosmic track finding" << endmsg;
    return {{}, {}, true};
  }

  // starts the NNet reconstruction

  // here starts the double loop over hits to build the neurons
  // Neurons are oriented segments with a tail (close to the IP) and a head
  // (far from the IP). Neurons can be connected head-head (tail-tail)
  // if they enter (exit) in (from) the same hit
  //
  // if holes are allowed (default) max neuron length is 2 else 1
  int neuronLength = m_allowHoles ? 2 : 1;
  debug() << "MAX NEURON LENGTH FOR THIS JOB IS: " << neuronLength << " Skipping Station " << m_skipStation << endmsg;

  std::list<MuonNeuron*> neurons;
  int                    Nneurons = 0;
  for ( auto ihT = trackhits.begin(); ihT != trackhits.end(); ihT++ ) {
    MuonHit *head, *tail;
    // skip a station for efficiency studies
    int stT = ihT->station();
    if ( stT == m_skipStation ) continue;

    int tID = ihT->hitID(); // tail ID
    for ( auto ihH = std::next( ihT ); ihH != trackhits.end(); ihH++ ) {

      // skip a station for efficiency studies
      int stH = ihH->station();
      if ( stH == m_skipStation ) continue;

      // cut on neuron length in terms of crossed stations

      if ( std::abs( double( ihH->station() - ihT->station() ) ) > neuronLength || ihH->station() == ihT->station() )
        continue;
      if ( ihH->station() > ihT->station() ) {
        head = &*ihH;
        tail = &*ihT;
      } else {
        head = &*ihT;
        tail = &*ihH;
      }

      int sta = nStations == 5 ? tail->station() + 1 : tail->station() + 2; // switch depends on no. of stations
      int reg = tail->region() + 1;
      int hID = head->hitID(); // head ID

      if ( m_assumePhysics ) { // tighter cuts on neurons
        double sf1, sf2, sf3, sf4;
        bool   neuron_OK = true;

        double tT  = atan2( ihT->Y(), ihT->Z() );
        double tH  = atan2( ihH->Y(), ihH->Z() );
        double pT  = atan2( ihT->X(), ihT->Z() );
        double pH  = atan2( ihH->X(), ihH->Z() );
        double dth = fabs( tT - tH );
        double dph = fabs( pT - pH );

        switch ( sta ) {
        case 1:
          sf1 = 2.0;
          sf2 = 1.5;
          sf3 = 2.0;
          sf4 = 2.0;
          switch ( reg ) {
          case 1:
            if ( dth > sf1 * 0.002 ) neuron_OK = false;
            if ( dph > sf1 * 0.0015 ) neuron_OK = false;
            break;
          case 2:
            if ( dth > sf2 * 0.004 ) neuron_OK = false;
            if ( dph > sf2 * 0.003 ) neuron_OK = false;
            break;
          case 3:
            if ( dth > sf3 * 0.008 ) neuron_OK = false;
            if ( dph > sf3 * 0.0065 ) neuron_OK = false;
            break;
          case 4:
            if ( dth > sf4 * 0.016 ) neuron_OK = false;
            if ( dph > sf4 * 0.013 ) neuron_OK = false;
            break;
          }
          break;
        case 2:
          sf1 = 3.0;
          sf2 = 3.0;
          sf3 = 6.0;
          sf4 = 3.0;
          switch ( reg ) {
          case 1:
            if ( dth > sf1 * 0.002 ) neuron_OK = false;
            if ( dph > sf1 * 0.0015 ) neuron_OK = false;
            break;
          case 2:
            if ( dth > sf2 * 0.004 ) neuron_OK = false;
            if ( dph > sf2 * 0.003 ) neuron_OK = false;
            break;
          case 3:
            if ( dth > sf3 * 0.008 ) neuron_OK = false;
            if ( dph > sf3 * 0.0065 ) neuron_OK = false;
            break;
          case 4:
            if ( dth > sf4 * 0.016 ) neuron_OK = false;
            if ( dph > sf4 * 0.013 ) neuron_OK = false;
            break;
          }
          break;
        default:
          sf1 = 5.0;
          sf2 = 3.0;
          sf3 = 2.0;
          sf4 = 2.0;
          switch ( reg ) {
          case 1:
            if ( dth > sf1 * 0.002 ) neuron_OK = false;
            if ( dph > sf1 * 0.0015 ) neuron_OK = false;
            break;
          case 2:
            if ( dth > sf2 * 0.004 ) neuron_OK = false;
            if ( dph > sf2 * 0.003 ) neuron_OK = false;
            break;
          case 3:
            if ( dth > sf3 * 0.008 ) neuron_OK = false;
            if ( dph > sf3 * 0.0065 ) neuron_OK = false;
            break;
          case 4:
            if ( dth > sf4 * 0.016 ) neuron_OK = false;
            if ( dph > sf4 * 0.013 ) neuron_OK = false;
            break;
          }
          break;
        }

        if ( !neuron_OK ) continue;
      }

      // here if everything OK. Now build the neurons.

      Nneurons++;
      // create the MuonNeuron
      MuonNeuron* Neuron = new MuonNeuron( head, tail, hID, tID, sta, reg );
      // store progressive neuron number for debugging
      Neuron->setNeuronID( Nneurons );
      // fill a neuron list to mainipulate them in the program
      neurons.push_back( Neuron ); // this is a std::list<MuonNeuron*>
    }
  }

  // the manipulation of neurons is made through the std::list neurons

  debug() << "Created neurons: " << neurons.size() << endmsg;

  // now calculate the weights

  for ( auto nl1 = neurons.begin(); nl1 != neurons.end(); nl1++ ) {

    for ( auto nl2 = std::next( nl1 ); nl2 != neurons.end(); nl2++ ) {

      double ww = 0;

      // neurons 1 and 2 are tail-head or head-tail
      if ( ( *nl1 )->tailHead( *( *nl2 ) ) || ( *nl1 )->headTail( *( *nl2 ) ) ) {
        double thxz = ( *nl1 )->angleXZ( *( *nl2 ) );
        double thyz = ( *nl1 )->angleYZ( *( *nl2 ) );
        ww          = m_aa * ( pow( ( 1 - sin( thxz ) ), m_xesp1 ) * pow( ( 1 - sin( thyz ) ), m_xesp2 ) );

        // store neuron 2 pointer in neuron 1 and its weight
        ( *nl1 )->setWeight( &( *( *nl2 ) ), ww );
        // store neuron 1 pointer in neuron 2 and its weight
        ( *nl2 )->setWeight( &( *( *nl1 ) ), ww );
      }
      // neurons 1 and 2 are head-head
      if ( ( *nl1 )->headHead( *( *nl2 ) ) ) {

        ww = -m_slamb * m_bb;
        // store neuron 2 pointer in neuron 1 and its weight
        ( *nl1 )->setWeight( &( *( *nl2 ) ), ww );
        // store neuron 1 pointer in neuron 2 and its weight
        ( *nl2 )->setWeight( &( *( *nl1 ) ), ww );
      }
      // neurons 1 and 2 are tail-tail
      if ( ( *nl1 )->tailTail( *( *nl2 ) ) ) {

        ww = -m_slamc * m_cc;
        // store neuron 2 pointer in neuron 1 and its weight
        ( *nl1 )->setWeight( &( *( *nl2 ) ), ww );
        // store neuron 1 pointer in neuron 1 and its weight
        ( *nl2 )->setWeight( &( *( *nl1 ) ), ww );
      }
    }
  }

  if ( m_allowHoles ) {
    // kill double length neurons if there is a unit length one
    for ( auto& n : neurons ) n->killDoubleLength( m_acut );
  }

  // clean up weights. only the tail-head and the head-tail with the best
  // weight are kept
  for ( auto& n : neurons ) n->cleanupWeights();

  //
  // now starts network evolution
  //

  int    iterations = 0;
  double ssum       = 2 * m_dsum;
  while ( ssum > m_dsum ) {
    iterations++;
    ssum = 0.0;
    /// update  neuron status
    for ( auto& n : neurons ) {

      const auto& wl = n->getWeights();

      double sum = std::accumulate( wl.begin(), wl.end(), 0.0, []( double s, const std::pair<MuonNeuron*, double>& p ) {
        return s + p.second * p.first->status();
      } );
      double s   = 0.5 * ( 1.0 + tanh( m_dd / m_temp + sum / m_temp ) ); // actual status
      double sp  = n->status();                                          // previous status
      if ( sum == 0 ) s = 0;
      ssum += fabs( s - sp ) / neurons.size();
      n->setStatus( s ); // update neuron status
    }
    if ( iterations == m_maxIterations ) break;
  }

  if ( iterations == m_maxIterations ) {

    Warning( "maximum number of iterations reached", StatusCode::SUCCESS, 0 ).ignore();
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "maximum number of iterations reached" << m_maxIterations << " reached" << endmsg;
  } else {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "network convergence after " << iterations << " iterations" << endmsg;
  }

  // drop OFF neurons

  int  pip = 0;
  auto nl1 = neurons.begin();
  while ( nl1 != neurons.end() ) {
    if ( ( *nl1 )->status() <= m_scut ) {
      nl1 = neurons.erase( nl1 );
    } else {
      pip++;
      // store progressive neuron number for debugging
      ( *nl1 )->setNeuronID( pip );
      nl1++;
    }
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << "ON neurons after convergence: " << neurons.size() << endmsg;

  //
  // create the tracks
  //

  std::list<MuonNeuron*> tmpList;
  nl1 = neurons.begin();
  std::vector<MuonTrack> tracks;
  tracks.reserve( neurons.size() );
  while ( ( nl1 != neurons.end() ) && ( !neurons.empty() ) ) {
    tracks.emplace_back();
    auto&                  muonTrack = tracks.back();
    std::list<MuonNeuron*> track; // create a new track
    // 1st neuron copied in temp list and deleted from primary
    tmpList.push_back( *nl1 );
    MuonNeuron* firstNeuron = *nl1;
    neurons.pop_front();
    // now loop over all other neurons and copy in the tmp list those in contact with 1st
    // also these neurons are removed from the primary list

    while ( !tmpList.empty() ) {

      auto nl2 = neurons.begin();

      while ( ( nl2 != neurons.end() ) && ( !neurons.empty() ) ) {

        if ( ( *nl2 )->connectedTo( *firstNeuron ) ) {
          tmpList.push_back( *nl2 );
          nl2 = neurons.erase( nl2 );
        } else {
          nl2++;
        }
      }
      // the first neuron of tmp is copied into the track and removed from
      // the tmp list; then we proceed with the 1st element of the tmp list
      // and look for connected neurons that are in turn added to the tmp list

      int      hID  = tmpList.front()->headTailID().first;
      int      tID  = tmpList.front()->headTailID().second;
      MuonHit* head = tmpList.front()->head();
      MuonHit* tail = tmpList.front()->tail();

      muonTrack.insert( hID, head ); // insert the head point
      muonTrack.insert( tID, tail ); // insert the tail point

      track.push_back( tmpList.front() );
      tmpList.pop_front();
      if ( !tmpList.empty() ) firstNeuron = tmpList.front();
    }

    // here the track should be filled and we can proceed with the next
    // remaining neuron

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "the track" << endmsg;
      for ( const auto& tk : track ) debug() << tk->neuronID() << endmsg;

      auto t_hits = muonTrack.getHits();
      debug() << "track hits " << t_hits.size() << endmsg;
      for ( const auto& tk : t_hits ) { debug() << tk->X() << " " << tk->Y() << " " << tk->Z() << endmsg; }
    }

    int span = muonTrack.getSpan();
    if ( msgLevel( MSG::DEBUG ) ) debug() << "track span " << span << endmsg;

    int firstat = muonTrack.getFiringStations();
    if ( msgLevel( MSG::DEBUG ) ) debug() << "firing stations " << firstat << endmsg;

    // fill the track vector only for those tracks where at least 3 stations
    // are firing
    if ( span >= m_span_cut && firstat > m_firing_cut ) {
      if ( m_XTalk ) {
        if ( msgLevel( MSG::DEBUG ) )
          debug() << " before Xtalk " << muonTrack.getHits().size() << " hits to the track" << endmsg;

        StatusCode sct = muonTrack.AddXTalk( trackhits, m_XtalkRadius, m_skipStation );

        if ( msgLevel( MSG::DEBUG ) )
          debug() << " After Xtalk " << muonTrack.getHits().size() << " hits to the track" << endmsg;

        if ( !sct ) Warning( "problem adding XTalk pads", sct, 0 ).ignore();
      }

    } else {
      tracks.pop_back();
    }

    nl1 = neurons.begin();
  }

  // here I have the list of good tracks selected by span cut.

  if ( msgLevel( MSG::DEBUG ) ) debug() << "selected tracks: " << tracks.size() << endmsg;

  // start track fitting

  StatusCode scf = trackFit( tracks );
  if ( !scf ) { Warning( "problem in track fitting", scf, 0 ).ignore(); }

  //  clearNeurons();
  return {std::move( trackhits ), std::move( tracks ), false};
}

//=============================================================================
//  track fitting
//=============================================================================

StatusCode MuonNNetRec::trackFit( std::vector<MuonTrack>& tracks ) const {

  for ( auto itk = tracks.begin(); itk != tracks.end(); itk++ ) {
    StatusCode tsc = itk->linFit();
    if ( !tsc ) {
      err() << "Error fitting track" << endmsg;
      return tsc;
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "sx : " << itk->sx() << " bx : " << itk->bx() << endmsg;
      debug() << "sy : " << itk->sy() << " by : " << itk->by() << endmsg;
      debug() << "esx : " << itk->errsx() << " ebx : " << itk->errbx() << endmsg;
      debug() << "esy : " << itk->errsy() << " eby : " << itk->errby() << endmsg;
    }

  } // end loop on tracks

  return StatusCode::SUCCESS;
}
