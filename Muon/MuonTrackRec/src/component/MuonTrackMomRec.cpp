/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
// Implementation file for class : MuonTrackMomRec
//
// 2010-02-10 : Giacomo Graziani
//-----------------------------------------------------------------------------
#include <DetDesc/GenericConditionAccessorHolder.h>
#include <Event/State.h>
#include <Event/StateParameters.h>
#include <Event/StateVector.h>
#include <Event/Track.h>
#include <Event/TrackParameters.h>
#include <Event/TrackTypes.h>
#include <GaudiAlg/GaudiTool.h>
#include <GaudiKernel/EventContext.h>
#include <GaudiKernel/PhysicalConstants.h>
#include <GaudiKernel/ThreadLocalContext.h>
#include <Kernel/IBIntegrator.h>
#include <Magnet/DeMagnet.h>
#include <MuonInterfaces/IMuonTrackMomRec.h>
#include <MuonInterfaces/MuonHit.h>
#include <MuonInterfaces/MuonLogPad.h>
#include <MuonInterfaces/MuonTrack.h>
#include <cmath>
#include <memory>
#include <vector>

/**
 *  @author Giacomo Graziani
 *  @date   2010-02-10
 *
 *  tool to compute momentum for standalone muon track
 *  adapted from the TrackPtKick tool by M. Needham
 */
class MuonTrackMomRec final : public LHCb::DetDesc::ConditionAccessorHolder<extends<GaudiTool, IMuonTrackMomRec>> {
public:
  using ConditionAccessorHolder::ConditionAccessorHolder;

  // from IMuonTrackMomRec
  std::unique_ptr<LHCb::Track> recMomentum( MuonTrack& track ) const override;
  double                       getBdl() const override;

private:
  Gaudi::Property<std::vector<float>> m_ParabolicCorrection{this, "ParabolicCorrection", {1.04, 0.14}};

  Gaudi::Property<std::vector<float>> m_resParams{this, "m_resParams", {0.015, 0.29}};

  Gaudi::Property<float> m_Constant{this, "ConstantCorrection", 0., "In MeV"};

  ToolHandle<IBIntegrator>    m_bIntegrator{this, "BIntegrator", "BIntegrator"};
  ConditionAccessor<DeMagnet> m_magnet{this, "Magnet", LHCb::Det::Magnet::det_path};

  auto fieldGrid() const { return m_magnet.get( getConditionContext( Gaudi::Hive::currentContext() ) ).fieldGrid(); }
};

DECLARE_COMPONENT( MuonTrackMomRec )

std::unique_ptr<LHCb::Track> MuonTrackMomRec::recMomentum( MuonTrack& track ) const {
  const auto Zfirst = StateParameters::ZEndRich2;
  // create a state at the Z of M1
  Gaudi::XYZPoint trackPos( track.bx() + track.sx() * Zfirst, track.by() + track.sy() * Zfirst, Zfirst );
  LHCb::State     state( LHCb::StateVector( trackPos, Gaudi::XYZVector( track.sx(), track.sy(), 1.0 ), 1. / 10000. ) );

  // helpers to preserve the original TrackPtKick code as much as possible
  auto   tState = &state;
  double tx_vtx = 0.;

  // copied from the TrackPtKick tool by M. Needham (comments starting with "// ==X== " are to highlight differences)

  // scan in cm steps
  // ==X== const Gaudi::XYZPoint begin{0., 0., 0.};
  Gaudi::XYZVector bdl;
  double           zCenter;

  // ==X== m_bIntegrator->calculateBdlAndCenter( magnet.fieldGrid(), {}, tState->position(), tState->tx(), tState->ty(),
  // ==X==                                      zCenter, bdl );
  m_bIntegrator->calculateBdlAndCenter( fieldGrid(), {}, {0., 0., StateParameters::ZEndRich2}, 0.0001, 0., zCenter,
                                        bdl );

  double q = 0.;
  double p = 1e6 * Gaudi::Units::MeV;

  if ( fabs( bdl.x() ) > TrackParameters::hiTolerance ) {
    // can estimate momentum and charge

    // Rotate to the  0-0-z axis and do the ptkick
    const double tX      = tState->tx();
    const double xCenter = tState->x() + tX * ( zCenter - tState->z() );

    const double zeta_trk = -tX / sqrt( 1.0 + tX * tX );
    // ==X== const double tx_vtx   = xCenter / zCenter;
    tx_vtx                = xCenter / zCenter;
    const double zeta_vtx = -tx_vtx / sqrt( 1.0 + tx_vtx * tx_vtx );

    // curvature
    const double curv = ( zeta_trk - zeta_vtx );

    // charge
    int sign = 1;
    if ( curv < TrackParameters::hiTolerance ) { sign *= -1; }
    if ( bdl.x() < TrackParameters::hiTolerance ) { sign *= -1; }
    q = -1. * ( bdl.x() > 0.0 ? 1 : -1 ) * sign;

    // momentum
    p = Gaudi::Units::eplus * Gaudi::Units::c_light * fabs( bdl.x() ) *
        sqrt( ( 1.0 + tX * tX + std::pow( tState->ty(), 2 ) ) / ( 1.0 + std::pow( tX, 2 ) ) ) / fabs( curv );

    //   Addition Correction factor for the angle of the track!
    if ( m_ParabolicCorrection.size() == 2u ) {
      // p*= (a + b*tx*tx )
      p += m_Constant;
      p *= ( m_ParabolicCorrection[0] + ( m_ParabolicCorrection[1] * tX * tX ) );
    }

  } else {
    // can't estimate momentum or charge
    error() << "B integral is 0!" << endmsg;
    return {};
  }

  double       qOverP       = q / p;
  const double err2         = std::pow( m_resParams[0], 2 ) + std::pow( m_resParams[1] / p, 2 );
  double       sigmaQOverP2 = err2 / std::pow( p, 2 );

  // fill momentum variables for state
  state.setQOverP( qOverP );

  Gaudi::TrackSymMatrix seedCov;
  seedCov( 0, 0 ) = track.errbx() * track.errbx();
  seedCov( 2, 2 ) = track.errsx() * track.errsx();
  seedCov( 1, 1 ) = track.errby() * track.errby();
  seedCov( 3, 3 ) = track.errsy() * track.errsy();
  seedCov( 4, 4 ) = sigmaQOverP2;
  state.setCovariance( seedCov );

  state.setLocation( LHCb::State::Location::Muon );

  debug() << "Muon state = " << state << endmsg;

  // set MuonTrack momentum variables (momentum at primary vertex)
  double           pz_vtx = state.p() * sqrt( 1 - tx_vtx * tx_vtx - state.ty() * state.ty() );
  Gaudi::XYZVector momentum_vtx( tx_vtx * pz_vtx, state.ty() * pz_vtx, pz_vtx );
  track.setP( state.p() );
  track.setPt( sqrt( std::pow( momentum_vtx.X(), 2 ) + std::pow( momentum_vtx.Y(), 2 ) ) );
  track.setqOverP( state.qOverP() );
  track.setMomentum( momentum_vtx );

  auto lbtrack = std::make_unique<LHCb::Track>();
  lbtrack->addToStates( state );

  int ntile = 0;
  for ( const auto& hit : track.getHits() ) {
    const auto Tiles = hit->getLogPadTiles();
    debug() << " Muon Hits has " << hit->getLogPadTiles().size() << " tiles in station " << hit->station() << endmsg;
    for ( const auto& t : Tiles ) {
      debug() << " Tile info ====== " << ( LHCb::LHCbID )( t ) << endmsg;
      lbtrack->addToLhcbIDs( ( LHCb::LHCbID )( t ) );
      ++ntile;
    }
  }
  debug() << " in total " << ntile << " tiles" << endmsg;

  lbtrack->setPatRecStatus( LHCb::Track::PatRecStatus::PatRecIDs );
  lbtrack->setType( LHCb::Track::Types::Muon );

  return lbtrack;
}

double MuonTrackMomRec::getBdl() const {
  Gaudi::XYZVector bdl;
  double           zCenter;

  m_bIntegrator->calculateBdlAndCenter( fieldGrid(), {}, {0., 0., StateParameters::ZEndRich2}, 0.0001, 0., zCenter,
                                        bdl );
  return bdl.x();
}
