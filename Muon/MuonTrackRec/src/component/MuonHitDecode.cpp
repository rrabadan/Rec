/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDAQ/IMuonRawBuffer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonInterfaces/IMuonHitDecode.h" // Interface
#include "MuonInterfaces/MuonLogHit.h"

#include "Detector/Muon/TileID.h"

#include "GaudiAlg/GaudiTool.h"

namespace LHCb {

  /**
   *  interface standard decoding tool (MuonRawBuffer) to MuonLogHit objects
   *
   *  @author Giacomo Graziani
   *  @date   2009-03-16
   *
   *  Parameters:
   *  - SkipHWNumber: Skip the calculation of the hardware numbers. Can be set 'true'
   *    for reconstructing MuonTT tracks as it is faster.
   */
  class MuonHitDecode : public extends<GaudiTool, IMuonHitDecode> {

  public:
    using extends::extends;

    std::vector<MuonLogHit> hits( LHCb::RawBank::View const& raw, const DeMuonDetector& muonDetector ) const override;
    // specific for Online Monitoring, not implemented here (just avoid compil. warnings)
    int                          banksSize( LHCb::RawBank::BankType, std::vector<int>& ) override { return 0; }
    unsigned int                 odeErrorWord( int, int ) override { return 0; }
    int                          bankVersion() override { return 0; }
    void                         dumpRawBanks() override {}
    void                         dumpFrame( int, int ) override {}
    bool                         mappingIsOld() override { return false; }
    int                          l0id() override { return 0; }
    int                          bcn() override { return 0; }
    int                          cbcn() override { return 0; }
    void                         setMultiBunch( int ) override {}
    void                         unsetMultiBunch() override {}
    bool                         multiBunch() override { return false; }
    int                          mbExtraBXPerside() override { return 0; }
    bool                         centralBX() override { return true; }
    bool                         firstBX() override { return true; }
    bool                         lastBX() override { return true; }
    LHCb::Detector::Muon::TileID tileFromODE( int, int ) override { return {}; }
    int                          odeIndex( int ) override { return 0; }
    int                          channelsPerQuadrant( int, int ) override { return 0; }
    int                          nPadX( int ) override { return 0; }
    int                          nPadY( int ) override { return 0; }
    int                          nPadXvy( int, int ) override { return 0; }
    int                          nPadYvx( int, int ) override { return 0; }
    float                        padSizeX( int, int ) override { return 0.; }
    float                        padSizeY( int, int ) override { return 0.; }
    float                        padSizeXvy( int, int ) override { return 0.; }
    float                        padSizeYvx( int, int ) override { return 0.; }
    LHCb::Detector::Muon::TileID tileFromLogCh( unsigned int, unsigned int, unsigned int, short int,
                                                unsigned int ) override {
      return {};
    }
    bool completeEvent() override { return true; }

  private:
    ToolHandle<IMuonRawBuffer> m_recTool{this, "MuonRawBuffer", "MuonRawBuffer"};
    Gaudi::Property<bool>      m_skipHWNumber{this, "SkipHWNumber", false};
  };

  DECLARE_COMPONENT_WITH_ID( MuonHitDecode, "MuonHitDecode" )
} // namespace LHCb

std::vector<MuonLogHit> LHCb::MuonHitDecode::hits( LHCb::RawBank::View const& raw,
                                                   const DeMuonDetector&      muonDetector ) const {
  auto tileAndTDC = m_recTool->getTileAndTDC( raw, *muonDetector.getDAQInfo() );
  for ( auto& ttdc : tileAndTDC ) { ttdc.second += 7 * 16; }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Size of tilesAndTDC container is: " << tileAndTDC.size() << endmsg;

  // create list of MuonLogHit objects
  std::vector<MuonLogHit> hits;
  hits.reserve( tileAndTDC.size() );
  for ( const auto& ttdc : tileAndTDC ) {
    hits.emplace_back( ttdc.first );
    auto& newhit     = hits.back();
    long  ODE_number = 0, ode_ch = 0;
    if ( !m_skipHWNumber.value() ) {
      auto address = muonDetector.getDAQInfo()->findHWNumber( ttdc.first );
      if ( address ) {
        ODE_number = address->ODENumber;
        ode_ch     = address->position;
      }
    }
    unsigned int on = ODE_number, oc = ode_ch;
    newhit.setOdeNumber( on );
    newhit.setOdeChannel( oc );
    short int OdeIndex = 1;
    newhit.setOdeIndex( OdeIndex );  // to be implemented
    int time = ttdc.second - 7 * 16; // remove the positive-forcing offset
    newhit.setTime( time );
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Size of MuonLogHit container is: " << hits.size() << endmsg;

  return hits;
}
