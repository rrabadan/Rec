/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Calo
// #include "CaloDet/DeCalorimeter.h"

// Event
#include "Event/PlumeAdc.h"
// #include "Event/CaloDigits_v2.h"
#include "Event/ODIN.h"

// Gaudi
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/Tuples.h"
#include "GaudiUtils/HistoLabels.h"
#include "LHCbAlgs/Consumer.h"

// AIDA
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

#include <TProfile.h>
#include <sstream>

using LHCb::Detector::Plume::ChannelID;
using Input   = LHCb::PlumeAdcs;
using BXTypes = LHCb::ODIN::BXTypes;

namespace {

  template <typename T>
  using axis1D = Gaudi::Accumulators::Axis<T>;

  template <typename T, typename K, typename OWNER>
  void map_emplace( T& t, K&& key, OWNER* owner, std::string const& name, std::string const& title,
                    Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis1 ) {
    t.emplace( std::piecewise_construct, std::forward_as_tuple( key ),
               std::forward_as_tuple( owner, name, title, axis1 ) );
  }

  template <typename T>
  std::string to_string( const T& streamable ) {
    std::ostringstream oss;
    oss << streamable;
    return oss.str();
  }

  const auto AxisBCID     = axis1D<double>( 3565, -0.5, 3564.5 );
  const auto AxisADC      = axis1D<double>( 4096 + 256, -256, 4096 );
  const auto AxisINDEXTAE = axis1D<double>( 9, 0.5, 9.5 );
} // namespace

class PlumeDigitMonitor final
    : public LHCb::Algorithm::Consumer<void( const Input&, const LHCb::ODIN& odin ),
                                       Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  // Standart constructor
  PlumeDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, {KeyValue{"Input", ""}, KeyValue{"ODIN", ""}} ){};

  virtual ~PlumeDigitMonitor() = default;
  StatusCode initialize() override;
  void       operator()( const Input&, const LHCb::ODIN& odin ) const override;

private:
  Gaudi::Property<float> m_timing_threshold{"TimingThreshold", 9.5};
  // DeCalorimeter* m_calo = nullptr;

  bool overThreshold( LHCb::PlumeAdc const& adc ) const {
    return ( adc.channelID().channelType() != ChannelID::ChannelType::TIME ) ? adc.overThreshold()
                                                                             : ( adc.adc() > m_timing_threshold );
  }

  void monitor( const Input& digits, const LHCb::ODIN& odin ) const;

  mutable std::mutex m_lazy_lock;

  mutable Gaudi::Accumulators::Histogram<1> m_over_thld_lumi{
      this, "over_threshold_lumi", "#events OT per channel: LUMI", axis1D<double>{48, -0.5, 47.5}};
  mutable Gaudi::Accumulators::Histogram<1> m_over_thld_time{
      this, "over_threshold_time", "#events OT per channel: TIME", axis1D<double>{64, -0.5, 63.5}};
  mutable Gaudi::Accumulators::Histogram<1> m_over_thld_pin{this, "over_threshold_pin", "#events OT per channel: PIN",
                                                            axis1D<double>{11, -0.5, 10.5}};
  mutable Gaudi::Accumulators::Histogram<1> m_over_thld_mon{this, "over_threshold_mon", "#events OT per channel: MON",
                                                            axis1D<double>{6, -0.5, 5.5}};

  mutable Gaudi::Accumulators::Histogram<2> m_bcid_adc_lumi{this, "bcid_adc", "BCID vs ADC (LUMI)", AxisBCID, AxisADC};
  mutable Gaudi::Accumulators::Histogram<2> m_bcid_adc_time{this, "bcid_adc_time", "BCID vs ADC time", AxisBCID,
                                                            axis1D<double>{256, -256, 4096 - 256}};
  mutable Gaudi::Accumulators::Histogram<2> m_bcid_adc_pin{this, "bcid_adc_pin", "BCID vs ADC pin", AxisBCID,
                                                           axis1D<double>{256, -256, 4096 - 256}};
  mutable Gaudi::Accumulators::Histogram<2> m_bcid_adc_mon{this, "bcid_adc_mon", "BCID vs ADC mon", AxisBCID,
                                                           axis1D<double>{256, -256, 4096 - 256}};
  mutable Gaudi::Accumulators::Histogram<2> m_bcid_adc_lumi_coarse{this, "bcid_adc_coarse", "BCID vs ADC (LUMI)",
                                                                   AxisBCID, axis1D<double>{410 + 26, -260, 4100}};

  mutable Gaudi::Accumulators::Histogram<2> m_xy_hits_front{
      this, "hits_front", "Hits in the front layer",
      axis1D<double>{
          11, -5.5, 5.5, "hits_front_xaxis", {"16", "15", "14", "13", "12", "", "00", "01", "02", "03", "04"}},
      axis1D<double>{
          11, -5.5, 5.5, "hits_front_yaxis", {"22", "21", "20", "19", "18", "", "06", "07", "08", "09", "10"}}};
  mutable Gaudi::Accumulators::Histogram<2> m_xy_hits_back{
      this, "hits_back", "Hits in the back layer",
      axis1D<double>{
          11, -5.5, 5.5, "hits_back_xaxis", {"40", "39", "38", "37", "36", "", "24", "25", "26", "27", "28"}},
      axis1D<double>{
          11, -5.5, 5.5, "hits_back_yaxis", {"46", "45", "44", "43", "42", "", "30", "31", "32", "33", "34"}}};

  mutable Gaudi::Accumulators::Histogram<1> m_occup_front_right{
      this, "hits_LUMI_00_04", "Number of hits in channels 00-04",
      axis1D<double>{5, 0, 5, "hits_LUMI_00_04_axis", {"00", "01", "02", "03", "04"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_front_left{
      this, "hits_LUMI_12_16", "Number of hits in channels 12-16",
      axis1D<double>{5, 0, 5, "hits_LUMI_12_16_axis", {"12", "13", "14", "15", "16"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_front_up{
      this, "hits_LUMI_06_10", "Number of hits in channels 06-10",
      axis1D<double>{5, 0, 5, "hits_LUMI_06_10_axis", {"06", "07", "08", "09", "10"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_front_down{
      this, "hits_LUMI_18_22", "Number of hits in channels 18-22",
      axis1D<double>{5, 0, 5, "hits_LUMI_18_22_axis", {"18", "19", "20", "21", "22"}}};

  mutable Gaudi::Accumulators::Histogram<1> m_occup_back_right{
      this, "hits_LUMI_24_28", "Number of hits in channels 24-28",
      axis1D<double>{5, 0, 5, "hits_LUMI_24_28_axis", {"24", "25", "26", "27", "28"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_back_left{
      this, "hits_LUMI_36_40", "Number of hits in channels 36-40",
      axis1D<double>{5, 0, 5, "hits_LUMI_36_40_axis", {"36", "37", "38", "39", "40"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_back_up{
      this, "hits_LUMI_30_34", "Number of hits in channels 30-34",
      axis1D<double>{5, 0, 5, "hits_LUMI_30_34_axis", {"30", "31", "32", "33", "34"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_occup_back_down{
      this, "hits_LUMI_42_46", "Number of hits in channels 42-46",
      axis1D<double>{5, 0, 5, "hits_LUMI_42_46_axis", {"42", "43", "44", "45", "46"}}};

  mutable Gaudi::Accumulators::Histogram<1> m_assym_front_vert{
      this, "hits_front_v", "Number of hits in front layer vertical PMTs",
      axis1D<double>{10, -5., 5., "hits_front_v_axis", {"16", "15", "14", "13", "12", "00", "01", "02", "03", "04"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_assym_front_horiz{
      this, "hits_front_h", "Number of hits in front layer horizontal PMTs",
      axis1D<double>{10, -5., 5., "hits_front_h_axis", {"22", "21", "20", "19", "18", "06", "07", "08", "09", "10"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_assym_back_vert{
      this, "hits_back_v", "Number of hits in back layer vertical PMTs",
      axis1D<double>{10, -5., 5., "hits_back_v_axis", {"40", "39", "38", "37", "36", "24", "25", "26", "27", "28"}}};
  mutable Gaudi::Accumulators::Histogram<1> m_assym_back_horiz{
      this, "hits_back_h", "Number of hits in back layer horizontal PMTs",
      axis1D<double>{10, -5., 5., "hits_back_h_axis", {"46", "45", "44", "43", "42", "30", "31", "32", "33", "34"}}};

  mutable Gaudi::Accumulators::StatCounter<> m_numAdcOTLumi = {this, "Number of ADC over threshold for LUMI"};
  mutable Gaudi::Accumulators::StatCounter<> m_numCoinc     = {this, "Number of coincidences"};

  // Calibration
  // ADC histogram for every channel (LUMI/PIN/...) only for calibration trigger (light)
  // (in MONET, "freeze" references for all of the above and compare with current histos)
  mutable Gaudi::Accumulators::Histogram<1> m_calibType{
      this, "odin_calib_type", "ODIN CalibrationType", {16, -0.5, 15.5}};
  // ADC histogram per channel type and BX type (only for calib triggers)
  mutable std::map<BXTypes, Gaudi::Accumulators::Histogram<1>> m_mon_bx_adc;
  mutable std::map<BXTypes, Gaudi::Accumulators::Histogram<1>> m_pin_bx_adc;

  // For physics monitoring
  // ADC histogram per channel type and BX type (excluding calib triggers)
  mutable std::map<BXTypes, Gaudi::Accumulators::Histogram<1>> m_lumi_bx_adc;
  mutable std::map<BXTypes, Gaudi::Accumulators::Histogram<1>> m_time_bx_adc;

  // ADC histogram for every channel per BB/EB/BE/EE (excluding calib triggers)
  mutable std::map<std::pair<ChannelID, BXTypes>, Gaudi::Accumulators::Histogram<1>> m_channel_bx_adc;

  // ADC vs BCID (TProfile1) for every channel
  mutable std::map<ChannelID, Gaudi::Accumulators::ProfileHistogram<1>> m_channel_bcid_adc;

  mutable Gaudi::Accumulators::ProfileHistogram<1> m_bcid_ncoinc_lumi{
      this, "bcid_ncoinc_LUMI", "Mean number of coincidences per event", AxisBCID};
  mutable Gaudi::Accumulators::ProfileHistogram<1> m_bcid_anycoinc_lumi{
      this, "bcid_anycoinc_LUMI", "Fraction of events with at least one coincidence", AxisBCID};

  mutable Gaudi::Accumulators::Histogram<2> m_bcid_lumi_tae{this, "bcid_lumi_tae", "ADC counts in lumi channels in TAE",
                                                            AxisINDEXTAE, AxisADC};
  // Timing data
  mutable Gaudi::Accumulators::Histogram<1> m_time_adc{this, "adc_TIME", "ADC for all timing channels", AxisADC};
  mutable std::map<ChannelID, Gaudi::Accumulators::Histogram<1>> m_channel_time;

  mutable unsigned long int m_runStartGps = 0;
  mutable unsigned int      m_lastRun     = 0;
};

// =============================================================================

DECLARE_COMPONENT( PlumeDigitMonitor )

// =============================================================================
// standard initialize method
// =============================================================================

StatusCode PlumeDigitMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    // FIXME we should loop over the fiber-channel maps (once they're in the DB) in order to
    // generate the full list of possible ChannelIDs. This would avoid hardcoding ranges here.
    std::vector<ChannelID> channels;
    for ( int i = 0; i < 48; ++i ) { channels.emplace_back( ChannelID::ChannelType::LUMI, i ); }
    for ( int i = 1; i <= 9; ++i ) { channels.emplace_back( ChannelID::ChannelType::PIN, i ); }
    for ( int i = 1; i <= 4; ++i ) { channels.emplace_back( ChannelID::ChannelType::MON, i ); }
    for ( int i : {5, 29, 11, 35} ) {
      for ( int j = 0; j <= 15; ++j ) channels.emplace_back( ChannelID::ChannelType::TIME, i, j );
      channels.emplace_back( ChannelID::ChannelType::TIME_T, i );
    }

    for ( const auto& ch : channels ) {
      if ( ch.channelType() != ChannelID::ChannelType::TIME_T ) {
        map_emplace( m_channel_bcid_adc, ch, this, "bcid_adc_" + ch.toString(), "ADC vs BCID for " + ch.toString(),
                     AxisBCID );
      }
      for ( auto bx : {BXTypes::NoBeam, BXTypes::Beam1, BXTypes::Beam2, BXTypes::BeamCrossing} ) {
        map_emplace( m_channel_bx_adc, std::make_pair( ch, bx ), this, "adc_" + ch.toString() + "_" + to_string( bx ),
                     "ADC for " + ch.toString() + "/" + to_string( bx ), AxisADC );
      }
      if ( ch.channelType() == ChannelID::ChannelType::TIME_T ) {
        map_emplace( m_channel_time, ch, this, "time_" + ch.toString(), "Time for " + ch.toString(), {4096, 0, 4096} );
      }
    }

    for ( auto bx : {BXTypes::NoBeam, BXTypes::Beam1, BXTypes::Beam2, BXTypes::BeamCrossing} ) {
      map_emplace( m_lumi_bx_adc, bx, this, "adc_LUMI_" + to_string( bx ), "ADC for LUMI/" + to_string( bx ), AxisADC );
      map_emplace( m_time_bx_adc, bx, this, "adc_TIME_" + to_string( bx ), "ADC for TIME/" + to_string( bx ), AxisADC );
      map_emplace( m_mon_bx_adc, bx, this, "adc_MON_" + to_string( bx ), "ADC for MON/" + to_string( bx ), AxisADC );
      map_emplace( m_pin_bx_adc, bx, this, "adc_PIN_" + to_string( bx ), "ADC for PIN/" + to_string( bx ), AxisADC );
    }
  } );
}

// ============================================================================
// standard execution method
// ============================================================================

void PlumeDigitMonitor::operator()( const Input& adcs, const LHCb::ODIN& odin ) const {
  // TODO: remove lock after migrating to new histograms
  std::lock_guard<std::mutex> guard_lock( m_lazy_lock );

  if ( odin.runNumber() > m_lastRun ) {
    m_lastRun     = odin.runNumber();
    m_runStartGps = odin.gpsTime();
  }

  monitor( adcs, odin );
}

// ============================================================================
// Fill histograms
// ============================================================================
void PlumeDigitMonitor::monitor( const Input& adcs, const LHCb::ODIN& odin ) const {
  unsigned int nOTLumi    = 0;
  unsigned int nCoincLumi = 0;

  auto       calibType = odin.calibrationType();
  auto       bx        = odin.bunchCrossingType();
  const auto bcid      = odin.bunchId();
  auto       indexTAE  = odin.timeAlignmentEventIndex();

  using ChannelType = ChannelID::ChannelType;

  ++m_calibType[calibType];

  for ( const auto& digit : adcs ) {
    const auto adc          = digit->adc();
    const auto type         = digit->channelID().channelType();
    const auto channel_id   = digit->channelID().channelID();
    const auto over_thld    = overThreshold( *digit );
    bool       complementOT = false;
    if ( type == ChannelType::LUMI || type == ChannelType::TIME ) {
      const auto* complement = adcs.object( ChannelID(
          type, channel_id < 24 ? ( channel_id + 24 ) : ( channel_id - 24 ), digit->channelID().channelSubID() ) );
      complementOT           = ( complement != nullptr ) && overThreshold( *complement );
    }

    if ( over_thld ) { m_channel_bcid_adc.at( digit->channelID() )[bcid] += adc; }

    if ( calibType == 0 && complementOT ) {
      auto key = std::make_pair( digit->channelID(), odin.bunchCrossingType() );
      ++m_channel_bx_adc.at( key )[adc];
    }

    switch ( type ) {
    case ChannelType::LUMI:
      nCoincLumi += over_thld && complementOT;
      if ( calibType == 0 && complementOT ) ++m_lumi_bx_adc.at( bx )[adc];
      break;
    case ChannelType::TIME:
      if ( calibType == 0 && complementOT ) ++m_time_bx_adc.at( bx )[adc];
      break;
    case ChannelType::PIN:
      if ( calibType != 0 ) ++m_pin_bx_adc.at( bx )[adc];
      break;
    case ChannelType::MON:
      if ( calibType != 0 ) ++m_mon_bx_adc.at( bx )[adc];
      break;
    default:
      break;
    }

    switch ( type ) {
    case ChannelID::ChannelType::LUMI: {
      if ( over_thld ) ++m_over_thld_lumi[channel_id];
      ++m_bcid_adc_lumi[{bcid, adc}];
      ++m_bcid_adc_lumi_coarse[{bcid, adc}];

      if ( indexTAE > 0 ) { ++m_bcid_lumi_tae[{10 - indexTAE, adc}]; }
      // fill histograms for front layer
      double x = channel_id % 6 + 1;
      double y = channel_id % 6 + 1;

      if ( over_thld ) {
        ++nOTLumi;

        if ( channel_id > 11 && channel_id < 24 ) {
          x *= -1;
          y *= -1;
        }
        if ( channel_id > 35 && channel_id < 48 ) {
          x *= -1;
          y *= -1;
        }
        ( channel_id % 12 ) < 5 ? y = 0 : x = 0;

        // fill outliers first
        if ( channel_id == 17 ) { ++m_xy_hits_front[{-2, -2}]; }
        if ( channel_id == 23 ) { ++m_xy_hits_front[{2, -2}]; }
        if ( channel_id == 41 ) { ++m_xy_hits_back[{-2, -2}]; }
        if ( channel_id == 47 ) { ++m_xy_hits_back[{2, -2}]; }

        if ( channel_id < 24 ) {
          ++m_xy_hits_front[{x, y}];
          if ( x > 0 && y == 0 ) {
            ++m_occup_front_right[channel_id % 6];
            ++m_assym_front_horiz[x - 0.5];
          }
          if ( x == 0 && y > 0 ) {
            ++m_occup_front_up[channel_id % 6];
            ++m_assym_front_vert[y - 0.5];
          }
          if ( x < 0 && y == 0 ) {
            ++m_occup_front_left[channel_id % 6];
            ++m_assym_front_horiz[x + 0.5];
          }
          if ( x == 0 && y < 0 ) {
            ++m_occup_front_down[channel_id % 6];
            ++m_assym_front_vert[y + 0.5];
          }
        }

        if ( channel_id >= 24 ) {
          ++m_xy_hits_back[{x, y}];
          if ( x > 0 && y == 0 ) {
            ++m_occup_back_right[channel_id % 6];
            ++m_assym_back_horiz[x - 0.5];
          }
          if ( x == 0 && y > 0 ) {
            ++m_occup_back_up[channel_id % 6];
            ++m_assym_back_vert[y - 0.5];
          }
          if ( x < 0 && y == 0 ) {
            ++m_occup_back_left[channel_id % 6];
            ++m_assym_back_horiz[x + 0.5];
          }
          if ( x == 0 && y < 0 ) {
            ++m_occup_back_down[channel_id % 6];
            ++m_assym_back_vert[y + 0.5];
          }
        }
      }
      break;
    }
    case ChannelID::ChannelType::TIME:
      ++m_time_adc[adc];
      if ( over_thld ) ++m_over_thld_time[channel_id];
      ++m_bcid_adc_time[{bcid, adc}];
      break;
    case ChannelID::ChannelType::PIN:
      if ( over_thld ) ++m_over_thld_pin[channel_id];
      ++m_bcid_adc_pin[{bcid, adc}];
      break;
    case ChannelID::ChannelType::MON:
      if ( over_thld ) ++m_over_thld_mon[channel_id];
      ++m_bcid_adc_mon[{bcid, adc}];
      break;
    case ChannelID::ChannelType::TIME_T:
      ++m_channel_time.at( digit->channelID() )[digit->adc()];
    default:
      break;
    }
  }

  nCoincLumi = nCoincLumi / 2; // we double count because of how we loop over ADCs
  m_bcid_ncoinc_lumi[bcid] += nCoincLumi;
  m_bcid_anycoinc_lumi[bcid] += nCoincLumi > 0;
  m_numAdcOTLumi += nOTLumi;
}
