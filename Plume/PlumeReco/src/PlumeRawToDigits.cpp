/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PlumeAdc.h"
#include "Event/RawEvent.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "Kernel/PlumeChannelID.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/LHCbMath.h"
#include "boost/container/small_vector.hpp"

#include <bitset>
#include <fmt/core.h>
#include <map>
#include <vector>

using LHCb::Detector::Plume::ChannelID;

/** @class PlumeRawToDigits
 *
 * Decode Plume raw data to PlumeAdcs
 *
 * @author Vladyslav Orlov
 * @author Rosen Matev
 *
 */
class PlumeRawToDigits : public LHCb::Algorithm::MultiTransformer<std::tuple<LHCb::PlumeAdcs, LHCb::PlumeCoincidences>(
                             const LHCb::RawEvent& )> {

public:
  /// Standard constructor
  PlumeRawToDigits( const std::string&, ISvcLocator* );
  std::tuple<LHCb::PlumeAdcs, LHCb::PlumeCoincidences> operator()( const LHCb::RawEvent& ) const override;

private:
  Gaudi::Property<bool> m_read_all_channels{this, "ReadAllChannels", false,
                                            "Read all channels, including those that are nominally not used"};
  Gaudi::Property<int>  m_pedestalOffset{this, "PedestalOffset", 256, "Offset to subtract from raw ADC counts."};

  mutable Gaudi::Accumulators::StatCounter<> m_numFiredChannels = {this, "Number of PMTs that got hit in event"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noLumiBank{this, "No lumi bank found for Plume"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noMonitoringBank{this, "No monitoring bank found for Plume"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noTimingBank{this, "No timing bank found for Plume"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_tooManyBanks{this, "Too many banks for Plume"};
  mutable Gaudi::Accumulators::WeightedHistogram<1>     m_sumADCPerChannel{
      this, "SumADCPerChannel", "Sum of ADCs per channel", {4 * 32, 0, 4 * 32}};
  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, uint64_t> m_nOverThresholdPerChannel{
      this, "NOverThresholdPerChannel", "Number of set over-threshold bits per channel", {4 * 32, 0, 4 * 32}};
};

DECLARE_COMPONENT( PlumeRawToDigits )

//=============================================================================
// Standard creator, initializes variables
//=============================================================================
PlumeRawToDigits::PlumeRawToDigits( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer( name, pSvcLocator, KeyValue{"RawEventLocation", ""},
                        {KeyValue{"Output", ""}, KeyValue{"Coincidences", ""}} ) {}

//=========================================================================
//  Main execution
//=========================================================================

std::tuple<LHCb::PlumeAdcs, LHCb::PlumeCoincidences> PlumeRawToDigits::
                                                     operator()( const LHCb::RawEvent& rawEvt ) const {
  LHCb::PlumeAdcs         adcs;
  LHCb::PlumeCoincidences coincidences;

  auto banks = rawEvt.banks( LHCb::RawBank::Plume );

  if ( banks.size() > 3 ) { ++m_tooManyBanks; }

  bool foundLumiBank       = false;
  bool foundMonitoringBank = false;
  bool foundTimingBank     = false;

  for ( const auto& bank : banks ) {
    if ( bank->version() > 1 ) {
      error() << fmt::format( "Plume raw data version {} not known. Assuming version = 1", bank->version() ) << endmsg;
    }
    const int sourceID = bank->sourceID();

    std::bitset<64> bits_ovt{__builtin_bswap32( bank->range<uint32_t>()[0] ) +
                             ( uint64_t( __builtin_bswap32( bank->range<uint32_t>()[1] ) ) << 32 )};

    std::bitset<32> bits_ovt_0{__builtin_bswap32( bank->range<uint32_t>()[0] )};
    std::bitset<32> bits_ovt_1{__builtin_bswap32( bank->range<uint32_t>()[1] )};

    // std::bitset<32> bits_coinc_11{__builtin_bswap32( bank->range<uint32_t>()[26] )};
    // std::bitset<32> bits_coinc_00{__builtin_bswap32( bank->range<uint32_t>()[27] )};
    // std::bitset<32> bits_coinc_01{__builtin_bswap32( bank->range<uint32_t>()[28] )};
    // The above does not work because we have 4 control bits first. Below we shift them away and fill the missing
    // bits from the following word. This will change in the future.
    const auto      w26 = __builtin_bswap32( bank->range<uint32_t>()[26] );
    const auto      w27 = __builtin_bswap32( bank->range<uint32_t>()[27] );
    const auto      w28 = __builtin_bswap32( bank->range<uint32_t>()[28] );
    const auto      w29 = __builtin_bswap32( bank->range<uint32_t>()[29] );
    std::bitset<32> bits_coinc_11{( w26 << 4 ) | ( w27 >> 28 )};
    std::bitset<32> bits_coinc_00{( w27 << 4 ) | ( w28 >> 28 )};
    std::bitset<32> bits_coinc_01{( w28 << 4 ) | ( w29 >> 28 )};

    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << bits_ovt << endmsg;
      verbose() << bits_ovt_0 << endmsg;
      verbose() << bits_ovt_1 << endmsg;
      verbose() << bits_coinc_11 << endmsg;
      verbose() << bits_coinc_00 << endmsg;
      verbose() << bits_coinc_01 << endmsg;
    }

    using bitset = std::bitset<2 * 32 * 12>; // 2 FEBs x 32 channels x 12 bits
    bitset bits;
    for ( const auto& [i, data] : LHCb::range::enumerate( bank->range<std::byte>().subspan( 8, 96 ) ) ) {
      // usual bit order, seems to not correspond to what the firmware does
      // bits |= bitset{std::to_integer<unsigned int>( num )} << ( i * 8 );
      // instead swap nibbles, i.e. for 0b11110101 we first "push" 0b1111 and then 0b0101
      auto    num     = static_cast<uint8_t>( data );
      uint8_t swapped = ( num >> 4 ) | ( ( num & 0x0F ) << 4 );
      bits |= bitset{swapped} << ( i * 8 );

      if ( msgLevel( MSG::VERBOSE ) ) {
        // verbose() << fmt::format( "i={:02} num={:02x} swapped={:02x}", i, num, swapped ) << endmsg;
      }
    }

    constexpr int shift_feb_0 = 0;
    constexpr int shift_feb_1 = 32 * 12;

    if ( sourceID == LHCb::Plume::lumiSourceID ) {
      foundLumiBank = true;
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Reading PLUME bank from TELL40 0 stream_0" << endmsg;
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << bits_ovt << endmsg;
      for ( int i = 0; i < 32; i++ ) {
        // if ( msgLevel( MSG::VERBOSE ) ) verbose() << fmt::format( "bit number={:02} bit value={:01}", i,
        // bits_ovt[31 - i]) << endmsg;
      }

      for ( int feb = 0; feb < 2; feb++ ) {
        int n_lumi_channels = ( ( !m_read_all_channels ) ? 22 : 32 );
        // FIXME also loop over the spares, so 22+2=24
        for ( int i = 0; i < n_lumi_channels; ++i ) {
          int  shift = ( feb == 0 ? shift_feb_0 : shift_feb_1 );
          auto rawAdc = (unsigned int)( bitset{0xFFF} & ( bits >> ( shift + 12 * i ) ) ).to_ulong();
          // reverse the three nibbles in the ADC value
          rawAdc = ( ( rawAdc & 0x00F ) << 8 ) | ( rawAdc & 0x0F0 ) | ( rawAdc >> 8 );
          auto overThreshold = feb == 0 ? bits_ovt_0[i] : bits_ovt_1[i];

          auto pmt_id =
              ( !m_read_all_channels ) ? LHCb::Plume::lumiFebToLogicalChannel.at( 32 * feb + i ) : ( 32 * feb + i );
          auto adc = new LHCb::PlumeAdc( ChannelID( ChannelID::ChannelType::LUMI, pmt_id ), rawAdc - m_pedestalOffset,
                                         overThreshold );

          if ( msgLevel( MSG::VERBOSE ) )
            verbose() << "Fiber: " << sourceID << "/" << feb << "/" << i << " ADC: " << *adc << endmsg;

          adcs.insert( adc );
          ++m_numFiredChannels;
          m_sumADCPerChannel[feb * 32 + i] += rawAdc;
          if ( overThreshold ) { ++m_nOverThresholdPerChannel[feb * 32 + i]; }
        }
      }

      // The matrix for coincidences is effectively hardcoded below.
      // Bit `i` denotes the coincidence between FEB channels i and i+1
      for ( int i = 0; i < 22; i += 2 ) {
        bool coincidence_11 = bits_coinc_11[i];
        bool coincidence_00 = bits_coinc_00[i];
        bool coincidence_01 = bits_coinc_01[i];
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << fmt::format( "i={:02} coinc_11={:02} coinc_00={:02} coinc_01={:02} ", i, coincidence_11,
                                    coincidence_00, coincidence_01 )
                    << endmsg;

        if ( coincidence_00 ) {
          auto adc_1 =
              adcs.object( ChannelID( ChannelID::ChannelType::LUMI, LHCb::Plume::lumiFebToLogicalChannel.at( i ) ) );
          auto adc_2 = adcs.object(
              ChannelID( ChannelID::ChannelType::LUMI, LHCb::Plume::lumiFebToLogicalChannel.at( i + 1 ) ) );
          coincidences.emplace_back( *adc_1, *adc_2 );
        }

        if ( coincidence_11 ) {
          auto adc_1 = adcs.object(
              ChannelID( ChannelID::ChannelType::LUMI, LHCb::Plume::lumiFebToLogicalChannel.at( i + 32 ) ) );
          auto adc_2 = adcs.object(
              ChannelID( ChannelID::ChannelType::LUMI, LHCb::Plume::lumiFebToLogicalChannel.at( i + 32 + 1 ) ) );
          coincidences.emplace_back( *adc_1, *adc_2 );
        }
      }

    } else if ( sourceID == LHCb::Plume::monitoringSourceID ) {
      foundMonitoringBank = true;
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Reading PLUME bank from TELL40 0 stream_1" << endmsg;
      for ( int feb = 0; feb < 1; feb++ ) {
        int n_channels = 16; // 8 PIN + 4 MON + 4 spares
        for ( int i = 0; i < n_channels; ++i ) {
          int  shift = ( feb == 0 ? shift_feb_0 : shift_feb_1 );
          auto rawAdc = (unsigned int)( bitset{0xFFF} & ( bits >> ( shift + 12 * i ) ) ).to_ulong();
          // reverse the three nibbles in the ADC value
          rawAdc = ( ( rawAdc & 0x00F ) << 8 ) | ( rawAdc & 0x0F0 ) | ( rawAdc >> 8 );
          auto overThreshold = feb == 0 ? bits_ovt_0[i] : bits_ovt_1[i];

          const auto it = LHCb::Plume::monitoringFebToLogicalChannel.find( i );
          if ( it == LHCb::Plume::monitoringFebToLogicalChannel.end() ) continue;
          auto adc = new LHCb::PlumeAdc( ChannelID( it->second.first, it->second.second ), rawAdc - m_pedestalOffset,
                                         overThreshold );

          if ( msgLevel( MSG::VERBOSE ) )
            verbose() << "Fiber: " << sourceID << "/" << feb << "/" << i
                      << " raw ADC: " << fmt::format( "{:04} {:03X}", rawAdc, rawAdc ) << " ADC: " << *adc << endmsg;
          adcs.insert( adc );
          ++m_numFiredChannels;
          m_sumADCPerChannel[( feb + 2 ) * 32 + i] += rawAdc;
          if ( overThreshold ) { ++m_nOverThresholdPerChannel[( feb + 2 ) * 32 + i]; }
        }
      }
    } else if ( sourceID == LHCb::Plume::timingSourceID ) {
      foundTimingBank = true;
      // Timing channels
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Reading PLUME bank from TELL40 1 stream_2" << endmsg;
      for ( int feb = 0; feb < 2; feb++ ) {
        int n_channels = 32;
        for ( int i = 0; i < n_channels; ++i ) {
          int  shift = ( feb == 0 ? shift_feb_0 : shift_feb_1 );
          auto rawAdc = (unsigned int)( bitset{0xFFF} & ( bits >> ( shift + 12 * i ) ) ).to_ulong();
          // reverse the three nibbles in the ADC
          rawAdc = ( ( rawAdc & 0x00F ) << 8 ) | ( rawAdc & 0x0F0 ) | ( rawAdc >> 8 );

          const auto it = LHCb::Plume::timingFebToLogicalChannel[feb].find( i );
          if ( it == LHCb::Plume::timingFebToLogicalChannel[feb].end() ) continue;
          auto const& [channelID, channelSubID] = it->second;

          auto adc = new LHCb::PlumeAdc( ChannelID( ChannelID::ChannelType::TIME, channelID, channelSubID ),
                                         rawAdc - m_pedestalOffset, false );

          if ( msgLevel( MSG::VERBOSE ) )
            verbose() << "Fiber: " << sourceID << "/" << feb << "/" << i
                      << " raw ADC: " << fmt::format( "{:04} {:03X}", rawAdc, rawAdc ) << " ADC: " << *adc << endmsg;
          adcs.insert( adc );
          ++m_numFiredChannels;
          m_sumADCPerChannel[( feb + 3 ) * 32 + i] += rawAdc;
        }

        auto const& bits_time = feb == 0 ? bits_ovt_0 : bits_ovt_1;
        for ( int i = 0; i < 2; ++i ) {
          unsigned int time = ( bits_time >> ( 16 * i ) ).to_ulong() & ( ( 1 << 12 ) - 1 );
          auto const   channelID = LHCb::Plume::timingValueLogicalChannel[feb][i];
          auto         adc = new LHCb::PlumeAdc( ChannelID( ChannelID::ChannelType::TIME_T, channelID ), time, false );
          adcs.insert( adc );
        }
      }
    } else {
      warning() << fmt::format( "Unknown source ID {}", sourceID ) << endmsg;
    }
  }

  if ( !foundLumiBank ) ++m_noLumiBank;
  if ( !foundMonitoringBank ) ++m_noMonitoringBank;
  if ( !foundTimingBank ) ++m_noTimingBank;

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Found ADCs:\n";
    for ( const auto& adc : adcs ) { debug() << *adc << '\n'; }
    debug() << endmsg;

    debug() << "Found coincidences:\n";
    for ( const auto& coinc : coincidences ) { debug() << coinc.first << " " << coinc.second << '\n'; }
    debug() << endmsg;
  }

  return {std::move( adcs ), std::move( coincidences )};
}
