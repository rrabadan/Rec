###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import glob
from Gaudi.Configuration import VERBOSE
from GaudiConf.LbExec import Options

from PyConf.application import (
    default_raw_event,
    configure_input,
    configure,
    make_odin,
    CompositeNode,
)
from PyConf.Algorithms import (
    PlumeRawToDigits,
    PlumeDigitMonitor,
    PlumeTuple,
)

run_number = 232523
run_number = 232607
run_number = 234521
run_number = 234664
run_number = 231977
run_number = 227542
run_number = 237205
options = Options(
    input_type="RAW",
    simulation=True,
    data_type="Upgrade",
    input_files=sorted(glob.glob(f'/hlt2*/objects/*/0000{run_number}/*.mdf')),
    dddb_tag='dddb-20210617',
    conddb_tag='sim-20210617-vc-md100',
    evt_max=100000,
    histo_file=f'./plume_histos_{run_number}.root',
    ntuple_file=f'./plume_ntuple_{run_number}.root',
    monitoring_file=f"plume_output_{run_number}.json",
)
configure_input(options)  # must call this before calling default_raw_event
odin = make_odin()

read_all_channels = False

digits = PlumeRawToDigits(
    ReadAllChannels=read_all_channels,
    # OutputLevel=VERBOSE,
    RawEventLocation=default_raw_event(["Plume"])).Output

monitor = PlumeDigitMonitor(
    Input=digits,
    # OutputLevel=VERBOSE,
    ODIN=odin)

plume_tuple = PlumeTuple(
    Input=digits,
    ODIN=odin,
    # OutputLevel=VERBOSE,
    ReadAllChannels=read_all_channels)

top_node = CompositeNode("Top", [plume_tuple, monitor])
configure(options, top_node)

from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV
reserveIOV("reserveIOV").PreloadGeometry = False
