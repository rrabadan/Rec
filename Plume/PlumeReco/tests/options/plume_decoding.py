###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
from Gaudi.Configuration import VERBOSE, DEBUG
from PRConfig.TestFileDB import test_file_db

from PyConf.application import (
    default_raw_event,
    configure_input,
    configure,
    make_odin,
    CompositeNode,
)
from PyConf.Algorithms import (
    PlumeRawToDigits,
    PlumeDigitMonitor,
    PlumeTuple,
    PrintHeader,
)

options = test_file_db["plume-raw-data-v1.1"].make_lbexec_options(
    simulation=True,
    dddb_tag="dddb-20210617",
    conddb_tag="sim-20210617-vc-md100",
    python_logging_level=logging.WARNING,
    evt_max=200,
    histo_file='plume_decoding_histo.root',
    ntuple_file='plume_decoding_ntuple.root',
    output_file="output.root",
)

configure_input(options)  # must call this before calling default_raw_event
odin = make_odin()

digits = PlumeRawToDigits(
    OutputLevel=DEBUG, RawEventLocation=default_raw_event(["Plume"])).Output
monitor = PlumeDigitMonitor(Input=digits, ODIN=odin)
plume_tuple = PlumeTuple(Input=digits, ODIN=odin)
print_event = PrintHeader(ODINLocation=odin)

top_node = CompositeNode("Top", [print_event, monitor, plume_tuple])
configure(options, top_node)

from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV
reserveIOV("reserveIOV").PreloadGeometry = False
