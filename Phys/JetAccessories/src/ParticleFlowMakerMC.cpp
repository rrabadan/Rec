/*****************************************************************************\
* (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiAlg/MergingTransformer.h"
#include <Gaudi/Accumulators.h>

// Event.
#include "Event/MCParticle.h"
#include "Event/Particle.h"

#include "LHCbMath/MatVec.h"
#include "LHCbMath/Vec3.h"

#include "Kernel/JetEnums.h"

/*

Receives a vector of LHCb::MCParticle containers, basic  or composite, outputs a list of particles
removing duplicities(no MC particle is used more than once) and particles to be banned (with their daughters)
If use a composite particle, doesn't use its daughters. Else all basic stable particles are used.

TODO: The test performed to avoid double counting is done by using the particle's address.
We plan to include LoKi::CheckOverlap for next version.

*/

namespace LHCb {

  enum struct Code { NotFlaggetYet, OK, NotRequested, Used, Banned, NotFromPV, FromDetMatInteraction };

  using MCParticlesVector = const Gaudi::Functional::vector_of_const_<MCParticles>;

  class ParticleFlowMakerMC : public Gaudi::Functional::MergingTransformer<Particles( MCParticlesVector const& )> {

  public:
    /// Constructor.
    ParticleFlowMakerMC( const std::string& name, ISvcLocator* svc );

    // Main method
    Particles operator()( MCParticlesVector const& Inputs ) const override;

  private:
    Gaudi::Property<std::vector<int>> requestedParticlesPIDs{
        this, "requestedParticlesPIDs", {0}, "List of MC particles PIDs to keep in the particle flow for jets"};

    Gaudi::Property<std::vector<int>> banPIDs{
        this, "banPIDs", {0}, "List of MC particles PIDs to be rejected, including its daughters"};

    /**
     * Recursively determine the status of an MCParticle decay tree.
     * For each MC particle, check if it is in the particles to use list and comes from a PV.
     * Then check if it is not in the particle to ban list.
     * If not, scan its daughters, mark them as used
     * Codes: 0: Not flagged yet, 1: OK, 2: used, 3: banned, 4: from interactions with detector material, 5: Not from PV
     */
    std::vector<std::pair<const MCParticle*, Code>> status( std::vector<const MCParticle*> const& mcprts ) const;

    /// Check if a particle is from a collision.
    bool primary( const MCParticle* prt ) const;

    std::unique_ptr<Particle> PfromMCP( const MCParticle& mcprt ) const;

    mutable Gaudi::Accumulators::Counter<> m_count{this, "00: # Number of Events"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_emptyVec{this, "Empty Input vector. Skipping", 0};
  };

  DECLARE_COMPONENT_WITH_ID( ParticleFlowMakerMC, "ParticleFlowMakerMC" )

  /// Constructor.
  ParticleFlowMakerMC::ParticleFlowMakerMC( const std::string& name, ISvcLocator* svc )
      : MergingTransformer( name, svc, {"Inputs", {}}, {"Output", "Phys/ParticleFlow/Particles"} ) {}

  Particles ParticleFlowMakerMC::operator()( MCParticlesVector const& Inputs ) const {

    ++m_count;

    if ( msgLevel( MSG::DEBUG ) ) {
      info() << "=============== New event: " << m_count.nEntries() << "  ================" << endmsg;

      info() << "Number of input vectors: " << Inputs.size() << endmsg;

      info() << "Sizes of Input vectors: ";
      int total( 0 );
      for ( auto& Input : Inputs ) {
        info() << Input.size() << " ";
        total += Input.size();
      }
      info() << " Total: " << total << endmsg;
    }

    std::vector<const MCParticle*> mcprts;
    auto                           size = 0;
    for ( auto& Input : Inputs ) size += Input.size();
    mcprts.reserve( size );

    for ( auto& Input : Inputs ) {
      if ( Input.empty() ) {
        ++m_msg_emptyVec;
        continue;
      }

      //  Merge all inputs
      mcprts.insert( mcprts.end(), Input.begin(), Input.end() );
    }
    // Determine the status of each MC particle
    auto prtsStatus = status( mcprts );

    // Scan the MCParticle list again and save the ones with flag == 1 as Particles
    Particles prts;

    for ( auto iPrt : prtsStatus ) {
      if ( iPrt.second == Code::OK ) {
        auto prt       = PfromMCP( *( iPrt.first ) );
        auto newprtkey = prts.add( prt.release() );
        debug() << "--> Added particle with key " << newprtkey << endmsg;
      }
    }
    debug() << "OK: " << int( Code::OK ) << " Banned: " << int( Code::Banned )
            << " NotRequested: " << int( Code::NotRequested ) << " Used: " << int( Code::Used )
            << " NotFromPV: " << int( Code::NotFromPV ) << endmsg;
    debug() << "Returning " << prts.size() << " particles" << endmsg;

    return prts;
  }

  // Recursively determine the status of an MCParticle decay tree.
  // Codes: 1: OK, 2: not requested, 3: used or mother already used, 4: banned, 5: Not from PV
  //     enum struct Code { NotFlaggetYet, OK, NotRequested, Used, Banned, FromDetMatInteraction, NotFromPV };
  std::vector<std::pair<const MCParticle*, Code>>
  ParticleFlowMakerMC::status( std::vector<const MCParticle*> const& mcprts ) const {
    std::vector<std::pair<const MCParticle*, Code>> prtsStatus;
    prtsStatus.reserve( mcprts.size() );
    for ( auto iPrt : mcprts ) {
      for ( auto iPrtStats : prtsStatus )
        if ( iPrt == iPrtStats.first ) {
          continue; // Particles already flagged
        }
      auto flag = Code::OK; // Assume particle is to be added to the output vector
      int  pid( iPrt->particleID().pid() );

      if ( std::any_of( begin( banPIDs.value() ), end( banPIDs.value() ),
                        [&]( auto& PID ) { return PID == abs( pid ); } ) ) {
        flag = Code::Banned;
      } else if ( std::none_of( begin( requestedParticlesPIDs.value() ), end( requestedParticlesPIDs.value() ),
                                [&]( auto& PID ) { return PID == abs( pid ); } ) ) {
        flag = Code::NotRequested;
      } else if ( !primary( iPrt ) ) {
        flag = Code::NotFromPV;
      }

      for ( const auto& vrt : iPrt->endVertices() ) {
        // const MCVertex* vrt = ivrt->target();
        if ( !vrt ) { continue; }
        if ( vrt->type() < 1 || vrt->type() > 4 ) {
        } else if ( flag != Code::NotRequested && ( vrt->type() == 2 || vrt->type() == 3 ) ) {
          // If flag==NotRequested, don't flag daughters, as they might be requested. If not and it's a decay vertex,
          // flag the daughters
          auto drtFlag( flag );
          if ( Code::OK == flag ) drtFlag = Code::Used; // If use mother, don't use daughters
          for ( auto const& iDtr : vrt->products() ) {
            bool flagged( false );
            for ( auto iDtrStats : prtsStatus ) {
              if ( (const MCParticle*)iDtr == iDtrStats.first ) {
                iDtrStats.second = drtFlag;
                flagged          = true;
                break;
              }
            }
            if ( !flagged ) prtsStatus.emplace_back( iDtr, drtFlag );
          }
        }
      }
      prtsStatus.emplace_back( iPrt, flag );
    }
    return prtsStatus;
  }

  // Check if a particle is from a collision.
  bool ParticleFlowMakerMC::primary( const MCParticle* prt ) const {
    const MCVertex* vrt = prt->originVertex();
    if ( !vrt ) return false;
    if ( vrt->isPrimary() ) return true;
    if ( vrt->type() < 1 || vrt->type() > 4 ) return false;
    if ( !vrt->mother() ) return false;
    return primary( vrt->mother() );
  }

  std::unique_ptr<Particle> ParticleFlowMakerMC::PfromMCP( const MCParticle& mcprt ) const {
    auto key = mcprt.key();
    auto pid = mcprt.particleID();

    auto prt = std::make_unique<Particle>( pid, key );
    prt->setMeasuredMass( mcprt.virtualMass() );
    prt->setMomentum( mcprt.momentum() );

    auto pos = referencePoint( mcprt );
    prt->setReferencePoint( {pos.X().cast(), pos.Y().cast(), pos.Z().cast()} );

    // Use jets enums to save MC PV position into particle
    prt->addInfo( JetPFMCInfo::MCParticleKey, key );
    auto mcpv = mcprt.primaryVertex();
    if ( mcpv ) {
      prt->addInfo( JetPFMCInfo::mcpv_x, mcpv->position().X() );
      prt->addInfo( JetPFMCInfo::mcpv_y, mcpv->position().Y() );
      prt->addInfo( JetPFMCInfo::mcpv_z, mcpv->position().Z() );
      prt->addInfo( JetPFMCInfo::MCPVkey, mcpv->key() );
    }
    return prt;
  }
} // namespace LHCb
