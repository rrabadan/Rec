/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef KERNEL_PFCALOCLUSTER_H
#define KERNEL_PFCALOCLUSTER_H 1

// Include files
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloCluster.h"
#include "Kernel/PFParticle.h"

/** @class PFCaloCluster PFCaloCluster.h
 *  PFCaloCluster inherit from LHCb::CaloCluster
 *  and contains few usefull extra information
 *
 *  @author Victor Coco
 *  @date   2012-07-23
 */
namespace LHCb {

  class PFCaloCluster {
  public:
    enum BanStatusType { Available = 0, AvailableForNeutralRecovery, ContainsInfMomentum, Used };

    /// constructor from CaloCluster
    PFCaloCluster( const CaloCluster* c ) : m_cluster( c ) {}

    int nSat() const { return m_nSat; };

    int status() const { return m_status; };

    void setStatus( int status ) {
      // If anything else than ContainsInfMomentum and Used already, then fill
      if ( m_status < ContainsInfMomentum ) m_status = status;
      // Otherwise only fill if it is not already used
      else if ( m_status != Used )
        m_status = status;
    };
    // TODO: ther accessors
    void updateParticleList( const LHCb::PFParticle* part ) { m_particleList.push_back( part ); };

    std::vector<const LHCb::PFParticle*> particleList() const { return m_particleList; };

    const CaloCluster*                  cluster() const { return m_cluster; }
    double                              e() const { return cluster()->e(); }
    const LHCb::CaloCluster::Position&  position() const { return cluster()->position(); }
    const LHCb::Detector::Calo::CellID& seed() const { return cluster()->seed(); }

  private:
    const CaloCluster*             m_cluster = nullptr;
    int                            m_nSat    = 0; ///< Number of saturated cells
    int                            m_status  = BanStatusType::Available;
    std::vector<const PFParticle*> m_particleList;
  };
} // namespace LHCb

#endif // KERNEL_PFCALOCLUSTER_H
