###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DaVinciFilters
-------------------
#]=======================================================================]

gaudi_add_module(DaVinciFilters
    SOURCES
        src/CopyParticlesWithPVRelations.cpp
        src/CopyFlavourTagsWithParticlePointers.cpp
        src/ParticleVeto.cpp
        src/Velo2LongB2jpsiKTrackEff_VeloTrackEffFilter.cpp
	    src/KSLongVeloFilter.cpp
    LINK
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::CaloFutureUtils
        LHCb::DAQEventLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::PhysInterfacesLib
        Rec::DaVinciInterfacesLib
        Rec::DaVinciKernelLib
        Rec::DaVinciTypesLib
        Rec::DecayTreeFitterLib
        Rec::TrackInterfacesLib
)
