/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "LHCbAlgs/Transformer.h"

/** @brief Copy an LHCb::Particle container and its associated vertices and P2PV relations.
 *
 * P2PV relations are determined on the fly using the tool defined by the
 * `RelatedPVFinder` property. Relations are found for all particles in the
 * decay trees referenced by the top-level particles, i.e. those in the
 * container being copied. This is useful if you want to ensure P2PV relations
 * exist for all referenced objects.
 *
 * Copied objects are not guaranteed to have the same container key as the
 * originals.
 */
class CopyFlavourTagsWithParticlePointers
    : public LHCb::Algorithm::Transformer<LHCb::FlavourTags( const LHCb::FlavourTags&, const LHCb::Particles& )> {
public:
  CopyFlavourTagsWithParticlePointers( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, {KeyValue{"InputFlavourTags", ""}, KeyValue{"NewBCandidates", ""}},
                     {KeyValue{"OutputFlavourTags", ""}} ) {}

  LHCb::FlavourTags operator()( const LHCb::FlavourTags& flavourTags,
                                const LHCb::Particles&   bCandidates ) const override {
    // auto bCandidates = getIfExists<LHCb::Particle::Container>( m_newBCandLocation );

    LHCb::FlavourTags flavourTags_out;
    for ( const auto* ft : flavourTags ) {
      // 1. Clone the LHCb::Particle
      auto* ft_clone = ft->clone();
      // 2. clear the references to tagging particles (as they would be pointing to the original ones, which are not
      // persisted...)
      for ( auto& tagger : ft_clone->taggers() ) tagger.clearTaggerParts();
      // 3. Set the pointer to the B candidate
      LHCb::Particle* new_bCand = bCandidates.object( ft_clone->taggedB()->key() );
      ft_clone->setTaggedB( new_bCand );

      flavourTags_out.add( ft_clone );
    }
    return flavourTags_out;
  }
};

DECLARE_COMPONENT( CopyFlavourTagsWithParticlePointers )
