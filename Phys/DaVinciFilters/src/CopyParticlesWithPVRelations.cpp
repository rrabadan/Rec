/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "GaudiKernel/IRegistry.h"
#include "Kernel/DefaultDVToolTypes.h"
#include "Kernel/IRelatedPVFinder.h"
#include "Kernel/Particle2Vertex.h"
#include "LHCbAlgs/Transformer.h"

namespace {

  class RegistryGuard : public IRegistry {
    std::string m_name;
    DataObject* m_object;

  public:
    RegistryGuard( std::string name, DataObject* obj ) : m_name{std::move( name )}, m_object{obj} {
      m_object->setRegistry( this );
    }
    ~RegistryGuard() { m_object->setRegistry( nullptr ); }
    unsigned long     addRef() override { return 0; }
    unsigned long     release() override { return 0; }
    const name_type&  name() const override { return m_name; }
    const id_type&    identifier() const override { return m_name; }
    IDataProviderSvc* dataSvc() const override { return nullptr; }
    DataObject*       object() const override { return m_object; }
    IOpaqueAddress*   address() const override { return nullptr; }
    void              setAddress( IOpaqueAddress* ) override {}
  };

  using out_t = std::tuple<LHCb::Particle::Container, LHCb::Vertex::Container, Particle2Vertex::Table>;
  using base_class =
      LHCb::Algorithm::MultiTransformer<out_t( const LHCb::Particle::Range&, const LHCb::RecVertex::Container& )>;
  using KeyValue = typename base_class::KeyValue;

  void relate_decay_tree( const LHCb::Particle* particle, const LHCb::RecVertex::Container& pvs,
                          const IRelatedPVFinder& relatedpvfinder, Particle2Vertex::Table& p2pv_relations ) {
    // Nothing to do if we've already related this Particle (and by extension
    // any children)
    if ( !p2pv_relations.relations( particle ).empty() ) { return; }

    // Find and add relations for this particle
    const auto* related_pv = relatedpvfinder.relatedPV( particle, pvs );
    if ( related_pv ) p2pv_relations.relate( particle, related_pv ).ignore();
    // Recurse down the decay tree to add relations for any children
    for ( const auto* child : particle->daughtersVector() ) {
      relate_decay_tree( child, pvs, relatedpvfinder, p2pv_relations );
    }
  }
} // namespace

/** @brief Copy an LHCb::Particle container and its associated vertices and P2PV relations.
 *
 * P2PV relations are determined on the fly using the tool defined by the
 * `RelatedPVFinder` property. Relations are found for all particles in the
 * decay trees referenced by the top-level particles, i.e. those in the
 * container being copied. This is useful if you want to ensure P2PV relations
 * exist for all referenced objects.
 *
 * Copied objects are not guaranteed to have the same container key as the
 * originals.
 */
struct CopyParticlesWithPVRelations : public base_class {
  CopyParticlesWithPVRelations( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer( name, pSvcLocator, {KeyValue{"InputParticles", ""}, KeyValue{"InputPrimaryVertices", ""}},
                          {KeyValue{"OutputParticles", ""}, KeyValue{"OutputParticleVertices", ""},
                           KeyValue{"OutputP2PVRelations", ""}} ) {}

  out_t operator()( const LHCb::Particle::Range&      particles,
                    const LHCb::RecVertex::Container& primary_vertices ) const override {
    LHCb::Particle::Container particles_out;
    LHCb::Vertex::Container   vertices_out;
    Particle2Vertex::Table    p2pv_relations_out;
    {
      // make sure particles_out has both a valid and correct registry, as the Relations code
      // will use it to sort the 'from' entries, i.e. the particles
      // NOTE: A better solution would be to split this algorithm in two algorithms:
      //        - one which copies the particles
      //        - one which creates the relations
      //       as there does not seem to be a reason why these two things have to be
      //       done in the same algorithm....
      auto _ = RegistryGuard{outputLocation<typename LHCb::Particle::Container>(), &particles_out};
      for ( const auto* p : particles ) {
        // 1. Clone the LHCb::Particle
        auto* p_clone = p->clone();
        particles_out.add( p_clone );
        // 2. Clone the LHCb::Vertex if we have a composite
        if ( p->endVertex() ) {
          const auto v_clone = p->endVertex()->clone();
          vertices_out.add( v_clone );
          p_clone->setEndVertex( v_clone );
        }
        // 3. Add the 'best' PVs for the particle and its children to the
        // relations table
        if ( !primary_vertices.empty() )
          relate_decay_tree( p_clone, primary_vertices, *m_relatedpvfinder, p2pv_relations_out );
      }
      p2pv_relations_out.i_sort();
      // end of scope for the registry guard -- which will reset the registry entry in particles_out...
    }
    return {std::move( particles_out ), std::move( vertices_out ), std::move( p2pv_relations_out )};
  }

private:
  // The default value is that of `DaVinci::DefaultTools::PVRelator` translated
  // into a string that can be imported from the `Configurables` module in
  // Python. This is necessary in order for code using this algorithm to be able
  // to configure (Gaudi won't find the configurable if it has the C++ name)
  ToolHandle<IRelatedPVFinder> m_relatedpvfinder{
      this, "RelatedPVFinder", "GenericParticle2PVRelator__p2PVWithIPChi2_OfflineDistanceCalculatorName_"};
};

DECLARE_COMPONENT( CopyParticlesWithPVRelations )
