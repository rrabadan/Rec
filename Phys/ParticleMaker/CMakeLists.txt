###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/ParticleMaker
------------------
#]=======================================================================]

gaudi_add_module(ParticleMaker
    SOURCES
        src/FunctionalDiElectronMaker.cpp
        src/FunctionalParticleMaker.cpp
        src/NeutralMakers.cpp
        src/Particle2State.cpp
        src/ParticleMakerForParticleFlow.cpp
        src/ParticleMassDTFMonitor.cpp
        src/ParticleMassMonitor.cpp
        src/ParticleWithBremMaker.cpp
        src/V0FromDstMaker.cpp
        src/ParticlesEmptyProducer.cpp
    LINK
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::CaloDetLib
        LHCb::CaloFutureInterfaces
        LHCb::CaloFutureUtils
        LHCb::DetDescLib
        LHCb::LHCbAlgsLib
        LHCb::LHCbMathLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        Rec::DaVinciInterfacesLib
        Rec::DaVinciKernelLib
        Rec::DecayTreeFitterLib
        Rec::TrackInterfacesLib
        Rec::FunctorCoreLib
)
