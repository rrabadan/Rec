/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "Event/TwoProngVertex.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleID.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbAlgs/Transformer.h"

namespace LHCb {

  /**
   * Algorithm to build particles from the Brunel V0 list.
   *
   * @author Wouter Hulsbergen
   * @date 2012-01-01
   */
  class V0FromDstMaker : public Algorithm::MultiTransformer<
                             std::tuple<Particle::Container, Vertex::Container, Particle::Container, Vertex::Container,
                                        Particle::Container, Vertex::Container, Particle::Container, Vertex::Container>(
                                 TwoProngVertex::Range const&, Particle::Range const&, Particle::Range const&,
                                 Particle::Range const&, Particle::Range const& )> {

    using OutputType = std::tuple<Particle::Container, Vertex::Container, Particle::Container, Vertex::Container,
                                  Particle::Container, Vertex::Container, Particle::Container, Vertex::Container>;

  public:
    V0FromDstMaker( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            {{"V0InputLocation", "Rec/Vertex/V0"},
                             {"LongPionLocation", "Phys/StdAllNoPIDsPions/Particles"},
                             {"DownstreamPionLocation", "Phys/StdNoPIDsDownPions/Particles"},
                             {"LongProtonLocation", "Phys/StdAllNoPIDsProtons/Particles"},
                             {"DownstreamProtonLocation", "Phys/StdNoPIDsDownProtons/Particles"}},
                            {{"KsLLOutputParticlesLocation", "Phys/StdKs2PiPiLL/Particles"},
                             {"KsLLOutputVerticesLocation", "Phys/StdKs2PiPiLL/Vertices"},
                             {"KsDDOutputParticlesLocation", "Phys/StdKs2PiPiDD/Particles"},
                             {"KsDDOutputVerticesLocation", "Phys/StdKs2PiPiDD/Vertices"},
                             {"LambdaLLOutputParticlesLocation", "Phys/StdLambda2PiPiLL/Particles"},
                             {"LambdaLLOutputVerticesLocation", "Phys/StdLambda2PiPiLL/Vertices"},
                             {"LambdaDDOutputParticlesLocation", "Phys/StdLambda2PiPiDD/Particles"},
                             {"LambdaDDOutputVerticesLocation", "Phys/StdLambda2PiPiDD/Vertices"}} ) {}
    StatusCode initialize() override;
    OutputType operator()( TwoProngVertex::Range const&, Particle::Range const&, Particle::Range const&,
                           Particle::Range const&, Particle::Range const& ) const override;

  private:
    Particle* build( const TwoProngVertex& vertex, const ParticleID& pid, const Particle::Range& posDaughters,
                     const Particle::Range& negDaughters ) const;
    Particle* build( const TwoProngVertex& vertex, const ParticleID& pid, const Particle& daughterA,
                     const Particle& daughterB ) const;
    ServiceHandle<IParticlePropertySvc> m_particlePropertySvc{this, "ParticlePropertySvc", "LHCb::ParticlePropertySvc",
                                                              "To get KS0 and Lambda0 particle properties"};
    ParticleProperty const*             m_ksProperty;
    ParticleProperty const*             m_lambdaProperty;
  };

  // Declaration of the Tool Factory
  DECLARE_COMPONENT_WITH_ID( V0FromDstMaker, "V0FromDstMaker" )

} // namespace LHCb

StatusCode LHCb::V0FromDstMaker::initialize() {
  return MultiTransformer::initialize().andThen( [&] {
    m_ksProperty     = m_particlePropertySvc->find( "KS0" );
    m_lambdaProperty = m_particlePropertySvc->find( "Lambda0" );
    return StatusCode::SUCCESS;
  } );
}

LHCb::Particle* LHCb::V0FromDstMaker::build( const LHCb::TwoProngVertex& vertex, const LHCb::ParticleID& pid,
                                             const LHCb::Particle& daughterA, const LHCb::Particle& daughterB ) const {
  Particle* v0 = new Particle( pid );
  v0->addToDaughters( &daughterA );
  v0->addToDaughters( &daughterB );
  v0->setReferencePoint( vertex.position() );
  double massA = daughterA.momentum().M();
  double massB = daughterB.momentum().M();

  v0->setMomentum( vertex.momentum( massA, massB ) );
  v0->setMeasuredMass( v0->momentum().M() );
  Gaudi::SymMatrix7x7 cov = vertex.covMatrix7x7( massA, massB );
  v0->setMomCovMatrix( cov.Sub<Gaudi::SymMatrix4x4>( 3, 3 ) );
  v0->setPosCovMatrix( cov.Sub<Gaudi::SymMatrix3x3>( 0, 0 ) );
  v0->setPosMomCovMatrix( cov.Sub<Gaudi::Matrix4x3>( 3, 0 ) );

  // now create the vertex object
  Vertex* particlevertex = new Vertex( vertex.position() );
  particlevertex->addToOutgoingParticles( &daughterA );
  particlevertex->addToOutgoingParticles( &daughterB );
  particlevertex->setChi2AndDoF( vertex.chi2(), vertex.nDoF() );
  particlevertex->setCovMatrix( v0->posCovMatrix() );

  v0->setEndVertex( particlevertex );
  return v0;
}

LHCb::Particle* LHCb::V0FromDstMaker::build( const LHCb::TwoProngVertex& vertex, const LHCb::ParticleID& pid,
                                             const LHCb::Particle::Range& particlesA,
                                             const LHCb::Particle::Range& particlesB ) const {
  // locate patricles associated to this V0 in input lists
  Particle*       v0( 0 );
  const Particle *dauA( 0 ), *dauB( 0 );
  const Track*    track;
  for ( const Particle* dau : particlesA ) {
    if ( dau->proto() && ( track = dau->proto()->track() ) && track == vertex.trackA() ) dauA = dau;
  }
  for ( const Particle* dau : particlesB ) {
    if ( dau->proto() && ( track = dau->proto()->track() ) && track == vertex.trackB() ) dauB = dau;
  }
  if ( dauA && dauB ) v0 = build( vertex, pid, *dauA, *dauB );
  // info() << "Did we find the pions? " << pionA << " " << pionB << endmsg ;
  return v0;
}

LHCb::V0FromDstMaker::OutputType LHCb::V0FromDstMaker::
                                 operator()( TwoProngVertex::Range const& vertices, LHCb::Particle::Range const& longpions,
            LHCb::Particle::Range const& downstreampions, LHCb::Particle::Range const& longprotons,
            LHCb::Particle::Range const& downstreamprotons ) const {
  V0FromDstMaker::OutputType output;
  auto& [ksllparticles, ksllvertices, ksddparticles, ksddvertices, lambdallparticles, lambdallvertices,
         lambdaddparticles,
         lambdaddvertices] = output;
  // horrible logic, but I don't know how to do this otherwise, at
  // least, not at this hour of the day
  for ( const TwoProngVertex* vertex : vertices ) {
    assert( vertex->trackA()->firstState().qOverP() > 0 );
    assert( vertex->trackB()->firstState().qOverP() < 0 );
    // just make sure they are unique
    std::set<ParticleID> pids( vertex->compatiblePIDs().begin(), vertex->compatiblePIDs().end() );
    for ( ParticleID pid : pids ) {
      if ( pid == m_ksProperty->pdgID() ) {
        Particle* p = build( *vertex, pid, longpions, longpions );
        if ( p ) {
          ksllparticles.insert( p );
          ksllvertices.insert( const_cast<Vertex*>( p->endVertex() ) );
        } else {
          p = build( *vertex, pid, downstreampions, downstreampions );
          if ( p ) {
            ksddparticles.insert( p );
            ksddvertices.insert( const_cast<Vertex*>( p->endVertex() ) );
          }
        }
      } else if ( std::abs( pid.pid() ) == std::abs( m_lambdaProperty->pdgID().pid() ) ) {
        const Particle::Range* rangeA = &longprotons;
        const Particle::Range* rangeB = &longpions;
        if ( pid.pid() < 0 ) std::swap( rangeA, rangeB );
        Particle* p = build( *vertex, pid, *rangeA, *rangeB );
        if ( p ) {
          lambdallparticles.insert( p );
          lambdallvertices.insert( const_cast<Vertex*>( p->endVertex() ) );
        } else {
          const Particle::Range* rangeA = &downstreamprotons;
          const Particle::Range* rangeB = &downstreampions;
          if ( pid.pid() < 0 ) std::swap( rangeA, rangeB );
          p = build( *vertex, pid, *rangeA, *rangeB );
          if ( p ) {
            lambdaddparticles.insert( p );
            lambdaddvertices.insert( const_cast<Vertex*>( p->endVertex() ) );
          }
        }
      }
    }
  }
  return output;
}
