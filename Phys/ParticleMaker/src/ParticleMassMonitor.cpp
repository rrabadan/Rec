/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"

/** @class ParticleMassMonitor
 *
 *  Trivial consumer to produce mass and momentum histograms out of LHCb::Particles,
 *  using the mean, sigma and bins specified by the user for the mass plots and a
 *  given range for the momentum plots. For the mass plots the range of the histogram
 *  is set to 5*sigma around the mean on each side.
 *
 *  Upon request, it also generates tuples with information about the particles.
 *
 */

// ParticleMassMonitor class definition
class ParticleMassMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::Particle::Range& ),
                                                             LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg>> {

public:
  ParticleMassMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, {KeyValue{"Particles", ""}} ) {}

  StatusCode initialize() override {
    return Consumer::initialize().andThen( [&] {
      /*Assign the names of the daughter particles of the decay. So far only considering D0 -> KPi, Z -> mumu and JPs ->
       mumu used for alignment mass constraints*/
      if ( m_mother == "D0" ) {
        m_daughter1 = "K";
        m_daughter2 = "Pi";
      } else {
        m_daughter1 = "Mu1";
        m_daughter2 = "Mu2";
      }

      // Mass histograms
      m_histograms_mass.emplace(
          std::piecewise_construct, std::forward_as_tuple( 0 ),
          std::forward_as_tuple( this, m_bins, m_mother, m_mass, m_sigma, m_minpt, m_maxpt, m_minp, m_maxp ) );

      // Momentum histograms
      std::vector<std::string> particles = {m_mother, m_daughter1, m_daughter2};
      for ( int i = 0; i < 3; i++ ) {
        m_histograms_momentum.emplace(
            std::piecewise_construct, std::forward_as_tuple( i ),
            std::forward_as_tuple( this, m_bins, particles[i], m_minpt, m_maxpt, m_minp, m_maxp ) );
      }
    } );
  }

  void operator()( LHCb::Particle::Range const& ) const override;

private:
  // Histogram settings. Default values are the standard ones for D0 -> KPi candidates
  Gaudi::Property<unsigned int> m_bins{this, "Bins", 100};
  Gaudi::Property<std::string>  m_mother{this, "ParticleType", "D0"};
  Gaudi::Property<float>        m_mass{this, "MassMean", 1864.84 * Gaudi::Units::MeV};
  Gaudi::Property<float>        m_sigma{this, "MassSigma", 90 * Gaudi::Units::MeV};
  Gaudi::Property<float>        m_minpt{this, "MinPt", 800 * Gaudi::Units::MeV};
  Gaudi::Property<float>        m_maxpt{this, "MaxPt", 6000 * Gaudi::Units::MeV};
  Gaudi::Property<float>        m_minp{this, "MinP", 2000 * Gaudi::Units::MeV};
  Gaudi::Property<float>        m_maxp{this, "MaxP", 150000 * Gaudi::Units::MeV};

  // Flag for tuple generation
  Gaudi::Property<bool> m_generateTuples{this, "GenerateTuples", false};
  // counters
  mutable Gaudi::Accumulators::Counter<> m_n1sigma{this, "Candidates in 1 sigma"};

  // Daughter particles
  std::string m_daughter1;
  std::string m_daughter2;

  // Definition of histograms
  struct HistoMomentum {
    mutable Gaudi::Accumulators::Histogram<1> CandidatePt;
    mutable Gaudi::Accumulators::Histogram<1> CandidateMom;

    HistoMomentum( const ParticleMassMonitor* owner, unsigned int const& bins, std::string const& name,
                   float const& min_pt, float const& max_pt, float const& min_p, float const& max_p )
        : CandidatePt{owner, name + "_Pt", name + "_Pt", {bins, min_pt, max_pt, "pt [MeV/c]"}}
        , CandidateMom{owner, name + "_P", name + "_P", {bins, min_p, max_p, "p [MeV/c]"}} {}
  };

  struct HistoMass {
    mutable Gaudi::Accumulators::Histogram<1> CandidateMass;
    mutable Gaudi::Accumulators::Histogram<2> MassvsPt;
    mutable Gaudi::Accumulators::Histogram<2> MassvsP;

    HistoMass( const ParticleMassMonitor* owner, unsigned int const& bins, std::string const& name, float const& mass,
               float const& sigma, float const& min_pt, float const& max_pt, float const& min_p, float const& max_p )
        : CandidateMass{owner,
                        name + "_mass",
                        name + " mass",
                        {bins, mass - 5 * sigma, mass + 5 * sigma, "m [MeV/c^{2}]"}}
        , MassvsPt{owner,
                   name + "_MassvsPt",
                   name + " mass vs pt",
                   {{bins, min_pt, max_pt, "pt [MeV/c]"}, {bins, mass - 5 * sigma, mass + 5 * sigma, "m [MeV/c^{2}]"}}}
        , MassvsP{owner,
                  name + "_MassvsP",
                  name + " mass vs p",
                  {{bins, min_p, max_p, "p [MeV/c]"}, {bins, mass - 5 * sigma, mass + 5 * sigma, "m [MeV/c^{2}]"}}} {}
  };

  // Histogram objects are stored inside of maps
  std::map<int, HistoMomentum> m_histograms_momentum;
  std::map<int, HistoMass>     m_histograms_mass;
};

DECLARE_COMPONENT( ParticleMassMonitor )

// Algorithm
void ParticleMassMonitor::operator()( LHCb::Particle::Range const& particles ) const {
  auto b_1sig = m_n1sigma.buffer();
  for ( const auto& p : particles ) {
    const double m         = p->momentum().M();
    const auto   daughters = p->daughtersVector();
    const double pt        = p->pt();
    const double mom       = p->p();

    // Mass plots
    auto& mass_histo = m_histograms_mass.at( 0 );

    // count candidates in 1 sigma
    if ( std::abs( m_mass - m ) / m_sigma < 1 ) b_1sig++;

    ++mass_histo.CandidateMass[m];
    ++mass_histo.MassvsPt[{pt, m}];
    ++mass_histo.MassvsP[{mom, m}];

    // Momentum plots
    int daughters_counter = 0;
    for ( int i = 0; i < 3; i++ ) {
      auto& momentum_histo = m_histograms_momentum.at( i );
      if ( i == 0 ) {
        ++momentum_histo.CandidatePt[pt];
        ++momentum_histo.CandidateMom[mom];
      } else {
        ++momentum_histo.CandidatePt[daughters[daughters_counter]->pt()];
        ++momentum_histo.CandidateMom[daughters[daughters_counter]->p()];
        daughters_counter++;
      }
    }

    // Code for tuple generation
    if ( m_generateTuples ) {
      // Extra variables to store in the tuples
      const auto slopes1 = daughters[0]->slopes();
      const auto slopes2 = daughters[1]->slopes();
      const auto id      = p->particleID().pid();
      const auto id1     = daughters[0]->particleID().pid();
      const auto id2     = daughters[1]->particleID().pid();

      // Creation of the tuple
      Tuple particleTuple = nTuple( "Particletuples", "" );
      particleTuple->column( m_mother + "_M", m ).ignore();
      particleTuple->column( m_mother + "_Pt", pt ).ignore();
      particleTuple->column( m_daughter1 + "_Pt", daughters[0]->pt() ).ignore();
      particleTuple->column( m_daughter2 + "_Pt", daughters[1]->pt() ).ignore();
      particleTuple->column( m_mother + "_ID", id ).ignore();
      particleTuple->column( m_daughter1 + "_ID", id1 ).ignore();
      particleTuple->column( m_daughter2 + "_ID", id2 ).ignore();
      particleTuple->column( m_mother + "_P", mom ).ignore();
      particleTuple->column( m_daughter1 + "_P", daughters[0]->p() ).ignore();
      particleTuple->column( m_daughter2 + "_P", daughters[1]->p() ).ignore();
      particleTuple->column( m_daughter1 + "_PxoverPz", slopes1.X() ).ignore();
      particleTuple->column( m_daughter2 + "_PxoverPz", slopes2.X() ).ignore();
      particleTuple->column( m_daughter1 + "_PyoverPz", slopes1.Y() ).ignore();
      particleTuple->column( m_daughter2 + "_PyoverPz", slopes2.Y() ).ignore();
      particleTuple->write().ignore();
    }
  }
}