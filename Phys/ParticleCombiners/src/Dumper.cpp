/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "SelAlgorithms/DumpContainer.h"

namespace Dumping::details {

  template <>
  struct Dump<std::vector<LHCb::Particle>> {
    using Signature                    = std::any( LHCb::Particle const& );
    constexpr static auto PropertyName = "Branches";
  };
} // namespace Dumping::details

using LegacyParticlePropertyDumper = Dumping::DumpContainer<std::vector<LHCb::Particle>, true>;
DECLARE_COMPONENT_WITH_ID( Dumping::DumpContainer<LHCb::Event::Composites>, "CompositePropertyDumper" )
DECLARE_COMPONENT_WITH_ID( Dumping::DumpContainer<LHCb::Event::ChargedBasics>, "BasicsPropertyDumper" )
DECLARE_COMPONENT_WITH_ID( LegacyParticlePropertyDumper, "LegacyParticlePropertyDumper" )
