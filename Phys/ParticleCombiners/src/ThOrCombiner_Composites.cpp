/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CombKernel/ThOrCombiner.h"

#include "Event/Particle_v2.h"

using ThOrCombiner__CompositesChargedBasics = ThOr::CombinerBest<LHCb::Event::Composites, LHCb::Event::ChargedBasics>;
using ThOrCombiner__Composites2ChargedBasics =
    ThOr::CombinerBest<LHCb::Event::Composites, LHCb::Event::ChargedBasics, LHCb::Event::ChargedBasics>;
using ThOrCombiner__Composites3ChargedBasics =
    ThOr::CombinerBest<LHCb::Event::Composites, LHCb::Event::ChargedBasics, LHCb::Event::ChargedBasics,
                       LHCb::Event::ChargedBasics>;

DECLARE_COMPONENT_WITH_ID( ThOrCombiner__CompositesChargedBasics, "ThOrCombiner__CompositesChargedBasics" )
DECLARE_COMPONENT_WITH_ID( ThOrCombiner__Composites2ChargedBasics, "ThOrCombiner__Composites2ChargedBasics" )
DECLARE_COMPONENT_WITH_ID( ThOrCombiner__Composites3ChargedBasics, "ThOrCombiner__Composites3ChargedBasics" )
