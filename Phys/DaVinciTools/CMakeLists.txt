###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DaVinciTools
-----------------
#]=======================================================================]

gaudi_add_module(DaVinciTools
    SOURCES
        src/ParticleDescendants.cpp
        src/PrintDecayTree.cpp
        src/SubstitutePIDAlg.cpp
        src/ParticleToSubcombinationsAlg.cpp
    LINK
        Gaudi::GaudiAlgLib
        Rec::LoKiPhysLib
        LHCb::PhysEvent
        Rec::DaVinciInterfacesLib
)

gaudi_install(PYTHON)