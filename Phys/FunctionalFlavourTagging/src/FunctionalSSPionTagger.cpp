/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "FunctionalSSPionTagger.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FunctionalSSPionTagger )

LHCb::FlavourTags FunctionalSSPionTagger::operator()( const LHCb::Particles&                         bCandidates,
                                                      const LHCb::Particle::Range&                   taggingPions,
                                                      const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                                      const DetectorElement& lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();

  // keyed container of FlavourTag objects, one FlavourTag per B candidate
  LHCb::FlavourTags flavourTags;

  m_BCount += bCandidates.size();
  m_pionCount += taggingPions.size();

  // is this still needed??? could be removed
  if ( bCandidates.size() == 0 || taggingPions.size() == 0 || primaryVertices.size() == 0 ) {
    for ( const auto* bCand : bCandidates ) {
      LHCb::Tagger tagger;
      tagger.setType( taggerType() );
      LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
      flavourTag->addTagger( tagger );
      flavourTag->setTaggedB( bCand );
      flavourTag->setDecision( LHCb::FlavourTag::none );
      flavourTags.insert( flavourTag );
    }
    return flavourTags;
  }

  // loop over B candidates
  for ( const auto* bCand : bCandidates ) {
    auto tagResult = performTagging( *bCand, taggingPions, primaryVertices, geometry );

    if ( !tagResult.has_value() ) {
      // no appropriate tagging candidate found, fill with "empty" FlavourTag object
      LHCb::Tagger tagger;
      tagger.setType( taggerType() );
      LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
      flavourTag->addTagger( tagger );
      flavourTag->setTaggedB( bCand );
      flavourTag->setDecision( LHCb::FlavourTag::none );
      flavourTags.insert( flavourTag );
      continue;
    }

    LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
    flavourTag->setTaggedB( bCand );
    flavourTag->setDecision( LHCb::FlavourTag::none );
    flavourTag->setTaggers( std::vector<LHCb::Tagger>{tagResult.value()} );
    flavourTags.insert( flavourTag );

    m_FTCount += 1;
  }

  return flavourTags;
}

std::optional<LHCb::Tagger>
FunctionalSSPionTagger::performTagging( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingPions,
                                        const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                        const IGeometryInfo&                           geometry ) const {
  const LHCb::Particle* bestTagCand = nullptr;

  double bestBDT = -99.0;

  Gaudi::LorentzVector b_mom = bCand.momentum();

  // find PV that best matches the B candidate
  const LHCb::VertexBase* bestPV = LHCb::bestPV( primaryVertices, bCand );
  // find PV that best matches the B candidate and unbias it
  const LHCb::VertexBase refittedPV = TaggingHelper::bestPVUnbiased( primaryVertices, bCand );
  ;

  auto pileups = TaggingHelper::pileUpVertices( primaryVertices, *bestPV );

  // loop over tagging particles
  for ( const auto* tagCand : taggingPions ) {
    Gaudi::LorentzVector tag_mom = tagCand->momentum();

    // if(std::abs(tag_mom.P()-1571.)>1) continue;
    if ( !m_taggingHelperTool->passesCommonPreSelection( *tagCand, bCand, pileups, geometry ) ) continue;

    // exclude tracks too far way from the signal
    double deltaPhi = std::fabs( TaggingHelper::dPhi( b_mom.phi(), tag_mom.phi() ) );
    if ( deltaPhi > m_maxDistPhi ) continue;

    const double deltaEta = std::fabs( b_mom.Eta() - tag_mom.Eta() );
    if ( deltaEta > m_maxDeltaEta ) continue;

    // calculate and cut on dQ
    const double dQ = ( b_mom + tag_mom ).M() - b_mom.M(); // NB: missing mp term as in Davide's computation (???)
    if ( dQ > m_maxDQB0Pion ) continue;

    // cut on the transverse momentum of the B - tagging particle combination
    const double btag_pT = ( b_mom + tag_mom ).Pt();
    if ( btag_pT < m_minPTB0Pion ) continue;

    // IP significance cut wrt bestPV
    std::optional<std::pair<double, double>> refittedPVIP =
        m_taggingHelperTool->calcIPWithChi2( *tagCand, refittedPV, geometry );

    if ( !refittedPVIP.has_value() ) continue;
    if ( std::sqrt( refittedPVIP.value().second ) > m_maxIpSigTagBestPV ) continue;

    // fit BCand and pionCand to vertex and cut on chi2/ndof
    std::optional<LHCb::Vertex> tagBVtx = m_taggingHelperTool->fitVertex( *tagCand, bCand, geometry );
    if ( !tagBVtx.has_value() ) continue;
    if ( tagBVtx.value().chi2() / tagBVtx.value().nDoF() > m_minChi2B0PionVertex ) continue;

    // inputs for the BDT
    const double               dR           = std::sqrt( deltaEta * deltaEta + deltaPhi * deltaPhi );
    const LHCb::ProtoParticle* tagProto     = tagCand->proto();
    const double               tagPIDk      = tagProto->info( LHCb::ProtoParticle::additionalInfo::CombDLLk, -1000.0 );
    const LHCb::Track*         tagTrack     = tagProto->track();
    const double               tagTrackChi2 = tagTrack->chi2PerDoF();
    const double               tagGhostProb = tagTrack->ghostProbability();

    // MVA !!
    std::vector<double> inputVals( 13 );
    inputVals[0]  = log( tag_mom.P() / Gaudi::Units::GeV );
    inputVals[1]  = log( tag_mom.Pt() / Gaudi::Units::GeV );
    inputVals[2]  = log( std::sqrt( refittedPVIP.value().second ) );
    inputVals[3]  = tagGhostProb;
    inputVals[4]  = log( deltaPhi );
    inputVals[5]  = dR;
    inputVals[6]  = log( deltaEta );
    inputVals[7]  = dQ / Gaudi::Units::GeV;
    inputVals[8]  = log( b_mom.Pt() / Gaudi::Units::GeV );
    inputVals[9]  = log( ( b_mom + tag_mom ).Pt() / Gaudi::Units::GeV );
    inputVals[10] = tagPIDk;
    inputVals[11] = tagTrackChi2;
    inputVals[12] = refittedPV.nDoF();

    const double BDT = m_classifier->getClassifierValue( inputVals );

    if ( BDT < bestBDT ) continue;

    bestTagCand = tagCand;
    bestBDT     = BDT;
  }

  double pn =
      1. - ( m_polP0 + m_polP1 * bestBDT + m_polP2 * bestBDT * bestBDT + m_polP3 * bestBDT * bestBDT * bestBDT );

  if ( pn < m_minPionProb || pn > 1 ) return std::nullopt;

  int tagdecision = bestTagCand->charge() > 0 ? 1 : -1;
  if ( bCand.particleID().hasUp() ) tagdecision = -tagdecision;

  LHCb::Tagger tagger;
  tagger.setDecision( tagdecision );
  tagger.setOmega( 1 - pn );
  tagger.setType( taggerType() );
  tagger.addToTaggerParts( bestTagCand );
  tagger.setCharge( bestTagCand->charge() );
  tagger.setMvaValue( bestBDT );

  return tagger;
}
