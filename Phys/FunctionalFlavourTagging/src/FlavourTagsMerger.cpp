/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "FlavourTagsMerger.h"

#include <typeinfo>

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FlavourTagsMerger )

LHCb::FlavourTags FlavourTagsMerger::
                  operator()( const Gaudi::Functional::details::vector_of_const_<LHCb::FlavourTags>& flavourTagsIterable ) const {
  m_FTCount += flavourTagsIterable.size();

  LHCb::FlavourTags flavourTags;

  std::vector<const LHCb::Particle*> uniqueB;

  for ( const auto& inFlavourTags : flavourTagsIterable ) {
    for ( const auto* flavourTag : inFlavourTags ) {
      const auto* taggedB = flavourTag->taggedB();

      if ( not( any_of( uniqueB.begin(), uniqueB.end(),
                        [this, taggedB]( const LHCb::Particle* bCand ) { return isSameB( bCand, taggedB ); } ) ) ) {
        uniqueB.push_back( taggedB );
      }
    }
  }

  m_BCount += uniqueB.size();

  for ( const auto* bCand : uniqueB ) {
    LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
    flavourTag->setTaggedB( bCand );
    flavourTag->setDecision( LHCb::FlavourTag::none );

    for ( const auto& inFlavourTags : flavourTagsIterable ) {
      for ( const auto* inFlavourTag : inFlavourTags ) {
        const auto* taggedB = inFlavourTag->taggedB();
        if ( isSameB( bCand, taggedB ) ) {
          for ( const auto& tagger : inFlavourTag->taggers() ) { flavourTag->addTagger( tagger ); }
        }
      }
    }

    flavourTags.insert( flavourTag );
  }

  return flavourTags;
}

bool FlavourTagsMerger::isSameB( const LHCb::Particle* bCand, const LHCb::Particle* taggedB ) const {
  return ( bCand->momentum() == taggedB->momentum() && bCand->referencePoint() == taggedB->referencePoint() );
}
