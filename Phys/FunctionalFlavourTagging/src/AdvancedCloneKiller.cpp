/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "AdvancedCloneKiller.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( AdvancedCloneKiller )

LHCb::Particle::Selection AdvancedCloneKiller::
                          operator()( const Gaudi::Functional::vector_of_const_<LHCb::Particle::Range>& inputParticles ) const {
  LHCb::Particle::Selection allParticles;

  for ( auto const& particleList : inputParticles ) { allParticles.insert( particleList.begin(), particleList.end() ); }
  m_inCount += allParticles.size();

  // sort them by quality
  auto qualitySort = []( const LHCb::Particle* p1, const LHCb::Particle* p2 ) {
    const LHCb::Track& t1 = *p1->proto()->track();
    const LHCb::Track& t2 = *p2->proto()->track();
    // prefer tracks which have more subdetectors in
    // where TT counts as half a subdetector
    const unsigned nSub1 = ( t1.hasVelo() ? 2 : 0 ) + ( t1.hasTT() ? 1 : 0 ) + ( t1.hasT() ? 2 : 0 );
    const unsigned nSub2 = ( t2.hasVelo() ? 2 : 0 ) + ( t2.hasTT() ? 1 : 0 ) + ( t2.hasT() ? 2 : 0 );
    if ( nSub1 != nSub2 ) return nSub1 > nSub2;

    // if available, prefer lower ghost probability
    const double ghProb1 = t1.ghostProbability();
    const double ghProb2 = t2.ghostProbability();
    if ( -0. <= ghProb1 && ghProb1 <= 1. && -0. <= ghProb2 && ghProb2 <= 1. && ghProb1 != ghProb2 )
      return ghProb1 < ghProb2;

    // prefer longer tracks
    if ( t1.nLHCbIDs() != t2.nLHCbIDs() ) return t1.nLHCbIDs() > t2.nLHCbIDs();

    // for same length tracks, have chi^2/ndf decide
    const double chi1 = t1.chi2() / double( t1.nDoF() );
    const double chi2 = t2.chi2() / double( t2.nDoF() );
    if ( chi1 != chi2 ) return ( chi1 < chi2 );

    // fall back on a pT comparison (higher is better) as last resort
    return p1->pt() > p2->pt();
  };

  std::stable_sort( allParticles.begin(), allParticles.end(), qualitySort );

  auto isClone = [&]( const LHCb::Particle* p, const LHCb::Particle::Selection& sel ) {
    const auto firstClone = std::find_if( sel.begin(), sel.end(), [&]( const LHCb::Particle* p2 ) {
      auto cc = cloneCategory( *p, *p2 );
      return cc;
    } );
    if ( firstClone != sel.end() ) return true;
    return false;
  };

  LHCb::Particle::Selection outputParticles;
  std::for_each( allParticles.begin(), allParticles.end(), [&]( const LHCb::Particle* p ) {
    if ( !isClone( p, outputParticles ) ) outputParticles.insert( p );
  } );

  m_outCount += outputParticles.size();
  return outputParticles;
}

TaggingHelper::CloneStatus AdvancedCloneKiller::cloneCategory( const LHCb::Particle& p1,
                                                               const LHCb::Particle& p2 ) const {

  if ( TaggingHelper::areHitFractionClones( p1, p2, m_maxSharedHitFraction ) ) return TaggingHelper::CloneHitContent;
  if ( TaggingHelper::areMomentumParameterClones( p1, p2 ) ) return TaggingHelper::CloneTrackParams;
  if ( TaggingHelper::areSlopeClones( p1, p2 ) ) return TaggingHelper::MattTxTy;
  if ( TaggingHelper::areSlopeChargeClones( p1, p2 ) ) return TaggingHelper::MattTxTyQp;
  if ( TaggingHelper::areConvertedGamma( p1, p2 ) ) return TaggingHelper::ConvertedGamma;

  // as far as we can tell, the two particles are different
  return TaggingHelper::DifferentParticles;
}

/* bool AdvancedCloneKiller::areHitFractionClones( const LHCb::Particle& p1, const LHCb::Particle& p2 ) const {
  const LHCb::Track* t1 = p1.proto()->track();
  const LHCb::Track* t2 = p2.proto()->track();

  int nCommonHits = t1->nCommonLhcbIDs(*t2);
  int norm = std::min(t1->nLHCbIDs(),  t2->nLHCbIDs());
  const double frac = double( nCommonHits ) / double( norm );
  if(frac > m_maxSharedHitFraction)
    return true;

  return false;
}


bool AdvancedCloneKiller::areMomentumParameterClones(const LHCb::Particle& p1, const LHCb::Particle& p2) const{
  const auto& mom1 = p1.momentum();
  const auto& mom2 = p2.momentum();

    const double deta = mom1.eta() - mom2.eta();
    const double dphi = TaggingHelper::dphi( mom1.x(), mom1.y(), mom2.x(), mom2.y() );

    const Gaudi::SymMatrix4x4& C1 = p1.momCovMatrix();
    const Gaudi::SymMatrix4x4& C2 = p2.momCovMatrix();
    double err1 = std::sqrt(
              ( mom1.x() * mom1.x() * C1( 0, 0 ) + mom1.y() * mom1.y() * C1( 1, 1 ) + mom1.z() * mom1.z() * C1( 2, 2 ) )
+
              2. * ( mom1.x() * mom1.y() * C1( 0, 1 ) + mom1.x() * mom1.z() * C1( 0, 2 ) + mom1.y() * mom1.z() * C1( 1,
2 ) ) ) / mom1.P(); double err2 =std::sqrt( ( mom2.x() * mom2.x() * C2( 0, 0 ) + mom2.y() * mom2.y() * C2( 1, 1 ) +
mom2.z() * mom2.z() * C2( 2, 2 ) ) +
              2. * ( mom2.x() * mom2.y() * C2( 0, 1 ) + mom2.x() * mom2.z() * C2( 0, 2 ) + mom2.y() * mom2.z() * C2( 1,
2 ) ) ) / mom2.P();

    // check if the particles are clones based on their momenta and angles
    if ( p1.charge() && p2.charge() ) {
      const double dqp = double( p1.charge() ) / mom1.P() - double( p2.charge() ) / mom2.P();
      const double dqperr =
          std::sqrt( ( err1 / mom1.P2() ) * ( err1 / mom1.P2() ) + ( err2 / mom2.P2() ) * ( err2 / mom2.P2() ) );
      if ( std::abs( dqp / dqperr ) < 3. && std::abs( deta ) < 0.1 && std::abs( dphi ) < 0.1 )
        return true;
    } else {
      const double dp    = mom1.P() - mom2.P();
      const double dperr = std::sqrt( err1 * err1 + err2 * err2 );
      if ( std::abs( dp / dperr ) < 3. && std::abs( deta ) < 0.1 && std::abs( dphi ) < 0.1 )
        return true;
    }
    return false;
}

bool AdvancedCloneKiller::areSlopeClones(const LHCb::Particle& p1, const LHCb::Particle& p2) const{
  const auto& mom1 = p1.momentum();
  const auto& mom2 = p2.momentum();

  const double dtx = mom1.x() / mom1.z() - mom2.x() / mom2.z();
  const double dty = mom1.y() / mom1.z() - mom2.y() / mom2.z();

  if ( std::abs( dtx ) < 0.0004 && std::abs( dty ) < 0.0002 ) return true;

  return false;
}

bool AdvancedCloneKiller::areSlopeChargeClones(const LHCb::Particle& p1, const LHCb::Particle& p2) const{
  const auto& mom1 = p1.momentum();
  const auto& mom2 = p2.momentum();

  const double dtx = mom1.x() / mom1.z() - mom2.x() / mom2.z();
  const double dty = mom1.y() / mom1.z() - mom2.y() / mom2.z();

  if ( p1.charge() && p2.charge() ) {
    const double dqp = double( p1.charge() ) / mom1.P() - double( p2.charge() ) / mom2.P();

    if ( std::abs( dtx ) < 0.005 && std::abs( dty ) < 0.005 && std::abs( dqp ) < 1e-6 ) return true;
    }
    return false;
}

bool AdvancedCloneKiller::areConvertedGamma(const LHCb::Particle& p1, const LHCb::Particle& p2) const{
  if ( p1.charge() * p2.charge() < 0 ) {
      const double mmu = 105.6583715, me = 0.510998928;
      Gaudi::LorentzVector mom1 = p1.momentum();
      Gaudi::LorentzVector mom2 = p2.momentum();
      mom1.SetE( std::sqrt( me * me + mom1.P2() ) );
      mom2.SetE( std::sqrt( me * me + mom2.P2() ) );
      // use 0.25 * mmu as cutoff for low mass - halfway between 0
      // and half a muon mass seems fair when you keep mass resolution
      // and numerical precision in mind...
      const bool isLowMassSq = ( std::abs( ( mom1 + mom2 ).M2() ) < .0625 * mmu * mmu );
      mom1.SetE( std::sqrt( mmu * mmu + mom1.P2() ) );
      mom2.SetE( std::sqrt( mmu * mmu + mom2.P2() ) );
      if ( isLowMassSq || ( std::abs( ( mom1 + mom2 ).M2() ) < .0625 * mmu * mmu ) ) {
        // the system has a small invariant mass - typically, the two
        // tracks will also have a small angle (after all, it's a
        // gamma on a fixed target... ;), so cut on the cosine of that
        // angle
        const double p1p2 = mom1.x() * mom2.x() + mom1.y() * mom2.y() + mom1.z() * mom2.z();
        if ( p1p2 / mom1.P() / mom2.P() > 0.999999 ) return true;
      }
    }

    return false;
} */
