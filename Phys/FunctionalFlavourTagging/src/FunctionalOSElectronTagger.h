/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "GaudiAlg/GaudiAlg.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"

// from Event
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/PrimaryVertices.h"
#include "Event/Tagger.h"

// from FunctionalFlavourTagging
#include "../Classification/OSElectron/OSElectron_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0.h"
#include "Utils/ITaggingHelperTool.h"
#include "Utils/TaggingHelper.h"
#include "Utils/TaggingHelperTool.h"

class FunctionalOSElectronTagger
    : public Gaudi::Functional::Transformer<LHCb::FlavourTags( const LHCb::Particles&, const LHCb::Particle::Range&,
                                                               const LHCb::Event::PV::PrimaryVertexContainer&,
                                                               const DetectorElement& ),
                                            LHCb::DetDesc::usesConditions<DetectorElement>> {
public:
  /// Standard constructor
  FunctionalOSElectronTagger( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"BCandidates", ""}, KeyValue{"TaggingElectrons", ""}, KeyValue{"PrimaryVertices", ""},
                      KeyValue{"StandardGeometry", LHCb::standard_geometry_top}},
                     KeyValue{"OutputFlavourTags", ""} ) {}

  LHCb::FlavourTags operator()( const LHCb::Particles& bCandidates, const LHCb::Particle::Range& taggingElectrons,
                                const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                const DetectorElement& ) const override;

  std::optional<LHCb::Tagger> performTagging( const LHCb::Particle&                          bCand,
                                              const LHCb::Particle::Range&                   taggingElectrons,
                                              const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                              const IGeometryInfo&                           geometry ) const;

  LHCb::Tagger::TaggerType taggerType() const { return LHCb::Tagger::TaggerType::OSElectronLatest; }

private:
  // cut values for OSElectron selection
  Gaudi::Property<double> m_minDistPhi{this, "MinDistPhi", 0.016728744332624838,
                                       "Tagging particle requirement: Minimum phi distance to B daughters"};
  Gaudi::Property<double> m_minIpSigTagBestPV{this, "MinIpSigTagBestPV", 0.0419896167433678,
                                              "Tagging particle requirement: Minimum IP significance wrt to best PV"};
  Gaudi::Property<double> m_minIpSigTagPileUpVertices{
      this, "MinIpSigPileUp", 9.334968382765219,
      "Tagging particle requirement: Minimum IP significance wrt to all pile-up PV"};

  Gaudi::Property<double> m_maxEOverP{this, "MaxEOverP", 2, "Tagging particle requirement: Maximum electron e/p."};
  Gaudi::Property<double> m_minEOverP{this, "MinEOverP", 0.85, "Tagging particle requirement: Minimum electron e/p."};
  Gaudi::Property<double> m_maxVeloCharge{this, "MaxVeloCharge", 1.4,
                                          "Tagging particle requirement: Maximum velo charge."};
  Gaudi::Property<double> m_minVeloCharge{this, "MinVeloCharge", 0.,
                                          "Tagging particle requirement: Minimum velo charge."};

  Gaudi::Property<double> m_maxProbNNmu{this, "MaxProbNNmu", 0.15808731889714595,
                                        "Tagging particle requirement: Maximum ProbNNmu."};
  Gaudi::Property<double> m_maxProbNNpi{this, "MaxProbNNpi", 0.9833355298219992,
                                        "Tagging particle requirement: Maximum ProbNNpi."};
  Gaudi::Property<double> m_maxProbNNp{this, "MaxProbNNp", 0.27070346277844964,
                                       "Tagging particle requirement: Maximum ProbNNp."};
  Gaudi::Property<double> m_maxProbNNk{this, "MaxProbNNk", 0.6949348710000995,
                                       "Tagging particle requirement: Maximum ProbNNk."};
  Gaudi::Property<double> m_minProbNNe{this, "MinProbNNe", 0.24262849376048545,
                                       "Tagging particle requirement: Minimum ProbNNe."};
  // values for calibration purposes
  // Gaudi::Property<double> m_averageEta{this, "AverageEta", 0.3949};
  // Gaudi::Property<double> m_minPosDecision{this, "MinPositiveTaggingDecision", 0.5,
  //                                          "Minimum value of the Eta for a tagging decision of +1"};
  // Gaudi::Property<double> m_maxNegDecision{this, "MaxNegativeTaggingDecision", 0.5,
  //                                          "Maximum value of the Eta for a tagging decision of -1"};

  mutable Gaudi::Accumulators::SummingCounter<> m_BCount{this, "#BCandidates"};
  mutable Gaudi::Accumulators::SummingCounter<> m_ElectronCount{this, "#taggingElectrons"};
  mutable Gaudi::Accumulators::SummingCounter<> m_FTCount{this, "#goodFlavourTags"};

  ToolHandle<ITaggingHelperTool>   m_taggingHelperTool{this, "TaggingHelper", "TaggingHelperTool"};
  ToolHandle<IParticleDescendants> m_particleDescendantTool{this, "ParticleDescendantTool", "ParticleDescendants"};

  std::unique_ptr<ITaggingClassifier> m_classifier = std::make_unique<OSElectron_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0>();

  // double m_polP0 = 0.025841;
  // double m_polP1 = -0.21766;
};
