/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "FunctionalSSProtonTagger.h"

#include <typeinfo>

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FunctionalSSProtonTagger )

LHCb::FlavourTags FunctionalSSProtonTagger::operator()( const LHCb::Particles&                         bCandidates,
                                                        const LHCb::Particle::Range&                   taggingProtons,
                                                        const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                                        const DetectorElement& lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();
  // keyed container of FlavourTag objects, one FlavourTag per B candidate
  LHCb::FlavourTags flavourTags;

  m_BCount += bCandidates.size();
  m_protonCount += taggingProtons.size();
  // always() << bCandidates.size() << "  " << taggingProtons.size() << "  " << primaryVertices.size() << endmsg;

  // is this still needed??? could be removed
  if ( bCandidates.size() == 0 || taggingProtons.size() == 0 || primaryVertices.size() == 0 ) {
    for ( const auto* bCand : bCandidates ) {
      m_BCount2 += 1;
      LHCb::Tagger tagger;
      tagger.setType( taggerType() );
      LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
      flavourTag->addTagger( tagger );
      flavourTag->setTaggedB( bCand );
      flavourTag->setDecision( LHCb::FlavourTag::none );
      flavourTags.insert( flavourTag );
      m_allFTCount += 1;
      m_0Count += 1;
    }
    return flavourTags;
  }

  // loop over B candidates
  for ( const auto* bCand : bCandidates ) {
    m_BCount2 += 1;

    auto tagResult = performTagging( *bCand, taggingProtons, primaryVertices, geometry );
    if ( !tagResult.has_value() ) {
      // no appropriate tagging candidate found, fill with "empty" FlavourTag object
      LHCb::Tagger tagger;
      tagger.setType( taggerType() );
      LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
      flavourTag->addTagger( tagger );
      flavourTag->setTaggedB( bCand );
      flavourTag->setDecision( LHCb::FlavourTag::none );
      flavourTags.insert( flavourTag );
      m_allFTCount += 1;
      m_invCount += 1;
      continue;
    }
    LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
    flavourTag->addTagger( tagResult.value() );
    flavourTag->setTaggedB( bCand );
    flavourTag->setDecision( LHCb::FlavourTag::none );
    flavourTags.insert( flavourTag );
    m_allFTCount += 1;
    m_FTCount += 1;
    m_Count += 1;
  }
  return flavourTags;
}

/*std::optional<std::pair<const LHCb::Particle*, double>>
FunctionalSSProtonTagger::searchForTaggingParticle( const LHCb::Particle&        bCand,*/
std::optional<LHCb::Tagger>
FunctionalSSProtonTagger::performTagging( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingProtons,
                                          const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                          const IGeometryInfo&                           geometry ) const {
  const LHCb::Particle* bestTagCand = nullptr;

  double bestBDT = -99.0;

  Gaudi::LorentzVector b_mom = bCand.momentum();

  // find PV that best fits the B candidate
  const LHCb::VertexBase* bestPV = LHCb::bestPV( primaryVertices, bCand );
  if ( bestPV == nullptr ) return std::nullopt;

  /*std::optional<const LHCb::VertexBase> refittedPV = m_taggingHelperTool->refitPVWithoutB(*bestPV, bCand, geometry);
  if(! refittedPV.has_value())
    return std::nullopt;
  */
  // PV refitting isn't possible in Moore (right now) since the PV doesn't save the particles it was made of
  std::optional<const LHCb::VertexBase> refittedPV = *bestPV;

  auto pileups = TaggingHelper::pileUpVertices( primaryVertices, *bestPV );

  // loop over tagging particles
  for ( const auto* tagCand : taggingProtons ) {
    Gaudi::LorentzVector tag_mom = tagCand->momentum();

    // preselection common to all taggers
    if ( !m_taggingHelperTool->passesCommonPreSelection( *tagCand, bCand, pileups, geometry ) ) continue;

    // selection specific to the ssproton tagger
    // exclude tracks too far way from the signal
    double deltaPhi = TaggingHelper::dPhi( b_mom.phi(), tag_mom.phi() );
    deltaPhi        = std::fabs( deltaPhi );
    if ( deltaPhi > m_maxDistPhi ) continue;

    const double deltaEta = std::fabs( b_mom.Eta() - tag_mom.Eta() );
    if ( deltaEta > m_maxDeltaEta ) continue;

    // calculate and cut on dQ
    const double dQ = ( b_mom + tag_mom ).M() - b_mom.M() - tag_mom.M();
    if ( dQ > m_maxDQB0Proton ) continue;

    // cut on the transverse momentum of the B - tagging particle combination
    const double btag_pT = ( b_mom + tag_mom ).Pt();
    if ( btag_pT < m_minPTB0Proton ) continue;

    // IP significance cut wrt bestPV
    std::optional<std::pair<double, double>> refittedPVIP =
        m_taggingHelperTool->calcIPWithErr( *tagCand, refittedPV.value(), geometry );
    if ( !refittedPVIP.has_value() ) continue;
    if ( refittedPVIP.value().second > m_maxIpSigTagBestPV ) continue;

    // fit BCand and tagging particle to vertex and cut on chi2/ndof
    std::optional<LHCb::Vertex> tagBVtx = m_taggingHelperTool->fitVertex( *tagCand, bCand, geometry );
    if ( !tagBVtx.has_value() ) continue;
    if ( tagBVtx.value().chi2() / tagBVtx.value().nDoF() > m_maxChi2B0ProtonVertex ) continue;

    // inputs for the BDT
    const double               dR       = std::sqrt( deltaEta * deltaEta + deltaPhi * deltaPhi );
    const LHCb::ProtoParticle* tagProto = tagCand->proto();
    const double               tagPIDp  = tagProto->info( LHCb::ProtoParticle::additionalInfo::CombDLLp, -1000.0 );

    // MVA
    std::vector<double> inputVals( 9 );
    inputVals[0] = log( tag_mom.P() / Gaudi::Units::GeV );
    inputVals[1] = log( tag_mom.Pt() / Gaudi::Units::GeV );
    inputVals[2] = log( refittedPVIP.value().second );
    inputVals[3] = dR;
    inputVals[4] = log( deltaEta );
    inputVals[5] = dQ / Gaudi::Units::GeV;
    inputVals[6] = log( ( b_mom + tag_mom ).Pt() / Gaudi::Units::GeV );
    inputVals[7] = log( refittedPV->nDoF() );
    inputVals[8] = log( tagPIDp );

    // for ( unsigned int i = 0; i < inputVals.size(); ++i ) always() << inputVals.at( i ) << endmsg;

    const double BDT = m_classifier->getClassifierValue( inputVals );

    // always() << "BDT " << BDT << endmsg;

    if ( BDT < bestBDT ) continue;

    bestTagCand = tagCand;
    bestBDT     = BDT;
  }

  // if ( bestTagCand ) return std::make_pair( bestTagCand, bestBDT);
  // return std::nullopt;

  double pn =
      1. - ( m_polP0 + m_polP1 * bestBDT + m_polP2 * std::pow( bestBDT, 2 ) + m_polP3 * std::pow( bestBDT, 3 ) );

  if ( pn < m_minProtonProb || pn > 1 ) return std::nullopt;

  int tagdecision = bestTagCand->charge() > 0 ? 1 : -1;
  if ( bCand.particleID().hasUp() ) tagdecision = -tagdecision;

  LHCb::Tagger tagger;
  tagger.setDecision( tagdecision );
  tagger.setOmega( 1 - pn );
  tagger.setType( taggerType() );
  tagger.addToTaggerParts( bestTagCand );
  tagger.setCharge( bestTagCand->charge() );
  tagger.setMvaValue( bestBDT );

  return tagger;
}

// bool FunctinalSSProtonTagger::passesSSProtonSelection(){
//}