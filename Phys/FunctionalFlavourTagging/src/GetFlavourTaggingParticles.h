/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "LHCbAlgs/Transformer.h"

// from LHCb
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/Particle.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IRelatedPVFinder.h"

// from FlavourTagging
#include "Utils/ITaggingHelperTool.h"
#include "Utils/TaggingHelper.h"
#include "Utils/TaggingHelperTool.h"

class GetFlavourTaggingParticles
    : public LHCb::Algorithm::Transformer<LHCb::Particle::Selection(
                                              const LHCb::Particle::Range&, const LHCb::Particle::Range&,
                                              const LHCb::Event::PV::PrimaryVertexContainer&, const DetectorElement& ),
                                          LHCb::DetDesc::usesConditions<DetectorElement>> {
public:
  GetFlavourTaggingParticles( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"BCandidates", ""}, KeyValue{"TaggingParticles", ""}, KeyValue{"PrimaryVertices", ""},
                      KeyValue{"StandardGeometry", LHCb::standard_geometry_top}},
                     KeyValue{"FlavourTaggingParticles", ""} ) {}

  LHCb::Particle::Selection operator()( const LHCb::Particle::Range&                   bCandidates,
                                        const LHCb::Particle::Range&                   taggingParticles,
                                        const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                        const DetectorElement& ) const override;

private:
  Gaudi::Property<double> m_minIpChi2{this, "MinIpChi2", 6.,
                                      "Tagging particle requirement: Minimum IPChi2 wrt B primary vertex"};

  ToolHandle<ITaggingHelperTool> m_taggingHelperTool{this, "TaggingHelper", "TaggingHelperTool"};

  mutable Gaudi::Accumulators::SummingCounter<> m_inCount{this, "#InputParticles"};
  mutable Gaudi::Accumulators::SummingCounter<> m_outCount{this, "#OutputParticles"};
};
