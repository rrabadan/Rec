/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "GaudiAlg/MergingTransformer.h"
#include <Gaudi/Accumulators.h>

// from LHCb
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/Tagger.h"

class FlavourTagsMerger : public Gaudi::Functional::MergingTransformer<LHCb::FlavourTags(
                              const Gaudi::Functional::details::vector_of_const_<LHCb::FlavourTags>& )> {
public:
  /// Standard constructor
  FlavourTagsMerger( const std::string& name, ISvcLocator* pSvcLocator )
      : MergingTransformer( name, pSvcLocator, {"FlavourTagsIterable", {""}}, {"AggregatedFlavourTags", ""} ) {}

  LHCb::FlavourTags operator()(
      const Gaudi::Functional::details::vector_of_const_<LHCb::FlavourTags>& flavourTagsIterable ) const override;

private:
  bool isSameB( const LHCb::Particle* bCand, const LHCb::Particle* taggedB ) const;

  mutable Gaudi::Accumulators::SummingCounter<> m_BCount{this, "#BCandidates"};
  mutable Gaudi::Accumulators::SummingCounter<> m_FTCount{this, "#FlavourTags"};
};
