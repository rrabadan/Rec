/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "FunctionalOSElectronTagger.h"

#include <typeinfo>

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FunctionalOSElectronTagger )

LHCb::FlavourTags FunctionalOSElectronTagger::
                  operator()( const LHCb::Particles& bCandidates, const LHCb::Particle::Range& taggingElectrons,
            const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
            const DetectorElement&                         lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();

  // keyed container of FlavourTag objects, one FlavourTag per B candidate
  LHCb::FlavourTags flavourTags;

  m_BCount += bCandidates.size();
  m_ElectronCount += taggingElectrons.size();

  // is this still needed??? could be removed
  if ( bCandidates.size() == 0 || taggingElectrons.size() == 0 || primaryVertices.size() == 0 ) {
    for ( const auto* bCand : bCandidates ) {
      LHCb::Tagger tagger;
      tagger.setType( taggerType() );
      LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
      flavourTag->addTagger( tagger );
      flavourTag->setTaggedB( bCand );
      flavourTag->setDecision( LHCb::FlavourTag::none );
      flavourTags.insert( flavourTag );
    }
    return flavourTags;
  }

  // loop over B candidates
  for ( const auto* bCand : bCandidates ) {

    auto tagger = performTagging( *bCand, taggingElectrons, primaryVertices, geometry );

    if ( !tagger.has_value() ) {
      // no appropriate tagging candidate found, fill with "empty" FlavourTag object
      LHCb::Tagger tagger;
      tagger.setType( taggerType() );
      LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
      flavourTag->addTagger( tagger );
      flavourTag->setTaggedB( bCand );
      flavourTag->setDecision( LHCb::FlavourTag::none );
      flavourTags.insert( flavourTag );
      continue;
    }

    LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
    flavourTag->setTaggedB( bCand );
    flavourTag->setDecision( LHCb::FlavourTag::none );
    flavourTag->setTaggers( std::vector<LHCb::Tagger>{tagger.value()} );
    flavourTags.insert( flavourTag );

    m_FTCount += 1;
  }

  return flavourTags;
}

std::optional<LHCb::Tagger>
FunctionalOSElectronTagger::performTagging( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingElectrons,
                                            const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                            const IGeometryInfo&                           geometry ) const {
  Gaudi::LorentzVector b_mom = bCand.momentum();

  // find PV that best fits the B candidate
  const LHCb::VertexBase* bestPV = LHCb::bestPV( primaryVertices, bCand );
  if ( bestPV == nullptr ) return std::nullopt;

  /*std::optional<const LHCb::VertexBase> refittedPV = m_taggingHelperTool->refitPVWithoutB(*bestPV, bCand);
  if(! refittedPV.has_value())
    return std::nullopt;
  */
  // PV refitting isn't possible in Moore (right now) since the PV doesn't save the particles it was made of
  std::optional<const LHCb::VertexBase> refittedPV = *bestPV;

  auto pileups = TaggingHelper::pileUpVertices( primaryVertices, *bestPV );

  auto b_daughters = m_particleDescendantTool->descendants( &bCand );
  b_daughters.push_back( &bCand );

  const LHCb::Particle*              bestTagCand = nullptr;
  double                             bestBDT     = -99.0;
  std::vector<const LHCb::Particle*> preselectionElectrons;

  // Preselection to choose Electrons which passed selections in BTaggingTool.cpp
  for ( const auto* tagCand : taggingElectrons ) {
    if ( !m_taggingHelperTool->passesCommonPreSelection( *tagCand, bCand, pileups, geometry ) ) continue;
    preselectionElectrons.push_back( tagCand );
  }

  for ( const auto* tagCand : preselectionElectrons ) {
    Gaudi::LorentzVector tag_mom = tagCand->momentum();

    const LHCb::ProtoParticle* tagProto = tagCand->proto();

    const double tagEOverP = tagProto->info( LHCb::ProtoParticle::CaloEoverP, -1000.0 );
    if ( tagEOverP < m_minEOverP ) continue;
    if ( tagEOverP > m_maxEOverP ) continue;

    const bool   isMuon      = tagProto->muonPID() ? tagProto->muonPID()->IsMuon() : false;
    const double tagProbNNk  = tagProto->info( LHCb::ProtoParticle::ProbNNk, -1000.0 );
    const double tagProbNNpi = tagProto->info( LHCb::ProtoParticle::ProbNNpi, -1000.0 );
    const double tagProbNNp  = tagProto->info( LHCb::ProtoParticle::ProbNNp, -1000.0 );
    const double tagProbNNmu = tagProto->info( LHCb::ProtoParticle::ProbNNmu, -1000.0 );
    const double tagProbNNe  = tagProto->info( LHCb::ProtoParticle::ProbNNe, -1000.0 );

    if ( isMuon ) continue;
    if ( tagProbNNk > m_maxProbNNk ) continue;
    if ( tagProbNNpi > m_maxProbNNpi ) continue;
    if ( tagProbNNp > m_maxProbNNp ) continue;
    if ( tagProbNNmu > m_maxProbNNmu ) continue;
    if ( tagProbNNe < m_minProbNNe ) continue;

    // const double tagVeloCharge = tagProto->info( LHCb::ProtoParticle::VeloCharge, -998 );
    const double tagInAccHcal = tagProto->info( LHCb::ProtoParticle::InAccHcal, -998 );
    // if (tagVeloCharge < m_minVeloCharge) continue;
    // if (tagVeloCharge > m_maxVeloCharge) continue;
    if ( tagInAccHcal != 1 ) continue;

    // Calculate minimum phi distance
    std::vector<double> bDaughtersPhi;
    std::transform( b_daughters.begin(), b_daughters.end(), std::back_inserter( bDaughtersPhi ),
                    [&tag_mom]( const LHCb::Particle* p ) {
                      return std::abs( TaggingHelper::dPhi( tag_mom.phi(), p->momentum().phi() ) );
                    } );
    const auto minPhi = std::min_element( bDaughtersPhi.begin(), bDaughtersPhi.end() );
    if ( *minPhi < m_minDistPhi ) continue;

    // IP significance cut wrt bestPV
    std::optional<std::pair<double, double>> refittedPVIP =
        m_taggingHelperTool->calcIPWithChi2( *tagCand, refittedPV.value(), geometry );
    if ( !refittedPVIP.has_value() ) continue;
    const double ipChi2 = refittedPVIP.value().second;
    const double ipSig  = std::sqrt( refittedPVIP.value().second );
    const double ipErr  = refittedPVIP.value().first / ipSig;
    const double absIp  = std::abs( refittedPVIP.value().first );
    if ( ipSig < m_minIpSigTagBestPV ) continue;
    if ( ipErr == 0. ) continue;

    // Minimum IP significance wrt to pile up PVs
    auto minIp = m_taggingHelperTool->calcMinIPWithChi2( *tagCand, pileups, geometry );
    if ( !minIp.has_value() ) continue;
    const double minIpSig = std::sqrt( minIp.value().second );
    if ( minIpSig < m_minIpSigTagPileUpVertices ) continue;

    const double tagProbNNghost = tagProto->info( LHCb::ProtoParticle::ProbNNghost, -1000.0 );

    // auto bestVertex = m_DVAlgorithm->bestVertex( tagCand, m_DVAlgorithm->geometry() );
    // std::optional<std::pair<double, double>> bestVertexIp =
    //     m_taggingHelperTool->calcIPWithChi2( *tagCand, *bestVertex, geometry );
    // if ( !bestVertexIp.has_value() ) continue;
    // const double bestVertexIpChi2 = bestVertexIp.value().second;

    const double         tagMass     = tagCand->measuredMass();
    Gaudi::LorentzVector tmp_tag_mom = tagCand->momentum();
    tmp_tag_mom.SetE( std::sqrt( tagMass * tagMass + tmp_tag_mom.P2() ) );
    const double deltaQ = ( b_mom + tmp_tag_mom ).M() - b_mom.M() - tagMass;

    const double deltaEta = std::fabs( b_mom.Eta() - tag_mom.Eta() );

    const double deltaPhi = TaggingHelper::dPhi( tag_mom.phi(), b_mom.phi() );
    const double deltaR   = std::sqrt( deltaEta * deltaEta + deltaPhi * deltaPhi );

    std::vector<double> inputVals( 12 );
    inputVals[0]  = tag_mom.Pt();
    inputVals[1]  = minIpSig;
    inputVals[2]  = preselectionElectrons.size(); // trackCounts
    inputVals[3]  = b_mom.Pt();
    inputVals[4]  = tagEOverP;
    inputVals[5]  = absIp;
    inputVals[6]  = ipErr;
    inputVals[7]  = ipChi2;
    inputVals[8]  = tagProbNNghost;
    inputVals[9]  = deltaQ;
    inputVals[10] = deltaEta;
    inputVals[11] = deltaR;

    const double mvaValue = m_classifier->getClassifierValue( inputVals );

    if ( mvaValue < bestBDT ) continue;

    bestTagCand = tagCand;
    bestBDT     = mvaValue;
  }

  if ( bestBDT < 0 || bestBDT > 1 ) return std::nullopt;

  int    tagdecision = bestTagCand->charge() > 0 ? -1 : 1;
  double omega       = 1 - bestBDT;
  if ( bestBDT < 0.5 ) {
    tagdecision *= -1;
    omega = bestBDT;
  }

  LHCb::Tagger tagger;
  tagger.setDecision( tagdecision );
  tagger.setOmega( omega );
  tagger.setType( taggerType() );
  tagger.addToTaggerParts( bestTagCand );
  tagger.setCharge( bestTagCand->charge() );
  tagger.setMvaValue( bestBDT );

  return tagger;
}
