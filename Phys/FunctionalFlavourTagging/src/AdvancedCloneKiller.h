/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "GaudiAlg/MergingTransformer.h"
#include <Gaudi/Accumulators.h>
// from LHCb
#include "Event/Particle.h"
#include "Event/Track.h"
// from Rec
#include "TrackKernel/TrackCloneData.h"
// from Phys/FunctionalFlavourTagging
#include "Utils/TaggingHelper.h"

class AdvancedCloneKiller : public Gaudi::Functional::MergingTransformer<LHCb::Particle::Selection(
                                const Gaudi::Functional::vector_of_const_<LHCb::Particle::Range>& inputParticles )> {
public:
  /// Standard constructor
  AdvancedCloneKiller( const std::string& name, ISvcLocator* pSvcLocator )
      : MergingTransformer( name, pSvcLocator, {"InputParticles", {""}}, {"OutputParticles", ""} ) {}

  LHCb::Particle::Selection
  operator()( const Gaudi::Functional::vector_of_const_<LHCb::Particle::Range>& inputParticles ) const override;

  TaggingHelper::CloneStatus cloneCategory( const LHCb::Particle& p1, const LHCb::Particle& p2 ) const;
  /* bool areHitFractionClones(const LHCb::Particle& p1, const LHCb::Particle& p2) const;
  bool areMomentumParameterClones(const LHCb::Particle& p1, const LHCb::Particle& p2) const;
  bool areSlopeClones(const LHCb::Particle& p1, const LHCb::Particle& p2) const;
  bool areSlopeChargeClones(const LHCb::Particle& p1, const LHCb::Particle& p2) const;
  bool areConvertedGamma(const LHCb::Particle& p1, const LHCb::Particle& p2) const; */

private:
  Gaudi::Property<double> m_maxSharedHitFraction{this, "MaxSharedHitFraction", 0.3};

  mutable Gaudi::Accumulators::SummingCounter<> m_inCount{this, "#InputParticles"};
  mutable Gaudi::Accumulators::SummingCounter<> m_outCount{this, "#OutputParticles"};
};
