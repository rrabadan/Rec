/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "FunctionalOSVertexChargeTagger.h"

#include <typeinfo>

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FunctionalOSVertexChargeTagger )

LHCb::FlavourTags FunctionalOSVertexChargeTagger::
                  operator()( const LHCb::Particles& bCandidates, const LHCb::Particle::Range& taggingParticles,
            const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
            const DetectorElement&                         lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();

  // keyed container of FlavourTag objects, one FlavourTag per B candidate
  LHCb::FlavourTags flavourTags;

  m_BCount += bCandidates.size();
  m_particleCount += taggingParticles.size();

  // is this still needed??? could be removed
  if ( bCandidates.size() == 0 || taggingParticles.size() == 0 || primaryVertices.size() == 0 ) {
    for ( const auto* bCand : bCandidates ) {
      LHCb::Tagger tagger;
      tagger.setType( taggerType() );
      LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
      flavourTag->addTagger( tagger );
      flavourTag->setTaggedB( bCand );
      flavourTag->setDecision( LHCb::FlavourTag::none );
      flavourTags.insert( flavourTag );
    }
    return flavourTags;
  }

  // loop over B candidates
  for ( const auto* bCand : bCandidates ) {

    auto tagger = performTagging( *bCand, taggingParticles, primaryVertices, geometry );

    if ( !tagger.has_value() ) {
      // no appropriate tagging candidate found, fill with "empty" FlavourTag object
      LHCb::Tagger tagger;
      tagger.setType( taggerType() );
      LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
      flavourTag->addTagger( tagger );
      flavourTag->setTaggedB( bCand );
      flavourTag->setDecision( LHCb::FlavourTag::none );
      flavourTags.insert( flavourTag );
      continue;
    }

    LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
    flavourTag->setTaggedB( bCand );
    flavourTag->setDecision( LHCb::FlavourTag::none );
    flavourTag->setTaggers( std::vector<LHCb::Tagger>{tagger.value()} );
    flavourTags.insert( flavourTag );

    m_FTCount += 1;
  }

  return flavourTags;
}

std::optional<LHCb::Tagger> FunctionalOSVertexChargeTagger::performTagging(
    const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingParticles,
    const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices, const IGeometryInfo& geometry ) const {
  // find PV that best fits the B candidate
  const LHCb::VertexBase* bestPV = LHCb::bestPV( primaryVertices, bCand );
  if ( bestPV == nullptr ) return std::nullopt;
  /*std::optional<const LHCb::VertexBase> refittedPV = m_taggingHelperTool->refitPVWithoutB(*bestPV, bCand);
  if(! refittedPV.has_value())
    return std::nullopt;
  */
  // PV refitting isn't possible in Moore (right now) since the PV doesn't save the particles it was made of
  std::optional<const LHCb::VertexBase> refittedPV = *bestPV;

  std::optional<const LHCb::Vertex> secondaryVertex =
      buildSecondaryVertex( refittedPV.value(), taggingParticles, geometry );
  if ( !( secondaryVertex.has_value() ) ) return std::nullopt;

  const auto& particlesFromSecondary = secondaryVertex->outgoingParticles();
  if ( particlesFromSecondary.empty() ) return std::nullopt;

  Gaudi::LorentzVector vertexSumMomentum( 0., 0., 0., 0. );
  double               vertexSumCharge = 0.;
  double               vertexSumPt     = 0.;
  double               vertexSumIpSig  = 0.;
  double               vertexSumDoca   = 0.;
  double               weightSum       = 0.;

  for ( const auto& particle : particlesFromSecondary ) {

    const auto ipChi2 = m_taggingHelperTool->calcIPWithChi2( *particle, refittedPV.value(), geometry );
    const auto doca   = m_taggingHelperTool->calcMinDocaWithErr( *particle, *particlesFromSecondary.at( 0 ),
                                                               *particlesFromSecondary.at( 1 ), geometry );
    if ( !( ipChi2.has_value() ) || !( doca.has_value() ) ) continue;

    vertexSumMomentum += particle->momentum();

    const double particlePt = particle->pt() / Gaudi::Units::GeV;
    vertexSumPt += particlePt;

    vertexSumIpSig += std::sqrt( ipChi2.value().second );

    vertexSumDoca += doca.value().first;

    const double weight = std::pow( particlePt, m_vertexChargeWeightPowerK );
    vertexSumCharge += weight * particle->charge();
    weightSum += weight;
  }

  if ( weightSum == 0. || vertexSumCharge == 0. ) return std::nullopt;

  const size_t nOutgoingParticles = particlesFromSecondary.size();
  const double vertexMeanCharge   = vertexSumCharge / weightSum;
  const double vertexMeanPt       = vertexSumPt / nOutgoingParticles;
  const double vertexMeanIpSig    = vertexSumIpSig / nOutgoingParticles;
  const double vertexMeanDoca     = vertexSumDoca / nOutgoingParticles;

  const auto secondaryToPrimary =
      secondaryVertex->position(); // position of secondary vertex relative to primary vertex
  const double vertexP  = vertexSumMomentum.P() / Gaudi::Units::GeV;
  const double vertexM  = vertexSumMomentum.M() / Gaudi::Units::GeV;
  const double vertexGP = vertexP / ( 0.16 * vertexM + 0.12 ); // corrected momentum, details
                                                               // are unfortunately unknown

  const double bMass        = 5.28;
  const double speedOfLight = 0.299792458;
  const double vertexTau    = std::sqrt( secondaryToPrimary.Mag2() ) * bMass /
                           ( vertexGP * speedOfLight ); // decay time of secondary vertex assuming B0 or B+
  const double vertexDeltaPhi =
      TaggingHelper::dPhi( vertexSumMomentum.phi(), secondaryToPrimary.phi() ); // phi angle between secondary vertex
                                                                                // momentum and PV-SV direction

  // Apply selection to secondary vertex
  if ( std::abs( vertexMeanCharge ) < m_minVertexCharge ) return std::nullopt;
  if ( vertexM < m_minVertexM ) return std::nullopt;
  if ( vertexP < m_minVertexP ) return std::nullopt;
  if ( vertexMeanPt < m_minVertexMeanPt ) return std::nullopt;
  if ( vertexSumPt < m_minVertexSumPt ) return std::nullopt;
  if ( vertexSumIpSig < m_minVertexSumIpSig ) return std::nullopt;
  if ( vertexSumDoca > m_maxVertexSumDoca ) return std::nullopt;

  std::vector<double> inputVals( 12 );
  inputVals[0]  = taggingParticles.size(); // trackCounts
  inputVals[1]  = primaryVertices.size();  // nPV
  inputVals[2]  = std::log( bCand.pt() / Gaudi::Units::GeV );
  inputVals[3]  = (double)nOutgoingParticles;
  inputVals[4]  = std::log( vertexMeanPt );
  inputVals[5]  = std::log( vertexMeanIpSig );
  inputVals[6]  = std::abs( vertexMeanCharge );
  inputVals[7]  = std::log( vertexM );
  inputVals[8]  = std::log( vertexP );
  inputVals[9]  = vertexDeltaPhi;
  inputVals[10] = std::log( vertexTau );
  inputVals[11] = vertexMeanDoca;

  const double mvaValue = m_classifier->getClassifierValue( inputVals );

  if ( mvaValue < 0 || mvaValue > 1 ) return std::nullopt;

  const int mvaDecision      = mvaValue < 0.5 ? -1 : 1;
  const int vertexChargeSign = vertexMeanCharge > 0 ? 1 : -1;
  const int tagDecision      = -1 * mvaDecision * vertexChargeSign;

  LHCb::Tagger tagger;
  tagger.setDecision( tagDecision );
  tagger.setOmega( -1. );
  tagger.setType( taggerType() );
  tagger.setCharge( vertexMeanCharge );
  tagger.setMvaValue( mvaValue );

  return tagger;
}

std::optional<LHCb::Vertex>
FunctionalOSVertexChargeTagger::buildSecondaryVertex( const LHCb::VertexBase&      primaryVertex,
                                                      const LHCb::Particle::Range& taggingParticles,
                                                      const IGeometryInfo&         geometry ) const {
  // Ported from SVertexOneSeedTool.cpp
  const LHCb::Particle*       seedA = nullptr;
  const LHCb::Particle*       seedB = nullptr;
  LHCb::Vertex                seedVertex;
  LHCb::Vertex                secondaryVertex;
  double                      maxPairProb = 0.;
  LHCb::VertexBase::ExtraInfo likelihoodInfo;
  Gaudi::XYZPoint             secondaryVertexPosition;

  for ( auto iteratorA = taggingParticles.cbegin(); iteratorA != taggingParticles.cend(); iteratorA++ ) {
    const LHCb::Particle* candidateA = *iteratorA;
    if ( !( passVertexSeedTrackCut( candidateA ) ) ) continue;

    // Calculate IP related variables for candidate A
    std::optional<std::pair<double, double>> ipChi2A =
        m_taggingHelperTool->calcIPWithChi2( *candidateA, primaryVertex, geometry );
    if ( !( ipChi2A.has_value() ) ) continue;
    const double absIpA = std::abs( ipChi2A.value().first );
    const double ipSigA = std::sqrt( ipChi2A.value().second );
    const double ipErrA = absIpA / ipSigA;
    if ( !( passVertexSeedIpCut( absIpA, ipErrA, ipSigA ) ) ) continue;

    for ( auto iteratorB = iteratorA + 1; iteratorB != taggingParticles.cend(); iteratorB++ ) {
      const LHCb::Particle* candidateB = *iteratorB;
      if ( !( passVertexSeedTrackCut( candidateB ) ) ) continue;

      // Calculate IP related variables for candidate B
      std::optional<std::pair<double, double>> ipChi2B =
          m_taggingHelperTool->calcIPWithChi2( *candidateB, primaryVertex, geometry );
      if ( !( ipChi2B.has_value() ) ) continue;
      double absIpB = std::abs( ipChi2B.value().first );
      double ipSigB = std::sqrt( ipChi2B.value().second );
      double ipErrB = absIpB / ipSigB;
      if ( !( passVertexSeedIpCut( absIpB, ipErrB, ipSigB ) ) ) continue;

      // Cut on 2 candidates kinematics
      if ( candidateA->proto()->track()->type() == LHCb::Track::Types::Upstream &&
           candidateB->proto()->track()->type() == LHCb::Track::Types::Upstream )
        continue; // SVertexOneSeedTool.cpp::L384

      if ( std::max( candidateA->pt(), candidateB->pt() ) / Gaudi::Units::GeV < 0.3 )
        continue; // SVertexOneSeedTool.cpp::L396

      const double deltaPhi =
          std::abs( TaggingHelper::dPhi( candidateA->momentum().phi(), candidateB->momentum().phi() ) );
      if ( deltaPhi < 0.001 ) continue;                // SVertexOneSeedTool.cpp::L399
      if ( deltaPhi < m_minTwoSeedDeltaPhi ) continue; // SVertexOneSeedTool.cpp::L464

      // Fit the vertex and cut on vertex quality
      const auto vertexCandidate = m_taggingHelperTool->fitVertex( *candidateA, *candidateB, geometry );
      if ( !( vertexCandidate.has_value() ) ) continue;
      if ( vertexCandidate->chi2PerDoF() > 10.0 ) continue; // SVertexOneSeedTool.cpp::L403

      auto relativePosition = vertexCandidate->position() - primaryVertex.position();
      ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<double>> secondaryToPrimary(
          relativePosition.x() / Gaudi::Units::mm, relativePosition.y() / Gaudi::Units::mm,
          relativePosition.z() / Gaudi::Units::mm );

      if ( secondaryToPrimary.z() < 0.0 ) continue; // SVertexOneSeedTool.cpp::L409
      if ( secondaryToPrimary.Theta() < 0.01 || secondaryToPrimary.Theta() > 0.35 )
        continue; // SVertexOneSeedTool.cpp::L410

      double rDistance =
          std::sqrt( secondaryToPrimary.x() * secondaryToPrimary.x() + secondaryToPrimary.y() * secondaryToPrimary.y() +
                     secondaryToPrimary.z() * secondaryToPrimary.z() * 0.074 * 0.074 );
      if ( rDistance < 0.3 ) continue; // SVertexOneSeedTool.cpp::L415
      if ( rDistance > 10. ) continue; // SVertexOneSeedTool.cpp::L416

      const double mass = ( candidateA->momentum() + candidateB->momentum() ).M() / Gaudi::Units::GeV;
      if ( mass < 0.2 ) continue; // SVertexOneSeedTool.cpp::L419

      // Check for Ks, SVertexOneSeedTool.cpp::L421-L438
      if ( candidateA->charge() * candidateB->charge() < 0 &&
           ( candidateA->particleID().abspid() == 211 || candidateB->particleID().abspid() == 211 ) ) {
        double pionMass = 139.570 * Gaudi::Units::MeV;

        Gaudi::LorentzVector pA = candidateA->momentum();
        Gaudi::LorentzVector pB = candidateB->momentum();
        pA.SetE( pA.P2() + pionMass * pionMass );
        pB.SetE( pB.P2() + pionMass * pionMass );

        const double KsCheckMass = ( pA + pB ).M();
        if ( KsCheckMass > 0.490 * Gaudi::Units::GeV && KsCheckMass < 0.505 * Gaudi::Units::GeV ) {
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << " Ks candidate (two pions or a pion + one mis-ID pion) ! " << KsCheckMass << endmsg;
          }
          continue;
        }
      }

      // evaluate likelihood
      const double prob_chi2 = polynomail( vertexCandidate->chi2PerDoF(), {0.632154, -0.0421607, 0.00181837} );

      const double minPt      = std::min( candidateA->pt(), candidateB->pt() ) / Gaudi::Units::GeV;
      const double prob_minPt = minPt < 1. ? polynomail( minPt, {0.0144075, 1.28718, -0.477544} )
                                           : polynomail( minPt, {0.536686, 0.382045, -0.0945185} );

      const double maxAbsIp      = std::max( absIpA, absIpB );
      const double prob_maxAbsIp = maxAbsIp < 0.4 ? polynomail( maxAbsIp, {0.490689, 1.21535, -1.87733} )
                                                  : polynomail( maxAbsIp, {0.790337, -0.229543, 0.00438416} );

      const double minIpSig      = std::min( ipSigA, ipSigB );
      const double prob_minIpSig = minIpSig < 6 ? polynomail( minIpSig, {-0.442382, 0.326852, -0.0258772} )
                                                : polynomail( minIpSig, {0.540565, 0.0130981, -0.000204322} );

      const double prob_deltaPhi  = polynomail( deltaPhi, {0.611887, 0.0165528, -0.0398336} );
      const double prob_rDistance = polynomail( rDistance, {0.673403, -0.0743514, 0.00646535} );

      const double deltaTheta      = secondaryToPrimary.Theta();
      const double prob_deltaTheta = deltaTheta < 0.05 ? polynomail( deltaTheta, {0.466409, 7.29019, -94.1293} )
                                                       : polynomail( deltaTheta, {0.555528, 0.98924, -5.43018} );

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "  prob_chi2 : " << prob_chi2 << "  prob_minPt : " << prob_minPt
                << "  prob_maxAbsIp : " << prob_maxAbsIp << "  prob_minIpSig : " << prob_minIpSig
                << "  prob_deltaTheta : " << prob_deltaTheta << "  prob_deltaPhi : " << prob_deltaPhi
                << "  prob_rDistance : " << prob_rDistance << endmsg;
      }

      double thisPairProb = combine( prob_chi2, prob_minPt, prob_maxAbsIp, prob_minIpSig, prob_deltaPhi, prob_rDistance,
                                     prob_deltaTheta );

      if ( msgLevel( MSG::DEBUG ) ) { debug() << " secondary vertex formed - likelihood: " << thisPairProb << endmsg; }

      if ( thisPairProb >= maxPairProb ) {
        seedVertex = vertexCandidate.value();
        if ( thisPairProb > 1 ) thisPairProb = 1.;
        maxPairProb = thisPairProb;
        seedA       = candidateA;
        seedB       = candidateB;
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "       === pt1=" << seedA->pt() << "       === pt2=" << seedB->pt() << endmsg;
        }

        secondaryVertexPosition.SetX( secondaryToPrimary.x() );
        secondaryVertexPosition.SetY( secondaryToPrimary.y() );
        secondaryVertexPosition.SetZ( secondaryToPrimary.z() );

        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "       svpos (SV-RV): " << secondaryVertexPosition.x() << ", " << secondaryVertexPosition.y()
                  << ", " << secondaryVertexPosition.z() << endmsg;
        }
      }
    } // End of candidateB loop
  }   // End of candidateA loop

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "maxprobf=" << maxPairProb << endmsg;

  if ( maxPairProb <= m_minSeedVertexProb ) return std::nullopt; // SVertexOneSeedTool.cpp::L503
  // if ( !( seedA ) || !( seedB ) ) return std::nullopt;

  likelihoodInfo.insert( LHCb::Vertex::CreationMethod::LastGlobal + 1, maxPairProb );
  secondaryVertex.setExtraInfo( likelihoodInfo );
  secondaryVertex.setPosition( secondaryVertexPosition );
  secondaryVertex.addToOutgoingParticles( seedA );
  secondaryVertex.addToOutgoingParticles( seedB );

  for ( const auto* particle : taggingParticles ) {
    if ( particle == seedA ) continue;
    if ( particle == seedB ) continue;

    const auto* track = particle->proto()->track();
    if ( track->ghostProbability() > m_maxTrackGhostProb ) continue; // SVertexOneSeedTool.cpp::L521
    if ( track->chi2PerDoF() > m_maxAddedTrackChi2ndof ) continue;   // SVertexOneSeedTool.cpp::L522

    auto ipWithChi2PV = m_taggingHelperTool->calcIPWithChi2( *particle, primaryVertex, geometry );
    if ( !ipWithChi2PV.has_value() ) continue;
    double absIpPV = std::abs( ipWithChi2PV.value().first );
    double ipSigPV = std::sqrt( ipWithChi2PV.value().second );
    if ( absIpPV < 0.1 ) continue; // SVertexOneSeedTool.cpp::L528
    if ( ipSigPV < 3.5 ) continue; // SVertexOneSeedTool.cpp::L527
    // Don't need ipErr cut since we check if ip calculatation fails

    auto ipWithChi2SV = m_taggingHelperTool->calcIPWithChi2( *particle, seedVertex, geometry );
    if ( !ipWithChi2SV.has_value() ) continue;
    double absIpSV = std::abs( ipWithChi2SV.value().first );
    if ( absIpSV > 0.9 ) continue; // SVertexOneSeedTool.cpp::L533

    auto docaWithErrToSV = m_taggingHelperTool->calcMinDocaWithErr( *particle, *seedA, *seedB, geometry );
    if ( !docaWithErrToSV.has_value() ) continue;
    double docaToSV = docaWithErrToSV.value().first;
    if ( docaToSV > 0.2 ) continue; // SVertexOneSeedTool.cpp::L537

    secondaryVertex.addToOutgoingParticles( particle );
  }

  return secondaryVertex;
}

bool FunctionalOSVertexChargeTagger::passVertexSeedTrackCut( const LHCb::Particle* candidate ) const {
  if ( !( candidate->proto() ) ) return false;

  const LHCb::Track* track    = candidate->proto()->track();
  const double       chi2ndof = track->chi2PerDoF();

  if ( candidate->pt() / Gaudi::Units::GeV < 0.15 ) return false;        // SVertexOneSeedTool.cpp::L395
  if ( candidate->pt() / Gaudi::Units::GeV < m_minSeedPt ) return false; // SVertexOneSeedTool.cpp::L446

  switch ( track->type() ) {
  case LHCb::Track::Types::Long:
    if ( chi2ndof > m_maxLongSeedTrackChi2ndof ) { // SVertexOneSeedTool.cpp::L360,L380
      return false;
    }
    break;
  case LHCb::Track::Types::Upstream:
    if ( chi2ndof > m_maxUpstreamSeedTrackChi2ndof ) { // SVertexOneSeedTool.cpp::L362,L382
      return false;
    }
    break;
  default:
    return false; // Do we need this?
  }

  if ( track->ghostProbability() > m_maxTrackGhostProb ) return false; // SVertexOneSeedTool.cpp::L364,L385

  return true;
}

bool FunctionalOSVertexChargeTagger::passVertexSeedIpCut( const double absIp, const double ipErr,
                                                          const double ipSig ) const {
  if ( ipErr == 0. ) return false;            // SVertexOneSeedTool.cpp::L368,L389
  if ( ipErr > 1.0 ) return false;            // SVertexOneSeedTool.cpp::L369,L390
  if ( ipSig < 2.5 ) return false;            // SVertexOneSeedTool.cpp::L370,L391
  if ( ipSig > 100. ) return false;           // SVertexOneSeedTool.cpp::L371,L392
  if ( absIp > 3.0 ) return false;            // SVertexOneSeedTool.cpp::L393
  if ( ipSig < m_minSeedIpSig ) return false; // SVertexOneSeedTool.cpp::L459

  return true;
}

double FunctionalOSVertexChargeTagger::polynomail( double x, std::vector<double> coefficients ) const {
  double accumulate = 0.;
  double exponent   = 1.;

  for ( const double coefficient : coefficients ) {
    accumulate += coefficient * exponent;
    exponent *= x;
  }

  return accumulate;
}

double FunctionalOSVertexChargeTagger::combine( double p1, double p2, double p3, double p4, double p5, double p6,
                                                double p7 ) const {
  const double probs = p1 * p2 * p3 * p4 * p5 * p6 * p7;
  const double probb = ( 1 - p1 ) * ( 1 - p2 ) * ( 1 - p3 ) * ( 1 - p4 ) * ( 1 - p5 ) * ( 1 - p6 ) * ( 1 - p7 );

  return probs / ( probs + probb );
}
