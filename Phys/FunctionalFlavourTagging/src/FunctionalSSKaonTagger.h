/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"

// from Event
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/PrimaryVertices.h"
#include "Event/Tagger.h"

// from FunctionalFlavourTagging
#include "../Classification/SSKaon/SSKaon_BDTeta_dev.h"
#include "../Classification/SSKaon/SSKaon_BDTseltracks_dev.h"
#include "Utils/ITaggingHelperTool.h"
#include "Utils/TaggingHelper.h"
#include "Utils/TaggingHelperTool.h"

class FunctionalSSKaonTagger
    : public Gaudi::Functional::Transformer<LHCb::FlavourTags( const LHCb::Particles&, const LHCb::Particle::Range&,
                                                               const LHCb::Event::PV::PrimaryVertexContainer&,
                                                               const DetectorElement& ),
                                            LHCb::DetDesc::usesConditions<DetectorElement>> {
public:
  /// Standard constructor
  FunctionalSSKaonTagger( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"BCandidates", ""}, KeyValue{"TaggingKaons", ""}, KeyValue{"PrimaryVertices", ""},
                      KeyValue{"StandardGeometry", LHCb::standard_geometry_top}},
                     KeyValue{"OutputFlavourTags", ""} ) {}

  LHCb::FlavourTags operator()( const LHCb::Particles& bCandidates, const LHCb::Particle::Range& taggingKaons,
                                const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                const DetectorElement& ) const override;

  std::optional<LHCb::Tagger> performTagging( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingKaons,
                                              const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                              const IGeometryInfo&                           geometry ) const;

  LHCb::Tagger::TaggerType taggerType() const { return LHCb::Tagger::TaggerType::SSKaonLatest; }

private:
  // cut values for sskaon selection
  Gaudi::Property<double> m_maxIpSigTagBestPV{this, "MaxIpSigTagBestPV", 14.0,
                                              "Tagging particle requirement: Maximum IP significance wrt to best PV"};

  Gaudi::Property<double> m_minTagSelBDTValue{
      this, "MinTagSelBDTValue", 0.72,
      "Tagging particle requirement: Minimum BDT output of the tagging-candidate selector BDT "};

  // values for calibration purposes
  Gaudi::Property<double> m_averageEta{this, "AverageEta", 0.3949};
  Gaudi::Property<double> m_minPosDecision{this, "MinPositiveTaggingDecision", 0.5,
                                           "Minimum value of the Eta for a tagging decision of +1"};
  Gaudi::Property<double> m_maxNegDecision{this, "MaxNegativeTaggingDecision", 0.5,
                                           "Maximum value of the Eta for a tagging decision of -1"};

  mutable Gaudi::Accumulators::SummingCounter<> m_BCount{this, "#BCandidates"};
  mutable Gaudi::Accumulators::SummingCounter<> m_kaonCount{this, "#taggingKaons"};
  mutable Gaudi::Accumulators::SummingCounter<> m_FTCount{this, "#goodFlavourTags"};

  ToolHandle<ITaggingHelperTool> m_taggingHelperTool{this, "TaggingHelper", "TaggingHelperTool"};
  // ToolHandle<IVertexFit>              m_vertexFitTool{this, "VertexFitter", "LoKi::VertexFitter"};

  std::vector<std::string> m_tagSelBdtVariables = {
      "log(Tr_P)",          "log(Tr_PT)",          "log(B_PT)",     "log(abs(deltaPhi))",
      "log(abs(deltaEta))", "log(pTrel)",          "log(Tr_BPVIP)", "log(sqrt(Tr_BPVIPCHI2))",
      "log(Tr_TRCHI2DOF)",  "log(nTracks_presel)", "nPV",           "Tr_PROBNNk",
      "Tr_PROBNNpi",        "Tr_PROBNNp"};
  BDTseltracksReaderCompileWrapper m_tagSelBDTReader = BDTseltracksReaderCompileWrapper( m_tagSelBdtVariables );

  std::vector<std::string>   m_BdtEtaVariables = {"log(B_PT)",
                                                "nTracks_presel_BDTGcut",
                                                "nPV",
                                                "(Tr_Charge*Tr_BDTG_selBestTracks_14vars)",
                                                "(Tr_Charge_2nd*Tr_BDTG_selBestTracks_14vars_2nd)",
                                                "(Tr_Charge_3rd*Tr_BDTG_selBestTracks_14vars_3rd)"};
  BDTetaReaderCompileWrapper m_etaBdtReader    = BDTetaReaderCompileWrapper( m_BdtEtaVariables );

  double m_polP0 = 0.025841;
  double m_polP1 = -0.21766;

  struct TagCandidate {
    const LHCb::Particle* particle;
    double                refittedPVIP;
    double                ipSig;
    double                selBDTVal;

    static bool sortTagCandidates( const TagCandidate& t1, const TagCandidate& t2 ) {
      return t1.selBDTVal > t2.selBDTVal;
    }
  };
};
