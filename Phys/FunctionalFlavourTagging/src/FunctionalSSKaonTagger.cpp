/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "FunctionalSSKaonTagger.h"

#include <typeinfo>

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FunctionalSSKaonTagger )

LHCb::FlavourTags FunctionalSSKaonTagger::operator()( const LHCb::Particles&                         bCandidates,
                                                      const LHCb::Particle::Range&                   taggingKaons,
                                                      const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                                      const DetectorElement& lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();
  // keyed container of FlavourTag objects, one FlavourTag per B candidate
  LHCb::FlavourTags flavourTags;

  m_BCount += bCandidates.size();
  m_kaonCount += taggingKaons.size();
  // always() << bCandidates.size() << "  " << taggingKaons.size() << "  " << primaryVertices.size() << endmsg;

  // is this still needed??? could be removed
  if ( bCandidates.size() == 0 || taggingKaons.size() == 0 || primaryVertices.size() == 0 ) {
    for ( const auto* bCand : bCandidates ) {
      LHCb::Tagger tagger;
      tagger.setType( taggerType() );
      LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
      flavourTag->addTagger( tagger );
      flavourTag->setTaggedB( bCand );
      flavourTag->setDecision( LHCb::FlavourTag::none );
      flavourTags.insert( flavourTag );
    }
    return flavourTags;
  }

  // loop over B candidates
  for ( const auto* bCand : bCandidates ) {

    auto tagResult = performTagging( *bCand, taggingKaons, primaryVertices, geometry );

    if ( !tagResult.has_value() ) {
      // no appropriate tagging candidate found, fill with "empty" FlavourTag object
      LHCb::Tagger tagger;
      tagger.setType( taggerType() );
      LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
      flavourTag->addTagger( tagger );
      flavourTag->setTaggedB( bCand );
      flavourTag->setDecision( LHCb::FlavourTag::none );
      flavourTags.insert( flavourTag );
      continue;
    }
    // always() << "tagger found with decision: " << tagger.value().decision() << endmsg;
    LHCb::FlavourTag* flavourTag = new LHCb::FlavourTag();
    flavourTag->setTaggedB( bCand );
    flavourTag->setDecision( LHCb::FlavourTag::none );
    flavourTag->setTaggers( std::vector<LHCb::Tagger>{tagResult.value()} );
    flavourTags.insert( flavourTag );

    // always() << bestTagCand.charge() << "  " << tagger.charge() << endmsg;

    m_FTCount += 1;
  }

  always() << "n_FTtags" << m_FTCount << endmsg;
  return flavourTags;
}

std::optional<LHCb::Tagger>
FunctionalSSKaonTagger::performTagging( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingKaons,
                                        const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                        const IGeometryInfo&                           geometry ) const {
  Gaudi::LorentzVector b_mom = bCand.momentum();

  // find PV that best fits the B candidate
  const LHCb::VertexBase* bestPV = LHCb::bestPV( primaryVertices, bCand );
  if ( bestPV == nullptr ) return std::nullopt;

  /*std::optional<const LHCb::VertexBase> refittedPV = m_taggingHelperTool->refitPVWithoutB(*bestPV, bCand, geometry);
  if(! refittedPV.has_value())
    return std::nullopt;
  */
  // PV refitting isn't possible in Moore (right now) since the PV doesn't save the particles it was made of
  std::optional<const LHCb::VertexBase> refittedPV = *bestPV;

  auto pileups = TaggingHelper::pileUpVertices( primaryVertices, *bestPV );

  std::vector<TagCandidate> preSelTagCandidates;
  for ( const auto* tagCand : taggingKaons ) {
    // Gaudi::LorentzVector tag_mom = tagCand->momentum();

    if ( !m_taggingHelperTool->passesCommonPreSelection( *tagCand, bCand, pileups, geometry ) ) continue;

    // IP significance cut wrt bestPV
    std::optional<std::pair<double, double>> refittedPVIP =
        m_taggingHelperTool->calcIPWithErr( *tagCand, refittedPV.value(), geometry );
    if ( !refittedPVIP.has_value() ) continue;
    const double ipSig = refittedPVIP.value().first / refittedPVIP.value().second;
    if ( ipSig > m_maxIpSigTagBestPV ) continue;

    preSelTagCandidates.push_back( TagCandidate( {tagCand, refittedPVIP.value().first, ipSig, -999.} ) );
  }

  // always() << preSelTagCandidates.size() << " kaons survived presel" << endmsg;

  std::vector<TagCandidate> selTagCandidates;
  for ( auto& tagCand : preSelTagCandidates ) {
    Gaudi::LorentzVector tag_mom = tagCand.particle->momentum();
    // inputs for the BDT
    double deltaPhi       = TaggingHelper::dPhi( b_mom.phi(), tag_mom.phi() ); // ??? check if bigger than pi
    deltaPhi              = fabs( deltaPhi );
    const double deltaEta = std::fabs( b_mom.Eta() - tag_mom.Eta() );

    const double btag_rel =
        ( b_mom.Px() * tag_mom.Px() + b_mom.Py() * tag_mom.Py() + b_mom.Pz() * tag_mom.Pz() ) / b_mom.P();
    const double btag_relPt = sqrt( tag_mom.P() * tag_mom.P() - btag_rel * btag_rel );

    const LHCb::ProtoParticle* tagProto     = tagCand.particle->proto();
    const double               tagPIDk      = tagProto->info( LHCb::ProtoParticle::ProbNNk, -1000.0 );
    const double               tagPIDpi     = tagProto->info( LHCb::ProtoParticle::ProbNNpi, -1000.0 );
    const double               tagPIDp      = tagProto->info( LHCb::ProtoParticle::ProbNNp, -1000.0 );
    const LHCb::Track*         tagTrack     = tagProto->track();
    const double               tagTrackChi2 = tagTrack->chi2PerDoF();

    // const double tagPIDmu = tagProto->info( LHCb::ProtoParticle::ProbNNp, -1000.0 );
    // always() << "ProbNNK " << tagPIDk << endmsg;
    // always() << "ProbNNpi " << tagPIDpi << endmsg;
    // always() << "ProbNNp " << tagPIDp << endmsg;
    // always() << "ProbNNmu " << tagPIDmu << endmsg;

    std::vector<double> inputVals( 14 );
    inputVals[0]  = log( tag_mom.P() );
    inputVals[1]  = log( tag_mom.Pt() );
    inputVals[2]  = log( b_mom.Pt() );
    inputVals[3]  = log( deltaPhi );
    inputVals[4]  = log( deltaEta );
    inputVals[5]  = log( btag_relPt );
    inputVals[6]  = log( tagCand.refittedPVIP );
    inputVals[7]  = log( tagCand.ipSig );
    inputVals[8]  = log( tagTrackChi2 );
    inputVals[9]  = log( preSelTagCandidates.size() );
    inputVals[10] = primaryVertices.size();
    inputVals[11] = tagPIDk;
    inputVals[12] = tagPIDpi;
    inputVals[13] = tagPIDp;

    auto tagCand_selBDTVal = m_tagSelBDTReader.GetMvaValue( inputVals );

    if ( tagCand_selBDTVal < m_minTagSelBDTValue ) continue;

    tagCand.selBDTVal = tagCand_selBDTVal;

    selTagCandidates.push_back( tagCand );
  }

  // always() << selTagCandidates.size() << " kaons survive BDT selection" << endmsg;
  if ( selTagCandidates.size() == 0 ) return std::nullopt;

  std::stable_sort( selTagCandidates.begin(), selTagCandidates.end(), TagCandidate::sortTagCandidates );

  int goodTagCandidates = selTagCandidates.size();

  while ( selTagCandidates.size() < 3 ) {
    selTagCandidates.push_back( TagCandidate( {new LHCb::Particle(), 0, 0, 0} ) );
  }

  std::vector<double> values;
  values.push_back( log( b_mom.Pt() ) );
  values.push_back( goodTagCandidates );
  values.push_back( primaryVertices.size() );
  values.push_back( selTagCandidates[0].particle->charge() * selTagCandidates[0].selBDTVal );
  values.push_back( selTagCandidates[1].particle->charge() * selTagCandidates[1].selBDTVal );
  values.push_back( selTagCandidates[2].particle->charge() * selTagCandidates[2].selBDTVal );
  double etaBdtVal      = m_etaBdtReader.GetMvaValue( values );
  double calibEtaBdtVal = ( etaBdtVal / 0.71 + 1. ) * 0.5;

  std::vector<double> ccValues;
  ccValues.push_back( log( b_mom.Pt() ) );
  ccValues.push_back( goodTagCandidates );
  ccValues.push_back( primaryVertices.size() );
  ccValues.push_back( -1. * selTagCandidates[0].particle->charge() * selTagCandidates[0].selBDTVal );
  ccValues.push_back( -1. * selTagCandidates[1].particle->charge() * selTagCandidates[1].selBDTVal );
  ccValues.push_back( -1. * selTagCandidates[2].particle->charge() * selTagCandidates[2].selBDTVal );
  double etaCCBdtVal      = m_etaBdtReader.GetMvaValue( ccValues );
  double calibEtaCCBdtVal = ( etaCCBdtVal / 0.71 + 1. ) * 0.5;
  double eta              = ( calibEtaBdtVal + ( 1. - calibEtaCCBdtVal ) ) * 0.5;

  int tagdecision = 0;
  if ( eta >= m_minPosDecision )
    tagdecision = 1;
  else if ( eta < m_maxNegDecision )
    tagdecision = -1;
  if ( eta >= 0.5 ) eta = 1. - eta;

  double calibEta = ( m_polP0 + m_averageEta ) + ( 1 + m_polP1 ) * ( eta - m_averageEta );

  LHCb::Tagger tagger;
  tagger.setDecision( tagdecision );
  tagger.setOmega( calibEta );
  tagger.setType( taggerType() );
  tagger.setMvaValue( calibEta );

  for ( int i = 0; i < 1; i++ ) tagger.addToTaggerParts( selTagCandidates[i].particle );
  // TODO: Fix persistency to not segfault when adding more than 1 tagging particle
  // for ( int i = 0; i < 3; i++ ) tagger.addToTaggerParts( selTagCandidates[i].particle );

  // always() << "tagging decision made: " << tagdecision << endmsg;
  return tagger;
}
