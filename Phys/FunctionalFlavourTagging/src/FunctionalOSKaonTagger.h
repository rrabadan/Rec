/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"

// from Event
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/Tagger.h"

// from FunctionalFlavourTagging
#include "../Classification/OSKaon/OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v2r0.h"
#include "Utils/ITaggingHelperTool.h"
#include "Utils/TaggingHelper.h"
#include "Utils/TaggingHelperTool.h"

class FunctionalOSKaonTagger
    : public Gaudi::Functional::Transformer<LHCb::FlavourTags( const LHCb::Particles&, const LHCb::Particle::Range&,
                                                               const LHCb::Event::PV::PrimaryVertexContainer&,
                                                               const DetectorElement& ),
                                            LHCb::DetDesc::usesConditions<DetectorElement>> {
public:
  /// Standard constructor
  FunctionalOSKaonTagger( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"BCandidates", ""}, KeyValue{"TaggingKaons", ""}, KeyValue{"PrimaryVertices", ""},
                      KeyValue{"StandardGeometry", LHCb::standard_geometry_top}},
                     KeyValue{"OutputFlavourTags", ""} ) {}

  LHCb::FlavourTags operator()( const LHCb::Particles& bCandidates, const LHCb::Particle::Range& taggingKaons,
                                const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                const DetectorElement& ) const override;

  std::optional<LHCb::Tagger> performTagging( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingKaons,
                                              const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                              const IGeometryInfo&                           geometry ) const;

  LHCb::Tagger::TaggerType taggerType() const { return LHCb::Tagger::TaggerType::OSKaonLatest; }

private:
  // cut values for OSKaon selection
  Gaudi::Property<double> m_minDistPhi{this, "MinDistPhi", 0.0025,
                                       "Tagging particle requirement: Minimum phi distance to B daughters"};
  Gaudi::Property<double> m_maxAbsIpTagBestPV{this, "MaxAbsIpTagBestPV", 0.85,
                                              "Tagging particle requirement: Maximum absolute IP wrt to best PV"};
  Gaudi::Property<double> m_minIpSigTagBestPV{this, "MinIpSigTagBestPV", 3.77,
                                              "Tagging particle requirement: Minimum IP significance wrt to best PV"};
  Gaudi::Property<double> m_maxAbsIp{this, "MaxAbsIpBestPV", 0.85,
                                     "Tagging particle requirement: Maximum absolute IP wrt to best PV"};
  Gaudi::Property<double> m_minIpSigTagPileUpVertices{
      this, "MinIpSigPileUp", 4.0, "Tagging particle requirement: Minimum IP significance wrt to all pile-up PV"};

  Gaudi::Property<double> m_maxProbNNmu{this, "MaxProbNNmu", 0.81, "Tagging particle requirement: Maximum ProbNNmu."};
  Gaudi::Property<double> m_maxProbNNpi{this, "MaxProbNNpi", 0.96, "Tagging particle requirement: Maximum ProbNNpi."};
  Gaudi::Property<double> m_maxProbNNp{this, "MaxProbNNp", 0.9, "Tagging particle requirement: Maximum ProbNNp."};
  Gaudi::Property<double> m_maxProbNNe{this, "MaxProbNNe", 0.89, "Tagging particle requirement: Maximum ProbNNe."};
  Gaudi::Property<double> m_minProbNNk{this, "MinProbNNk", 0.35, "Tagging particle requirement: Minimum ProbNNk."};

  // values for calibration purposes
  // Gaudi::Property<double> m_averageEta{this, "AverageEta", 0.3949};
  // Gaudi::Property<double> m_minPosDecision{this, "MinPositiveTaggingDecision", 0.5,
  //                                          "Minimum value of the Eta for a tagging decision of +1"};
  // Gaudi::Property<double> m_maxNegDecision{this, "MaxNegativeTaggingDecision", 0.5,
  //                                          "Maximum value of the Eta for a tagging decision of -1"};

  mutable Gaudi::Accumulators::SummingCounter<> m_BCount{this, "#BCandidates"};
  mutable Gaudi::Accumulators::SummingCounter<> m_kaonCount{this, "#taggingKaons"};
  mutable Gaudi::Accumulators::SummingCounter<> m_FTCount{this, "#goodFlavourTags"};

  ToolHandle<ITaggingHelperTool>   m_taggingHelperTool{this, "TaggingHelper", "TaggingHelperTool"};
  ToolHandle<IParticleDescendants> m_particleDescendantTool{this, "ParticleDescendantTool", "ParticleDescendants"};

  std::unique_ptr<ITaggingClassifier> m_classifier = std::make_unique<OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v2r0>();

  // double m_polP0 = 0.025841;
  // double m_polP1 = -0.21766;
};
