/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from Rec
#include "Kernel/ICheckOverlap.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IPVReFitter.h"
#include "Kernel/IParticleDescendants.h"
#include "Kernel/IRelatedPVFinder.h"
#include "Kernel/IVertexFit.h"
// from FlavourTagging
#include "ITaggingHelperTool.h"

class TaggingHelperTool : public GaudiTool, virtual public ITaggingHelperTool {

public:
  /// Standard constructor
  TaggingHelperTool( const std::string& type, const std::string& name, const IInterface* parent )
      : GaudiTool( type, name, parent ){};

  virtual ~TaggingHelperTool() = default;
  StatusCode initialize() override;

  std::optional<std::pair<double, double>> calcIPWithErr( const LHCb::Particle& part, const LHCb::VertexBase& vtx,
                                                          const IGeometryInfo& geometry ) const override;
  std::optional<std::pair<double, double>> calcMinIPWithErr( const LHCb::Particle& part,
                                                             LHCb::span<LHCb::VertexBase const* const>,
                                                             const IGeometryInfo& geometry ) const override;
  std::optional<std::pair<double, double>> calcIPWithChi2( const LHCb::Particle& part, const LHCb::VertexBase& vtx,
                                                           const IGeometryInfo& geometry ) const override;
  std::optional<std::pair<double, double>> calcMinIPWithChi2( const LHCb::Particle& part,
                                                              LHCb::span<LHCb::VertexBase const* const>,
                                                              const IGeometryInfo& geometry ) const override;
  std::optional<std::pair<double, double>> calcMinDocaWithErr( const LHCb::Particle& particle,
                                                               const LHCb::Particle& referenceParticleA,
                                                               const LHCb::Particle& referenceParticleB,
                                                               const IGeometryInfo&  geometry ) const override;

  double calcMinIPChi2( const LHCb::Particle& part, const LHCb::Event::PV::PrimaryVertexContainer& vertices,
                        const IGeometryInfo& geometry ) const override;

  bool hasOverlap( const LHCb::Particle& taggingCand, const LHCb::Particle& bCand ) const override;

  std::optional<LHCb::Vertex> fitVertex( const LHCb::Particle& tagCand, const LHCb::Particle& bCand,
                                         const IGeometryInfo& geometry ) const override;

  TaggingHelper::CloneStatus cloneCategory( const LHCb::Particle& tagCand, const LHCb::Particle& bCand,
                                            double maxSharedHitFraction ) const override;
  bool                       passesCommonPreSelection( const LHCb::Particle& tagCand, const LHCb::Particle& bCand,
                                                       LHCb::span<LHCb::VertexBase const* const> pileups,
                                                       const IGeometryInfo&                      geometry ) const override;

private:
  Gaudi::Property<double> m_minDistPhi{this, "MinDistPhi", 0.005,
                                       "Tagging particle requirement: Minimum phi distance to B candidate."};
  Gaudi::Property<double> m_minIpSigTagPileUpVertices{
      this, "MinIpSigTagPileUpVertices", 3.0,
      "Tagging particle requirement: Minimim IP significance wrt to all pileup vertices."};
  Gaudi::Property<double> m_maxSharedHitFraction{this, "MaxSharedHitFraction", 0.3,
                                                 "Maximum allowed fraction of hits shared between tracks."};

  ToolHandle<IDistanceCalculator> m_distCalcTool{this, "DistanceCalculatorTool", "LoKi::DistanceCalculator"};
  ToolHandle<ICheckOverlap>       m_overlapTool{this, "OverlapTool", "CheckOverlap"};
  ToolHandle<IRelatedPVFinder>    m_relatedPVTool{
      this, "RelatedPVFinderTool", "GenericParticle2PVRelator__p2PVWithIPChi2_OnlineDistanceCalculatorName_"};
  ToolHandle<IPVReFitter>          m_pvRefitterTool{this, "PVRefitterTool", "LoKi::PVReFitter"};
  ToolHandle<IVertexFit>           m_vertexFitTool{this, "VertexFitterTool", "LoKi::VertexFitter"};
  ToolHandle<IParticleDescendants> m_descendantsTool{this, "ParticleDescentantsTool", "ParticleDescendants"};
};
