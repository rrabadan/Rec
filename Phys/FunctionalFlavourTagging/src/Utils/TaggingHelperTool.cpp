/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TaggingHelperTool.h"
#include "TaggingHelper.h"

#include <cmath>

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TaggingHelperTool )

StatusCode TaggingHelperTool::initialize() {
  StatusCode sc = GaudiTool::initialize();

  m_distCalcTool.retrieve().ignore();
  m_overlapTool.retrieve().ignore();
  m_relatedPVTool.retrieve().ignore();
  m_vertexFitTool.retrieve().ignore();

  return sc;
}

std::optional<std::pair<double, double>> TaggingHelperTool::calcIPWithErr( const LHCb::Particle&   part,
                                                                           const LHCb::VertexBase& vertex,
                                                                           const IGeometryInfo&    geometry ) const {
  double     ip     = -100.0;
  double     iperr  = 0.0;
  double     ipChi2 = 0;
  StatusCode sc     = m_distCalcTool->distance( &part, &vertex, ip, ipChi2, geometry );
  if ( sc ) {
    iperr = ip / std::sqrt( ipChi2 );
    return std::make_pair( ip, iperr );
  }
  return std::nullopt;
}

std::optional<std::pair<double, double>>
TaggingHelperTool::calcMinIPWithErr( const LHCb::Particle& part, LHCb::span<LHCb::VertexBase const* const> vertices,
                                     const IGeometryInfo& geometry ) const {
  double minIp    = std::numeric_limits<double>::max();
  double minIpErr = 0.0;

  for ( auto const* vertex : vertices ) {
    auto tempIP = calcIPWithErr( part, *vertex, geometry );
    if ( tempIP.has_value() ) {
      if ( tempIP.value().first < minIp ) {
        minIp    = tempIP.value().first;
        minIpErr = tempIP.value().second;
      }
    }
  }
  if ( minIp < std::numeric_limits<double>::max() )
    return std::make_pair( minIp, minIpErr );
  else
    return std::nullopt;
}

std::optional<std::pair<double, double>> TaggingHelperTool::calcIPWithChi2( const LHCb::Particle&   part,
                                                                            const LHCb::VertexBase& vertex,
                                                                            const IGeometryInfo&    geometry ) const {
  double     ip     = -100.0;
  double     ipChi2 = 0;
  StatusCode sc     = m_distCalcTool->distance( &part, &vertex, ip, ipChi2, geometry );
  if ( sc ) { return std::make_pair( ip, ipChi2 ); }
  return std::nullopt;
}

std::optional<std::pair<double, double>>
TaggingHelperTool::calcMinIPWithChi2( const LHCb::Particle& part, LHCb::span<LHCb::VertexBase const* const> vertices,
                                      const IGeometryInfo& geometry ) const {
  double minIp     = std::numeric_limits<double>::max();
  double minIpChi2 = 0.0;

  for ( auto const* vertex : vertices ) {
    auto tempIP = calcIPWithChi2( part, *vertex, geometry );
    if ( tempIP.has_value() ) {
      if ( tempIP.value().first < minIp ) {
        minIp     = tempIP.value().first;
        minIpChi2 = tempIP.value().second;
      }
    }
  }
  if ( minIp < std::numeric_limits<double>::max() )
    return std::make_pair( minIp, minIpChi2 );
  else
    return std::nullopt;
}

double TaggingHelperTool::calcMinIPChi2( const LHCb::Particle&                          part,
                                         const LHCb::Event::PV::PrimaryVertexContainer& vertices,
                                         const IGeometryInfo&                           geometry ) const {
  double minIpChi2 = std::numeric_limits<double>::max();

  for ( const auto& vertex : vertices ) {
    auto tempIP = calcIPWithChi2( part, vertex, geometry );
    if ( tempIP.has_value() ) {
      if ( tempIP.value().second < minIpChi2 ) { minIpChi2 = tempIP.value().second; }
    }
  }
  return minIpChi2;
}

std::optional<std::pair<double, double>>
TaggingHelperTool::calcMinDocaWithErr( const LHCb::Particle& particle, const LHCb::Particle& referenceParticleA,
                                       const LHCb::Particle& referenceParticleB, const IGeometryInfo& geometry ) const {
  double docaA    = 0;
  double docaErrA = 0;
  double docaB    = 0;
  double docaErrB = 0;

  const StatusCode scA = m_distCalcTool->distance( &particle, &referenceParticleA, docaA, docaErrA, geometry );
  const StatusCode scB = m_distCalcTool->distance( &particle, &referenceParticleB, docaB, docaErrB, geometry );

  double doca;
  double docaErr;
  if ( docaA < docaB ) {
    doca    = docaA;
    docaErr = docaErrA;
  } else {
    doca    = docaB;
    docaErr = docaErrB;
  }

  if ( scA && scB ) {
    return std::make_pair( doca, docaErr );
  } else {
    return std::nullopt;
  }
}

bool TaggingHelperTool::passesCommonPreSelection( const LHCb::Particle& tagCand, const LHCb::Particle& bCand,
                                                  LHCb::span<LHCb::VertexBase const* const> pileups,
                                                  const IGeometryInfo&                      geometry ) const {
  if ( m_overlapTool->foundOverlap( &tagCand, &bCand ) ) return false;

  LHCb::Particle::ConstVector bDaughters = m_descendantsTool->descendants( &bCand );
  bDaughters.push_back( &bCand );

  double minPhi = std::numeric_limits<double>::max();
  for ( const auto& bDaughter : bDaughters ) {
    if ( cloneCategory( tagCand, *bDaughter, m_maxSharedHitFraction ) ) return false;

    double tempPhi = fabs( TaggingHelper::dPhi( tagCand.momentum().phi(), bDaughter->momentum().phi() ) );
    if ( tempPhi < minPhi ) minPhi = tempPhi;
  }
  if ( minPhi < m_minDistPhi ) return false;

  if ( pileups.size() == 0 ) return true;
  std::optional<std::pair<double, double>> puMinIP = calcMinIPWithChi2( tagCand, pileups, geometry );
  if ( !puMinIP.has_value() ) return false;
  if ( std::sqrt( puMinIP.value().second ) < m_minIpSigTagPileUpVertices ) return false;

  return true;
}

TaggingHelper::CloneStatus TaggingHelperTool::cloneCategory( const LHCb::Particle& p1, const LHCb::Particle& p2,
                                                             double maxSharedHitFraction ) const {
  if ( TaggingHelper::areHitFractionClones( p1, p2, maxSharedHitFraction ) ) return TaggingHelper::CloneHitContent;
  if ( TaggingHelper::areMomentumParameterClones( p1, p2 ) ) return TaggingHelper::CloneTrackParams;
  if ( TaggingHelper::areSlopeClones( p1, p2 ) ) return TaggingHelper::MattTxTy;
  if ( TaggingHelper::areSlopeChargeClones( p1, p2 ) ) return TaggingHelper::MattTxTyQp;
  if ( TaggingHelper::areConvertedGamma( p1, p2 ) ) return TaggingHelper::ConvertedGamma;
  return TaggingHelper::DifferentParticles;
}

bool TaggingHelperTool::hasOverlap( const LHCb::Particle& tagCand, const LHCb::Particle& bCand ) const {
  return m_overlapTool->foundOverlap( &tagCand, &bCand );
}

std::optional<LHCb::Vertex> TaggingHelperTool::fitVertex( const LHCb::Particle& tagCand, const LHCb::Particle& bCand,
                                                          const IGeometryInfo& geometry ) const {
  LHCb::Vertex vtx;
  StatusCode   sc = m_vertexFitTool->fit( vtx, tagCand, bCand, geometry );
  if ( sc.isFailure() )
    return std::nullopt;
  else
    return vtx;
}
