/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/IAlgTool.h"

// from LHCb
#include "DetDesc/IGeometryInfo.h"
#include "Event/Particle.h"
#include "Event/PrimaryVertices.h"

// from FunctionalFlavourTagging
#include "TaggingHelper.h"

class ITaggingHelperTool : virtual public IAlgTool {

  DeclareInterfaceID( ITaggingHelperTool, 3, 0 );

public:
  virtual std::optional<std::pair<double, double>> calcIPWithErr( const LHCb::Particle&, const LHCb::VertexBase&,
                                                                  const IGeometryInfo& ) const  = 0;
  virtual std::optional<std::pair<double, double>> calcIPWithChi2( const LHCb::Particle&, const LHCb::VertexBase&,
                                                                   const IGeometryInfo& ) const = 0;
  virtual std::optional<std::pair<double, double>>
  calcMinIPWithErr( const LHCb::Particle&, LHCb::span<LHCb::VertexBase const* const>, const IGeometryInfo& ) const = 0;
  virtual std::optional<std::pair<double, double>>
  calcMinIPWithChi2( const LHCb::Particle&, LHCb::span<LHCb::VertexBase const* const>, const IGeometryInfo& ) const = 0;

  virtual double calcMinIPChi2( const LHCb::Particle& part, const LHCb::Event::PV::PrimaryVertexContainer& vertices,
                                const IGeometryInfo& geometry ) const                               = 0;
  virtual std::optional<std::pair<double, double>> calcMinDocaWithErr( const LHCb::Particle&, const LHCb::Particle&,
                                                                       const LHCb::Particle&,
                                                                       const IGeometryInfo& ) const = 0;

  virtual bool hasOverlap( const LHCb::Particle&, const LHCb::Particle& ) const = 0;

  virtual std::optional<LHCb::Vertex> fitVertex( const LHCb::Particle&, const LHCb::Particle&,
                                                 const IGeometryInfo& ) const = 0;

  virtual TaggingHelper::CloneStatus cloneCategory( const LHCb::Particle&, const LHCb::Particle&, double ) const = 0;

  virtual bool passesCommonPreSelection( const LHCb::Particle&, const LHCb::Particle&,
                                         const LHCb::span<LHCb::VertexBase const* const>,
                                         const IGeometryInfo& ) const = 0;
};
