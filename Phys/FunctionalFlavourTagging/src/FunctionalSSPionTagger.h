/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Transformer.h"

// from LHCb
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/Tagger.h"

// from FunctionalFlavourTagging
#include "../Classification/SSPion/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1.h"
#include "Utils/ITaggingHelperTool.h"
#include "Utils/TaggingHelper.h"
#include "Utils/TaggingHelperTool.h"

class FunctionalSSPionTagger
    : public LHCb::Algorithm::Transformer<LHCb::FlavourTags( const LHCb::Particles&, const LHCb::Particle::Range&,
                                                             const LHCb::Event::PV::PrimaryVertexContainer&,
                                                             const DetectorElement& ),
                                          LHCb::DetDesc::usesConditions<DetectorElement>> {
public:
  /// Standard constructor
  FunctionalSSPionTagger( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"BCandidates", ""}, KeyValue{"TaggingPions", ""}, KeyValue{"PrimaryVertices", ""},
                      KeyValue{"StandardGeometry", LHCb::standard_geometry_top}},
                     KeyValue{"OutputFlavourTags", ""} ) {}

  LHCb::FlavourTags operator()( const LHCb::Particles& bCandidates, const LHCb::Particle::Range& taggingPions,
                                const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                const DetectorElement& ) const override;

  std::optional<LHCb::Tagger> performTagging( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingPions,
                                              const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                              const IGeometryInfo&                           geometry ) const;

  LHCb::Tagger::TaggerType taggerType() const { return LHCb::Tagger::TaggerType::SS_PionBDT; }

private:
  Gaudi::Property<double> m_maxDistPhi{this, "MaxDistPhi", 1.1,
                                       "Tagging particle requirement: Maximum phi distance to B candidate"};
  Gaudi::Property<double> m_maxDeltaEta{this, "MaxDeltaEta", 1.2,
                                        "Tagging particle requirement: Maximum eta distance to B candidate"};
  Gaudi::Property<double> m_maxIpSigTagBestPV{this, "MaxIpSigTagBestPV", 4.0,
                                              "Tagging particle requirement: Maximum IP significance wrt to best PV"};
  Gaudi::Property<double> m_maxDQB0Pion{this, "MaxDQB0Pion", 1.2 * Gaudi::Units::GeV, "I don't know what this is"};
  Gaudi::Property<double> m_minPTB0Pion{this, "MaxPTB0Pion", 3.0 * Gaudi::Units::GeV,
                                        "Maximum transverse momentum of the B0-Pion combination"};
  Gaudi::Property<double> m_minChi2B0PionVertex{this, "MinChi2B0PionVertex", 100};
  Gaudi::Property<double> m_minPionProb{this, "MinPionProb", 0.5};

  mutable Gaudi::Accumulators::SummingCounter<> m_BCount{this, "#BCandidates"};
  mutable Gaudi::Accumulators::SummingCounter<> m_pionCount{this, "#taggingPions"};
  mutable Gaudi::Accumulators::SummingCounter<> m_FTCount{this, "#goodFlavourTags"};

  ToolHandle<ITaggingHelperTool> m_taggingHelperTool{this, "TaggingHelper", "TaggingHelperTool"};
  // ToolHandle<IVertexFit>              m_vertexFitTool{this, "VertexFitter", "LoKi::VertexFitter"};

  std::unique_ptr<ITaggingClassifier> m_classifier = std::make_unique<SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1>();

  double m_polP0 = 0.4523;
  double m_polP1 = -0.117;
  double m_polP2 = -0.122;
  double m_polP3 = -0.081;
};
