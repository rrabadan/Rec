/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GetFlavourTaggingParticles.h"

DECLARE_COMPONENT( GetFlavourTaggingParticles )

LHCb::Particle::Selection GetFlavourTaggingParticles::
                          operator()( const LHCb::Particle::Range& bCandidates, const LHCb::Particle::Range& taggingParticles,
            const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
            const DetectorElement&                         lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();

  LHCb::Particle::Selection selectedParticles;

  for ( const auto* particle : taggingParticles ) {
    const LHCb::VertexBase* bestParticlePV = LHCb::bestPV( primaryVertices, *particle );
    bool                    hasSamePV      = false;

    for ( const auto* bCand : bCandidates ) {
      const LHCb::VertexBase* bestbCandPV = LHCb::bestPV( primaryVertices, *bCand );
      if ( bestbCandPV == bestParticlePV ) {
        selectedParticles.insert( particle );
        hasSamePV = true;
        break;
      }
    }

    if ( !hasSamePV ) {
      double minIPChi2 = m_taggingHelperTool->calcMinIPChi2( *particle, primaryVertices, geometry );
      if ( minIPChi2 > m_minIpChi2 ) { selectedParticles.insert( particle ); }
    }
  }

  m_inCount += taggingParticles.size();
  m_outCount += selectedParticles.size();
  return selectedParticles;
}
