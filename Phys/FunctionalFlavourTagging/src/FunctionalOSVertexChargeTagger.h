/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"

// from Event
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/Tagger.h"

// from FunctionalFlavourTagging
#include "../Classification/OSVertexCharge/OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0.h"
#include "Utils/ITaggingHelperTool.h"
#include "Utils/TaggingHelper.h"
#include "Utils/TaggingHelperTool.h"

class FunctionalOSVertexChargeTagger
    : public Gaudi::Functional::Transformer<LHCb::FlavourTags( const LHCb::Particles&, const LHCb::Particle::Range&,
                                                               const LHCb::Event::PV::PrimaryVertexContainer&,
                                                               const DetectorElement& ),
                                            LHCb::DetDesc::usesConditions<DetectorElement>> {
public:
  /// Standard constructor
  FunctionalOSVertexChargeTagger( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"BCandidates", ""}, KeyValue{"TaggingParticles", ""}, KeyValue{"PrimaryVertices", ""},
                      KeyValue{"StandardGeometry", LHCb::standard_geometry_top}},
                     KeyValue{"OutputFlavourTags", ""} ) {}

  LHCb::FlavourTags operator()( const LHCb::Particles& bCandidates, const LHCb::Particle::Range& taggingParticles,
                                const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                const DetectorElement&                         geometry ) const override;

  std::optional<LHCb::Tagger> performTagging( const LHCb::Particle&                          bCand,
                                              const LHCb::Particle::Range&                   taggingParticles,
                                              const LHCb::Event::PV::PrimaryVertexContainer& primaryVertices,
                                              const IGeometryInfo&                           geometry ) const;

  LHCb::Tagger::TaggerType taggerType() const { return LHCb::Tagger::TaggerType::OSVtxCh; }

private:
  // weights for particles from secondary vertex
  Gaudi::Property<double> m_vertexChargeWeightPowerK{this, "VertexChargeWeightPowerK", 0.55,
                                                     "Power of particle PT weights to sum vertex charge."};

  // cut values for secondary vertex
  Gaudi::Property<double> m_minVertexCharge{this, "MinVertexCharge", 0.2,
                                            "Tagging vertex requirement: Minimum vertex charge."};
  Gaudi::Property<double> m_minVertexM{this, "MinVertexM", 0.6, "Tagging vertex requirement: Minimum mass."};
  Gaudi::Property<double> m_minVertexP{this, "MinVertexP", 8., "Tagging vertex requirement: Minimum P."};
  Gaudi::Property<double> m_minVertexMeanPt{this, "MinVertexMeanPt", 0.,
                                            "Tagging vertex requirement: Minimum vertex particles mean PT."};
  Gaudi::Property<double> m_minVertexSumPt{this, "MinVertexSumPt", 2.2,
                                           "Tagging vertex requirement: Minimum vertex particles summed PT."};
  Gaudi::Property<double> m_minVertexSumIpSig{this, "MinVertexSumIpSig", 0.,
                                              "Tagging vertex requirement: Minimum vertex particles summed IpSig."};
  Gaudi::Property<double> m_maxVertexSumDoca{
      this, "MaxVertexSumDoca", 0.5,
      "Tagging vertex requirement: Maximum vertex particles summed DOCA (relative to vertex)."};

  // cut values for secondary vertex seed and particles
  Gaudi::Property<double> m_minSeedVertexProb{this, "MinSeedVertexProb", 0.42,
                                              "Seed vertex requirement: Minimum likelihood."};
  Gaudi::Property<double> m_maxTrackGhostProb{
      this, "MaxTrackGhostProb", 0.37, "Secondary vertex particle requirement: Maximum track ghost probability."};
  Gaudi::Property<double> m_maxAddedTrackChi2ndof{
      this, "MaxAddedTrackChi2ndof", 3.0, "Secondary vertex added particle requirement: Maximum track chi2 per ndof."};
  Gaudi::Property<double> m_minSeedPt{this, "MinSeedPt", 0.1, "Vertex seed particle requirement: Minimum PT."};
  Gaudi::Property<double> m_maxLongSeedTrackChi2ndof{this, "MaxLongSeedTrackChi2ndof", 2.5,
                                                     "Vertex seed particle requirement: Maximum long track chi2ndof."};
  Gaudi::Property<double> m_maxUpstreamSeedTrackChi2ndof{
      this, "MaxUpstreamSeedTrackChi2ndof", 5.0, "Vertex seed particle requirement: Maximum upstream track chi2ndof."};
  Gaudi::Property<double> m_minSeedIpSig{this, "MinSeedIpSig", 2.5,
                                         "Vertex seed particle requirement: Minimum IpSig relative to PV."};

  Gaudi::Property<double> m_minTwoSeedDeltaPhi{
      this, "MinTwoSeedDeltaPhi", 0., "Vertex seed particle requirement: Minimum phi distance between two seeds."};

  mutable Gaudi::Accumulators::SummingCounter<> m_BCount{this, "#BCandidates"};
  mutable Gaudi::Accumulators::SummingCounter<> m_particleCount{this, "#taggingParticles"};
  mutable Gaudi::Accumulators::SummingCounter<> m_FTCount{this, "#goodFlavourTags"};

  ToolHandle<ITaggingHelperTool>   m_taggingHelperTool{this, "TaggingHelper", "TaggingHelperTool"};
  ToolHandle<IParticleDescendants> m_particleDescendantTool{this, "ParticleDescendantTool", "ParticleDescendants"};

  std::unique_ptr<ITaggingClassifier> m_classifier = std::make_unique<OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0>();

  std::optional<LHCb::Vertex> buildSecondaryVertex( const LHCb::VertexBase&      primaryVertex,
                                                    const LHCb::Particle::Range& taggingParticles,
                                                    const IGeometryInfo&         geometry ) const;
  bool                        passVertexSeedTrackCut( const LHCb::Particle* candidate ) const;
  bool                        passVertexSeedIpCut( const double absIp, const double ipErr, const double ipChi2 ) const;
  double                      polynomail( double x, std::vector<double> coefficients ) const;
  double combine( double p1, double p2, double p3, double p4, double p5, double p6, double p7 ) const;

  // double m_polP0 = 0.025841;
  // double m_polP1 = -0.21766;
};
