###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/FunctionalFlavourTagging
-------------------
#]=======================================================================]


gaudi_add_module(FunctionalFlavourTagging
            SOURCES
                    src/AdvancedCloneKiller.cpp
                    src/FunctionalOSMuonTagger.cpp
                    src/FunctionalOSKaonTagger.cpp
                    src/FunctionalOSElectronTagger.cpp
                    src/FunctionalSSKaonTagger.cpp
                    src/FunctionalSSPionTagger.cpp
                    src/FunctionalSSProtonTagger.cpp
                    src/FunctionalOSVertexChargeTagger.cpp
                    src/Utils/TaggingHelperTool.cpp
		    src/Utils/ParticleTaggerAlg.cpp
                    src/GetFlavourTaggingParticles.cpp
                    src/FlavourTagsMerger.cpp
                    Classification/TaggingClassifierTMVA.cpp
                    Classification/OSMuon/weights/OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0.cpp
                    Classification/OSElectron/weights/OSElectron_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0.cpp
                    Classification/OSKaon/weights/OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v2r0.cpp
                    Classification/SSKaon/SSKaon_BDTeta_dev.cpp
                    Classification/SSKaon/SSKaon_BDTseltracks_dev.cpp
                    Classification/SSPion/weights/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_0-299.cpp
                    Classification/SSPion/weights/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_1200-1499.cpp
                    Classification/SSPion/weights/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_1500-1799.cpp
                    Classification/SSPion/weights/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_1800-2099.cpp
                    Classification/SSPion/weights/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_2100-2399.cpp
                    Classification/SSPion/weights/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_2400-2699.cpp
                    Classification/SSPion/weights/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_2700-2999.cpp
                    Classification/SSPion/weights/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_300-599.cpp
                    Classification/SSPion/weights/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_600-899.cpp
                    Classification/SSPion/weights/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_900-1199.cpp
                    Classification/SSProton/weights/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_0-299.cpp
                    Classification/SSProton/weights/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_1200-1499.cpp
                    Classification/SSProton/weights/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_1500-1799.cpp
                    Classification/SSProton/weights/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_1800-2099.cpp
                    Classification/SSProton/weights/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_2100-2399.cpp
                    Classification/SSProton/weights/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_2400-2699.cpp
                    Classification/SSProton/weights/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_2700-2999.cpp
                    Classification/SSProton/weights/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_300-599.cpp
                    Classification/SSProton/weights/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_600-899.cpp
                    Classification/SSProton/weights/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1_900-1199.cpp
                    Classification/OSVertexCharge/weights/OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0.cpp
            LINK
                    Boost::headers
                    Boost::log
                    Boost::regex
                    Gaudi::GaudiAlgLib
                    Gaudi::GaudiKernel
                    LHCb::CaloFutureUtils
                    LHCb::LHCbKernel
                    LHCb::MCEvent
                    LHCb::MCInterfaces
                    LHCb::PhysEvent
                    LHCb::PhysInterfacesLib
                    LHCb::RecEvent
                    LHCb::TrackEvent
                    Rec::DaVinciMCKernelLib
                    Rec::DaVinciKernelLib
                    Rec::TrackKernel
                    Rec::RecInterfacesLib
                    ROOT::Core
                    ROOT::GenVector
                    ROOT::MathCore
                    ROOT::TMVA
        )

# gaudi_install(PYTHON)

# Disable all but leak sanitizers for all factory compilations that include TMVA
# auto-generated code, as these tend to explode compilation times..
if(CMAKE_BUILD_TYPE MATCHES ".*San$")
    message(STATUS "Disabling ${CMAKE_BUILD_TYPE} sanitizer for TMVA generated code.")
    file(GLOB_RECURSE FT_TMVA_FILES "src/Taggers/*.cpp")
    foreach(TMVA_FILE IN LISTS FT_TMVA_FILES)
        # This disables all sanitizers for all files under src/Taggers
        # Probably more than really required, but easiest for now.
        set_property(SOURCE ${TMVA_FILE} APPEND PROPERTY COMPILE_OPTIONS "-fno-sanitize=all")
    endforeach()
endif()
