###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV

from Moore import options, run_moore
from Moore.config import HltLine

from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import upfront_reconstruction, make_pvs
from RecoConf.hlt1_tracking import require_pvs, default_ft_decoding_version

from Hlt2Conf.standard_particles import make_has_rich_long_pions
from Hlt2Conf.lines.bandq.builders import dimuon, charged_hadrons, b_hadrons
from Hlt2Conf.algorithms import require_all, ParticleFilterWithPVs

from PyConf.Algorithms import FunctionalSSPionTagger

default_ft_decoding_version.global_bind(value=2)


def make_tagging_pions(make_particles=make_has_rich_long_pions,
                       pvs=make_pvs,
                       p_min=2 * GeV,
                       p_max=200 * GeV,
                       pt_min=0.4 * GeV,
                       pt_max=10 * GeV,
                       eta_min=1.068,
                       pidp_max=5,
                       pidk_max=5,
                       ghostprob_max=0.5):
    code = require_all('P > {p_min}', 'P < {p_max}', 'PT > {pt_min}',
                       'PT < {pt_max}', 'PIDp < {pidp_max}',
                       'PIDK < {pidk_max}', 'TRGHOSTPROB < {ghostprob_max}',
                       'ETA > {eta_min}').format(
                           p_min=p_min,
                           p_max=p_max,
                           pt_min=pt_min,
                           pt_max=pt_max,
                           eta_min=eta_min,
                           pidp_max=pidp_max,
                           pidk_max=pidk_max,
                           ghostprob_max=ghostprob_max)
    return ParticleFilterWithPVs(make_particles(), pvs(), Code=code)


# make sure we passed GEC and have PV in event
def prefilters():
    return [require_pvs(make_pvs())]


def b2jpsik_line(name='Hlt2BToJpsiKLine', prescale=1):
    jpsi = dimuon.make_jpsi()
    kaons = charged_hadrons.make_detached_kaons()
    b2jpsik = b_hadrons.make_b_hadron(
        particles=[jpsi, kaons], descriptor='[B+ -> J/psi(1S) K+]cc')

    taggingParticles = make_tagging_pions()

    pvs = make_pvs()

    flavourTags = FunctionalSSPionTagger(
        BCandidates=b2jpsik,
        TaggingPions=taggingParticles,
        PrimaryVertices=pvs)

    return [
        HltLine(
            name=name,
            algs=upfront_reconstruction() + prefilters() + [b2jpsik],
            prescale=prescale,
            extra_outputs=[("FlavourTags", flavourTags),
                           ('TaggingPions', taggingParticles)])
    ]


public_tools = [stateProvider_with_simplified_geom()]
options.input_files = [
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00109583/0000/00109583_00000024_2.xdst"
]
options.input_type = 'ROOT'
options.input_raw_format = 4.3
options.evt_max = 100
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20201211'
options.conddb_tag = 'sim-20201218-vc-md100'

# 5. Name of the dst that is going to be written
options.output_file = 'out_B2JpsiK_FT_new.dst'
options.output_type = 'ROOT'

config = run_moore(options, b2jpsik_line, public_tools)

from Moore.tcks import dump_hlt2_configuration
dump_hlt2_configuration(config, "B2JpsiK_FT_new.tck.json")
