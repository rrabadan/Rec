###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import DEBUG, VERBOSE
from GaudiKernel.SystemOfUnits import GeV, MeV

from Moore import options

from RecoConf.hlt1_tracking import require_gec, default_ft_decoding_version
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction, reconstruction
from RecoConf.protoparticles import make_charged_protoparticles

from Hlt2Conf.standard_particles import make_has_rich_long_pions
from Hlt2Conf.lines.onia.builders import dimuon, charged_hadrons, b_hadrons
from Hlt2Conf.algorithms import require_all, ParticleFilterWithPVs

from PyConf.Algorithms import FunctionalSSPionTagger
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic

default_ft_decoding_version.global_bind(value=6)


def taggingPions(make_particles=make_has_rich_long_pions,
                 pvs=make_pvs,
                 p_min=2 * MeV,
                 p_max=200 * GeV,
                 pt_min=0.4 * GeV,
                 pt_max=10 * GeV,
                 eta_min=1.068,
                 pidp_max=5,
                 pidk_max=5,
                 ghostprob_max=0.5):
    code = require_all('P > {p_min}', 'P < {p_max}', 'PT > {pt_min}',
                       'PT < {pt_max}', 'PIDp < {pidp_max}',
                       'PIDK < {pidk_max}', 'TRGHOSTPROB < {ghostprob_max}',
                       'ETA > {eta_min}').format(
                           p_min=p_min,
                           p_max=p_max,
                           pt_min=pt_min,
                           pt_max=pt_max,
                           eta_min=eta_min,
                           pidp_max=pidp_max,
                           pidk_max=pidk_max,
                           ghostprob_max=ghostprob_max)
    return ParticleFilterWithPVs(make_particles(), pvs(), Code=code)


def runFT():
    jpsi = dimuon.make_jpsi()
    kaons = charged_hadrons.make_detached_kaons()
    b2jpsik = b_hadrons.make_onia_Bu(
        particles=[jpsi, kaons], descriptors=['[B+ -> J/psi(1S) K+]cc'])

    print("bCandidates", b2jpsik)
    taggingParticles = taggingPions()
    print("Tagging pions location", taggingParticles)

    pvs = make_pvs()

    sspionTagging = FunctionalSSPionTagger(
        BCandidates=b2jpsik,
        TaggingPions=taggingParticles,
        PrimaryVertices=pvs)

    nodes = upfront_reconstruction()
    nodes += [require_gec()]
    nodes += [b2jpsik]
    nodes += [sspionTagging]

    return CompositeNode(
        'flavourtagging',
        children=nodes,
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True)


from Gaudi.Configuration import FileCatalog
FileCatalog().Catalogs = ["xmlcatalog_file:B2JpsiK.xml"]

options.input_files = [
    'LFN:/lhcb/MC/Upgrade/XDST/00109583/0000/00109583_00000042_2.xdst',
    'LFN:/lhcb/MC/Upgrade/XDST/00109583/0000/00109583_00000007_2.xdst',
    'LFN:/lhcb/MC/Upgrade/XDST/00109583/0000/00109583_00000003_2.xdst',
    'LFN:/lhcb/MC/Upgrade/XDST/00109583/0000/00109583_00000023_2.xdst',
    'LFN:/lhcb/MC/Upgrade/XDST/00109583/0000/00109583_00000036_2.xdst',
    'LFN:/lhcb/MC/Upgrade/XDST/00109583/0000/00109583_00000047_2.xdst'
]
options.input_type = 'ROOT'
options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20190223'
options.conddb_tag = 'sim-20180530-vc-md100'
options.input_raw_format = 4.3
options.output_file = 'myCuteDST.dst'
options.output_type = 'ROOT'

with reconstruction.bind(from_file=True), make_charged_protoparticles.bind(
        enable_muon_id=True):
    config = configure_input(options)
    top_cf_node = runFTuple()
    config.update(configure(options, top_cf_node))
