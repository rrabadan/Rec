###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import DEBUG, VERBOSE
from GaudiKernel.SystemOfUnits import GeV, MeV

from Moore import options

from RecoConf.hlt1_tracking import require_gec, default_ft_decoding_version
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction, reconstruction
from RecoConf.protoparticles import make_charged_protoparticles

from Hlt2Conf.standard_particles import make_mass_constrained_jpsi2mumu, make_has_rich_long_pions
from Hlt2Conf.lines.b_to_charmonia.prefilters import b2cc_prefilters
from Hlt2Conf.lines.b_to_charmonia.builders import b_builder, basic_builder
from Hlt2Conf.algorithms import require_all, ParticleFilterWithPVs
from Hlt2Conf.data_from_file import mc_unpackers

from PyConf.Algorithms import FunctionalSSPionTagger, FlavourTagTuple
from PyConf.Tools import BackgroundCategory, P2MCPFromProtoP, DaVinciSmartAssociator
from PyConf.application import configure_input, configure, make_odin
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf import configurable
from PyConf.components import force_location

from Configurables import NTupleSvc, ApplicationMgr

from Moore.persistence.truth_matching import truth_match_candidates, CHARGED_PP2MC_LOC, NEUTRAL_PP2MC_LOC

default_ft_decoding_version.global_bind(value=6)


def taggingPions(make_particles=make_has_rich_long_pions,
                 pvs=make_pvs,
                 p_min=2 * MeV,
                 p_max=200 * GeV,
                 pt_min=0.4 * GeV,
                 pt_max=10 * GeV,
                 eta_min=1.068,
                 pidp_max=5,
                 pidk_max=5,
                 ghostprob_max=0.5):
    code = require_all('P > {p_min}', 'P < {p_max}', 'PT > {pt_min}',
                       'PT < {pt_max}', 'PIDp < {pidp_max}',
                       'PIDK < {pidk_max}', 'TRGHOSTPROB < {ghostprob_max}',
                       'ETA > {eta_min}').format(
                           p_min=p_min,
                           p_max=p_max,
                           pt_min=pt_min,
                           pt_max=pt_max,
                           eta_min=eta_min,
                           pidp_max=pidp_max,
                           pidk_max=pidk_max,
                           ghostprob_max=ghostprob_max)
    return ParticleFilterWithPVs(make_particles(), pvs(), Code=code)


def runNTuple():
    phi = basic_builder.make_selected_phi()
    jpsi = make_mass_constrained_jpsi2mumu()
    b2jpsiphi = b_builder.make_B2JpsiX(
        particles=[jpsi, phi], descriptors=['B_s0 -> J/psi(1S) phi(1020)'])

    taggingParticles = taggingPions()

    pvs = make_pvs()

    sspionTagging = FunctionalSSPionTagger(
        BCandidates=b2jpsiphi,
        TaggingPions=taggingParticles,
        PrimaryVertices=pvs)

    truthMatching = truth_match_candidates([b2jpsiphi])

    relationsTablesLocations = [CHARGED_PP2MC_LOC, NEUTRAL_PP2MC_LOC]
    p2mcpTool = P2MCPFromProtoP(Locations=relationsTablesLocations)
    bkgTool = BackgroundCategory(P2MCPFromProtoP=p2mcpTool)
    smartAssTool = DaVinciSmartAssociator(
        BackgroundCatTool=bkgTool, P2MCPFromProtoP=p2mcpTool)

    odin_loc = make_odin()
    ftTuple = FlavourTagTuple(
        OdinLocation=odin_loc,
        FlavourTags=sspionTagging,
        MCParticles=truthMatching[0].children[0],
        DaVinciSmartAssociator=smartAssTool)

    nodes = upfront_reconstruction()
    nodes += [require_gec()]
    nodes += [b2jpsiphi]
    nodes += truthMatching[1]
    nodes += [sspionTagging]
    nodes += [ftTuple]

    return CompositeNode(
        'flavourtaggingtuple',
        children=nodes,
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True)


input_files = [
    "root://tmpmmTC0l@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000008_1.xdigi",
    #"root://lhcb-sdpd7.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000006_1.xdigi"
    #"root://x509up_u38339@xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000003_1.xdigi",
    #"root://x509up_u38339@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000004_1.xdigi"
    #"root://x509up_u38339@xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000005_1.xdigi",
    #"root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122700/0000/00122700_00000001_1.xdigi",
    #"root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000002_1.xdigi",
    #"root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00122700/0000/00122700_00000007_1.xdigi",
    #"root://tmpmmTC0l@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000008_1.xdigi",
    #"root://x509up_u38339@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000011_1.xdigi",
    #"root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00122700/0000/00122700_00000013_1.xdigi"
]
options.input_files = input_files
options.input_type = 'ROOT'
options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20201211'
options.conddb_tag = 'sim-20201218-vc-md100'

options.output_file = 'myCuteDST.dst'
options.output_type = 'ROOT'

ApplicationMgr().HistogramPersistency = "ROOT"
ntSvc = NTupleSvc()
ntSvc.Output = [
    "FILE1 DATAFILE='Bs2JpsiPhi_FlavourTagTuple.root' OPT='NEW' TYP='ROOT'"
]
ApplicationMgr().ExtSvc += [ntSvc]

with reconstruction.bind(from_file=False), make_charged_protoparticles.bind(
        enable_muon_id=True):
    config = configure_input(options)
    top_cf_node = runNTuple()
    config.update(configure(options, top_cf_node))
