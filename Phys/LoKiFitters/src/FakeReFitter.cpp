/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "Kernel/IParticleReFitter.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IService.h"

/**
 *  This file is a part of
 *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
 *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2008-03-16
 */

namespace LoKi {

  /** @class FakeReFitter
   *  The most trivial implementation of
   *  the abstract interface IParticleReFitter
   *
   *  It is "refitter" which essentially does nothing.
   *
   *  @see IParticleReFitter
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2008-03-16
   */
  struct FakeReFitter : extends<GaudiTool, IParticleReFitter> {

    /** The basic method for "refit" of the particle
     *  @see IParticleReFitter
     */
    StatusCode reFit( LHCb::Particle& particle, IGeometryInfo const& geometry ) const override;

    /// initialize the tool
    StatusCode initialize() override;

    using base_class::base_class;
  };

} //                                                      end of namespace LoKi

StatusCode LoKi::FakeReFitter::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return sc; }
  // get LoKi-service
  svc<IService>( "LoKiSvc", true );
  return StatusCode::SUCCESS;
}

StatusCode LoKi::FakeReFitter::reFit( LHCb::Particle& particle, IGeometryInfo const& ) const {
  // erase the corresponding info
  if ( particle.hasInfo( LHCb::Particle::Chi2OfParticleReFitter ) ) {
    particle.eraseInfo( LHCb::Particle::Chi2OfParticleReFitter );
  }
  return StatusCode::SUCCESS;
}

/// Declaration of the Tool Factory
DECLARE_COMPONENT( LoKi::FakeReFitter )
