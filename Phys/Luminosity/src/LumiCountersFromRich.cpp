/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiAlg/Transformer.h"

#include "Event/HltLumiSummary.h"
#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichSmartIDs.h"

/** @class LumiCountersFromRich
 *
 * @brief Create an HltLumiSummary object filled with counters based on a DAQ::DecodedData object.
 *
 * Takes a single DAQ::DecodedData as input and outputs an HltLumiSummary object,
 * which maps counter names to values.
 *
 * @see LumiCounterMerger for merging multiple summary objects into one.
 */
class LumiCountersFromRich
    : public Gaudi::Functional::Transformer<LHCb::HltLumiSummary( const Rich::Future::DAQ::DecodedData& )> {
public:
  using base_class = Gaudi::Functional::Transformer<LHCb::HltLumiSummary( const Rich::Future::DAQ::DecodedData& )>;

  LumiCountersFromRich( const std::string& name, ISvcLocator* pSvc )
      : base_class( name, pSvc, {"InputContainer", ""}, {"OutputSummary", ""} ) {}

  StatusCode initialize() override {
    StatusCode sc = base_class::initialize();
    return sc;
  }

  LHCb::HltLumiSummary operator()( Rich::Future::DAQ::DecodedData const& data ) const override {

    int hits[48] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    // Loop over RICHes
    for ( const auto rich : Rich::detectors() ) {

      // data for this RICH
      const auto& rD = data[rich];

      // sides per RICH
      for ( const auto& pD : rD ) {

        // PD modules per side
        for ( const auto& mD : pD ) {

          // PDs per module
          for ( const auto& PD : mD ) {

            // PD ID
            const auto pdID = PD.pdID();
            if ( pdID.isValid() ) {

              // Determine the index of the counter
              unsigned counterIndex = 16u * pdID.rich();

              if ( pdID.rich() == 0 ) {
                if ( pdID.panelLocalModuleColumn() < 6 || pdID.panelLocalModuleColumn() > 9 ) continue;
                if ( pdID.columnLocalModuleNum() < 2 || pdID.columnLocalModuleNum() > 3 ) continue;
                counterIndex += 2u * ( pdID.panelLocalModuleColumn() - 6u );
                counterIndex += pdID.columnLocalModuleNum() - 2u;
              } else {
                if ( pdID.panelLocalModuleColumn() < 5 || pdID.panelLocalModuleColumn() > 8 ) continue;
                if ( pdID.columnLocalModuleNum() < 1 || pdID.columnLocalModuleNum() > 4 ) continue;
                counterIndex += 4u * ( pdID.panelLocalModuleColumn() - 5u );
                counterIndex += pdID.columnLocalModuleNum() - 1u;
              }

              // Vector of SmartIDs
              const auto& rawIDs = PD.smartIDs();
              hits[counterIndex] += rawIDs.size();
            }
          }
        }
      }
    }

    LHCb::HltLumiSummary summary{};
    for ( auto i = 0u; i < 48; ++i ) { summary.addInfo( m_counterBaseName + m_counterNames[i], hits[i] ); }

    return summary;
  }

private:
  Gaudi::Property<std::string> m_counterBaseName{this, "CounterBaseName", "", "Prefix for luminosity counter names"};
  Gaudi::Property<std::vector<std::string>> m_counterNames{
      this,
      "CounterNames",
      {"1S1C6M2Hits", "1S1C6M3Hits", "1S1C7M2Hits", "1S1C7M3Hits", "1S1C8M2Hits", "1S1C8M3Hits", "1S1C9M2Hits",
       "1S1C9M3Hits", "1S2C6M2Hits", "1S2C6M3Hits", "1S2C7M2Hits", "1S2C7M3Hits", "1S2C8M2Hits", "1S2C8M3Hits",
       "1S2C9M2Hits", "1S2C9M3Hits", "2S1C5M1Hits", "2S1C5M2Hits", "2S1C5M3Hits", "2S1C5M4Hits", "2S1C6M1Hits",
       "2S1C6M2Hits", "2S1C6M3Hits", "2S1C6M4Hits", "2S1C7M1Hits", "2S1C7M2Hits", "2S1C7M3Hits", "2S1C7M4Hits",
       "2S1C8M1Hits", "2S1C8M2Hits", "2S1C8M3Hits", "2S1C8M4Hits", "2S2C5M1Hits", "2S2C5M2Hits", "2S2C5M3Hits",
       "2S2C5M4Hits", "2S2C6M1Hits", "2S2C6M2Hits", "2S2C6M3Hits", "2S2C6M4Hits", "2S2C7M1Hits", "2S2C7M2Hits",
       "2S2C7M3Hits", "2S2C7M4Hits", "2S2C8M1Hits", "2S2C8M2Hits", "2S2C8M3Hits", "2S2C8M4Hits"},
      "Names of the counters"};
  Gaudi::Property<std::vector<unsigned>> m_counterMax{
      this,
      "CounterMax",
      {1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023,
       1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023,
       1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023},
      "Required maximum values of the counters"};
  Gaudi::Property<std::map<std::string, std::pair<double, double>>> m_counterShiftAndScale{
      this,
      "CounterShiftAndScale",
      {},
      "Optional shift and scale factors used to process counters when encoding to a LumiSummary bank"};
};

DECLARE_COMPONENT( LumiCountersFromRich )
