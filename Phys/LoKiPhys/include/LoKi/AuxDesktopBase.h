/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/IDVAlgorithm.h"

#include "LoKi/AuxFunBase.h"
#include "LoKi/Interface.h"

namespace LoKi {

  /**
   *  The helper class to implement many "context-dependent" functors
   *  @author Vanya BELYAEV Ivan.Belyaev@physics.syr.edu
   *  @date   2008-01-16
   */
  class GAUDI_API AuxDesktopBase : public virtual LoKi::AuxFunBase {
  public:
    /// default constructor (invalid desktop!)
    AuxDesktopBase() = delete;
    /// constructor form the desktop
    AuxDesktopBase( const IDVAlgorithm* desktop );
    /// constructor from the desktop
    AuxDesktopBase( const LoKi::Interface<IDVAlgorithm>& desktop );
    /// copy constrictor
    AuxDesktopBase( const AuxDesktopBase& right );
    /// destructor
    virtual ~AuxDesktopBase();

    /// get the desktop
    const LoKi::Interface<IDVAlgorithm>& desktop() const { return m_desktop; }
    /// get the desktop
    IDVAlgorithm* getDesktop() const { return desktop(); }

    /// set the desktop
    void setDesktop( const IDVAlgorithm* d ) { m_desktop = d; }
    /// set the desktop
    void setDesktop( const LoKi::Interface<IDVAlgorithm>& d ) { m_desktop = d; }

    /// check the desktop
    bool validDesktop() const { return m_desktop.validPointer(); }

    /// load the desktop
    StatusCode loadDesktop() const;

    // cast to desktop
    operator const LoKi::Interface<IDVAlgorithm>&() const { return desktop(); }

    /// get "the best related vertex"
    const LHCb::VertexBase* bestVertex( const LHCb::Particle* p, IGeometryInfo const& geometry ) const;
    /// get all primary vertices
    LHCb::RecVertex::Range primaryVertices() const;

    /// notify that we need he context algorithm
    static bool context_dvalgo() { return true; }

    /// allow_late_desktop aquire ?
    static bool allow_late_desktop_acquire();

    /// allow_late_desktop aquire ?
    static bool allow_late_desktop_acquire( const bool allow );

  private:
    /// the desktop itself
    mutable LoKi::Interface<IDVAlgorithm> m_desktop; // the desktop itself

    /// allow late acquire of desktop?
    static bool s_allow_late_desktop_acquire;
  };

} // namespace LoKi
