/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_VERTICES5_H
#  define LOKI_VERTICES5_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/AuxDesktopBase.h"
#  include "LoKi/PhysTypes.h"
#  include "LoKi/Vertices1.h"
// ============================================================================
/** @file
 *  Collection of "context-dependent" functors, needed for the
 *  new framework "CombineParticles", developed by Juan PALACIOS,
 *   Patrick KOPPENBURG and Gerhard RAVEN.
 *
 *  Essentially all these functord depends on "event-data" and
 *  get the nesessary "context-dependent" data from Algorithm Context Service
 *
 *  The basic ingredients here:
 *   - LoKi Service
 *   - Algorithm Context Service
 *   - PhysDesktop
 *   - LoKi::getPhysDesktop
 *   - Gaudi::Utils::getDValgorithm
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2008-03-28
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Vertices {
    // ========================================================================
    /** @class MinVertexDistanceWithSource
     *  The simple functor which evaluates the minimal distance
     *  between the vertex and vertices from the "source"
     *  @see LoKi::Vertices::MinVertexDistance
     *  @see LoKi::Cuts::VMINVDSOURCE
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-28
     */
    class GAUDI_API MinVertexDistanceWithSource : public LoKi::Vertices::MinVertexDistance {
      // ======================================================================
      // the source of vertices
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source _Source;
      // ======================================================================
    public:
      // ======================================================================
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source Source;
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from the source
      MinVertexDistanceWithSource( const _Source& source );
      /// MANDATORY: virtual destructor
      virtual ~MinVertexDistanceWithSource(){};
      /// MANDATORY: clone method ("virtual constructor")
      MinVertexDistanceWithSource* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument v ) const override { return minvdsource( v ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// get the source
      const Source& source() const { return m_source; }
      // cast to the source
      operator const Source&() const { return source(); }
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructo is disabled
      MinVertexDistanceWithSource(); // the default constructo is disabled
      // ======================================================================
    public:
      // ======================================================================
      /// make the actual evaluation
      result_type minvdsource( argument v ) const;
      // ======================================================================
    private:
      // ======================================================================
      /// the source of the vertices
      LoKi::Assignable<_Source>::Type m_source; // the source
      // ======================================================================
    };
    // ========================================================================
    /** @class MinVertexDistanceDV
     *  The special version of LoKi::Vertices::MinVertexDistanceWithSource functor
     *  which gets all the primary vertices from the Desktop
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @see LoKi::Cuts::VMINVDDV
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-28
     */
    class GAUDI_API MinVertexDistanceDV : public LoKi::Vertices::MinVertexDistanceWithSource {
    public:
      // ======================================================================
      /// the default constructor
      MinVertexDistanceDV( const IDVAlgorithm* algorithm );
      /// the constructor from the vertex filter
      MinVertexDistanceDV( const IDVAlgorithm* algorithm, const LoKi::PhysTypes::VCuts& cut );
      /// MANDATORY: virtual destructor
      virtual ~MinVertexDistanceDV(){};
      /// MANDATORY: clone method ("virtual constructor")
      MinVertexDistanceDV* clone() const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
      // ======================================================================
    public:
      // ======================================================================
      /// get the access to the cut:
      const LoKi::PhysTypes::VCuts& cut() const { return m_cut; }
      // ======================================================================
    private:
      // ======================================================================
      /// The vertex selector
      LoKi::PhysTypes::VCut m_cut; // The vertex selector
      // ======================================================================
    };
    // ========================================================================
    /** @class MinVertexChi2DistanceWithSource
     *  The simple functor which evaluates the minimal distance
     *  between the vertex and vertices from the "source"
     *  @see LoKi::Vertices::MinVertexChi2Distance
     *  @see LoKi::Cuts::VMINVDCHI2SOURCE
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-28
     */
    class GAUDI_API MinVertexChi2DistanceWithSource : public LoKi::Vertices::MinVertexChi2Distance {
      // ======================================================================
      // the source of vertices
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source _Source;
      // ======================================================================
    public:
      // ======================================================================
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source Source;
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from the source
      MinVertexChi2DistanceWithSource( const _Source& source );
      /// MANDATORY: virtual destructor
      virtual ~MinVertexChi2DistanceWithSource(){};
      /// MANDATORY: clone method ("virtual constructor")
      MinVertexChi2DistanceWithSource* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument v ) const override { return minvdchi2source( v ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// get the source
      const Source& source() const { return m_source; }
      // cast to the source
      operator const Source&() const { return source(); }
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructor is disabled
      MinVertexChi2DistanceWithSource(); // the default constructo is disabled
      // ======================================================================
    public:
      // ======================================================================
      /// make the actual evaluation
      result_type minvdchi2source( argument v ) const;
      // ======================================================================
    private:
      // ======================================================================
      /// the source of the vertices
      LoKi::Assignable<_Source>::Type m_source; // the source
      // ======================================================================
    };
    // ========================================================================
    /** @class MinVertexChi2DistanceDV
     *  The special version of LoKi::Vertices::MinVertexChi2DistanceWithSource functor
     *  which gets all the primary vertices from the Desktop
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @see LoKi::Cuts::VMINVDCHI2DV
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-28
     */
    class GAUDI_API MinVertexChi2DistanceDV : public LoKi::Vertices::MinVertexChi2DistanceWithSource {
    public:
      // ======================================================================
      /// the default constructor
      MinVertexChi2DistanceDV( const IDVAlgorithm* algorithm );
      /// the constructor from the vertex filter
      MinVertexChi2DistanceDV( const IDVAlgorithm* algorithm, const LoKi::PhysTypes::VCuts& cut );
      /// MANDATORY: virtual destructor
      virtual ~MinVertexChi2DistanceDV(){};
      /// MANDATORY: clone method ("virtual constructor")
      MinVertexChi2DistanceDV* clone() const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
      // ======================================================================
    public:
      // ======================================================================
      /// get the access to the cut:
      const LoKi::PhysTypes::VCuts& cut() const { return m_cut; }
      // ======================================================================
    private:
      // ======================================================================
      /// The vertex selector
      LoKi::PhysTypes::VCut m_cut; // The vertex selector
      // ======================================================================
    };
    // ========================================================================
  } // namespace Vertices
  // ==========================================================================
} // namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_VERTICES5_H
// ============================================================================
