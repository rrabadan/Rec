/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LoKi/AuxDesktopBase.h"
#include "LoKi/Interface.h"
#include "LoKi/PhysTypes.h"

#include "RecInterfaces/IChargedProtoANNPIDTool.h"

#include "GaudiKernel/Kernel.h"

namespace LoKi {

  namespace Particles {

    /** @class ANNPID
     *  Wrapper for ANNGlobalPID::IChargedProtoANNPIDTool to access ANNPID
     *  variables for various tunings
     *  @see LoKi::Cuts::ANNPID
     *  @see ANNGlobalPID::IChargedProtoANNPIDTool
     *  @see ANNGlobalPID::ChargedProtoANNPIDTool
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2017-07-22
     */
    class GAUDI_API ANNPID : public LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                             virtual public LoKi::AuxDesktopBase {
    public:
      /** constructor from particle type, tune & tool
       *  @param ptype the  type of particle
       *  @param tune  tune
       *  @param tool  typename for the tool
       */
      ANNPID( IDVAlgorithm const* algorithm, const std::string& ptype, const std::string& tune,
              const std::string& tool );
      /** constructor from particle type & tune
       *  @param ptype the  type of particle
       *  @param tune  tune
       */
      ANNPID( IDVAlgorithm const* algorithm, const std::string& ptype, const std::string& tune );
      /// mandatory:  virtual destructor
      virtual ~ANNPID();
      /// MANDATORY: clone method ("virtual contructor")
      virtual ANNPID* clone() const override;
      /// MANDATORY: the only one important method
      virtual result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      virtual std::ostream& fillStream( std::ostream& s ) const override;
      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }

    private:
      /// no  default constructor
      ANNPID(); // no default constructor

      /// particle type
      std::string m_type; // particle type
      /// ANN tune
      std::string m_tune; // ANN tune
      /// tool typename
      std::string m_toolname; // tool typename
      /// particle type
      LHCb::ParticleID m_pid; // particle type
      /// ANNPID tool
      LoKi::Interface<ANNGlobalPID::IChargedProtoANNPIDTool> m_tool;
    };

  } // namespace Particles

} // namespace LoKi

namespace LoKi {

  namespace Cuts {

    /** @typedef ANNPID
     *  @code
     *  Fun fun = ANNPID ( "e+" , "MC15TuneFLAT4dV1" ) ;
     *  const LHCb::Particle* p = ... ;
     *  double result = fun ( p ) ;
     *  @endcode
     *  @see LoKi::Particles::ANNPID
     *  @see ANNGlobalPID::IChargedProtoANNPIDTool
     *  @see ANNGlobalPID::ChargedProtoANNPIDTool
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2017-07-22
     */
    typedef LoKi::Particles::ANNPID ANNPID;

  } // namespace Cuts

} // namespace LoKi
