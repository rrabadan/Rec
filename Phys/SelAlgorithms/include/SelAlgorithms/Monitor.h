/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/Particle.h"
#include "Functors/Filter.h"
#include "Functors/with_functors.h"
#include "Functors/with_output_tree.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/EventLocalUnique.h"
#include "LHCbAlgs/Consumer.h"
#include "SelKernel/TrackZips.h"
#include "SelKernel/Utilities.h"

#include <optional>
#include <vector>

namespace SelAlgorithms {

  template <class T>
  struct undefined;

  namespace detail {

    // Needed to determine if we need to make a zip of the input types. If it
    // is already a zip, this is a no-op
    template <class T>
    struct iterate_natively : std::false_type {};

    template <>
    struct iterate_natively<LHCb::Particle::Range> : std::true_type {};

    template <class T>
    static constexpr auto iterate_natively_v = iterate_natively<T>::value;

  } // namespace detail

  namespace {

    template <class T>
    struct TypeHelper {};

    // Define a helper with the input and item types
    // defining the template arguments, and the signature of the functor.

    // Implementation for a single input container (must be a zip)
    template <typename Ts>
    struct MonitorVariable {
      constexpr static auto PropertyName = "Variable";
      using InputType                    = Ts;
      using ItemType =
          typename LHCb::Event::simd_zip_t<SIMDWrapper::Scalar,
                                           InputType>::template zip_proxy_type<LHCb::Pr::ProxyBehaviour::Contiguous>;
      using Signature = std::any( ItemType const& );
    };

    // Implementation for a particle range
    template <>
    struct MonitorVariable<LHCb::Particle::Range> {
      constexpr static auto PropertyName = "Variable";
      using InputType                    = LHCb::Particle::Range;
      using ItemType                     = typename LHCb::Particle::Range::value_type;
      using Signature                    = std::any( ItemType const& );
    };

    // Implementation for a LHCb variant
    template <class... Ts>
    struct MonitorVariable<LHCb::variant<Ts...>> {
      constexpr static auto PropertyName = "Variable";
      using InputType                    = LHCb::variant<Ts...>;
      using ItemType  = std::variant<typename LHCb::Event::simd_zip_t<SIMDWrapper::Scalar, Ts>::template zip_proxy_type<
          LHCb::Pr::ProxyBehaviour::Contiguous>...>;
      using Signature = std::any( ItemType const& );
    };

    /** @class MonitorBase MonitorBase.h
     *
     *  MonitorBase<T> Base class to define the properties of a monitoring algorithm
     */
    template <typename T>
    class MonitorBase : public with_functors<LHCb::Algorithm::Consumer<void( T const& )>, MonitorVariable<T>> {

    public:
      using base_type  = with_functors<LHCb::Algorithm::Consumer<void( T const& )>, MonitorVariable<T>>;
      using value_type = float;

      MonitorBase( std::string const& name, ISvcLocator* pSvcLocator )
          : base_type( name, pSvcLocator, typename base_type::KeyValue{"Input", ""} ) {}

      StatusCode initialize() override {

        auto sc = base_type::initialize();

        if ( sc.isFailure() ) return sc;

        if ( m_histogram_title.value().empty() ) m_histogram_title = m_histogram_name.value();

        m_histogram.emplace(
            this, m_histogram_name.value(), m_histogram_title.value(),
            Gaudi::Accumulators::Axis<value_type>{m_bins.value(), m_range.value().first, m_range.value().second} );

        return sc;
      }

      void operator()( T const& in ) const override = 0;

    protected:
      template <class C>
      void fill( C&& input_container ) const {

        auto const& pred = this->template getFunctor<MonitorVariable<T>>();

        // Convert the input type to an STL-like iterable object
        auto const& iterable = [&]() -> decltype( auto ) {
          if constexpr ( detail::iterate_natively_v<std::decay_t<decltype( input_container )>> )
            return input_container;
          else
            return LHCb::Event::make_zip<SIMDWrapper::Scalar>( input_container );
        }();

        for ( auto const& item : iterable ) {

          // An object that wraps a variant of several numeric types and initializes
          // it with the return type of the functor (using std::type_info)
          Functors::detail::with_output_tree::numeric_value_wrapper w( pred.rtype() );

          // Visit the variant and fill the histogram. The type of the variant is
          // determined by std::visit
          std::visit(
              [&]( auto& x ) {
                auto unwrapped = std::any_cast<std::decay_t<decltype( x )>>( std::invoke( pred, item ) );
                // convert a SIMDWrapper::scalar::... to a basic type
                ++( *m_histogram )[LHCb::Utils::as_arithmetic( unwrapped )];
              },
              w.get() );
        }
      }

    private:
      // properties
      Gaudi::Property<std::string>  m_histogram_name{this, "HistogramName", "Histogram", "Histogram name"};
      Gaudi::Property<std::string>  m_histogram_title{this, "HistogramTitle", "",
                                                     "Histogram title (it defaults to the histogram name)"};
      Gaudi::Property<unsigned int> m_bins{this, "Bins", 100};
      Gaudi::Property<std::pair<value_type, value_type>> m_range{this, "Range", {0., 1.}};
      mutable std::optional<Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, value_type>>
          m_histogram;
    };
  } // namespace

  /** @class Monitor Monitor.h
   *
   *  Monitor<T> evaluates a functor in the input type and generates a histogram
   *
   *  @tparam T The selected object type (e.g. Track, Particle, ...). If multiple template
   * arguments are provided, a zip container is created.
   */
  template <typename T>
  class Monitor final : public MonitorBase<T> {
  public:
    using base_type = MonitorBase<T>;

    Monitor( const std::string& name, ISvcLocator* pSvcLocator ) : base_type( name, pSvcLocator ) {}

    void operator()( T const& in ) const override { this->fill( in ); }
  };

  template <typename... T>
  class Monitor<LHCb::variant<T...>> final : public MonitorBase<LHCb::variant<T...>> {
  public:
    using base_type  = MonitorBase<LHCb::variant<T...>>;
    using input_type = LHCb::variant<T...>;

    Monitor( const std::string& name, ISvcLocator* pSvcLocator ) : base_type( name, pSvcLocator ) {}

    void operator()( input_type const& in ) const override {

      LHCb::invoke_or_visit( [&]( auto const& x ) { this->fill( x ); }, in );
    }
  };

} // namespace SelAlgorithms
