/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CombineTracksSIMD.h"

#include "Event/Track_v3.h"

using namespace SelAlgorithms::CombineTracksSIMD;

using CombineTracksSIMD__2Body__PrFittedForwardTracks = CombineTracksSIMD<LHCb::Event::v3::Tracks, 2>;

DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__2Body__PrFittedForwardTracks,
                           "CombineTracksSIMD__2Body__PrFittedForwardTracks" )
