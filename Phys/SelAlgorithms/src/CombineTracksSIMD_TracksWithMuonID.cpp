/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CombineTracksSIMD.h"

#include "SelKernel/TrackZips.h"

using namespace SelAlgorithms::CombineTracksSIMD;

// Aliases are needed because there are multiple template arguments and the
// commas separating them confuse the DECLARE_COMPONENT_WITH_ID macro.
// Note that the input type is always the same; the convention is that the type
// stored on the TES is a default/best zip, and if (as here) an algorithm is to
// instantiated with different backends then it still takes a default/best zip
// and changes the setting internally

template <std::size_t N>
using CombineTracksSIMD__NBody__PrFittedForwardTracksWithMuonID =
    CombineTracksSIMD<LHCb::Event::v3::TracksWithMuonID, N>;

template <std::size_t N>
using CombineTracksSIMD__NBody__PrFittedForwardTracksWithMuonID_Scalar =
    CombineTracksSIMD<LHCb::Event::v3::TracksWithMuonID, N, SIMDWrapper::Scalar>;

DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithMuonID<2>,
                           "CombineTracksSIMD__2Body__PrFittedForwardTracksWithMuonID" )
DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithMuonID<3>,
                           "CombineTracksSIMD__3Body__PrFittedForwardTracksWithMuonID" )
DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithMuonID_Scalar<2>,
                           "CombineTracksSIMD__2Body__PrFittedForwardTracksWithMuonID_Scalar" )
DECLARE_COMPONENT_WITH_ID( CombineTracksSIMD__NBody__PrFittedForwardTracksWithMuonID_Scalar<3>,
                           "CombineTracksSIMD__3Body__PrFittedForwardTracksWithMuonID_Scalar" )
