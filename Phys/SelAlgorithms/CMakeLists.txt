###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/SelAlgorithms
------------------
#]=======================================================================]

gaudi_add_header_only_library(SelAlgorithmsLib
    LINK
        Gaudi::GaudiAlgLib
        LHCb::EventBase
        LHCb::LHCbAlgsLib
        Rec::FunctorCoreLib
)

gaudi_add_module(SelAlgorithms
    SOURCES
        src/CombineTracksSIMD_FittedForwardTracks.cpp
        src/CombineTracksSIMD_TracksWithMuonID.cpp
        src/CombineTracksSIMD_TracksWithPVs.cpp
        src/DumpContainer.cpp
        src/InstantiateFunctors.cpp
        src/MakePVRelations.cpp
        src/FlattenDecayTree.cpp
        src/MakeZip.cpp
	src/Monitor.cpp
        src/TestFunctors_Composites.cpp
        src/TestFunctors_PrTracks.cpp
        src/TestFunctors_void.cpp
        src/VoidFilter.cpp
    LINK
        SelAlgorithmsLib
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::EventBase
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::TrackEvent
        Rec::FunctorCoreLib
        Rec::PrKernel
        Rec::SelKernelLib
        Rec::TrackKernel
        Rec::LoKiPhysLib
)

gaudi_add_tests(QMTest)
