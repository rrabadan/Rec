/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/IAlgTool.h"

class StatusCode;
#include "DetDesc/IGeometryInfo.h"
namespace LHCb {
  class Particle;
  class VertexBase;
} // namespace LHCb

/**
 *  Interface for PVReFitter
 *  @author Yuehong Xie
 *  @date   17/08/2005
 */
struct GAUDI_API IPVReFitter : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IPVReFitter, 2, 0 );

  /// refit PV
  virtual StatusCode reFit( LHCb::VertexBase*, IGeometryInfo const& geometry ) const = 0;

  /// remove track used for a (B) LHCb::Particle and refit PV
  virtual StatusCode remove( LHCb::Particle const*, LHCb::VertexBase*, IGeometryInfo const& geometry ) const = 0;
};
