/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Particle.h"
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/VectorMap.h"

#include <string>

#include "DetDesc/IGeometryInfo.h"

static const InterfaceID IID_IParticleDictTool( "IParticleDictTool", 2, 0 );

/**
 *  returns a dictionary of variable-names and their values
 *
 *  @author Sebastian Neubert
 *  @date   2013-07-08
 *
 */
struct IParticleDictTool : virtual IAlgTool {

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IParticleDictTool; }

  /// Dict definition
  typedef GaudiUtils::VectorMap<std::string, double> DICT;

  /** fill the dictionary:
   *  @param p (INPUT) the particle
   *  @param dict (UPDATE) the dictionary to be filled
   */
  virtual StatusCode fill( const LHCb::Particle* p, DICT& dict, IGeometryInfo const& geometry ) const = 0;
};
