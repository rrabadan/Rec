/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "Kernel/IExtraInfoTool.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/PhysicalConstants.h"

/**
 * \brief Fill track isolation for DecayTreeTuple.
 *    Open up a cone around head, exclude all tracks
 *    that are in the decay descriptor
 *    (i.e. that belong to the decay you are looking for),
 *    build the variables with the remaining tracks.
 * \sa DecayTreeTuple
 *
 *  @author Michel De Cian
 *  @date   2009-08-04
 */
class ConeVariablesForEW : public GaudiTool, virtual public IExtraInfoTool {

public:
  /// Standard constructor
  ConeVariablesForEW( const std::string& type, const std::string& name, const IInterface* parent );
  /// Loop over differnt conesizes and fill the variables into the tuple
  StatusCode calculateExtraInfo( LHCb::Particle const*, LHCb::Particle const*, IGeometryInfo const& ) override;
  int        getFirstIndex( void ) override;
  int        getNumberOfParameters( void ) override;
  int        getInfo( int index, double& value, std::string& name ) override;

private:
  double m_coneAngle;
  int    m_coneNumber;

  int    m_mult;
  double m_px;
  double m_py;
  double m_pz;
  double m_vp;
  double m_vpt;
  double m_sp;
  double m_spt;
  double m_tp;
  double m_tpt;
  double m_minpte;
  double m_maxpte;
  double m_minptmu;
  double m_maxptmu;
  int    m_nmult;
  double m_npx;
  double m_npy;
  double m_npz;
  double m_nvp;
  double m_nvpt;
  double m_nsp;
  double m_nspt;

  int         m_trackType;
  std::string m_extraParticlesLocation;
  std::string m_extraPhotonsLocation;

  std::vector<const LHCb::Particle*> m_decayParticles;

  /// Save all particles in your decay descriptor in a vector
  void saveDecayParticles( const LHCb::Particle* top );

  /// Calculate properties of your remaining tracks inside the cone
  StatusCode ChargedCone( const LHCb::Particle* seed, const LHCb::Particle::Range& parts, const double rcut, int& mult,
                          std::vector<double>& vP, double& sP, double& sPt, double& minPtE, double& maxPtE,
                          double& minPtMu, double& maxPtMu );

  StatusCode NeutralCone( const LHCb::Particle* seed, const LHCb::Particle::Range& parts, const double rcut, int& mult,
                          std::vector<double>& vP, double& sP, double& sPt );

  /// Check if your track belongs to your decay or not
  bool isTrackInDecay( const LHCb::Track* track );
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ConeVariablesForEW )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ConeVariablesForEW::ConeVariablesForEW( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<IExtraInfoTool>( this );

  declareProperty( "ConeNumber", m_coneNumber = 1, "Number of cone variables record (1-4)" );

  declareProperty( "ConeAngle", m_coneAngle = 0., "Set the deltaR of the cone around the seed" );

  declareProperty( "TrackType", m_trackType = 3, "Set the type of tracks which are considered inside the cone" );

  declareProperty( "ExtreParticlesLocation", m_extraParticlesLocation = "StdAllNoPIDsMuons",
                   "Set the type of particles which are considered in the charged cone" );
  declareProperty( "ExtrePhotonsLocation", m_extraPhotonsLocation = "StdLoosePhotons",
                   "Set the type of photons which are considered in the neutral cone" );
}

//=============================================================================
// Calculate cone variables
//=============================================================================
StatusCode ConeVariablesForEW::calculateExtraInfo( LHCb::Particle const* top, LHCb::Particle const* seed,
                                                   IGeometryInfo const& ) {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Fill" << endmsg;

  // -- The vector m_decayParticles contains all the particles that belong to the given decay
  // -- according to the decay descriptor.

  // -- Clear the vector with the particles in the specific decay
  m_decayParticles.clear();

  // -- Add the mother (prefix of the decay chain) to the vector
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Filling particle with ID " << top->particleID().pid() << endmsg;
  m_decayParticles.push_back( top );

  // -- Save all particles that belong to the given decay in the vector m_decayParticles
  saveDecayParticles( top );

  // -- Get all particles in the event
  const LHCb::Particle::Range parts = get<LHCb::Particle::Range>( "Phys/" + m_extraParticlesLocation + "/Particles" );
  if ( parts.empty() ) {
    if ( msgLevel( MSG::WARNING ) ) warning() << "Could not retrieve extra-particles. Skipping" << endmsg;
    return StatusCode::FAILURE;
  }

  if ( seed ) {

    // -- Retrieve information in the charged cone
    int                 multiplicity = 0;
    std::vector<double> vectorP;
    double              scalarP     = 0.;
    double              scalarPt    = 0.;
    double              minimumPtE  = 0.;
    double              maximumPtE  = 0.;
    double              minimumPtMu = 0.;
    double              maximumPtMu = 0.;

    StatusCode sc = ChargedCone( seed, parts, m_coneAngle, multiplicity, vectorP, scalarP, scalarPt, minimumPtE,
                                 maximumPtE, minimumPtMu, maximumPtMu );
    if ( sc.isFailure() ) multiplicity = -1;

    // -- Create a vector with the summed momentum of all tracks in the cone
    Gaudi::XYZVector coneMomentum;
    coneMomentum.SetX( vectorP[0] );
    coneMomentum.SetY( vectorP[1] );
    coneMomentum.SetZ( vectorP[2] );

    // -- Create a vector with the summed momentum of all tracks in the cone + seed
    Gaudi::XYZVector totalMomentum;
    totalMomentum.SetX( seed->momentum().X() + coneMomentum.X() );
    totalMomentum.SetY( seed->momentum().Y() + coneMomentum.Y() );
    totalMomentum.SetZ( seed->momentum().Z() + coneMomentum.Z() );

    // -- Fill the tuple with the variables
    m_mult = multiplicity;
    m_px   = coneMomentum.X();
    m_py   = coneMomentum.Y();
    m_pz   = coneMomentum.Z();
    m_vp   = sqrt( coneMomentum.Mag2() );
    m_vpt  = sqrt( coneMomentum.Perp2() );
    m_sp   = scalarP;
    m_spt  = scalarPt;
    m_tp   = sqrt( totalMomentum.Mag2() );
    m_tpt  = sqrt( totalMomentum.Perp2() );
    if ( m_coneAngle == 0. ) {
      m_minpte  = minimumPtE;
      m_maxpte  = maximumPtE;
      m_minptmu = minimumPtMu;
      m_maxptmu = maximumPtMu;
    }

    // -- Retrieve information in the neutral cone
    int                 nmultiplicity = 0;
    std::vector<double> nvectorP;
    double              nscalarP  = 0.;
    double              nscalarPt = 0.;

    // -- Get all photons in the event
    const LHCb::Particle::Range photons = get<LHCb::Particle::Range>( "Phys/" + m_extraPhotonsLocation + "/Particles" );
    if ( !photons.empty() ) {
      StatusCode nsc = NeutralCone( seed, photons, m_coneAngle, nmultiplicity, nvectorP, nscalarP, nscalarPt );
      if ( nsc.isFailure() ) nmultiplicity = -1;
    } else {
      if ( msgLevel( MSG::WARNING ) ) Warning( "Could not retrieve photons" ).ignore();
      nmultiplicity = -1;
      nvectorP.push_back( 0 );
      nvectorP.push_back( 0 );
      nvectorP.push_back( 0 );
    }

    // -- Create a vector with the summed momentum of all tracks in the cone
    Gaudi::XYZVector neutralConeMomentum;
    neutralConeMomentum.SetX( nvectorP[0] );
    neutralConeMomentum.SetY( nvectorP[1] );
    neutralConeMomentum.SetZ( nvectorP[2] );

    // -- Fill the tuple with the variables
    m_nmult = nmultiplicity;
    m_npx   = neutralConeMomentum.X();
    m_npy   = neutralConeMomentum.Y();
    m_npz   = neutralConeMomentum.Z();
    m_nvp   = sqrt( neutralConeMomentum.Mag2() );
    m_nvpt  = sqrt( neutralConeMomentum.Perp2() );
    m_nsp   = nscalarP;
    m_nspt  = nscalarPt;

  } else {

    if ( msgLevel( MSG::WARNING ) ) warning() << "The seed particle is not valid. Skipping" << endmsg;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}
//=============================================================================
// Save the particles in the decay chain (recursive function)
//=============================================================================
void ConeVariablesForEW::saveDecayParticles( const LHCb::Particle* top ) {

  // -- Get the daughters of the top particle
  const SmartRefVector<LHCb::Particle> daughters = top->daughters();

  // -- Fill all the daugthers in m_decayParticles
  for ( SmartRefVector<LHCb::Particle>::const_iterator ip = daughters.begin(); ip != daughters.end(); ++ip ) {

    // -- If the particle is stable, save it in the vector, or...
    if ( ( *ip )->isBasicParticle() ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Filling particle with ID " << ( *ip )->particleID().pid() << endmsg;
      m_decayParticles.push_back( ( *ip ) );
    } else {
      // -- if it is not stable, call the function recursively
      m_decayParticles.push_back( ( *ip ) );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Filling particle with ID " << ( *ip )->particleID().pid() << endmsg;
      saveDecayParticles( ( *ip ) );
    }
  }

  return;
}
//=============================================================================
// Loop over all the tracks in the cone which do not belong to the desired decay
//=============================================================================
StatusCode ConeVariablesForEW::ChargedCone( const LHCb::Particle* seed, const LHCb::Particle::Range& parts,
                                            const double rcut, int& mult, std::vector<double>& vP, double& sP,
                                            double& sPt, double& minPtE, double& maxPtE, double& minPtMu,
                                            double& maxPtMu ) {

  // -- Initialize values
  mult       = 0;
  double sPx = 0.;
  double sPy = 0.;
  double sPz = 0.;
  sP         = 0.;
  sPt        = 0.;
  minPtE     = 1.e10;
  int minQE  = 0;
  maxPtE     = 0.;
  int maxQE  = 0;
  minPtMu    = 1.e10;
  int minQMu = 0;
  maxPtMu    = 0.;
  int maxQMu = 0;

  // -- Get the 4-momentum of the seed particle
  Gaudi::LorentzVector seedMomentum = seed->momentum();

  for ( auto particle : parts ) {
    const LHCb::ProtoParticle* proto = particle->proto();
    if ( proto ) {

      const LHCb::Track* track = proto->track();
      if ( track ) {

        // -- Check if the track belongs to the decay itself
        bool isInDecay = isTrackInDecay( track );
        if ( isInDecay ) continue;

        // -- Get the 3-momentum of the track
        Gaudi::XYZVector trackMomentum = track->momentum();

        // -- Calculate the difference in Eta and Phi between the seed particle and a track
        double deltaPhi = fabs( seedMomentum.Phi() - trackMomentum.Phi() );
        if ( deltaPhi > M_PI ) deltaPhi = 2 * M_PI - deltaPhi;
        double deltaEta = seedMomentum.Eta() - trackMomentum.Eta();
        double deltaR   = sqrt( deltaPhi * deltaPhi + deltaEta * deltaEta );

        if ( static_cast<int>( track->type() ) == m_trackType ) {
          if ( ( rcut == 0. ) || ( deltaR < rcut ) ) {
            // -- Calculate vector information
            sPx += trackMomentum.X();
            sPy += trackMomentum.Y();
            sPz += trackMomentum.Z();

            // -- Calculate scalar information
            sP += sqrt( trackMomentum.Mag2() );
            sPt += sqrt( trackMomentum.Perp2() );

            mult++;
          }

          if ( rcut == 0. ) {

            // Extra Electron
            double eCalEoP = .10;
            double hCalEoP = .05;
            if ( proto->info( LHCb::ProtoParticle::additionalInfo::CaloEcalE, -1. ) / track->p() > eCalEoP ) {
              if ( ( proto->info( LHCb::ProtoParticle::additionalInfo::CaloHcalE, -1. ) > 0 ) &&
                   ( proto->info( LHCb::ProtoParticle::additionalInfo::CaloHcalE, -1. ) / track->p() < hCalEoP ) ) {
                if ( track->pt() < minPtE ) {
                  minPtE = track->pt();
                  minQE  = track->charge();
                }
                if ( track->pt() > maxPtE ) {
                  maxPtE = track->pt();
                  maxQE  = track->charge();
                }
              }
            }

            // Extra Muon
            double minP = 10.e3;
            if ( track->p() > minP ) {
              const LHCb::MuonPID* muonPID = proto->muonPID();
              if ( muonPID ) {
                if ( muonPID->IsMuon() ) {
                  if ( track->pt() < minPtMu ) {
                    minPtMu = track->pt();
                    minQMu  = track->charge();
                  }
                  if ( track->pt() > maxPtMu ) {
                    maxPtMu = track->pt();
                    maxQMu  = track->charge();
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  vP.push_back( sPx );
  vP.push_back( sPy );
  vP.push_back( sPz );

  if ( minPtE == 1.e10 ) minPtE = 0.;
  if ( maxPtE == 0. ) maxPtE = 0.;
  minPtE *= minQE;
  maxPtE *= maxQE;

  if ( minPtMu == 1.e10 ) minPtMu = 0.;
  if ( maxPtMu == 0. ) maxPtMu = 0.;
  minPtMu *= minQMu;
  maxPtMu *= maxQMu;

  return StatusCode::SUCCESS;
}

StatusCode ConeVariablesForEW::NeutralCone( const LHCb::Particle* seed, const LHCb::Particle::Range& photons,
                                            const double rcut, int& mult, std::vector<double>& vP, double& sP,
                                            double& sPt ) {

  // -- Initialize values
  mult       = 0;
  double sPx = 0.;
  double sPy = 0.;
  double sPz = 0.;
  sP         = 0.;
  sPt        = 0.;

  // -- Get the 4-momentum of the seed particle
  Gaudi::LorentzVector seedMomentum = seed->momentum();

  for ( auto photon : photons ) {
    // -- Get the 3-momentum of the photon
    Gaudi::XYZVector photonMomentum = photon->momentum().Vect();

    // -- Calculate the difference in Eta and Phi between the seed particle and a photons
    double deltaPhi = fabs( seedMomentum.Phi() - photonMomentum.Phi() );
    if ( deltaPhi > M_PI ) deltaPhi = 2 * M_PI - deltaPhi;
    double deltaEta = seedMomentum.Eta() - photonMomentum.Eta();
    double deltaR   = sqrt( deltaPhi * deltaPhi + deltaEta * deltaEta );

    if ( ( rcut == 0. ) || ( deltaR < rcut ) ) {
      // -- Calculate vector information
      sPx += photonMomentum.X();
      sPy += photonMomentum.Y();
      sPz += photonMomentum.Z();

      // -- Calculate scalar information
      sP += sqrt( photonMomentum.Mag2() );
      sPt += sqrt( photonMomentum.Perp2() );

      mult++;
    }
  }

  vP.push_back( sPx );
  vP.push_back( sPy );
  vP.push_back( sPz );

  return StatusCode::SUCCESS;
}
//=============================================================================
// Check if the track is already in the decay
//=============================================================================
bool ConeVariablesForEW::isTrackInDecay( const LHCb::Track* track ) {

  bool isInDecay = false;

  for ( std::vector<const LHCb::Particle*>::iterator ip = m_decayParticles.begin(); ip != m_decayParticles.end();
        ++ip ) {
    const LHCb::ProtoParticle* proto = ( *ip )->proto();
    if ( proto ) {

      const LHCb::Track* myTrack = proto->track();
      if ( myTrack ) {

        if ( myTrack == track ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Track is in decay, skipping it" << endmsg;
          isInDecay = true;
        }
      }
    }
  }

  return isInDecay;
}

int ConeVariablesForEW::getFirstIndex( void ) {
  switch ( m_coneNumber ) {
  case 1:
    return LHCb::Particle::additionalInfo::EWCone1Index;
  case 2:
    return LHCb::Particle::additionalInfo::EWCone2Index;
  case 3:
    return LHCb::Particle::additionalInfo::EWCone3Index;
  case 4:
    return LHCb::Particle::additionalInfo::EWCone4Index;
  default:
    return LHCb::Particle::additionalInfo::EWCone1Index;
  }
}

int ConeVariablesForEW::getNumberOfParameters( void ) {
  return 23; // This tool returns 23 parameters
}

int ConeVariablesForEW::getInfo( int index, double& value, std::string& name ) {

  switch ( index - getFirstIndex() ) {
  case 0:
    value = m_coneAngle;
    name  = "angle";
    break;
  case 1:
    value = (double)m_mult;
    name  = "mult";
    break;
  case 2:
    value = m_px;
    name  = "px";
    break;
  case 3:
    value = m_py;
    name  = "py";
    break;
  case 4:
    value = m_pz;
    name  = "pz";
    break;
  case 5:
    value = m_vp;
    name  = "vp";
    break;
  case 6:
    value = m_vpt;
    name  = "vpt";
    break;
  case 7:
    value = m_sp;
    name  = "sp";
    break;
  case 8:
    value = m_spt;
    name  = "spt";
    break;
  case 9:
    value = m_tp;
    name  = "tp";
    break;
  case 10:
    value = m_tpt;
    name  = "tpt";
    break;
  case 11:
    value = m_minpte;
    name  = "minpte";
    break;
  case 12:
    value = m_maxpte;
    name  = "maxpte";
    break;
  case 13:
    value = m_minptmu;
    name  = "minptmu";
    break;
  case 14:
    value = m_maxptmu;
    name  = "maxptmu";
    break;
  case 15:
    value = (double)m_nmult;
    name  = "nmult";
    break;
  case 16:
    value = m_npx;
    name  = "npx";
    break;
  case 17:
    value = m_npy;
    name  = "npy";
    break;
  case 18:
    value = m_npz;
    name  = "npz";
    break;
  case 19:
    value = m_nvp;
    name  = "nvp";
    break;
  case 20:
    value = m_nvpt;
    name  = "nvpt";
    break;
  case 21:
    value = m_nsp;
    name  = "nsp";
    break;
  case 22:
    value = m_nspt;
    name  = "nspt";
    break;
  default:
    break;
  }

  return 1;
}
