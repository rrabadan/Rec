###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DaVinciKernel
------------------
#]=======================================================================]

gaudi_add_library(DaVinciKernelLib
    SOURCES
        src/Lib/DaVinciFun.cpp
        src/Lib/DaVinciStringUtils.cpp
        src/Lib/Escape.cpp
        src/Lib/GetDecay.cpp
        src/Lib/GetTESLocations.cpp
        src/Lib/PVSentry.cpp
        src/Lib/ParticleFilters.cpp
        src/Lib/TransporterFunctions.cpp
    LINK
        PUBLIC
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::PartPropLib
            LHCb::PhysEvent
            LHCb::PhysInterfacesLib
            LHCb::RecEvent
            Rec::DaVinciInterfacesLib
            Rec::DaVinciTypesLib
            ROOT::GenVector
        PRIVATE
            Boost::headers
            Boost::regex
            LHCb::LHCbMathLib
)

gaudi_add_module(DaVinciKernel
    SOURCES
        src/component/DecodeSimpleDecayString.cpp
        src/component/DecayFinder.cpp
    LINK
        Boost::regex
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::DAQEventLib
        LHCb::EventBase
        LHCb::LHCbKernel
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::TrackEvent
        Rec::DaVinciInterfacesLib
        Rec::DaVinciKernelLib
)

gaudi_add_dictionary(DaVinciKernelDict
    HEADERFILES dict/DaVinciKernelDict.h
    SELECTION dict/DaVinciKernelDict.xml
    LINK DaVinciKernelLib
    OPTIONS ${PHYS_DICT_GEN_DEFAULT_OPTS}
)

if(BUILD_TESTING)
    foreach(n IN ITEMS 1 2 3)
        gaudi_add_executable(Combiner${n}
            SOURCES
                tests/combiner${n}.cpp
                tests/Combine.cpp
            LINK
                Gaudi::GaudiKernel
                LHCb::PhysEvent
        )
    endforeach()
endif()

gaudi_add_tests(pytest ${CMAKE_CURRENT_SOURCE_DIR}/tests/decays/test_descriptor_parsing.py)
gaudi_add_tests(pytest ${CMAKE_CURRENT_SOURCE_DIR}/tests/decays/test_decayfinder.py)
gaudi_add_tests(pytest ${CMAKE_CURRENT_SOURCE_DIR}/tests/decays/test_simple_descriptors.py)
