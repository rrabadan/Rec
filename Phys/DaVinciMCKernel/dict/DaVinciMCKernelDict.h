/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DICT_DAVINCIMCKERNELDICT_H
#define DICT_DAVINCIMCKERNELDICT_H 1

// Include files

/** @file DaVinciMCKernelDict DaVinciMCKernelDict.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2006-10-02
 */
// ============================================================================
// DaVinciMCKernel
// ============================================================================
#include "Kernel/IBackgroundCategory.h"
#include "Kernel/ICheatedLifetimeFitter.h"
#include "Kernel/IDaVinciAssociatorsWrapper.h"
#include "Kernel/IFilterMCParticles.h"
#include "Kernel/IMC2Collision.h"
#include "Kernel/IMCParticleArrayFilter.h"
#include "Kernel/IPV2MC.h"
#include "Kernel/IParticle2MCAssociator.h"
#include "Kernel/IParticle2MCWeightedAssociator.h"
#include "Kernel/IPrintDecayTreeTool.h"
#include "Kernel/Particle2MCParticle.h"

#endif // DICT_DAVINCIMCKERNELDICT_H
