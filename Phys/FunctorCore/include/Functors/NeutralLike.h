/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/ProtoParticle.h"
#include "Functors/Function.h"

namespace Functors::detail {

  template <typename T>
  constexpr bool ai_always_false = false;

} // namespace Functors::detail

namespace Functors::Neutral {

  template <LHCb::ProtoParticle::additionalInfo ai>
  struct ExtraInfo_t : public Function {
    template <typename T>
    auto operator()( const T& d ) const {
      if constexpr ( std::is_pointer_v<T> ) {
        return operator()( *d ); // return itself with class
      } else if constexpr ( detail::has_proto_v<T> ) {
        LHCb::ProtoParticle const* pp = d.proto();
        return pp ? pp->info( ai, invalid_value ) : invalid_value;
      } else {
        static_assert( detail::ai_always_false<T>, "The type T does not have a `proto()` member function"
                                                   "-- sorry, not supported" );
        return invalid_value;
      }
    }
  };

  using IsPhoton_t         = ExtraInfo_t<LHCb::ProtoParticle::additionalInfo::IsPhoton>;
  using IsNotH_t           = ExtraInfo_t<LHCb::ProtoParticle::additionalInfo::IsNotH>;
  using ShowerShape_t      = ExtraInfo_t<LHCb::ProtoParticle::additionalInfo::ShowerShape>;
  using NeutralE19_t       = ExtraInfo_t<LHCb::ProtoParticle::additionalInfo::CaloNeutralE19>;
  using NeutralID_t        = ExtraInfo_t<LHCb::ProtoParticle::additionalInfo::CaloNeutralID>;
  using ClusterMass_t      = ExtraInfo_t<LHCb::ProtoParticle::additionalInfo::ClusterMass>;
  using NeutralEcal_t      = ExtraInfo_t<LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal>;
  using NeutralHcal2Ecal_t = ExtraInfo_t<LHCb::ProtoParticle::additionalInfo::CaloNeutralHcal2Ecal>;
  using NeutralE49_t       = ExtraInfo_t<LHCb::ProtoParticle::additionalInfo::CaloNeutralE49>;
  using Saturation_t       = ExtraInfo_t<LHCb::ProtoParticle::additionalInfo::Saturation>;

} // namespace Functors::Neutral
