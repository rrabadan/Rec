/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Event/Particle_v2.h"
#include "Functors/Function.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ITisTos.h"
#include "Kernel/ParticleProperty.h"
#include "SelKernel/Utilities.h"
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/ServiceHandle.h>

/** @file  Particle.h
 *  @brief Definitions of functors for particle-like objects
 *  (common to both track-like and composite).
 */

namespace Functors::detail {

  template <typename T>
  using has_track_t = decltype( std::declval<T>().track() );
  template <typename T>
  constexpr bool has_track =
      Gaudi::cpp17::is_detected_v<has_track_t, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>>;

  template <typename T>
  constexpr bool is_charged_basic =
      std::is_same_v<LHCb::Event::ChargedBasics, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>>;

  // define the various trigger categories
  enum class TriggerCategories : unsigned int { TOS = 0, TIS, TUS };
  // define a helper class for checking the trigger category
  template <TriggerCategories TrigCat>
  struct IsTrigCategory : public Predicate {
    bool operator()( const LHCb::detail::TisTosResult_t& r ) const {
      if constexpr ( TrigCat == TriggerCategories::TOS )
        return r.TOS();
      else if constexpr ( TrigCat == TriggerCategories::TIS )
        return r.TIS();
      else if constexpr ( TrigCat == TriggerCategories::TUS )
        return r.TUS();
      else
        throw GaudiException( "Unknown trigger type", "IsTos", StatusCode::FAILURE );
    }
  };
}; // namespace Functors::detail

/** @namespace Functors::Particle
 *
 *  Functors that make sense for all particles (both track-like
 *  and composite)
 */

namespace Functors::Particle {

  // detect if T is std::string
  template <typename T>
  constexpr bool is_string_v = std::is_same_v<std::decay_t<T>, std::string> || std::is_same_v<std::decay_t<T>, char*> ||
                               std::is_same_v<std::decay_t<T>, const char*>;

  // enable the class if IDInput is string or int
  template <typename IDInput, std::enable_if_t<is_string_v<IDInput> || std::is_integral_v<IDInput>, bool> = true>
  struct ParticlePropertyUser : public Function {
    /** Base class for functors that use ParticlePropertySvc
     *  to convert string ID into integer ID.
     */
    ParticlePropertyUser( IDInput id_input ) : m_id_input{id_input} {}

    void bind( TopLevelInfo& top_level ) {
      m_pp_svc.emplace( top_level.algorithm(), top_level.generate_property_name(), "LHCb::ParticlePropertySvc" );
    }

    template <typename Data>
    auto operator()( Data const& ) const {
      assert( m_pp_svc.has_value() );
      const LHCb::ParticleProperty* pp = m_pp_svc.value()->find( m_id_input );
      if ( !pp )
        throw GaudiException{"Couldn't get ParticleProperty for particle '" + std::string( m_id_input ) + "'",
                             "Functors::Particle::GetParticleProperty()", StatusCode::FAILURE};
      return pp;
    }

  private:
    IDInput                                                  m_id_input;
    std::optional<ServiceHandle<LHCb::IParticlePropertySvc>> m_pp_svc{std::nullopt};
  };

  /** @brief get the track object from the charged basic particle
   * E.g. When requisting track momentum one can use this
   * functor in the composition via F.TRACK_PT = F.PT @ F.TRACK
   */
  struct GetTrack : public Function {
    // Perfect forward
    template <typename Data, typename = std::enable_if_t<!is_legacy_particle<Data> && !detail::has_track<Data> &&
                                                         !detail::is_charged_basic<Data>>>
    decltype( auto ) operator()( Data&& value ) const {
      return std::forward<Data>( value );
    }

    // Legacy particles: LHCb::Particle and LHCb::ProtoParticle
    template <typename Data, typename = std::enable_if_t<(is_legacy_particle<Data> ||
                                                          detail::has_track<Data>)&&!detail::is_charged_basic<Data>>>
    auto operator()( Data&& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        auto trk = Sel::Utils::get_track_from_particle( d );
        return ( trk ) ? Functors::Optional{trk} : std::nullopt;
      } else {
        auto trk = Sel::Utils::deref_if_ptr( d ).track();
        return ( trk ) ? Functors::Optional{trk} : std::nullopt;
      };
    }

    // V2 LHCb::Event::ChargedBasics
    template <typename Data, typename = std::enable_if_t<!is_legacy_particle<Data> && !detail::has_track<Data> &&
                                                         detail::is_charged_basic<Data>>>
    auto const& operator()( Data const& d ) const {
      return d.tracks();
    }
  };

  /** @brief get the protoparticle from the  particle
   * E.g. need while Relation tables are made from ProtoParticles
   */
  struct GetProtoParticle : public Function {
    // for v1 const ref
    auto operator()( LHCb::Particle const& d ) const { return d.proto(); }

    // for v1 pointer
    auto operator()( LHCb::Particle const* p ) const {
      assert( p != nullptr && "The input particle cannot be NULL." );
      return ( *this )( *p );
    }
  };

  /** @brief covariance matrix for (px, py, pz)
   */
  struct threeMomCovMatrix : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::threeMomCovMatrix;
      return threeMomCovMatrix( d );
    }
  };

  /** @brief covariance matrix for (px, py, pz) x (x, y, z)
   * [(x,px), (x,py), (x,pz)]
   * [(y,px), (y,py), (y,pz)]
   * [(z,px), (z,py), (z,pz)]
   */
  struct threeMomPosCovMatrix : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::threeMomPosCovMatrix;
      return threeMomPosCovMatrix( d );
    }
  };

  /** @brief covariance matrix for (px, py, pz, pe) x (x, y, z)
   * [(x,px), (x,py), (x,pz), (x,pe)]
   * [(y,px), (y,py), (y,pz), (y,pe)]
   * [(z,px), (z,py), (z,pz), (z,pe)]
   */
  struct momPosCovMatrix : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::momPosCovMatrix;
      return momPosCovMatrix( d );
    }
  };

  /** @brief covariance matrix for (x, y, z)
   */
  struct posCovMatrix : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::posCovMatrix;
      return posCovMatrix( d );
    }
  };

  template <LHCb::ProtoParticle::additionalInfo ai>
  struct PPHasInfo : Predicate {
    bool operator()( LHCb::ProtoParticle const& pp ) const { return pp.hasInfo( ai ); }
    bool operator()( LHCb::ProtoParticle const* pp ) const { return pp && ( *this )( *pp ); }
  };

  using PPHasRich     = PPHasInfo<LHCb::ProtoParticle::RichPIDStatus>;
  using PPHasMuonInfo = PPHasInfo<LHCb::ProtoParticle::InAccMuon>;

  struct PPHasEcalInfo : public Predicate {
    bool operator()( LHCb::ProtoParticle const& pp ) const {
      constexpr auto ecal_info = std::array{
          LHCb::ProtoParticle::additionalInfo::EcalPIDe,          LHCb::ProtoParticle::additionalInfo::BremPIDe,
          LHCb::ProtoParticle::additionalInfo::EcalPIDmu,         LHCb::ProtoParticle::additionalInfo::CaloTrMatch,
          LHCb::ProtoParticle::additionalInfo::CaloElectronMatch, LHCb::ProtoParticle::additionalInfo::CaloBremMatch,
          LHCb::ProtoParticle::additionalInfo::CaloChargedEcal,   LHCb::ProtoParticle::additionalInfo::CaloDepositID,
          LHCb::ProtoParticle::additionalInfo::ShowerShape,       LHCb::ProtoParticle::additionalInfo::ClusterMass,
          LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal,   LHCb::ProtoParticle::additionalInfo::CaloEcalE,
          LHCb::ProtoParticle::additionalInfo::CaloEcalChi2,      LHCb::ProtoParticle::additionalInfo::CaloBremChi2,
          LHCb::ProtoParticle::additionalInfo::CaloClusChi2,      LHCb::ProtoParticle::additionalInfo::CaloTrajectoryL,
          LHCb::ProtoParticle::additionalInfo::PhotonID};
      return pp.info( LHCb::ProtoParticle::additionalInfo::InAccEcal, 0 ) != 0 &&
             std::any_of( ecal_info.begin(), ecal_info.end(), [&]( auto i ) { return pp.hasInfo( i ); } );
    }
    bool operator()( LHCb::ProtoParticle const* pp ) const { return ( *this )( *pp ); }
  };

  /**
   * @brief Check if the "TriggerResult_t" object is TOS
   */
  using IsTos = detail::IsTrigCategory<detail::TriggerCategories::TOS>;

  /**
   * @brief Check if the "TriggerResult_t" object is TIS
   */
  using IsTis = detail::IsTrigCategory<detail::TriggerCategories::TIS>;

  /** @brief Check if the particle is a basic particle (not composite) */
  struct IsBasicParticle : public Predicate {
    template <typename Particle>
    auto operator()( Particle const& particle ) const {
      return Sel::Utils::deref_if_ptr( particle ).isBasicParticle();
    }
  };

} // namespace Functors::Particle
