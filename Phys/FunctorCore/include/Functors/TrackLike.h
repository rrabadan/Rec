/*****************************************************************************\
* (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/Bremsstrahlung.h"
#include "Event/ProtoParticle.h"
#include "Event/TrackEnums.h"
#include "Event/Track_v3.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "GaudiKernel/detected.h"
#include "MCInterfaces/IMCReconstructed.h"
#include "SelKernel/ParticleAccessors.h"
#include "SelKernel/Utilities.h"
#include "SelKernel/VertexRelation.h"
#include <type_traits>

/** @file  TrackLike.h
 *  @brief Definitions of functors for track-like objects.
 */

/** @namespace Functors::Track
 *
 *  This defines some basic functors that operate on track-like, or maybe more
 *  accurately charged-particle-like, objects. Both predicates and functions
 *  are defined in the same file for the moment.
 */

namespace Functors::detail {
  template <typename T>
  constexpr bool is_proto_particle =
      std::is_same_v<LHCb::ProtoParticle, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>>;

  template <typename T>
  constexpr bool is_legacy_track = std::is_same_v<LHCb::Track, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>>;

  /*
   * Helper for ProbNN
   */
  enum struct Pid { electron, muon, pion, kaon, proton, deuteron, ghost };

  constexpr LHCb::ProtoParticle::additionalInfo to_ppai( Pid pid ) {
    switch ( pid ) {
    case Pid::electron:
      return LHCb::ProtoParticle::additionalInfo::ProbNNe;
    case Pid::muon:
      return LHCb::ProtoParticle::additionalInfo::ProbNNmu;
    case Pid::pion:
      return LHCb::ProtoParticle::additionalInfo::ProbNNpi;
    case Pid::kaon:
      return LHCb::ProtoParticle::additionalInfo::ProbNNk;
    case Pid::proton:
      return LHCb::ProtoParticle::additionalInfo::ProbNNp;
    case Pid::deuteron:
      return LHCb::ProtoParticle::additionalInfo::ProbNNd;
    case Pid::ghost:
      return LHCb::ProtoParticle::additionalInfo::ProbNNghost;
    }
    throw std::domain_error{"impossible PID type"};
  }

  constexpr LHCb::ProtoParticle::additionalInfo to_ppai_combdll( Pid pid ) {
    switch ( pid ) {
    case Pid::muon:
      return LHCb::ProtoParticle::additionalInfo::CombDLLmu;
    case Pid::proton:
      return LHCb::ProtoParticle::additionalInfo::CombDLLp;
    case Pid::electron:
      return LHCb::ProtoParticle::additionalInfo::CombDLLe;
    case Pid::kaon:
      return LHCb::ProtoParticle::additionalInfo::CombDLLk;
    case Pid::pion:
      return LHCb::ProtoParticle::additionalInfo::CombDLLpi;
    default:
      throw std::domain_error{"unsupported PID type"};
    }
  }

  template <Pid pid, typename T>
  constexpr auto combDLL( const T& d ) {
    if constexpr ( is_legacy_particle<T> ) {
      auto const* pp = Sel::Utils::deref_if_ptr( d ).proto();
      return ( pp ) ? combDLL<pid>( *pp ) : std::nullopt;
    } else if constexpr ( detail::is_proto_particle<T> ) {
      constexpr auto id = to_ppai_combdll( pid );
      return Sel::Utils::deref_if_ptr( d ).hasInfo( id )
                 ? Functors::Optional{Sel::Utils::deref_if_ptr( d ).info( id, 0. )}
                 : std::nullopt;
    } else {
      switch ( pid ) {
      case Pid::electron:
        return d.CombDLLe();
      case Pid::muon:
        return d.CombDLLmu();
      case Pid::pion:
        return d.CombDLLpi();
      case Pid::kaon:
        return d.CombDLLk();
      case Pid::proton:
        return d.CombDLLp();
      default:
        throw std::domain_error{"impossible PID type"};
      }
    }
  }

  /** @brief helpers for probNN
   */
  template <Pid id>
  struct has_probNN {
    template <typename T>
    using check_for_probNN_id = decltype( std::declval<T const&>().template probNN<id>() );
    template <typename T>
    static constexpr bool value = Gaudi::cpp17::is_detected_v<check_for_probNN_id, T>;
  };

  template <Pid id, typename T>
  constexpr bool has_probNN_v = has_probNN<id>::template value<T>;

  template <typename StatePos_t, typename StateDir_t, typename VertexPos_t>
  [[gnu::always_inline]] inline auto impactParameterSquared( StatePos_t const& state_pos, StateDir_t const& state_dir,
                                                             VertexPos_t const& vertex ) {
    // TODO: handle one track, multiple vertices case..
    return ( state_pos - StatePos_t{vertex} ).Cross( state_dir ).mag2() / state_dir.mag2();
  }

} // namespace Functors::detail

namespace Functors::Track {

  /**
   * @brief Retrieve const Container with pointers to all the states
   */
  struct States : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).states();
    }
  };

  /**
   * @brief referencePoint, as defined by the referencePoint() accessor.
   * @note referencePoint is the position at which the object has its threeMomentum
   * defined
   */
  struct ReferencePoint : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::referencePoint;
      return referencePoint( d );
    }
  };

  /** @brief slopes vector, as defined by the slopes() accessor.
   */
  struct Slopes : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::slopes;
      return slopes( d );
    }
  };

  /** @brief FourMomentum vector e.g. (px, py, pz, E)
   */
  struct FourMomentum : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::fourMomentum;
      return fourMomentum( d );
    }
  };

  /** @brief ThreeMomentum vector e.g. (px, py, pz)
   */
  struct ThreeMomentum : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      using LHCb::Event::threeMomentum;
      return threeMomentum( d );
    }
  };

  /** @brief Return covariance matrix of input
   */
  struct Covariance : public Function {
    template <typename Data>
    decltype( auto ) operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).covariance();
    }
  };

  /** @brief Charge, as defined by the charge() accessor.
   */
  struct Charge : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).charge();
    }
  };

  /** @brief IsMuon, as defined by the accessor of the same name.
   */
  struct IsMuon : public Predicate {
    bool operator()( LHCb::ProtoParticle const& pp ) const {
      if ( auto pid = pp.muonPID(); pid ) return ( *this )( *pid );
      return bool( pp.info( LHCb::ProtoParticle::additionalInfo::MuonPIDStatus, 0 ) );
    }

    bool operator()( LHCb::Particle const& p ) const {
      auto const* pp = p.proto();
      return pp && ( *this )( *pp );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).IsMuon() ) {
      return Sel::Utils::deref_if_ptr( d ).IsMuon();
    }
  };

  /** @brief InMuon to access the Muon Inacceptance information.
   */
  struct InAcceptance : public Predicate {
    auto operator()( LHCb::Particle const& p ) const {
      auto const* pp = p.proto();
      if ( !pp ) { return false; }
      auto pid = pp->muonPID();
      if ( pid ) return pid->InAcceptance();
      return bool( pp->info( std::decay_t<decltype( *pp )>::additionalInfo::MuonPIDStatus, 0 ) );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    // template <typename Data>
    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( d.InAcceptance() ) {
      return d.InAcceptance();
    }
  };

  /** @brief IsMuonTight, as defined by the accessor of the same name.
   */
  struct IsMuonTight : public Predicate {
    bool operator()( LHCb::ProtoParticle const& pp ) const {
      auto pid = pp.muonPID();
      if ( pid ) return pid->IsMuonTight();
      return bool( pp.info( LHCb::ProtoParticle::additionalInfo::MuonPIDStatus, 0 ) );
    }

    bool operator()( LHCb::Particle const& p ) const {
      auto const* pp = p.proto();
      return pp && ( *this )( *pp );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).IsMuonTight() ) {
      return Sel::Utils::deref_if_ptr( d ).IsMuonTight();
    }
  };

  /** @brief Chi2Corr, as defined by the accessor of the same name. Chi2 taking into account correlations using only
   * hits from the Muon detector.
   */
  struct MuonChi2Corr : public Function {
    float operator()( LHCb::Particle const& p ) const {
      auto const* pp = p.proto();
      if ( !pp ) { return false; }
      auto pid = pp->muonPID();
      return pid->chi2Corr();
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).Chi2Corr() ) {
      return Sel::Utils::deref_if_ptr( d ).Chi2Corr();
    }
  };

  /** @brief MuonLLMu, as defined by the accessor LLMu. Muon likelihood using only Muon detector information
   */
  struct MuonLLMu : public Function {
    float operator()( LHCb::Particle const& p ) const {
      auto const* pp = p.proto();
      if ( !pp ) { return false; }
      auto pid = pp->muonPID();
      return pid->MuonLLMu();
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).LLMu() ) {
      return Sel::Utils::deref_if_ptr( d ).LLMu();
    }
  };

  /** @brief MuonLLBg, as defined by the accessor LLBg. Background likelihood using only Muon detector information
   */
  struct MuonLLBg : public Function {
    float operator()( LHCb::Particle const& p ) const {
      auto const* pp = p.proto();
      if ( !pp ) { return false; }
      auto pid = pp->muonPID();
      return pid->MuonLLBg();
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).LLBg() ) {
      return Sel::Utils::deref_if_ptr( d ).LLBg();
    }
  };

  /** @brief MuonCatBoost, as defined by the accessor CatBoost. MVA output using only Muon detector and track
   * information
   */
  struct MuonCatBoost : public Function {
    float operator()( LHCb::Particle const& p ) const {
      auto const* pp = p.proto();
      if ( !pp ) { return false; }
      auto pid = pp->muonPID();
      return pid->muonMVA2();
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( Sel::Utils::deref_if_ptr( d ).CatBoost() ) {
      return Sel::Utils::deref_if_ptr( d ).CatBoost();
    }
  };

  using additInfo = LHCb::ProtoParticle::additionalInfo;
  /** @brief Bool and Numerical (Float and Integer) AdditionalInfo, get information from Calo Additional Info and from
   * Particle v2
   */
  template <additInfo ai>
  struct BoolAdditionalInfo : public Predicate {
    auto operator()( LHCb::Particle const& p ) const {
      auto const* pp = p.proto();
      if ( !pp ) { return false; }
      return bool( pp->info( ai, 0 ) );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const {
      switch ( ai ) {
      case additInfo::CaloHasBrem:
        return d.HasBrem();
      case additInfo::InAccEcal:
        return d.InEcal();
      case additInfo::InAccHcal:
        return d.InHcal();
      case additInfo::InAccBrem:
        return d.InBrem();
      default:
        throw std::domain_error{"Invalid additional info"};
      }
    }
  };

  using HasBrem = BoolAdditionalInfo<additInfo::CaloHasBrem>;
  using InEcal  = BoolAdditionalInfo<additInfo::InAccEcal>;
  using InHcal  = BoolAdditionalInfo<additInfo::InAccHcal>;
  using InBrem  = BoolAdditionalInfo<additInfo::InAccBrem>;

  template <additInfo ai>
  struct NumAdditionalInfo : public Function {
    auto operator()( LHCb::Particle const& p ) const {
      auto const* pp = p.proto();
      return ( pp && pp->hasInfo( ai ) ) ? Functors::Optional{pp->info( ai, 0.f )} : std::nullopt;
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const {
      switch ( ai ) {
      case additInfo::CaloBremEnergy:
        return d.BremEnergy();
      case additInfo::CaloBremBendingCorr:
        return d.BremBendingCorrection();
      case additInfo::BremPIDe:
        return d.BremPIDe();
      case additInfo::EcalPIDe:
        return d.EcalPIDe();
      case additInfo::EcalPIDmu:
        return d.EcalPIDmu();
      case additInfo::HcalPIDe:
        return d.HcalPIDe();
      case additInfo::HcalPIDmu:
        return d.HcalPIDmu();
      case additInfo::RichDLLp:
        return d.RichDLLp();
      case additInfo::RichDLLk:
        return d.RichDLLk();
      case additInfo::RichDLLpi:
        return d.RichDLLpi();
      case additInfo::RichDLLd:
        return d.RichDLLd();
      case additInfo::RichDLLbt:
        return d.RichDLLbt();
      case additInfo::CaloEoverP:
        return d.ElectronShowerEoP();
      case additInfo::ElectronShowerDLL:
        return d.ElectronShowerDLL();
      case additInfo::CaloTrMatch:
        return d.ClusterMatch();
      case additInfo::CaloElectronMatch:
        return d.ElectronMatch();
      case additInfo::CaloBremHypoID:
        return d.BremHypoID();
      case additInfo::CaloBremMatch:
        return d.BremHypoMatch();
      case additInfo::CaloChargedEcal:
        return d.ElectronEnergy();
      case additInfo::CaloBremHypoEnergy:
        return d.BremHypoEnergy();
      case additInfo::CaloBremHypoDeltaX:
        return d.BremHypoDeltaX();
      case additInfo::CaloBremTBEnergy:
        return d.BremTrackBasedEnergy();
      default:
        throw std::domain_error{"Invalid additional info"};
      }
    }
  };

  using BremEnergy           = NumAdditionalInfo<additInfo::CaloBremEnergy>;
  using BremBendCorr         = NumAdditionalInfo<additInfo::CaloBremBendingCorr>;
  using BremPIDe             = NumAdditionalInfo<additInfo::BremPIDe>;
  using EcalPIDe             = NumAdditionalInfo<additInfo::EcalPIDe>;
  using EcalPIDmu            = NumAdditionalInfo<additInfo::EcalPIDmu>;
  using HcalPIDe             = NumAdditionalInfo<additInfo::HcalPIDe>;
  using HcalPIDmu            = NumAdditionalInfo<additInfo::HcalPIDmu>;
  using RichDLLe             = NumAdditionalInfo<additInfo::RichDLLe>;
  using RichDLLmu            = NumAdditionalInfo<additInfo::RichDLLmu>;
  using RichDLLp             = NumAdditionalInfo<additInfo::RichDLLp>;
  using RichDLLk             = NumAdditionalInfo<additInfo::RichDLLk>;
  using RichDLLpi            = NumAdditionalInfo<additInfo::RichDLLpi>;
  using RichDLLd             = NumAdditionalInfo<additInfo::RichDLLd>;
  using RichDLLbt            = NumAdditionalInfo<additInfo::RichDLLbt>;
  using ElectronShowerEoP    = NumAdditionalInfo<additInfo::CaloEoverP>;
  using ElectronShowerDLL    = NumAdditionalInfo<additInfo::ElectronShowerDLL>;
  using ClusterMatch         = NumAdditionalInfo<additInfo::CaloTrMatch>;
  using ElectronMatch        = NumAdditionalInfo<additInfo::CaloElectronMatch>;
  using BremHypoMatch        = NumAdditionalInfo<additInfo::CaloBremMatch>;
  using ElectronEnergy       = NumAdditionalInfo<additInfo::CaloChargedEcal>;
  using BremHypoID           = NumAdditionalInfo<additInfo::CaloBremHypoID>;
  using BremHypoEnergy       = NumAdditionalInfo<additInfo::CaloBremHypoEnergy>;
  using BremHypoDeltaX       = NumAdditionalInfo<additInfo::CaloBremHypoDeltaX>;
  using BremTrackBasedEnergy = NumAdditionalInfo<additInfo::CaloBremTBEnergy>;
  using ElectronID           = NumAdditionalInfo<additInfo::CaloChargedID>;

  /** @brief HcalEoP, as defined by the HcalEoP accessor.
   */
  struct HcalEoP : public Function {
    auto operator()( LHCb::Particle const& p ) const {
      auto const* pp    = p.proto();
      auto const* track = pp ? pp->track() : nullptr;
      return ( pp && pp->hasInfo( additInfo::CaloHcalE ) && track->p() )
                 ? Functors::Optional{( pp->info( additInfo::CaloHcalE, 0.f ) ) / ( track->p() )}
                 : std::nullopt;
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }

    template <typename Data>
    auto operator()( Data const& d ) const {
      return d.HcalEoP();
    }
  };

  /** @brief ClusterID, as defined by the accessor of the same name.
   */
  struct ClusterID : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return d.ClusterID();
    }
  };

  /** @brief Number of degrees of freedom, as defined by the nDoF accessor.
   */
  struct nDoF : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nDoF();
    }
  };

  /** @brief chi^2/d.o.f., as defined by the chi2PerDoF accessor.
   *
   * If the input is a legacy LHCb::Particle with a track, the track object's
   * accessor is used. If a track is not present but a vertex is the vertex's
   * accessor is used.
   */
  struct Chi2PerDoF : public Function {
    template <typename Data>
    auto operator()( Data&& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        auto pp  = Sel::Utils::deref_if_ptr( d ).proto();
        auto trk = ( pp ) ? pp->track() : nullptr;
        auto ev  = Sel::Utils::deref_if_ptr( d ).endVertex();
        return ( trk ) ? ( Functors::Optional{Sel::get::chi2PerDoF( Sel::Utils::deref_if_ptr( trk ) )} )
                       : ( ( ev ) ? Functors::Optional{Sel::get::chi2PerDoF( Sel::Utils::deref_if_ptr( ev ) )}
                                  : std::nullopt );
      } else {
        return Sel::get::chi2PerDoF( Sel::Utils::deref_if_ptr( d ) );
      }
    }
  };

  /** @brief chi^2, as defined by the chi2 accessor.
   *
   * If the input is a legacy LHCb::Particle with a track, the track object's
   * accessor is used. If a track is not present but a vertex is the vertex's
   * accessor is used.
   */
  struct Chi2 : public Function {
    template <typename Data>
    auto operator()( Data&& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        auto pp  = Sel::Utils::deref_if_ptr( d ).proto();
        auto trk = ( pp ) ? pp->track() : nullptr;
        auto ev  = Sel::Utils::deref_if_ptr( d ).endVertex();
        return ( trk ) ? ( Functors::Optional{Sel::Utils::deref_if_ptr( trk ).chi2()} )
                       : ( ( ev ) ? Functors::Optional{Sel::Utils::deref_if_ptr( ev ).chi2()} : std::nullopt );
      } else {
        return Sel::Utils::deref_if_ptr( d ).chi2();
      }
    }
  };

  /** @brief q/p, as defined by the qOverP accessor.
   */
  struct QoverP : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      if constexpr ( detail::is_legacy_track<decltype( d )> ) {
        return Sel::Utils::deref_if_ptr( d ).firstState().qOverP();
      } else {
        return Sel::Utils::deref_if_ptr( d ).qOverP();
      }
    }
  };

  /** @brief Ghost probability, as defined by the ghostProbability accessor.
   */
  struct GhostProbability : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).ghostProbability();
    }
  };

  struct ClosestToBeamState : public Function {
    template <typename Data>
    auto operator()( Data const& track ) const {
      using LHCb::Event::trackState;
      return trackState( track );
    }
  };

  template <detail::Pid pid>
  struct ComDLL : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return detail::combDLL<pid>( d );
    }
  };

  /** @brief PIDmu, as defined by the CombDLLmu variable
   */
  using PIDmu = ComDLL<detail::Pid::muon>;

  /** @brief PIDp, as defined by the CombDLLp variable
   */
  using PIDp = ComDLL<detail::Pid::proton>;

  /** @brief PIDe, as defined by the CombDLLe variable
   */
  using PIDe = ComDLL<detail::Pid::electron>;

  /** @brief PIDk, as defined by the CombDLLk variable
   */
  using PIDk = ComDLL<detail::Pid::kaon>;

  /** @brief PIDpi, as defined by the CombDLLpi variable
   */
  using PIDpi = ComDLL<detail::Pid::pion>;

  /** @brief ProbNN templated definition
   */
  template <detail::Pid id>
  struct ProbNN : public Function {
    template <typename T>
    auto operator()( const T& d ) const {
      if constexpr ( detail::has_probNN_v<id, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>> ) {
        return Sel::Utils::deref_if_ptr( d ).template probNN<id>();
      } else if constexpr ( detail::has_proto_v<std::decay_t<std::remove_pointer_t<std::decay_t<T>>>> ) {
        auto        pp = Sel::Utils::deref_if_ptr( d ).proto();
        return pp ? operator()( *pp ) : std::nullopt;
      } else if constexpr ( detail::is_proto_particle<T> ) {
        auto constexpr pid = to_ppai( id );
        return Sel::Utils::deref_if_ptr( d ).hasInfo( pid )
                   ? Functors::Optional{Sel::Utils::deref_if_ptr( d ).info( pid, 0. )}
                   : std::nullopt;
      } else {
        throw GaudiException{"The type T neither has a `proto()` member function nor a `probNN<id>`() member function "
                             "-- sorry, not supported",
                             "Functors::Track::ProbNN::operator()", StatusCode::FAILURE};
      }
    }
  };

  /** @brief The explicit definition for the probNN quantities
   * the interpreter doesn't like that. See
   * https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/2471#note_4863872
   *
   *    constexpr auto PROBNN_D     = ProbNN<detail::Pid::deuteron>{};
   *    constexpr auto PROBNN_E     = ProbNN<detail::Pid::electron>{};
   *    constexpr auto PROBNN_GHOST = ProbNN<detail::Pid::ghost>{};
   *    constexpr auto PROBNN_K     = ProbNN<detail::Pid::kaon>{};
   *    constexpr auto PROBNN_MU    = ProbNN<detail::Pid::muon>{};
   *    constexpr auto PROBNN_PI    = ProbNN<detail::Pid::pion>{};
   *    constexpr auto PROBNN_P     = ProbNN<detail::Pid::proton>{};
   */
  /** @brief The explicit definition for the probNN quantities
   * Warning: these are types, so you need a {} to get an instance.
   */
  using PROBNN_D_t     = ProbNN<detail::Pid::deuteron>;
  using PROBNN_E_t     = ProbNN<detail::Pid::electron>;
  using PROBNN_GHOST_t = ProbNN<detail::Pid::ghost>;
  using PROBNN_K_t     = ProbNN<detail::Pid::kaon>;
  using PROBNN_MU_t    = ProbNN<detail::Pid::muon>;
  using PROBNN_PI_t    = ProbNN<detail::Pid::pion>;
  using PROBNN_P_t     = ProbNN<detail::Pid::proton>;

  /** @brief nHits, as defined by the nHits accessor.
   */
  struct nHits : public Function {
    static constexpr auto name() { return "nHits"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nHits();
    }
  };

  /** @brief number of VP hits
   */
  struct nVPHits : public Function {
    static constexpr auto name() { return "nVPHits"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nVPHits();
    }
  };

  /** @brief number of UT hits
   */
  struct nUTHits : public Function {
    static constexpr auto name() { return "nUTHits"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nUTHits();
    }
  };

  /** @brief number of FT hits
   */
  struct nFTHits : public Function {
    static constexpr auto name() { return "nFTHits"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nFTHits();
    }
  };

  /** @brief Track history
   */
  struct History : public Function {
    static constexpr auto name() { return "History"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).history();
    }
  };

  /** @brief Track flag
   */
  struct Flag : public Function {
    static constexpr auto name() { return "Flag"; }
    template <typename Data>
    LHCb::Event::Enum::Track::Flag operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).flag();
    }
  };

  template <LHCb::Event::Enum::Track::Flag f>
  struct HasTrackFlag : public Predicate {
    static constexpr auto name() { return "HasTrackFlag"; }
    template <typename Data>
    bool operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).checkFlag( f );
    }
  };

  /** @brief Track type
   */
  struct Type : public Function {
    static constexpr auto name() { return "Type"; }
    template <typename Data>
    LHCb::Event::Enum::Track::Type operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).type();
    }
  };

  template <LHCb::Event::Enum::Track::Type t>
  struct IsTrackType : public Predicate {
    static constexpr auto name() { return "IsTrackType"; }
    template <typename Data>
    bool operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).type() == t;
    }
  };

  /** @brief Track hasT, the input of this functor must be the track type
   */
  struct HasT : public Predicate {
    static constexpr auto name() { return "HasT"; }
    bool                  operator()( LHCb::Event::Enum::Track::Type t ) const {
      return t == LHCb::Event::Enum::Track::Type::Ttrack || t == LHCb::Event::Enum::Track::Type::Downstream ||
             t == LHCb::Event::Enum::Track::Type::Long || t == LHCb::Event::Enum::Track::Type::SeedMuon;
    }
  };

  /** @brief Track hasUT, the input of this functor must be the track type
   */
  struct HasUT : public Predicate {
    static constexpr auto name() { return "HasUT"; }
    bool                  operator()( LHCb::Event::Enum::Track::Type t ) const {
      return t == LHCb::Event::Enum::Track::Type::Downstream || t == LHCb::Event::Enum::Track::Type::Upstream ||
             t == LHCb::Event::Enum::Track::Type::Long || t == LHCb::Event::Enum::Track::Type::MuonUT;
    }
  };

  /** @brief Track hasVelo, the input of this functor must be the track type
   */
  struct HasVelo : public Predicate {
    static constexpr auto name() { return "HasVelo"; }
    bool                  operator()( LHCb::Event::Enum::Track::Type t ) const {
      return t == LHCb::Event::Enum::Track::Type::Velo || t == LHCb::Event::Enum::Track::Type::Upstream ||
             t == LHCb::Event::Enum::Track::Type::Long || t == LHCb::Event::Enum::Track::Type::VeloMuon ||
             t == LHCb::Event::Enum::Track::Type::SeedMuon || t == LHCb::Event::Enum::Track::Type::MuonUT;
    }
  };

  /**
   * @brief MC_Reconstructed, return the reconstructed category
   * for MC Particle. The input of this functor must be reconstructed
   * object, such as LHCb::ProtoParticle and LHCb::Particle.
   *
   * For possible values of IMCReconstructed::RecCategory, see IMCReconstructed.h
   *
   */
  struct MC_Reconstructed : public Function {
    static constexpr auto name() { return "MC_Reconstructed"; }
    template <typename Data>
    IMCReconstructed::RecCategory operator()( Data const& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        const auto* pp = Sel::Utils::deref_if_ptr( d ).proto();
        return get_reconstructed_category( pp );
      } else {
        const auto* rec = &Sel::Utils::deref_if_ptr( d );
        return get_reconstructed_category( rec );
      };
    }

  private:
    template <typename T>
    IMCReconstructed::RecCategory get_reconstructed_category( const T* rec ) const {
      if ( !rec ) return IMCReconstructed::RecCategory::NotReconstructed;

      if ( !rec->charge() ) { /// Neutral particle
        return IMCReconstructed::RecCategory::Neutral;
      }
      const auto* track = rec->track();
      if ( track && !track->checkFlag( LHCb::Track::Flags::Clone ) ) {
        LHCb::Event::Enum::Track::Type t{track->type()};
        switch ( t ) {
        case LHCb::Event::Enum::Track::Type::Long:
          return IMCReconstructed::RecCategory::ChargedLong;
        case LHCb::Event::Enum::Track::Type::Downstream:
          return IMCReconstructed::RecCategory::ChargedDownstream;
        case LHCb::Event::Enum::Track::Type::Upstream:
          return IMCReconstructed::RecCategory::ChargedUpstream;
        case LHCb::Event::Enum::Track::Type::Unknown:
          return IMCReconstructed::RecCategory::CatUnknown;
        case LHCb::Event::Enum::Track::Type::Velo:
          return IMCReconstructed::RecCategory::ChargedVelo;
        case LHCb::Event::Enum::Track::Type::Ttrack:
          return IMCReconstructed::RecCategory::ChargedTtrack;
        case LHCb::Event::Enum::Track::Type::Muon:
          return IMCReconstructed::RecCategory::ChargedMuon;
        default: /* empty on purpose */;
        }
      }
      return IMCReconstructed::RecCategory::NotReconstructed;
    }
  };

  /** @brief Wrapper around Particle/ChargedBasic/Track to access brem corrected information
   */
  struct Bremsstrahlung : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return LHCb::Event::Bremsstrahlung::BremsstrahlungWrapper<Data>( d );
    }
  };

} // namespace Functors::Track
