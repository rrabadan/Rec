/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "Gaudi/Parsers/Grammars.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/SerializeSTL.h"
#include "GaudiKernel/StatusCode.h"
#include <Gaudi/Parsers/Factory.h>

namespace ThOr {
  struct FunctorDesc {
    std::string code{};
    std::string repr{};
  };

  inline bool operator==( FunctorDesc const& lhs, FunctorDesc const& rhs ) {
    return lhs.code == rhs.code && lhs.repr == rhs.repr;
  }

  namespace Defaults {
    inline FunctorDesc const ALL{"::Functors::AcceptAll{}", "ALL"};
  } // namespace Defaults
} // namespace ThOr

BOOST_FUSION_ADAPT_STRUCT( ThOr::FunctorDesc, ( std::string, code )( std::string, repr ) )

namespace Gaudi::Parsers {
  template <typename Iterator, typename Skipper>
  struct FunctorDescGrammar : qi::grammar<Iterator, ThOr::FunctorDesc(), Skipper> {
    using ResultT = ThOr::FunctorDesc;
    FunctorDescGrammar() : FunctorDescGrammar::base_type( FunctorDesc_literal ) {
      FunctorDesc_literal = '(' >> gstring >> ',' >> gstring >> ')';
    }
    qi::rule<Iterator, ThOr::FunctorDesc(), Skipper> FunctorDesc_literal;
    StringGrammar<Iterator, Skipper>                 gstring;
  };
  REGISTER_GRAMMAR( ThOr::FunctorDesc, FunctorDescGrammar );
} // namespace Gaudi::Parsers

namespace std {
  std::ostream& operator<<( std::ostream& o, ThOr::FunctorDesc const& f );
} // namespace std
