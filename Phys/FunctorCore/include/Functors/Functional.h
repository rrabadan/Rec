/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include "Functors/Utilities.h"

namespace Functors::Functional {

  /** @brief map functor over range.
   *
   * */
  template <typename F>
  struct Map final : public Function {

    Map( F f ) : m_f{std::move( f )} {}

    constexpr static bool requires_explicit_mask = true;
    static_assert( detail::is_functor_function_v<F>, "Functors::Functional::Map must wrap a functor" );

    /* Improve error messages. */
    constexpr auto name() const { return "Map( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      return
          [functor = detail::prepare( m_f, evtCtx, top_level )]( auto mask, auto const& range, auto const&... input ) {
            using std::begin;
            using std::end;
            std::vector<std::invoke_result_t<decltype( functor ), mask_arg_t, decltype( mask ),
                                             decltype( *begin( range ) ), decltype( input )...>>
                out;
            out.reserve( range.size() );
            std::transform( begin( range ), end( range ), std::back_inserter( out ),
                            [&]( const auto& val ) { return functor( mask_arg, mask, val, input... ); } );

            return out;
          };
    }

  private:
    F m_f;
  };

  /** @brief map predicate functor over range.
   *  Early stopping if one value is true
   * */
  template <typename Pred>
  struct MapAnyOf final : public Predicate {

    MapAnyOf( Pred f ) : m_f{std::move( f )} {}

    constexpr static bool requires_explicit_mask = true;
    static_assert( detail::is_functor_predicate_v<Pred>, "Functors::Functional::MapAnyOf must wrap a Predicate" );

    /* Improve error messages. */
    constexpr auto name() const { return "MapPredicate( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      return
          [functor = detail::prepare( m_f, evtCtx, top_level )]( auto mask, auto const& range, auto const&... input ) {
            for ( auto const& item : range ) {
              // mask could be boolean or SIMD mask type
              using Sel::Utils::any;

              mask = mask && functor( mask_arg, mask, item, input... );
              if ( any( mask ) ) { break; }
            }
            return mask;
          };
    }

  private:
    Pred m_f;
  };

  /** @brief map predicate functor over range.
   *  Early stopping if one value is false
   * */
  template <typename Pred>
  struct MapAllOf final : public Predicate {

    MapAllOf( Pred f ) : m_f{std::move( f )} {}

    constexpr static bool requires_explicit_mask = true;
    static_assert( detail::is_functor_predicate_v<Pred>, "Functors::Functional::MapAllOf must wrap a Predicate" );

    /* Improve error messages. */
    constexpr auto name() const { return "MapPredicate( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      return
          [functor = detail::prepare( m_f, evtCtx, top_level )]( auto mask, auto const& range, auto const&... input ) {
            for ( auto const& item : range ) {
              // mask could be boolean or SIMD mask type
              using Sel::Utils::none;

              mask = mask && functor( mask_arg, mask, item, input... );
              if ( none( mask ) ) { break; }
            }
            return mask;
          };
    }

  private:
    Pred m_f;
  };

  /**
   * @brief return front element of range
   * */
  struct Front final : public Function {
    template <typename Data>
    decltype( auto ) operator()( Data&& range ) const {
      return range.front();
    }
  };
  /**
   * @brief return back element of range
   * */
  struct Back final : public Function {
    template <typename Data>
    decltype( auto ) operator()( Data&& range ) const {
      return range.back();
    }
  };

  /* *
   * @brief reverse the order of an range
   * */
  struct Reverse final : public Function {
    template <typename Data>
    auto operator()( Data&& range ) const {
      return std::vector<std::decay_t<decltype( range[0] )>>{std::rbegin( range ), std::rend( range )};
    }
  };

  /* *
   * @brief return min element of range
   * */
  struct Min final : public Function {
    template <typename Data>
    decltype( auto ) operator()( Data&& range ) const {
      using float_t = std::remove_reference_t<decltype( range[0] )>;
      using std::min;

      // FIXME doc.
      return std::accumulate( std::begin( range ), std::end( range ), float_t{std::numeric_limits<float>::max()},
                              []( auto curr_min_value, auto value ) { return min( curr_min_value, value ); } );
    }
  };

  /* *
   * @brief return max element of range
   * */
  struct Max final : public Function {
    template <typename Data>
    decltype( auto ) operator()( Data&& range ) const {
      using float_t = std::remove_reference_t<decltype( range[0] )>;
      using std::max;

      // FIXME doc.
      return std::accumulate( std::begin( range ), std::end( range ), float_t{std::numeric_limits<float>::lowest()},
                              []( auto curr_max_value, auto value ) { return max( curr_max_value, value ); } );
    }
  };

  /* *
   * @brief return sum element of range
   * */
  struct Sum final : public Function {

    Sum( float init_value = 0. ) : m_init{init_value} {}

    template <typename Data>
    auto operator()( Data&& range ) const {
      using val_t = std::remove_reference_t<decltype( range[0] )>;
      return std::accumulate( std::begin( range ), std::end( range ), val_t{m_init} );
    }

    float m_init;
  };

  /**
   * @brief Cast the provided argument to the specified template argument
   * optional is empty
   * */
  template <typename T>
  struct CastTo final : public Function {
    template <typename U>
    auto operator()( U&& value ) const {
      return static_cast<T>( std::forward<U>( value ) );
    }
  };

  /**
   * @brief Return the value inside a Functors::Optional, or a default value if
   * optional is empty
   * */
  template <typename T>
  struct ValueOr final : public Function {
    ValueOr( T value ) : m_value{std::move( value )} {}

    template <typename U>
    auto operator()( U&& opt_value ) const {
      constexpr bool input_is_optional = detail::is_our_optional_v<std::decay_t<decltype( opt_value )>>;
      constexpr bool value_is_tuple    = detail::is_tuple_v<std::decay_t<decltype( m_value )>>;

      if constexpr ( input_is_optional ) {
        if ( opt_value.has_value() ) { return std::forward<U>( opt_value ).value(); }

        // Expand tuple to initialize the default value, this is used to initialize classes such as
        // LHCb::LinAlg::Vec<float_v,N>
        if constexpr ( value_is_tuple ) {
          using value_type_t = typename std::decay_t<U>::value_type;
          static_assert( detail::is_constructible_from_tuple_v<value_type_t, std::decay_t<decltype( m_value )>>,
                         "The return type must be constructible by the default args." );
          return std::apply( []( auto&&... args ) { return value_type_t{args...}; }, m_value );
        }
        // Static cast the elemental types
        else {
          static_assert( std::is_constructible_v<typename U::value_type, T>,
                         "Return type value_t needs to be constructible from the member variable of type T" );
          return static_cast<typename U::value_type>( m_value );
        }
      }
      // Forward the not optional elements
      else {
        return std::forward<U>( opt_value );
      }
    }

  private:
    T m_value;
  };

  /**
   * @brief Return the value from a map given a key
   * */
  template <typename Key>
  struct ValueFromDict final : public Function {
  public:
    ValueFromDict( Key k ) : m_key{std::move( k )} {}

    template <typename Dict>
    auto operator()( Dict&& dict ) const {
      auto i = dict.find( m_key );
      return i != dict.end() ? Functors::Optional{i->second} : std::nullopt;
    }

  private:
    Key m_key;
  };

  /**
   * @brief Return the value inside a Functors::Optional.
   * If nullopt it throws an error.
   * */
  struct Value final : public Function {
    template <typename U>
    auto operator()( U&& opt_value ) const {
      /// check if its is optional
      if constexpr ( detail::is_our_optional_v<std::decay_t<decltype( opt_value )>> ) {
        if ( opt_value.has_value() ) // return the value if it exists
          return std::forward<U>( opt_value ).value();
        else // throw an exception if it does not
          throw GaudiException( "Functors::Value: Optional has no value. Consider using F.VALUE_OR functor instead.",
                                "Functors::Value", StatusCode::FAILURE );
      } else { // if it is not optional just return the value
        return std::forward<U>( opt_value );
      }
    }
  };

  /**
   * @brief Return true if the input has a valid value.
   * */
  struct HasValue final : public Predicate {
    template <typename U>
    bool operator()( U&& opt_value ) const {
      if constexpr ( detail::is_our_optional_v<std::decay_t<decltype( opt_value )>> ) {
        return opt_value.has_value();
      } else {
        return true;
      }
    }
  };

} // namespace Functors::Functional

// specialization that tells the composedfunctor that this functor will turn an
// optional into a concrete value even if optional is not engaged
namespace Functors::detail {
  template <typename T>
  struct is_optional_unwrapper<Functors::Functional::ValueOr<T>> : std::true_type {};

  template <>
  struct is_optional_unwrapper<Functors::Functional::Value> : std::true_type {};
  template <>
  struct is_optional_unwrapper<Functors::Functional::HasValue> : std::true_type {};
} // namespace Functors::detail
