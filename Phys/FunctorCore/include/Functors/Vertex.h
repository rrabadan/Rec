/*****************************************************************************\
 * (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/System.h"
#include "Kernel/IVertexFit.h"
#include <GaudiAlg/GaudiTupleAlg.h>

/** @file Vertex.h
 *  @brief Definitions of functors for vertex fit.
 *
 */
namespace Functors::detail {

  class IVertexFitHolder {

    class Parent {
      const void* m_parent                                                                   = nullptr;
      const LHCb::DetDesc::ConditionContext& ( *m_cond )( void const*, EventContext const& ) = nullptr;
      MsgStream& ( *m_get_warning )( void const* )                                           = nullptr;
      LHCb::DetDesc::ConditionAccessor<DetectorElement> m_det;

    public:
      template <typename T>
      Parent( T* ptr, const std::string& geom_name )
          : m_parent{ptr}
          , m_cond{[]( const void* parent, const EventContext& ctx ) -> decltype( auto ) {
            return static_cast<T const*>( parent )->getConditionContext( ctx );
          }}
          , m_get_warning{[]( const void* parent ) -> decltype( auto ) {
            return static_cast<T const*>( parent )->warning();
          }}
          , m_det( ptr, geom_name ) {}

      MsgStream& warning() const { return ( *m_get_warning )( m_parent ); }

      auto* geometry( EventContext const& ctx ) const { return m_det.get( ( *m_cond )( m_parent, ctx ) ).geometry(); }
    };

    std::optional<Parent>                 m_parent;
    std::string                           m_tool_name;
    std::string                           m_geom_name;
    std::optional<ToolHandle<IVertexFit>> m_vtxfit_tool;

  public:
    IVertexFitHolder( std::string tool_name, std::string geom_name = LHCb::standard_geometry_top )
        : m_tool_name{std::move( tool_name )}, m_geom_name{std::move( geom_name )} {}

    void emplace( TopLevelInfo& top_level ) {
      m_vtxfit_tool.emplace( m_tool_name, top_level.algorithm() );

      auto try_emplace = [&]( auto* parent ) {
        if ( parent ) {
          m_parent.emplace( parent, m_geom_name );
          m_vtxfit_tool->retrieve().ignore();
        }
        return parent;
      };

      if ( try_emplace( dynamic_cast<LHCb::DetDesc::AlgorithmWithCondition<>*>( top_level.algorithm() ) ) ) return;
      if ( try_emplace( dynamic_cast<LHCb::DetDesc::AlgorithmWithCondition<GaudiTupleAlg>*>( top_level.algorithm() ) ) )
        return;

      throw GaudiException{"VtxFitChi2::bind", "attempting to bind to algorithm which is not a condition holder",
                           StatusCode::FAILURE};
    }

    auto prepare( EventContext const& ctx ) const {
      assert( m_vtxfit_tool.has_value() );
      assert( m_parent.has_value() );
      if ( !m_vtxfit_tool->get() ) { m_parent.value().warning() << "tool retrieval too late!!!" << endmsg; }
      return [tool = m_vtxfit_tool->get(), geom = m_parent->geometry( ctx ),
              parent = LHCb::get_pointer( m_parent )]( auto const& part1, auto const& part2 ) {
        LHCb::Vertex vtx{};
        auto const*  p1 = &Sel::Utils::deref_if_ptr( part1 );
        auto const*  p2 = &Sel::Utils::deref_if_ptr( part2 );
        return tool->fit( vtx, *p1, *p2, *geom ).isSuccess() ? vtx.chi2PerDoF() : std::numeric_limits<double>::max();
      };
    }
  };
} // namespace Functors::detail

namespace Functors::Vertex {

  /*  @brief Performs a vertex fit of the reference particle and candidate.
   *  @args: Algorithm name (string) to use for fitting
   *  @warning The vertex fit is not guaranteed to suceed!
   *  @return  The CHI2 value of the fit, or std::numeric_limits<double>::max() in case of failure.
   */
  struct VtxFitChi2 : public Function {
    VtxFitChi2( std::string tool_name = "ParticleVertexFitter" ) : m_vtxfit{std::move( tool_name )} {}

    void bind( TopLevelInfo& top_level ) { m_vtxfit.emplace( top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& ) const {
      return [calc = m_vtxfit.prepare( evtCtx )]( const auto& p1, const auto& p2 ) { return calc( p1, p2 ); };
    }

  private:
    detail::IVertexFitHolder m_vtxfit;
  };

  /*  @brief Performs a vertex fit of the reference particle and candidate.
   *  @args: Algorithm name (string) to use for fitting,
   *        Threshold value (float) of the CHI2 of the fit
   *  @warning The vertex fit is not guaranteed to suceed!
   *  @return Boolean value
   **/
  struct VtxFitChi2Cut : public Predicate {
    VtxFitChi2Cut( float thresh, std::string tool_name = "ParticleVertexFitter" )
        : m_thresh{thresh}, m_vtxfit{std::move( tool_name )} {}

    void bind( TopLevelInfo& top_level ) { m_vtxfit.emplace( top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& ) const {
      return [calc = m_vtxfit.prepare( evtCtx ), threshold = m_thresh]( const auto& p1, const auto& p2 ) {
        return calc( p1, p2 ) < threshold;
      };
    }

  private:
    float                    m_thresh;
    detail::IVertexFitHolder m_vtxfit;
  };
} // namespace Functors::Vertex
