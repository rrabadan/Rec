/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_LOKIARRAY_DCT_H
#  define LOKI_LOKIARRAY_DCT_H 1
// ============================================================================
// Include files
// ============================================================================
// Event
// ============================================================================
#  include "Event/Particle.h"
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/Dicts.h"
#  include "LoKi/FuncOps.h"
#  include "LoKi/MoreFunctions.h"
#  include "LoKi/Operators.h"
// ============================================================================
#  include "LoKi/AChild.h"
#  include "LoKi/AKinematics.h"
#  include "LoKi/AParticleCuts.h"
#  include "LoKi/ATypes.h"
#  include "LoKi/LoKiArrayFunctors.h"
// ============================================================================
#  include "LoKi/DictTransform.h"
#  include "LoKi/HybridEngine.h"
#  include "LoKi/IHybridTool.h"
// ============================================================================
/** @file
 *  The dictionaries for the package Phys/LoKiArrayFunctors
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2007-12-01
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Dicts {
    // ========================================================================
    template <>
    class FunCalls<LoKi::ATypes::Combination> {
    private:
      // ======================================================================
      typedef LoKi::ATypes::Combination           Type;
      typedef LoKi::BasicFunctors<Type>::Function Fun;
      // ======================================================================
    public:
      // ======================================================================
      static Fun::result_type __call__( const Fun& fun, const Type& o ) { return fun( o ); }
      static Fun::result_type __call__( const Fun& fun, const Type::Container& o ) {
        return fun( Type( o.begin(), o.end() ) );
      }
      // ======================================================================
      static Fun::result_type __call__( const Fun& fun, const SmartRefVector<LHCb::Particle>& o ) {
        Type::Container c( o.begin(), o.end() );
        return fun( Type( c.begin(), c.end() ) );
      }
      // ======================================================================
    public:
      // ======================================================================
      // __rrshift__
      static Fun::result_type __rrshift__( const Fun& fun, const Type& o ) { return o >> fun; }
      // __rrshift__
      static Fun::result_type __rrshift__( const Fun& fun, const Type::Container& o ) {
        return fun( Type( o.begin(), o.end() ) );
      }
      // __rrshift__
      // static std::vector<Fun::result_type>
      //__rrshift__ ( const Fun& fun  , const std::vector<Type>& o  )
      //{ return o  >> fun  ; }
      // ======================================================================
    public:
      // ======================================================================
      // __rshift__
      static LoKi::FunctorFromFunctor<Type, double> __rshift__( const Fun&                           fun,
                                                                const LoKi::Functor<double, double>& o ) {
        return fun >> o;
      }
      // ======================================================================
      // __rshift__
      static LoKi::FunctorFromFunctor<Type, bool> __rshift__( const Fun& fun, const LoKi::Functor<double, bool>& o ) {
        return fun >> o;
      }
      // ======================================================================
    };
    // ========================================================================
    template <>
    class CutCalls<LoKi::ATypes::Combination> {
    private:
      // ======================================================================
      typedef LoKi::ATypes::Combination            Type;
      typedef LoKi::BasicFunctors<Type>::Predicate Fun;
      // ======================================================================
    public:
      // ======================================================================
      static Fun::result_type __call__( const Fun& fun, const Type& o ) { return fun( o ); }
      static Fun::result_type __call__( const Fun& fun, const Type::Container& o ) {
        return fun( Type( o.begin(), o.end() ) );
      }
      // ======================================================================
      static Fun::result_type __call__( const Fun& fun, const SmartRefVector<LHCb::Particle>& o ) {
        Type::Container c( o.begin(), o.end() );
        return fun( Type( c.begin(), c.end() ) );
      }
      // ======================================================================
    public:
      // ======================================================================
      // __rrshift__
      static Fun::result_type __rrshift__( const Fun& fun, const Type& o ) { return o >> fun; }
      // __rrshift__
      static Fun::result_type __rrshift__( const Fun& fun, const Type::Container& o ) {
        return fun( Type( o.begin(), o.end() ) );
      }
      // __rrshift__
      // static std::vector<Type>
      //__rrshift__ ( const Fun& fun  , const  std::vector<Type>& o )
      //{ return o >> fun  ; }
      // ======================================================================
    public:
      // ======================================================================
      // __rshift__
      static LoKi::FunctorFromFunctor<Type, bool> __rshift__( const Fun& fun, const Fun& o ) { return fun >> o; }
      // ======================================================================
    };
    // ========================================================================
    /// Wrapper class for operations with functions
    template <>
    class FuncOps<LoKi::ATypes::Combination> : public FuncOps_<LoKi::ATypes::Combination> {};
    template <>
    class CutsOps<LoKi::ATypes::Combination> : public CutsOps_<LoKi::ATypes::Combination> {};
    // ========================================================================
  } // end of namespace Dicts
  // ==========================================================================
} // namespace LoKi
// ============================================================================
namespace {
  // ==========================================================================
  struct LoKiArrayFunctors_Instantiations {
    // ========================================================================
    /// fictive constructor
    LoKiArrayFunctors_Instantiations();
    // ========================================================================
    // the basic functions
    LoKi::Dicts::Funcs<LoKi::ATypes::Combination>  m_f1;
    LoKi::Dicts::VFuncs<LoKi::ATypes::Combination> m_f2;
    // operators
    LoKi::Dicts::FuncOps<LoKi::ATypes::Combination> m_o1;
    LoKi::Dicts::CutsOps<LoKi::ATypes::Combination> m_o2;

    // LoKi::Dicts::MapsOps<LoKi::ATypes::Combination>        m_o3  ;
    // LoKi::Dicts::PipeOps<LoKi::ATypes::Combination>        m_o4  ;
    // LoKi::Dicts::FunValOps<LoKi::ATypes::Combination>      m_o5  ;
    // LoKi::Dicts::CutValOps<LoKi::ATypes::Combination>      m_o51 ;
    // LoKi::Dicts::ElementOps<LoKi::ATypes::Combination>     m_o6  ;

    /// mathematics:
    LoKi::Dicts::FunCalls<LoKi::ATypes::Combination> m_c1;
    LoKi::Dicts::CutCalls<LoKi::ATypes::Combination> m_c2;
  };
  // ==========================================================================
} // end of anonymous namespace
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_LOKIARRAY_DCT_H
// ============================================================================
