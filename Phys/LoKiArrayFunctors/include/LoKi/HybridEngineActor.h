/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_HYBRID_ENGINE_ACTOR_H
#  define LOKI_HYBRID_ENGINE_ACTOR_H 1
// ============================================================================
// STD & STL
// ============================================================================
#  include <stack>
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/Context.h"
#  include "LoKi/IHybridTool.h"
#  include "LoKi/Interface.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Hybrid {
    // ========================================================================
    /** @class EngineActor LoKi/HybridEngineActor.h
     *
     *  This file is a part of LoKi project -
     *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
     *
     *  The package has been designed with the kind help from
     *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
     *  contributions and advices from G.Raven, J.van Tilburg,
     *  A.Golutvin, P.Koppenburg have been used in the design.
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2004-06-29
     */
    class EngineActor {
      // ======================================================================
    public:
      // ======================================================================
      /// get the static instance
      static EngineActor& instance(); // get the static instance
      /// connect the hybrid tool for code translation
      StatusCode connect( const LoKi::IHybridTool* tool, const LoKi::Context& context );
      /// disconnect the tool
      StatusCode disconnect( const LoKi::IHybridTool* tool );
      /** get the current context
       *  contex is valid only in between <code>connect/disconnect</code>
       *  @return the current active context
       */
      const LoKi::Context* context() const;
      // ======================================================================
    public:
      // ======================================================================
      // predicates
      // ======================================================================
      /// propagate the cut to the tool
      StatusCode process( const std::string& name, const LoKi::Types::Cuts& cut ) const;
      /// propagate the cut to the tool
      StatusCode process( const std::string& name, const LoKi::Types::VCuts& cut ) const;
      /// propagate the cut to the tool
      StatusCode process( const std::string& name, const LoKi::Types::ACuts& cut ) const;
      // ======================================================================
      // functions
      // ======================================================================
      /// propagate the function to the tool
      StatusCode process( const std::string& name, const LoKi::Types::Func& cut ) const;
      /// propagate the function to the tool
      StatusCode process( const std::string& name, const LoKi::Types::VFunc& cut ) const;
      /// propagate the function to the tool
      StatusCode process( const std::string& name, const LoKi::Types::AFunc& cut ) const;
      // ======================================================================
      // maps
      // ======================================================================
      /// propagate the map to the tool
      StatusCode process( const std::string& name, const LoKi::Types::Maps& cut ) const;
      /// propagate the map to the tool
      StatusCode process( const std::string& name, const LoKi::Types::VMaps& cut ) const;
      // ======================================================================
      // pipes
      // ======================================================================
      /// propagate the pipe to the tool
      StatusCode process( const std::string& name, const LoKi::Types::Pipes& cut ) const;
      /// propagate the pipe to the tool
      StatusCode process( const std::string& name, const LoKi::Types::VPipes& cut ) const;
      // ======================================================================
      // fun-vals
      // ======================================================================
      /// propagate the fun-val to the tool
      StatusCode process( const std::string& name, const LoKi::Types::FunVals& cut ) const;
      /// propagate the fun-val to the tool
      StatusCode process( const std::string& name, const LoKi::Types::VFunVals& cut ) const;
      // ======================================================================
      // cut-vals
      // ======================================================================
      /// propagate the cut-val to the tool
      StatusCode process( const std::string& name, const LoKi::Types::CutVals& cut ) const;
      /// propagate the cut-val to the tool
      StatusCode process( const std::string& name, const LoKi::Types::VCutVals& cut ) const;
      // ======================================================================
      // sources
      // ======================================================================
      /// propagate the source to the tool
      StatusCode process( const std::string& name, const LoKi::Types::Sources& cut ) const;
      /// propagate the source to the tool
      StatusCode process( const std::string& name, const LoKi::Types::VSources& cut ) const;
      // ======================================================================
    protected:
      // ======================================================================
      /// Standard constructor
      EngineActor(); // Standard constructor
      // Destructor
      virtual ~EngineActor(); //           Destructor
      // ======================================================================
    private:
      // ======================================================================
      /// just to save some lines
      template <class TYPE>
      inline StatusCode _add( const std::string& ename,
                              const TYPE&        cut ) const; // just to save some lines
      // ======================================================================
    private:
      // ======================================================================
      // the copy contructor is disabled
      EngineActor( const EngineActor& );
      // ther assignement operator is disabled
      EngineActor& operator=( const EngineActor& );
      // ======================================================================
    private:
      // ======================================================================
      typedef LoKi::Interface<LoKi::IHybridTool> Tool;
      typedef std::pair<Tool, LoKi::Context>     Entry;
      typedef std::stack<Entry>                  Stack;
      /** a stack of tools -- since tools might instantiate other tools we need
       *   a stack of active (anti)factories
       */
      Stack m_stack{}; // the queue of active factories
      // ======================================================================
    };
    // ========================================================================
  } // namespace Hybrid
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_CUTSHOLDER_H
// ============================================================================
