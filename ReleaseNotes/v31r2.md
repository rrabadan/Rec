2020-10-19 Rec v31r2
===

This version uses
Lbcom [v31r2](../../../../Lbcom/-/tags/v31r2),
LHCb [v51r2](../../../../LHCb/-/tags/v51r2),
Gaudi [v34r1](../../../../Gaudi/-/tags/v34r1) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to Rec [v31r1](../-/tags/v31r1), with the following changes:

### New features ~"new feature"

- ~Decoding ~FT ~Monitoring | Add SciFi data monitoring, !2129 (@legreeve)
- ~Tracking | Use transport-matrix similarity transform, !2214 (@ldufour)
- ~Composites ~Filters ~Functors | Changes supporting the new ThOr::Combiner<T>, !2146 (@nnolte)
- ~"MC checking" | Add ProtoParticle association algorithms, !2172 (@apearce) [Moore#197] :star:


### Fixes ~"bug fix" ~workaround

- ~Muon | Cleanup of the MuonMatching code, !2212 (@cprouve) :star:
- ~Calo | Fix hypo -> cluster navigation in E-correction for SplitPhotons, !2187 (@graven)
- ~"MC checking" | Bug fix in TrackIPResolutionCheckerNT, !2200 (@gbassi)
- ~Build | Add missing inline to please gcc10, !2173 (@cattanem)
- ~Build | Fix dependency of functor cache and GaudiConfig2, !2163 (@rmatev)
- Add Protection for the Padding to avoid overload to the container (follow up !2063), !2161 (@peilian)


### Enhancements ~enhancement

- ~Tracking | Remove internal allocations in TrackCloneKiller, !2221 (@ahennequ)
- ~Tracking | Adapt to tracks containers with flexible sizes, !2208 (@peilian) :star:
- ~Tracking | Adapt to flexible size of PrSeedTracks container, !2167 (@peilian)
- ~Tracking | Following change to template function that finds sectors in UT, !2147 (@decianm)
- ~Calo | Calo Showeroverlap: change default to not apply L/S correction, !2210 (@graven)
- ~Calo | Speedup determination of Calo area in TrackMatch::getTrackMatch3D, !2191 (@ahennequ)
- ~Calo | Adapt to changes in LHCb!2582, !2145 (@graven)
- ~RICH | Minor fix to restore builds using VE SIMD abstraction layer, !2211 (@jonrob)
- ~RICH ~Monitoring | RICH Protect against rare log(effective length <= 0) FPE due to TS returning a negative value, !2218 (@jonrob)
- ~Conditions | Various DD4hep fixes, !2070 (@sponce)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Fix python 3 support, !2224 (@rmatev)
- ~Tracking | Update references for !2215, !2219 (@gunther)
- ~Tracking | Remove numfieldcalls counter in TrackFieldExtrapolator base class, !2215 (@chasse)
- ~Tracking | Follow changes in LHCb!2680, !2155 (@graven)
- ~Calo | Follow simplification of IElectron interface + misc. cleanup, !2220 (@graven)
- ~Calo | New round of cleanup of old style counters, !2204 (@sponce)
- ~Calo | Calo{E,S,L}Corrections: Prefer LHCb::Math math functions over local implementation, !2202 (@graven)
- ~Calo | Remove unused code from CaloFutureTools, !2192 (@graven)
- ~Calo | CaloFutureMoniDst: amalgamate headers into component sources, !2189 (@graven)
- ~Calo | Modernize FutureGammaPi0XGBoostTool, !2188 (@graven)
- ~Calo | Cleanup CaloFutureReco, !2162 (@graven)
- ~Calo | Follow changes in LHCb!2667, !2156 (@graven)
- ~Calo | Remove unused/empty tool handle array in merged pi0 hypothesis maker, !2153 (@graven)
- ~Calo | Migrate more code to v2 calo clusters, !2080 (@graven)
- ~RICH | RichFutureRecSys - Tidy up example options for realistic PMT data format decoding checks, !2207 (@jonrob)
- ~Functors | Make the functor test that compares different SIMD backends more tolerant of numerical differences, !2217 (@olupton) [#160]
- ~"MC checking" | Remove use of make_MCTrackInfo, and corresponding direct use of evtSvc(), !2201 (@graven)
- ~Conditions | Update tests to latest physics constants in Gaudi, !2206 (@clemenci)
- ~Build | Add missing include to fix compilation error in dev3, !2171 (@cattanem)
- Adapt to new Gaudi Monitoring, !2209 (@sponce)
- Remove L0 from CaloFuture/CaloFutureMoniDst, !2198 (@pkoppenb)
- Remove L0 from PV monitoring, !2197 (@pkoppenb)
- Remove OTDAQ from RecSummary, !2196 (@pkoppenb)
- Remove L0 from LoKiTrack, !2195 (@pkoppenb) [Moore#209]
- Towards dropping old counters, !2181 (@sponce)
- Remove L0 from TrackMonitors, !2179 (@pkoppenb)
- Remove L0 from RecSummaryAlg, !2176 (@pkoppenb)
- Removal of L0Candidate track flag from LoKiTrack, !2175 (@pkoppenb)
- Remove Run 1-2 options incompatible with new DaVinci versions in ANNPID, !2174 (@pkoppenb)
- Migrate to Gaudi/Property.h, !2151 (@jonrob)
