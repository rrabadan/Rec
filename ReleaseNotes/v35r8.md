2023-05-09 Rec v35r8
===

This version uses
Lbcom [v34r8](../../../../Lbcom/-/tags/v34r8),
LHCb [v54r8](../../../../LHCb/-/tags/v54r8),
Gaudi [v36r12](../../../../Gaudi/-/tags/v36r12),
Detector [v1r12](../../../../Detector/-/tags/v1r12) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Rec [v35r7](/../../tags/v35r7), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- ~"Event model" | Added VeloBackward case to update_states., !3370 (@spradlin)


### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing



### Documentation ~Documentation


### Other

- ~"PV finding" | Add PV counters to PatPVLeftRightFinderMerger, !3386 (@freiss)
- ~UT | Adapt to updated UT channel ID class and geometry, !2403 (@xuyuan)
- ~Calo ~Functors ~"Event model" | Update definitions of Calo Functors only compatible with v2 event model, !3398 (@alopezhu)
- ~Functors | Improve TisTos: New functor + close issue https://gitlab.cern.ch/lhcb/Rec/-/issues/471 + better invalid value handling, !3337 (@amathad)
- Remove unneeded default baseclass for weightedrelationtable., !3394 (@ldufour)
- Update References for: LHCb!4077 based on lhcb-master-mr/7663, !3387 (@lhcbsoft)
- Add flatten decay tree + particle matching algorithms + ISBASICPARTICLE functor, !3385 (@ldufour)
- Add delta mass functor, !3138 (@mengzhen)
