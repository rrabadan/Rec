2022-06-10 Rec v34r3
===

This version uses
Lbcom [v33r9](../../../../Lbcom/-/tags/v33r9),
LHCb [v53r9](../../../../LHCb/-/tags/v53r9),
Detector [v1r2](../../../../Detector/-/tags/v1r2),
Gaudi [v36r5](../../../../Gaudi/-/tags/v36r5) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to Rec [v34r2](/../../tags/v34r2), with the following changes:

### New features ~"new feature"

- ~VP | RetinaCluster v1, !2695 (@gbassi)
- ~Functors | Feat: functional composition of functors, !2683 (@chasse)

### Fixes ~"bug fix" ~workaround

- ~Functors | Add missing deref in Lifetime functor, !2931 (@chasse) [#344]
- ~Functors | Fix: FunctorFactory Caching behaviour, !2901 (@chasse)
- Fix TestFunctors (bug caused by interference of 2 merged MRs), !2914 (@chasse)
- Fix memory leak in TMVATransform and SelTools due to the direct use of TMVA::BookMVA, !2864 (@graven) [#323]

### Enhancements ~enhancement

- ~Tracking | Vectorized Matching, !2861 (@sesen)
- ~Tracking ~Monitoring | Template VPTrackMonitor on fit node type, !2789 (@jkubat) [Alignment#16]
- ~Muon | Refactor(MuonIDAlg) use paramfilesvc instead of direct file access, !2941 (@chasse)
- ~Calo | Move BremAdder to use SelectiveBremMatchAlg output, !2817 (@mveghel)
- ~Functors | Refactor(functors): LifetimeFitter - use a warning counter to avoid spamming error log, !2940 (@chasse) [#252]
- ~Functors | Functors: Remove DataDepWrapper, !2909 (@chasse)
- ~Functors | Implement all IP and IPCHI2 functors with composition, see details below:, !2890 (@chasse)
- ~Functors | Fix(functors): move "flattening" many levels of chain or & composition from c++ into python, !2886 (@chasse)
- Vectorized Matching, !2898 (@sesen)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Tracking | Use PrUTHits for ghost prob, !2856 (@decianm)
- ~Muon | Cleanup and modernization of muon code, !2855 (@sponce)
- ~Calo | Rm getDet from cov and spread tools, !2896 (@cmarinbe) [#324,#338]
- ~RICH | RichDetailedTrSegMakerFromTracks - Adapt to changes in beampipe intersections call, !2836 (@jonrob)
- ~Functors | Refactor(functors): removal of obsolete code, !2928 (@chasse)
- ~Monitoring | Convert TrackFitMatchMonitor histos, !2840 (@jcobbled)
- ~Core | Funnel all access to files in $PARAMFILESROOT through ParamFileSvc, !2783 (@graven)
- Remove deprecated filling of Gaudi histograms, !2903 (@rmatev)
- Remove IDecayFinder, !2895 (@pkoppenb)
- Improved use of `span` in GhostID and use of `is` in FitNode, !2862 (@graven)
- Add missing links to libraries, !2859 (@clemenci)
- Migrate MatrixnetTransform to use ParamFileSvc, !2858 (@graven)
- Ignore DQFiltering in LumiIntegrateFSR, !2857 (@clemenci)
- Update TrackPV2HalfMonitor to use new histograms, !2838 (@jcobbled)

### Other

- ~Configuration | Replace PyConf.application.ApplicationOptions with GaudiConf.LbExec.Options, !2816 (@cburr)
- ~FT | Allow FTTrackSelector to exclude channels on all SiPMs or all mats for coldbox/edge effects, !2891 (@shollitt)
- ~Calo | Cleaning CaloDAQ, !2930 (@jmarchan)
- ~Calo | Add flag to write nTuple, !2923 (@nuvallsc)
- ~Calo | Modifications needed for the new Calo Digit MC linker, !2870 (@jmarchan)
- ~Calo | Truncate digit energy resolution issue Rec#276, !2821 (@nuvallsc) [#276]
- ~Calo ~PID | Remove obsolete calo code, !2871 (@mveghel)
- ~RICH | Use specific DD4HEP compat geom that supports current DetDesc MC samples, !2925 (@jonrob)
- ~RICH | Adapt to minor change in RichDetectors base class name, !2921 (@jonrob)
- ~RICH | Adapt to change in Rich{1,2} derived condition objects, !2910 (@jonrob) [(lhcb/LHCb#226]
- ~RICH | Enable the Magnet with DD4HEP in the RICH Rec tests, !2899 (@jonrob)
- ~RICH | Update ref for rich-dst-reco-pmt-v3.ref.dd4hep, !2894 (@jonrob)
- ~RICH ~"MC checking" | Avoid using auto with Vc operator[int] accessor, !2893 (@jonrob)
- ~Functors | Decision not lines and add suffix check for the decision, !2874 (@pkoppenb)
- Revert "Merge branch 'chasse_lifetimefunctor_warning_counter' into 'master'", !2947 (@chasse)
- Remove getDet usage from SubClusterSelector, !2936 (@cmarinbe) [#347]
- Cleanup of unused code in the Muon, !2933 (@sponce)
- Improved dumps for phoenix, !2927 (@sponce)
- Revert "Merge branch 'sponce_ImprovedPhenixDumps' into 'master'", !2922 (@sponce)
- Add CatBoost in Muon HLT, !2920 (@rvazquez)
- Export Protoparticles to Phoenix JSON format, !2919 (@bcouturi)
- Replace avx256 with best, !2918 (@decianm)
- Add missing link to LHCb::CaloFutureUtils, !2915 (@clemenci)
- Allow PrimaryVertices to be filtered with ThOr functors, !2911 (@samarian)
- Make instance of PrLHCbID2MCParticle with VP and FT only, !2906 (@decianm)
- Add cell by cell histograms for calo Time Alignment, !2905 (@nuvallsc)
- Improved dumps for phoenix, !2900 (@sponce)
- Revert "Merge branch 'sevda-matching' into 'master'", !2897 (@sponce)
- Migrate various components to include GenericConditionAccessorHolder.h, !2892 (@jonrob) [#334]
- Fix usage of beamSpot in PatPV3DFuture, !2888 (@sponce)
- Split MeasurementProvider<T> and adapted to DD4hep, !2887 (@clemenci) [#340,#342]
- Updated beamspot access, !2885 (@wouter)
- JSON extraction for Phoenix including particle ID, !2884 (@bcouturi)
- Make derived conditions default constructible, !2883 (@clemenci)
- Make the magnet an input instead of getting it through a service, !2880 (@decianm)
- Disable SimplifiedMaterialLocator in DD4HEP builds, !2879 (@wouter)
- Changed PrForwardTracking to use DeMagnet, !2878 (@bcouturi)
- Migrate SelReportsMaker to LHCb, !2877 (@graven)
- Update of lifetime fit, !2873 (@wouter)
- Use DeMagnet instead of MagneticFieldSvc, !2869 (@bcouturi)
- Adding counters to store scifi hits, !2866 (@lohenry)
- Introduced flavour tagging functor, !2854 (@dtou)
- Add implemenation of IPrAddUTHitsTool which does not do anything, !2852 (@graven)
- Misc DD4hep fixes to tests, !2851 (@clemenci)
- Add EmptyProducer for UTHits, !2848 (@freiss)
- Update method to count hits for outlier rejection in TrackMasterFitter, !2847 (@wouter)
- UTClustering decoder, !2842 (@xuyuan)
- Flatten recursive AND composition in ThOr, !2694 (@graven)
- Add flag in ParticleCombiner to allow different algorithm inputs for children with the same ID, !2681 (@gtuci)
- Catching an empty calo hypothesis for a protoparticle, !2679 (@valukash)
