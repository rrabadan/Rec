2021-10-22 Rec v33r3
===

This version uses
Lbcom [v33r4](../../../../Lbcom/-/tags/v33r4),
LHCb [v53r4](../../../../LHCb/-/tags/v53r4),
Gaudi [v36r2](../../../../Gaudi/-/tags/v36r2) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to Rec [v33r2](/../../tags/v33r2), with the following changes:

### New features ~"new feature"

- ~Tracking ~"PV finding" | Add producers of empty PV and Track containers, !2571 (@cmarinbe)
- ~Functors | Add subcombination and child functor, !2573 (@nnolte)
- ~Functors | Add ProbNNx functors, !2471 (@pkoppenb) [gitlab.cern.ch/lhcb/Rec/-/merge_requests/2471/diffs#26,gitlab.cern.ch/lhcb/Rec/-/merge_requests/2471/diffs#2735,gitlab.cern.ch/lhcb/Rec/-/merge_requests/2471/diffs#3]


### Fixes ~"bug fix" ~workaround

- ~Functors | Fix XYZ functions in ThOr and fix functor testing, !2570 (@mengzhen)
- ~Functors | Fix BPVCORRM functor, !2568 (@gtuci) [#216]


### Enhancements ~enhancement

- ~Tracking | Adapt PrAlgorithms, converters, track fit to change of State definition, !2574 (@decianm)
- ~Tracking ~Monitoring | Use PrFitNode in TrackFitMatchMonitor, !2569 (@jkubat)
- ~Functors ~Monitoring | Implement a ThOr functor-based monitoring, !2582 (@mramospe)
- ~Utilities | Speed up Runge-Kutta Extrapolator, !2579 (@gunther)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~PID | CatBoost 0.26.1 for MuID, !2513 (@nkazeev)
- ~Persistency | Fixes for the new ODIN class, !2464 (@clemenci)
- ~Monitoring | Remove AlignmentOnlineMonitor, !2578 (@decianm)
- Fix: avoid allocating very large objects on the stack, !2593 (@chasse)
- Fix: avoid alloacting huge objects on stack, !2587 (@chasse)
