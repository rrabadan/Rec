/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// Include
#include "TrackKernel/Track1DTabFunc.h"

// Create object
/**
 * @brief flattening function of the GhostProbability
 *
 * @return instance of function. Gives ownership! User has to delete!
 */

namespace {
  std::unique_ptr<Track::TabulatedFunction1D> Update_TtrackTable() {

    // Initialisation (should do this just once, slow)
    return std::make_unique<Track::TabulatedFunction1D>( std::initializer_list<float>{
        0.,         0.000476262, 0.0010469,  0.00144554, 0.00184006, 0.00222548, 0.00264152, 0.00307332, 0.00353017,
        0.00391549, 0.00435516,  0.00479435, 0.00520594, 0.00560878, 0.00606257, 0.0065108,  0.0070143,  0.00748843,
        0.00791586, 0.00846129,  0.00891729, 0.00940603, 0.00989213, 0.0103665,  0.0108959,  0.0114066,  0.0119594,
        0.0124393,  0.0129671,   0.0134874,  0.0140309,  0.0145814,  0.015102,   0.0156682,  0.0161891,  0.0168121,
        0.0173361,  0.0178908,   0.018491,   0.0191139,  0.0197329,  0.0203536,  0.0209584,  0.0215654,  0.022084,
        0.0227272,  0.0233797,   0.0240072,  0.024709,   0.025321,   0.0259609,  0.0266467,  0.0273139,  0.0280142,
        0.0287285,  0.0294307,   0.0301016,  0.0307532,  0.0314846,  0.0322443,  0.032966,   0.0337479,  0.0345197,
        0.0353027,  0.0361214,   0.036873,   0.0376823,  0.0384577,  0.0392793,  0.0401103,  0.0409382,  0.0416885,
        0.0425766,  0.0433412,   0.0442213,  0.0451055,  0.0460403,  0.0470096,  0.0478711,  0.0488594,  0.049864,
        0.0509127,  0.0520176,   0.053106,   0.0541855,  0.0553313,  0.0564863,  0.0575976,  0.0587658,  0.0599112,
        0.0610028,  0.0622198,   0.0635227,  0.0647411,  0.0659974,  0.0672754,  0.0685953,  0.0700152,  0.0713279,
        0.0726977,  0.0741543,   0.0756241,  0.0772739,  0.0786326,  0.0801768,  0.08177,    0.0834239,  0.085108,
        0.0867763,  0.088485,    0.0903656,  0.092217,   0.0940077,  0.0959737,  0.0978341,  0.0998085,  0.101992,
        0.104168,   0.106275,    0.108573,   0.11087,    0.113316,   0.115869,   0.118642,   0.121441,   0.124181,
        0.127063,   0.129758,    0.132613,   0.135692,   0.138809,   0.142238,   0.145854,   0.149737,   0.153533,
        0.157613,   0.161595,    0.165682,   0.16966,    0.174046,   0.178913,   0.184138,   0.189398,   0.194607,
        0.199711,   0.2055,      0.211778,   0.218091,   0.2235,     0.23003,    0.236416,   0.243385,   0.250327,
        0.258091,   0.266729,    0.274294,   0.28266,    0.2916,     0.301136,   0.311589,   0.322919,   0.333572,
        0.343917,   0.354604,    0.366968,   0.379231,   0.393425,   0.406919,   0.420323,   0.434268,   0.449318,
        0.464926,   0.478638,    0.496052,   0.511447,   0.530458,   0.550414,   0.570079,   0.591715,   0.612935,
        0.634833,   0.658277,    0.679958,   0.703598,   0.728042,   0.750196,   0.770161,   0.793318,   0.820067,
        0.846355,   0.869497,    0.885776,   0.885776,   0.892515,   0.913525,   0.926751,   0.937641,   0.94781,
        0.957518,   0.969785,    1.} );
  }
} // namespace
