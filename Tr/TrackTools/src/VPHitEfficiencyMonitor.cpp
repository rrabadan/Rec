/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/PrHits.h"

#include "VPDet/DeVP.h"
#include "VPDet/DeVPSensor.h"

#include "Event/PrHits.h"
#include "Event/Track.h"

#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbAlgs/Consumer.h"
#include <Gaudi/Accumulators/Histogram.h>

#include "GaudiAlg/GaudiHistoAlg.h"

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackInterpolator.h"

namespace VPHitsTag = LHCb::Pr::VP::VPHitsTag;

class VPHitEfficiencyMonitor
    : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&, const LHCb::Pr::VP::Hits&,
                                             const DetectorElement&, const DeVP& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DetectorElement, DeVP>> {
private:
  Gaudi::Property<unsigned int> m_sensorUnderStudy{this, "SensorUnderStudy"};

  Gaudi::Property<float> m_tolerance{this, "ResidualTolerance", 0.1 * Gaudi::Units::mm}; // tolerance in mm, was 0.2
  Gaudi::Property<float> m_maxTrackError{this, "MaxTrackCov", 0.2 * Gaudi::Units::mm};

  Gaudi::Property<float> m_track_min_momentum{this, "TrackMinP", 5000 * Gaudi::Units::MeV};
  Gaudi::Property<float> m_track_min_pt{this, "TrackMinPT", 400 * Gaudi::Units::MeV};

  Gaudi::Property<bool> m_fillHot{this, "FillHotEfficiencies", true};
  Gaudi::Property<bool> m_checkSensor{this, "CheckSensorPosition", true};
  Gaudi::Property<int>  m_minVPHits{this, "MinHitsOnTrack", 3};

  ToolHandle<ITrackInterpolator> m_interpolator{this, "Interpolator", "TrackInterpolator"};
  ToolHandle<ITrackExtrapolator> m_linearextrapolator{this, "Extrapolator", "TrackRungeKuttaExtrapolator"};

  mutable Gaudi::Accumulators::ProfileHistogram<2> m_hitEfficiencyPerXY{
      this,
      "hitEfficiencyVsXY",
      "hit efficiency per XY per sensor",
      {80, -60. * Gaudi::Units::mm, 60. * Gaudi::Units::mm},
      {80, -60 * Gaudi::Units::mm, 60 * Gaudi::Units::mm}};

  mutable Gaudi::Accumulators::ProfileHistogram<2> m_hotEfficiencyPerXY{
      this,
      "hotEfficiencyVsXY",
      "hot efficiency per XY per sensor",
      {80, -60. * Gaudi::Units::mm, 60. * Gaudi::Units::mm},
      {80, -60 * Gaudi::Units::mm, 60 * Gaudi::Units::mm}};

  mutable Gaudi::Accumulators::ProfileHistogram<2> m_hotGivenHitEfficiencyPerXY{
      this,
      "hotGivenHitEfficiencyVsXY",
      "hot efficiency per XY per sensor, provided there was a hit",
      {80, -60. * Gaudi::Units::mm, 60. * Gaudi::Units::mm},
      {80, -60 * Gaudi::Units::mm, 60 * Gaudi::Units::mm}};

  mutable Gaudi::Accumulators::ProfileHistogram<1> m_hitEfficiency{
      this, "hitEfficiencyASIC", "hit efficiency per ASIC", {3, -0.5, 2.5}};

  mutable Gaudi::Accumulators::ProfileHistogram<1> m_hotEfficiency{
      this, "hotEfficiencyASIC", "hot efficiency per ASIC", {3, -0.5, 2.5}};

  mutable Gaudi::Accumulators::Histogram<3> m_residualPerXY{this,
                                                            "residualVsXY",
                                                            "residual per XY, layer under study",
                                                            {100, -80. * Gaudi::Units::mm, 80. * Gaudi::Units::mm},
                                                            {124, -80 * Gaudi::Units::mm, 80 * Gaudi::Units::mm},
                                                            {100, -1.0 * Gaudi::Units::mm, 1.0 * Gaudi::Units::mm}};

  mutable Gaudi::Accumulators::Histogram<2> m_xyResiduals{
      this,
      "xyResiduals",
      "residual in x,y",
      {600, -5.0 * Gaudi::Units::mm, 5.0 * Gaudi::Units::mm},
      {600, -5.0 * Gaudi::Units::mm, 5.0 * Gaudi::Units::mm},
  };
  mutable Gaudi::Accumulators::Histogram<1> m_eta{
      this,
      "eta",
      "pseudoRapidity distribution of input tracks",
      {200, 1.5, 5.0},
  };

  mutable Gaudi::Accumulators::SummingCounter<> m_interPolationError{this, "Could not interpolate"};
  mutable Gaudi::Accumulators::SummingCounter<> m_extrapolationError{this, "Could not extrapolate"};
  mutable Gaudi::Accumulators::SummingCounter<> m_moduleError{this, "Could not find this module"};
  mutable Gaudi::Accumulators::SummingCounter<> m_hitsError{
      this, "Could not find hits downstream or upstream of the sensor being studied"};
  mutable Gaudi::Accumulators::SummingCounter<> m_activeAreaError{this, "NotInActiveArea"};

public:
  VPHitEfficiencyMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  {KeyValue{"TrackLocation", ""}, KeyValue{"PrVPHitsLocation", ""},
                   KeyValue{"LHCbGeoLocation", LHCb::standard_geometry_top},
                   KeyValue{"VPLocation", DeVPLocation::Default}} ) {}

  void operator()( const LHCb::Track::Range& tracks, const LHCb::Pr::VP::Hits& hits, const DetectorElement& lhcb,
                   const DeVP& vpDet ) const override {
#ifdef USE_DD4HEP
    const auto&         my_sensor = vpDet.sensor( LHCb::Detector::VPChannelID::SensorID( m_sensorUnderStudy.value() ) );
    float               sizeX     = my_sensor.activeSizeX();
    float               sizeY     = my_sensor.activeSizeY();
    const auto          left_top  = my_sensor.localToGlobal( Gaudi::XYZPoint( -0.5 * sizeX, 0.5 * sizeY, 0.0 ) );
    const auto          right_top = my_sensor.localToGlobal( Gaudi::XYZPoint( 0.5 * sizeX, 0.5 * sizeY, 0.0 ) );
    const auto          right_bottom = my_sensor.localToGlobal( Gaudi::XYZPoint( 0.5 * sizeX, -0.5 * sizeY, 0.0 ) );
    ROOT::Math::Plane3D my_plane( left_top, right_top, right_bottom );
    auto                z_position = my_sensor.z();

#else
    const auto& my_sensor = vpDet.sensor( LHCb::Detector::VPChannelID::SensorID( m_sensorUnderStudy.value() ) );

    float sizeX =
        my_sensor.numChips() * my_sensor.chipSize() + ( my_sensor.numChips() - 1 ) * my_sensor.interChipDist();
    float sizeY = my_sensor.chipSize();

    const auto          left_top     = my_sensor.localToGlobal( Gaudi::XYZPoint( -0.5 * sizeX, 0.5 * sizeY, 0.0 ) );
    const auto          right_top    = my_sensor.localToGlobal( Gaudi::XYZPoint( 0.5 * sizeX, 0.5 * sizeY, 0.0 ) );
    const auto          right_bottom = my_sensor.localToGlobal( Gaudi::XYZPoint( 0.5 * sizeX, -0.5 * sizeY, 0.0 ) );
    ROOT::Math::Plane3D my_plane( left_top, right_top, right_bottom );
    auto                z_position = my_sensor.z();

#endif

    const auto& all_hits      = hits.scalar();
    int         startPosition = 0;
    int         endPosition   = all_hits.size();

    for ( const auto& track : tracks ) {
      if ( m_track_min_momentum.value() > 0 && track->p() < m_track_min_momentum.value() ) continue;
      if ( m_track_min_pt.value() > 0 && track->pt() < m_track_min_pt.value() ) continue;
      if ( track->pseudoRapidity() < 1.5 || track->pseudoRapidity() > 5.1 ) continue;
      if ( track->nVPHits() < m_minVPHits.value() ) continue;

      LHCb::State state;
      if ( !m_interpolator->interpolate( *track, z_position, state, lhcb ).isSuccess() ) {
        ++m_interPolationError;
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Could not interpolate track." << endmsg;
        continue;
      }

      // only consider if state has reasonably small error. we should
      // still fine-tune this a little bit, make a propor projecion
      // etc. the best is to cut only when you have unbiased the
      // state. cutting too tight could introduce a bias in
      // the efficiency measurement?
      if ( std::sqrt( state.covariance()( 0, 0 ) ) > m_maxTrackError.value() ) {
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Too large cov element" << endmsg;
        continue;
      }

      // to get a slightly more precise answer, intersect with the plane
      if ( !m_linearextrapolator->propagate( state, my_plane, lhcb ).isSuccess() ) {
        ++m_extrapolationError;
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Could not extrapolate track" << endmsg;
        continue;
      }

      const ROOT::Math::XYZPoint localIntersection =
          my_sensor.globalToLocal( Gaudi::XYZPoint( state.x(), state.y(), state.z() ) );
      if ( !my_sensor.isInActiveArea( localIntersection ) ) {
        ++m_activeAreaError;
        continue;
      }

      // make sure the sensor is between the first and last hits of the tracks
      if ( m_checkSensor.value() ) {
        const auto lhcbids        = track->lhcbIDs();
        bool       find_lower_id  = false;
        bool       find_higher_id = false;
        for ( const auto& lhcbid : lhcbids ) {
          if ( lhcbid.isVP() ) {
            const auto  vpid     = LHCb::Detector::VPChannelID( lhcbid.vpID() );
            const auto& vpsensor = vpDet.sensor( vpid.sensor() );
            if ( vpsensor.z() < z_position ) find_lower_id = true;
            if ( vpsensor.z() > z_position ) find_higher_id = true;
            if ( find_lower_id && find_higher_id ) break;
          }
        }

        if ( !( find_lower_id && find_higher_id ) ) {
          ++m_hitsError;
          continue;
        }
      }

      ++m_eta[{track->pseudoRapidity()}];

      float                       foundHit        = 0;
      float                       foundHitOnTrack = 0; // bool
      LHCb::Detector::VPChannelID bestChannel;
      float                       bestResidual = -1;

      for ( int i_hit = endPosition; i_hit >= startPosition; i_hit-- ) {
        const auto&                 vphit         = all_hits[i_hit];
        const int                   unwrapped_num = vphit.get<VPHitsTag::ChannelId>().cast();
        LHCb::Detector::VPChannelID vpChannel( unwrapped_num );
        if ( unsigned( vpChannel.module() ) > unsigned( my_sensor.module() ) ) startPosition = i_hit;
        if ( unsigned( vpChannel.module() ) < unsigned( my_sensor.module() ) ) endPosition = i_hit;
        if ( to_unsigned( vpChannel.sensor() ) != m_sensorUnderStudy ) continue;

        // get vp hit position
        const auto            vec = vphit.get<VPHitsTag::pos>();
        const Gaudi::XYZPoint VPhit( vec.x().cast(), vec.y().cast(), vec.z().cast() );

        float dz    = state.z() - vec.z().cast();
        float new_x = state.x() - dz * state.tx();
        float new_y = state.y() - dz * state.ty();

        const auto residual_vector = my_sensor.globalToLocal( VPhit ) -
                                     my_sensor.globalToLocal( Gaudi::XYZPoint( new_x, new_y, vec.z().cast() ) );

        const float residual = residual_vector.R();
        if ( fabs( residual ) < m_tolerance ) {
          foundHit = 1;

          if ( bestResidual < 0 || residual < bestResidual ) {
            bestChannel  = vpChannel;
            bestResidual = residual;
          }
        }

        ++m_residualPerXY[{state.x(), state.y(), residual}];
        ++m_xyResiduals[{residual_vector.X(), residual_vector.Y()}];
        // find hit on track
        if ( m_fillHot.value() ) {
          auto it = std::find( track->lhcbIDs().begin(), track->lhcbIDs().end(), LHCb::LHCbID( vpChannel ) );
          if ( it != track->lhcbIDs().end() ) foundHitOnTrack = 1;
        }
      }

      LHCb::Detector::VPChannelID expChannel;
      bool                        findchannel = my_sensor.pointToChannel( localIntersection, true, expChannel );
      if ( !findchannel ) warning() << "not in the sensitive region ... no channel found!" << endmsg;

      m_hitEfficiencyPerXY[{state.x(), state.y()}] += foundHit; // Fill( state.x(), state.y(), sensor, foundHit );
      m_hitEfficiency[{unsigned( expChannel.chip() ) * 1.0}] += foundHit;

      // hit-on-track efficiency PerXY, sensor, chip, cov+row
      m_hotEfficiencyPerXY[{state.x(), state.y()}] += foundHitOnTrack;
      m_hotEfficiency[{unsigned( expChannel.chip() ) * 1.0}] += foundHitOnTrack;

      // hit-on-track-given-hit-was-there-in-detector
      if ( foundHit > 0 ) // only test this *if* we had a good hit
      {
        m_hotGivenHitEfficiencyPerXY[{state.x(), state.y()}] += foundHitOnTrack;
      }
    }
  }
};
DECLARE_COMPONENT( VPHitEfficiencyMonitor )
