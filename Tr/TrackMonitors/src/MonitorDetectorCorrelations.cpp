/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/CaloClusters_v2.h"
#include "Event/CaloDigits_v2.h"
#include "RichFutureUtils/RichDecodedData.h"
#include <Event/PrHits.h>
#include <Gaudi/Accumulators/Histogram.h>
#include <LHCbAlgs/Consumer.h>

class MonitorDetectorCorrelationsVeloSciFi
    : public LHCb::Algorithm::Consumer<void( LHCb::Pr::VP::Hits const&, LHCb::Pr::FT::Hits const& )> {
public:
  MonitorDetectorCorrelationsVeloSciFi( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, {KeyValue{"VeloHits", ""}, KeyValue{"SciFiHits", ""}} ) {}

  void operator()( LHCb::Pr::VP::Hits const& velo_hits, LHCb::Pr::FT::Hits const& scifi_hits ) const override {
    ++m_velo_scifi_hits_correlation[{velo_hits.size(), scifi_hits.size()}];
  }

private:
  mutable Gaudi::Accumulators::Histogram<2> m_velo_scifi_hits_correlation{
      this,
      "VeloScifiHitsCorrelation",
      "VeloScifiHitsCorrelation",
      {{100, 0, 6000, "Velo Hits"}, {100, 0, 10000, "ScifiHits"}}};
};

DECLARE_COMPONENT( MonitorDetectorCorrelationsVeloSciFi )

class MonitorDetectorCorrelations : public LHCb::Algorithm::Consumer<void(
                                        LHCb::Pr::VP::Hits const&, LHCb::Pr::FT::Hits const&, MuonHitContainer const&,
                                        LHCb::Event::Calo::v2::Clusters const&, LHCb::Event::Calo::Digits const&,
                                        Rich::Future::DAQ::DecodedData const& )> {
public:
  MonitorDetectorCorrelations( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  {KeyValue{"VeloHits", ""}, KeyValue{"SciFiHits", ""}, KeyValue{"MuonHits", ""},
                   KeyValue{"ECALClusters", ""}, KeyValue{"HCALDigits", ""}, KeyValue{"RichPixels", ""}} ) {}

  void operator()( LHCb::Pr::VP::Hits const& velo_hits, LHCb::Pr::FT::Hits const& scifi_hits,
                   MuonHitContainer const& muon_hits, LHCb::Event::Calo::v2::Clusters const& calo_clusters,
                   LHCb::Event::Calo::Digits const&      hcal_digits,
                   Rich::Future::DAQ::DecodedData const& rich_hits ) const override {

    ++m_velo_scifi_hits_correlation[{velo_hits.size(), scifi_hits.size()}];

    auto n_muon_hits = muon_hits.hits( 0 ).size() + muon_hits.hits( 1 ).size() + muon_hits.hits( 2 ).size() +
                       muon_hits.hits( 3 ).size();

    uint nClusters = 0;
    for ( const auto& cluster : calo_clusters.scalar() ) {
      if ( cluster.energy() > m_MinEECAL ) { ++nClusters; }
    }

    uint hcal_adc_abovethreshold = 0;
    for ( const auto& digit : hcal_digits ) {
      if ( digit.adc() > m_MinADCHCAL ) { hcal_adc_abovethreshold += digit.adc(); }
    }

    ++m_velo_muon_hits_correlation[{velo_hits.size(), n_muon_hits}];
    ++m_scifi_muon_hits_correlation[{scifi_hits.size(), n_muon_hits}];
    ++m_scifi_hits_ECAL_clusters_correlation[{scifi_hits.size(), nClusters}];
    ++m_scifi_hits_HCAL_energy_correlation[{scifi_hits.size(), hcal_adc_abovethreshold}];
    ++m_velo_rich1_hits_correlation[{velo_hits.size(), rich_hits.nTotalHits( Rich::Rich1 )}];
    ++m_scifi_rich1_hits_correlation[{scifi_hits.size(), rich_hits.nTotalHits( Rich::Rich1 )}];
    ++m_scifi_rich2_hits_correlation[{scifi_hits.size(), rich_hits.nTotalHits( Rich::Rich1 )}];
  }

private:
  mutable Gaudi::Accumulators::Histogram<2> m_velo_scifi_hits_correlation{
      this,
      "VeloScifiHitsCorrelation",
      "VeloScifiHitsCorrelation",
      {{200, 0, 3000, "Velo Hits"}, {200, 0, 10000, "ScifiHits"}}};
  mutable Gaudi::Accumulators::Histogram<2> m_velo_muon_hits_correlation{
      this,
      "VeloMuonHitsCorrelation",
      "VeloMuonHitsCorrelation",
      {{200, 0, 3000, "Velo Hits"}, {200, 0, 1000, "MuonHits"}}};
  mutable Gaudi::Accumulators::Histogram<2> m_scifi_muon_hits_correlation{
      this,
      "ScifiMuonHitsCorrelation",
      "ScifiMuonHitsCorrelation",
      {{200, 0, 10000, "SciFi Hits"}, {200, 0, 1000, "MuonHits"}}};
  mutable Gaudi::Accumulators::Histogram<2> m_scifi_hits_ECAL_clusters_correlation{
      this,
      "ScifiECALCorrelation",
      "ScifiECALCorrelation",
      {{200, 0, 10000, "SciFi Hits"}, {200, 0, 600, "ECAL Clusters"}}};
  mutable Gaudi::Accumulators::Histogram<2> m_scifi_hits_HCAL_energy_correlation{
      this,
      "ScifiHCALCorrelation",
      "ScifiHCALCorrelation",
      {{200, 0, 10000, "SciFi Hits"}, {200, 0, 20000, "Total HCAL ADC counts"}}};
  mutable Gaudi::Accumulators::Histogram<2> m_scifi_rich1_hits_correlation{
      this,
      "ScifiRICH1HitsCorrelation",
      "ScifiRICH1HitsCorrelation",
      {{200, 0, 10000, "SciFi Hits"}, {200, 0, 10000, "RICH1 Hits"}}};
  mutable Gaudi::Accumulators::Histogram<2> m_velo_rich1_hits_correlation{
      this,
      "VeloRICH1HitsCorrelation",
      "VeloRICH1HitsCorrelation",
      {{200, 0, 3000, "Velo Hits"}, {200, 0, 10000, "RICH1 Hits"}}};
  mutable Gaudi::Accumulators::Histogram<2> m_scifi_rich2_hits_correlation{
      this,
      "ScifiRICH2HitsCorrelation",
      "ScifiRICH2HitsCorrelation",
      {{200, 0, 10000, "SciFi Hits"}, {200, 0, 8000, "RICH2 Hits"}}};

  Gaudi::Property<int>   m_MinADCHCAL{this, "MinADCHCAL", 10}; // Minimum ADC threshold to have some zero suppression
  Gaudi::Property<float> m_MinEECAL{this, "MinEECAL",
                                    100 * Gaudi::Units::MeV}; // Minimum energy threshold to have some zero suppression
};

DECLARE_COMPONENT( MonitorDetectorCorrelations )
