/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <algorithm>
#include <array>
#include <limits>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "Event/Track.h"
#include "GaudiKernel/IFileAccess.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Transformer.h"

#include "evaluator.h"
#include "model_calcer_wrapper.h"
#include "wrapped_calcer.h"

class TtracksMVAFilter : public LHCb::Algorithm::Transformer<LHCb::Track::Selection( const LHCb::Track::Range& )> {

public:
  TtracksMVAFilter( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, KeyValue{"InputLocation", {}}, KeyValue{"OutputLocation", {}} ) {}

  StatusCode initialize() override;

  float applyBDT( const LHCb::Track* trk ) const;
  float applyTrackPairBDT( float ygap, float trackgap, float tiltY, float adtx, float adty, float yzIntersectionZ,
                           float yzIntersectionY ) const;

  LHCb::Track::Selection operator()( const LHCb::Track::Range& tracksin ) const override { // begin operator

    m_inputTracks += tracksin.size();
    LHCb::Track::Selection tracksout;

    if ( m_applySingleTrackFilter ) {
      for ( const auto& trk : tracksin ) { // begin loop over tracks

        if ( trk->p() < m_momentumThreshold ) continue;

        const float score = applyBDT( trk );

        if ( score < m_singleTrackMVAThreshold ) continue;

        tracksout.insert( trk );

      } // end loop over tracks
    }
    m_passed += tracksout.size();

    if ( !m_applyTrackPairFilter ) { return tracksout; }

    LHCb::Track::Selection tracksout2;
    for ( auto outer = tracksout.begin(); outer != tracksout.end(); ++outer ) { // begin outer loop
      const auto outerTrack = ( *outer );
      auto       it         = std::find( tracksout2.begin(), tracksout2.end(), ( *outer ) );
      if ( it != tracksout2.end() ) continue; // skip if already added
      bool outerTrackAdded = false;
      for ( auto inner = std::next( outer ); inner != tracksout.end(); ++inner ) { // begin inner loop
        const auto innerTrack = ( *inner );
        auto       jt         = std::find( tracksout2.begin(), tracksout2.end(), ( *inner ) );
        if ( jt != tracksout2.end() ) continue;                            // skip if already added
        if ( innerTrack->charge() != -( outerTrack->charge() ) ) continue; // skip if they don't have opposite charge

        // prepare features
        const float outerPosY   = outerTrack->position().y();
        const float innerPosY   = innerTrack->position().y();
        const float outerPosX   = outerTrack->position().x();
        const float innerPosX   = innerTrack->position().x();
        const float outerSlopeY = outerTrack->slopes().y();
        const float innerSlopeY = innerTrack->slopes().y();
        const float outerSlopeX = outerTrack->slopes().x();
        const float innerSlopeX = innerTrack->slopes().x();

        const float ygap     = std::abs( innerPosY - outerPosY );
        const float xgap     = std::abs( innerPosX - outerPosX );
        const float trackgap = std::sqrt( ygap * ygap + xgap * xgap );
        const float tiltY    = std::copysign( 1, outerSlopeY * innerSlopeY );
        if ( tiltY < 0 && m_applyTiltYcut ) continue;
        const float adtx = std::abs( innerSlopeX - outerSlopeX );
        const float adty = std::abs( innerSlopeY - outerSlopeY );

        const float ty0 = outerSlopeY;
        const float ty1 = innerSlopeY;
        const float z0  = outerTrack->position().z();
        const float z1  = innerTrack->position().z();
        const float y0  = outerPosY;
        const float y1  = innerPosY;
        const float c0  = y0 - ty0 * z0;
        const float c1  = y1 - ty1 * z1;

        const float yzIntersectionZ = ( c1 - c0 ) / ( ty0 - ty1 );
        const float yzIntersectionY = ty0 * yzIntersectionZ + c1;

        if ( yzIntersectionZ < m_zMin ) continue;

        const float score = applyTrackPairBDT( ygap, trackgap, tiltY, adtx, adty, yzIntersectionZ, yzIntersectionY );

        if ( score > m_trackPairMVAThreshold ) { // add the tracks if above threshold -- we check in beginning of loops
                                                 // they are not already added
          tracksout2.insert( innerTrack );
          if ( !outerTrackAdded ) { // we don't want to keep adding the same track, but we want to catch all candidate
                                    // pairs
            tracksout2.insert( outerTrack );
            outerTrackAdded = true;
          }
        }
      } // end inner loop
    }   // end outer loop

    m_passed2 += tracksout2.size();
    return tracksout2;
  } // end operator

private:
  ServiceHandle<IFileAccess> m_fileSvc{this, "FileSvc", "ParamFileSvc", "Service used to retrieve file contents"};

  mutable Gaudi::Accumulators::StatCounter<> m_inputTracks{this, "#input tracks"};
  mutable Gaudi::Accumulators::StatCounter<> m_passed{this, "#passed single track MVA selection"};
  mutable Gaudi::Accumulators::StatCounter<> m_passed2{this, "#single tracks passed track pair MVA selection"};

  Gaudi::Property<bool>  m_applySingleTrackFilter{this, "applySingleTrackFilter", true};
  Gaudi::Property<bool>  m_applyTrackPairFilter{this, "applyTrackPairFilter", true};
  Gaudi::Property<float> m_singleTrackMVAThreshold{this, "singleTrackMVAThreshold", 0.3};
  Gaudi::Property<float> m_trackPairMVAThreshold{this, "trackPairMVAThreshold", 0.75};
  Gaudi::Property<float> m_momentumThreshold{this, "momentumThreshold", 2000.};
  Gaudi::Property<bool>  m_applyTiltYcut{this, "applyTiltYcut", true};
  Gaudi::Property<float> m_zMin{this, "zMin", 1500};

  Gaudi::Property<std::string> m_paramFilesLocationSingleTrackMVA{
      this, "ParamFilesLocationSingleTrackMVA", "paramfile://data/ttracks_catboost_single_track_MVA_filter-202306.cbm"};
  Gaudi::Property<std::string> m_paramFilesLocationTrackPairMVA{
      this, "ParamFilesLocationTrackPairMVA", "paramfile://data/ttracks_catboost_track_pair_MVA_filter-202306.cbm"};
  std::unique_ptr<NCatboostStandalone::TOwningEvaluator> m_StandAloneCatBoostModelSingleTrackMVA;
  std::vector<unsigned char>                             m_CatBoostModelBlobSingleTrackMVA;
  std::unique_ptr<NCatboostStandalone::TOwningEvaluator> m_StandAloneCatBoostModelTrackPairMVA;
  std::vector<unsigned char>                             m_CatBoostModelBlobTrackPairMVA;
};

StatusCode TtracksMVAFilter::initialize() {
  auto sc = LHCb::Algorithm::Transformer<LHCb::Track::Selection( const LHCb::Track::Range& )>::initialize();
  if ( sc.isFailure() ) { return sc; }
  // initialise catboost model
  m_fileSvc.retrieve().orThrow( "Could not obtain IFileAccess Service" );
  auto const buffer = m_fileSvc->read( m_paramFilesLocationSingleTrackMVA );
  if ( !buffer ) {
    throw GaudiException( "Failed to read " + m_paramFilesLocationSingleTrackMVA, this->name(), StatusCode::FAILURE );
  }

  // get blob
  m_CatBoostModelBlobSingleTrackMVA.assign( buffer.value().begin(), buffer.value().end() );

  // load model
  try {
    m_StandAloneCatBoostModelSingleTrackMVA =
        std::make_unique<NCatboostStandalone::TOwningEvaluator>( m_CatBoostModelBlobSingleTrackMVA );
  } catch ( const std::runtime_error& e ) {
    error() << "Failed to load CatBoost model. Likely cases are "
            << "a malformed or missing file. Model file: " << m_paramFilesLocationSingleTrackMVA << std::endl;
    error() << "Message from Catboost:" << std::endl;
    error() << e.what();
    error() << endmsg;
    return StatusCode::FAILURE;
  }

  auto const buffer2 = m_fileSvc->read( m_paramFilesLocationTrackPairMVA );
  if ( !buffer2 ) {
    throw GaudiException( "Failed to read " + m_paramFilesLocationTrackPairMVA, this->name(), StatusCode::FAILURE );
  }

  // get blob
  m_CatBoostModelBlobTrackPairMVA.assign( buffer2.value().begin(), buffer2.value().end() );

  // load model
  try {
    m_StandAloneCatBoostModelTrackPairMVA =
        std::make_unique<NCatboostStandalone::TOwningEvaluator>( m_CatBoostModelBlobTrackPairMVA );
  } catch ( const std::runtime_error& e ) {
    error() << "Failed to load CatBoost model. Likely cases are "
            << "a malformed or missing file. Model file: " << m_paramFilesLocationTrackPairMVA << std::endl;
    error() << "Message from Catboost:" << std::endl;
    error() << e.what();
    error() << endmsg;
    return StatusCode::FAILURE;
  }

  return sc;
}

float TtracksMVAFilter::applyBDT( const LHCb::Track* trk ) const {
  // prepare features
  const float pt  = sqrt( trk->momentum().x() * trk->momentum().x() + trk->momentum().y() * trk->momentum().y() );
  const float pz  = trk->momentum().z();
  const float eta = trk->pseudoRapidity();
  const float y   = trk->position().y();
  const float x   = trk->position().x();
  const float r   = sqrt( x * x + y * y );

  // respect the order
  std::vector<float> features = {y, eta, pt, pz, r};
  const float        score =
      m_StandAloneCatBoostModelSingleTrackMVA->Apply( features, NCatboostStandalone::EPredictionType::Probability );

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Single track Ttracks CatBoost features and output probability:" << std::endl;
    debug() << "pt: " << pt << std::endl;
    debug() << "pz: " << pz << std::endl;
    debug() << "eta: " << eta << std::endl;
    debug() << "y: " << y << std::endl;
    debug() << "r: " << r << std::endl;
    debug() << "score: " << score << std::endl;
  }
  return score;
}

float TtracksMVAFilter::applyTrackPairBDT( float ygap, float trackgap, float tiltY, float adtx, float adty,
                                           float yzIntersectionZ, float yzIntersectionY ) const {
  std::vector<float> features = {ygap, trackgap, tiltY, adtx, adty, yzIntersectionZ, yzIntersectionY};
  const float        score =
      m_StandAloneCatBoostModelTrackPairMVA->Apply( features, NCatboostStandalone::EPredictionType::Probability );
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Track pair Ttracks CatBoost features and output probability:" << std::endl;
    debug() << "ygap: " << ygap << std::endl;
    debug() << "trackgap: " << trackgap << std::endl;
    debug() << "tiltY: " << tiltY << std::endl;
    debug() << "adtx: " << adtx << std::endl;
    debug() << "adty: " << adty << std::endl;
    debug() << "yzIntersectionZ: " << yzIntersectionZ << std::endl;
    debug() << "yzIntersectionY: " << yzIntersectionY << std::endl;
    debug() << "score: " << score << std::endl;
  }
  return score;
}

DECLARE_COMPONENT( TtracksMVAFilter )
