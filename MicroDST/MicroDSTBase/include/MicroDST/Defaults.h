/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_DEFAULTS_H
#define MICRODST_DEFAULTS_H 1

// Include files

/** @class Defaults Defaults.h MicroDST/Defaults.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2008-09-01
 */
template <typename T>
struct Defaults;
template <typename T>
struct Location;

#endif // MICRODST_DEFAULTS_H
