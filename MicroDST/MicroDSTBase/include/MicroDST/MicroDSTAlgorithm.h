/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODSTALGORITHM_H
#define MICRODSTALGORITHM_H 1

// Include files
#include <GaudiAlg/GaudiAlgorithm.h>
#include <GaudiKernel/IRegistry.h>
#include <MicroDST/MicroDSTCommon.h>

class ObjectContainerBase;

/** @class MicroDSTAlgorithm MicroDSTAlgorithm.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-10-11
 */
class GAUDI_API MicroDSTAlgorithm : public MicroDSTCommon<GaudiAlgorithm> {

public:
  /// Standard constructor
  using MicroDSTCommon<GaudiAlgorithm>::MicroDSTCommon;

  StatusCode initialize() override; ///< Algorithm initialization

protected:
  const std::string& inputTESLocation() const {
    return ( m_inputTESLocations.empty() ? m_inputTESLocation.value() : m_inputTESLocations[0] );
  }

  virtual const std::vector<std::string>& inputTESLocations() { return m_inputTESLocations; }

  void setInputTESLocation( const std::string& newLocation ) {
    m_inputTESLocations.clear();
    m_inputTESLocations.value().push_back( newLocation );
  }

private:
  Gaudi::Property<std::string>              m_inputTESLocation{this, "InputLocation", ""};
  Gaudi::Property<std::vector<std::string>> m_inputTESLocations{this, "InputLocations", {}};
};

#endif // COPYANDSTOREDATA_H
