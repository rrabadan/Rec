/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
// from Gaudi
#include "Functions.h"
#include <GaudiAlg/GaudiTool.h>
#include <MicroDST/MicroDSTCommon.h>

/** @class MicroDSTTool MicroDSTTool.h MicroDST/MicroDSTTool.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-12-04
 */
class GAUDI_API MicroDSTTool : public MicroDSTCommon<GaudiTool> {

public:
  /// Standard constructor
  using MicroDSTCommon::MicroDSTCommon;

  StatusCode initialize() override {
    return MicroDSTCommon::initialize().andThen(
        [&] { return MicroDST::synchroniseProperty( this->parent(), this, "OutputPrefix" ); } );
  }
};
