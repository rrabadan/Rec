/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MicroDST/MicroDSTAlgorithm.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MicroDSTAlgorithm
//
// 2007-10-11 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================
// Initialization
//=============================================================================
StatusCode MicroDSTAlgorithm::initialize() {
  StatusCode sc = MicroDSTCommon<GaudiAlgorithm>::initialize();
  if ( sc.isFailure() ) return sc;

  if ( !m_inputTESLocations.empty() && !m_inputTESLocation.empty() ) {
    sc = Error( "You have set both InputLocation AND InputLocations properties" );
  }

  if ( m_inputTESLocations.empty() ) { this->setInputTESLocation( m_inputTESLocation ); }

  return sc;
}

//=============================================================================
