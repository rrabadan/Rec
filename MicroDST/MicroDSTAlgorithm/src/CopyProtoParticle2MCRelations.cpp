/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "ICloneMCParticle.h"
#include "Kernel/Particle2MCParticle.h"
#include "MicroDST/Defaults.h"
#include "MicroDST/Functions.h"
#include "MicroDST/MicroDSTAlgorithm.h"
#include "boost/algorithm/string.hpp"
#include <boost/bind/bind.hpp>
#include <boost/function.hpp>
#include <boost/type_traits/remove_pointer.hpp>

/** @class BindType2Cloner BindType2ClonerDef.h MicroDST/BindType2ClonerDef.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2008-09-01
 */
template <typename T>
struct BindType2Cloner {
  typedef T                      Type;
  typedef MicroDST::BasicCopy<T> Cloner;
};

/** @namespace MicroDST RelTableFunctors.h MicroDST/RelTableFunctors.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2009-04-17
 */
namespace MicroDST {

  template <class TABLE>
  struct Cloners {
    typedef boost::function<typename TABLE::From( typename TABLE::From )> From;
    typedef boost::function<typename TABLE::To( typename TABLE::To )>     To;
  };

  template <class TABLE>
  struct EntryClonerBase {
    EntryClonerBase( const typename Cloners<TABLE>::From& fromCloner, const typename Cloners<TABLE>::To& toCloner )
        : m_fromCloner( fromCloner ), m_toCloner( toCloner ) {}

  protected:
    typename Cloners<TABLE>::From m_fromCloner;
    typename Cloners<TABLE>::To   m_toCloner;

  private:
    EntryClonerBase() {}
  };

  template <class TABLE, bool WT = true>
  struct EntryCloner : public EntryClonerBase<TABLE> {

    EntryCloner( const typename Cloners<TABLE>::From& fromCloner, const typename Cloners<TABLE>::To& toCloner )
        : EntryClonerBase<TABLE>( fromCloner, toCloner ) {}
    typename TABLE::Entry operator()( const typename TABLE::Entry& entry ) const {
      const typename TABLE::From clonedFrom = this->m_fromCloner( entry.from() );
      const typename TABLE::To   clonedTo   = this->m_toCloner( entry.to() );
      return typename TABLE::Entry( clonedFrom, clonedTo, entry.weight() );
    }
  };

  template <class TABLE>
  struct EntryCloner<TABLE, false> : public EntryClonerBase<TABLE> {
    EntryCloner( const typename Cloners<TABLE>::From& fromCloner, const typename Cloners<TABLE>::To& toCloner )
        : EntryClonerBase<TABLE>( fromCloner, toCloner ) {}
    typename TABLE::Entry operator()( const typename TABLE::Entry& entry ) const {
      const typename TABLE::From clonedFrom = this->m_fromCloner( entry.from() );
      const typename TABLE::To   clonedTo   = this->m_toCloner( entry.to() );
      return typename TABLE::Entry( clonedFrom, clonedTo );
    }
  };

  template <class TABLE>
  struct TableCloner {
    TableCloner( const typename Cloners<TABLE>::From& fromCloner, const typename Cloners<TABLE>::To& toCloner )
        : m_cloner( fromCloner, toCloner ) {}
    TABLE* operator()( const TABLE* table ) {
      TABLE* cloneTable = new TABLE();
      cloneTable->setVersion( table->version() );
      for ( const auto& rel : table->relations() ) {
        typename TABLE::Entry entryClone = m_cloner( rel );
        if ( isValid( entryClone.from() ) && isValid( entryClone.to() ) ) { cloneTable->add( entryClone ).ignore(); }
      } // loop on all relations
      return cloneTable;
    }

  protected:
    EntryCloner<TABLE, TABLE::weighted> m_cloner;

  private:
    TableCloner() {}
  };

  /** @class RelationsClonerAlg RelationsClonerAlg.h MicroDST/RelationsClonerAlg.h
   *
   *  Templated algorithm to clone a relations table between Froms and Tos. The Froms
   *  are assumed to be cloner already. The Tos are cloned, unless the ClonerType
   *  property is set to "NONE", in which case the cloner table points to the original
   *  Tos.
   *
   *  @author Juan PALACIOS juan.palacios@nikhef.nl
   *  @date   2009-04-14
   */

  template <typename TABLE>
  class RelationsClonerAlg : public MicroDSTAlgorithm {

  private:
    typedef Defaults<TABLE>                           DEFAULTS;
    typedef Location<TABLE>                           LOCATION;
    typedef typename BindType2Cloner<TABLE>::ToCloner CLONER;
    typedef MicroDST::TableCloner<TABLE>              TableCloner;

  public:
    //===========================================================================
    /// Standard constructor
    RelationsClonerAlg( const std::string& name, ISvcLocator* pSvcLocator )
        : MicroDSTAlgorithm( name, pSvcLocator )
        , m_cloner( NULL )
        , m_clonerType( DEFAULTS::clonerType )
        , m_useOriginalFrom( false )
        , m_tableCloner( boost::bind( &RelationsClonerAlg<TABLE>::cloneFrom, &( *this ), boost::placeholders::_1 ),
                         boost::bind( &RelationsClonerAlg<TABLE>::cloneTo, &( *this ), boost::placeholders::_1 ) ) {
      declareProperty( "ClonerType", m_clonerType );
      declareProperty( "UseOriginalFrom", m_useOriginalFrom, "Take 'from' object from original location" );
      // setProperty( "OutputLevel", 1 );
    }

    //===========================================================================

    StatusCode initialize() override {
      StatusCode sc = MicroDSTAlgorithm::initialize();
      if ( sc.isFailure() ) return sc;

      if ( inputTESLocations().empty() ) { setInputTESLocation( LOCATION::Default ); }
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "inputTESLocation() is " << inputTESLocation() << endmsg;

      if ( m_clonerType.empty() || m_clonerType == "NONE" ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "ClonerType set to NONE. No cloning of To performed." << endmsg;
      } else {
        m_cloner = tool<CLONER>( m_clonerType, this );
        if ( m_cloner ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Found cloner " << m_clonerType << endmsg;
          sc = StatusCode::SUCCESS;
        } else {
          sc = Error( "Failed to find cloner " + m_clonerType );
        }
      }

      return sc;
    }

    //=========================================================================

    StatusCode execute() override {
      setFilterPassed( true );

      for ( const auto& loc : inputTESLocations() ) { copyTableFromLocation( loc ); }

      return StatusCode::SUCCESS;
    }

    //=========================================================================

  protected:
    void copyTableFromLocation( const std::string& inputLocation ) {

      class DataObjectGuard {
        const DataObject* m_ptr;

      public:
        explicit DataObjectGuard( const DataObject* ptr ) : m_ptr( ptr ) {}
        ~DataObjectGuard() {
          if ( !m_ptr->registry() ) delete m_ptr;
        }
      };

      const std::string outputLocation = this->outputTESLocation( inputLocation );

      if ( exist<TABLE>( outputLocation ) ) {
        this->Warning( "Object " + outputLocation + " already exists. Not cloning.", StatusCode::SUCCESS, 0 ).ignore();
        return;
      }

      const TABLE* table = getIfExists<TABLE>( inputLocation );
      if ( table && !table->relations().empty() ) {

        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "Going to clone " << table->relations().size() << " relations from " << inputLocation << " into "
                    << outputLocation << endmsg;
        }

        // Note makes a new object !!
        TABLE* cloneTable = m_tableCloner( table );

        // deletes if not saved when leaving scope !
        DataObjectGuard guard( cloneTable );

        // Print
        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "Cloned Table :- " << endmsg;
          for ( const auto& rel : cloneTable->relations() ) {
            verbose() << "  From : " << *rel.from() << " To : " << *rel.to() << endmsg;
          }
        }

        // Save if not empty
        if ( !cloneTable->relations().empty() ) { this->put( cloneTable, outputLocation ); }

      } else {
        if ( msgLevel( MSG::VERBOSE ) ) {
          this->Warning( "Found no table at " + inputLocation, StatusCode::FAILURE, 0 ).ignore();
        }
      }
    }

    //===========================================================================

  private:
    typedef typename BindType2Cloner<TABLE>::ToType                    TO_TYPE;
    typedef typename boost::remove_pointer<typename TABLE::From>::type _From;

  private:
    const CLONER* cloner() { return m_cloner; }

    typename TABLE::From cloneFrom( const typename TABLE::From from ) {
      if ( m_useOriginalFrom ) { return from; }
      if ( !from ) {
        error() << "From is nullptr. Cannot clone!" << endmsg;
        return nullptr;
      }
      if ( !from->parent() ) {
        Warning( "From is not in TES. Cannot clone!", StatusCode::FAILURE, 0 ).ignore();
        return nullptr;
      }
      return getStoredClone<_From>( from );
    }

    typename TABLE::To cloneTo( const typename TABLE::To to ) {
      if ( !m_cloner ) { return to; }
      if ( !to ) {
        error() << "To is nullptr. Cannot clone!" << endmsg;
        return nullptr;
      }

      if ( !to->parent() ) {
        Warning( "To is not in TES. Cannot clone!", StatusCode::FAILURE, 0 ).ignore();
        return nullptr;
      }

      const typename TABLE::To storedTo = getStoredClone<TO_TYPE>( to );
      return ( storedTo ? storedTo : ( *m_cloner )( to ) );
    }

    //===========================================================================

  private:
    CLONER*     m_cloner;
    std::string m_clonerType;
    bool        m_useOriginalFrom;
    TableCloner m_tableCloner;
  };

  /** @class CopyProtoParticle2MCRelations CopyProtoParticle2MCRelations.h
   *
   *  Near-duplicate of the CopyParticle2MCRelations microDST algorithm that
   *  copies weighted LHCb::ProtoParticle to LHCb:MCParticle relations, rather
   *  than weighted LHCb::Particle to LHCb:MCParticle relations.
   *
   *  Another difference is that this algorithm is a specialisation of the
   *  MicroDST::RelationsClonerAlg template class, rather than the
   *  MicroDST::RelationsFromParticleLocationsClonerAlg class that
   *  CopyParticle2MCRelations specialises. With this difference, this class
   *  takes TES locations of relations tables as input, rather than taking
   *  particle locations and trying to deduce the relations table location from
   *  that. It will then take the locations of the associated LHCb::Particle
   *  objects by interrogating the relations table itself.
   */

  /** @class RelationsFromParticleLocationsClonerAlg MicroDST/RelationsFromParticleLocationsClonerAlg.h
   *
   *  Extension to RelationsClonerAlg that dynamically finds the relations to clone from Particles
   *
   *  @author Juan PALACIOS juan.palacios@nikhef.nl
   *  @date   2009-04-14
   */

  template <typename TABLE>
  class RelationsFromParticleLocationsClonerAlg : public RelationsClonerAlg<TABLE> {

  private:
    typedef Defaults<TABLE> DEFAULTS;

  public:
    //===========================================================================
    /// Standard constructor
    RelationsFromParticleLocationsClonerAlg( const std::string& name, ISvcLocator* pSvcLocator )
        : RelationsClonerAlg<TABLE>( name, pSvcLocator ) {
      this->declareProperty( "RemoveOriginals", m_removeOriginals = false );
      this->declareProperty( "RelationsBaseName", m_relationsBaseName = DEFAULTS::relationsName );
      this->declareProperty( "UseRelationsCLID", m_useClassID = false,
                             "Flag to turn on the automatic searching for relations to clone based on CLID" );
    }

    //===========================================================================

  public:
    //=========================================================================

    StatusCode execute() override {
      if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "==> Execute" << endmsg;

      this->setFilterPassed( true );

      // Clear the list of relations locations
      m_relationLocations.clear();

      // Load the Particles and iterate over them, and their daughters,
      // to find all the relations to save
      for ( const auto& loc : this->inputTESLocations() ) {
        const LHCb::Particle::Range particles = this->template getIfExists<LHCb::Particle::Range>( loc );
        if ( !particles.empty() ) { particleLoop( particles.begin(), particles.end() ); }
      }

      // Now, loop over the gathered relation locations and clone them
      for ( const auto& rloc : m_relationLocations ) {
        // Clone the relations and associated MCParticles
        this->copyTableFromLocation( rloc );
        // if requested, remove the originals
        if ( m_removeOriginals ) {
          TABLE* table = this->template getIfExists<TABLE>( rloc );
          if ( table ) {
            const StatusCode sc = this->evtSvc()->unregisterObject( table );
            if ( sc.isSuccess() ) {
              delete table;
            } else {
              this->Error( "Failed to delete '" + rloc + "'" ).ignore();
            }
          }
        }
      }

      return StatusCode::SUCCESS;
    }

    //===========================================================================

  private:
    template <class Iter>
    void particleLoop( Iter it, const Iter end ) {
      for ( ; it != end; ++it ) {
        // If m_relationsBaseName is not empty, use it to deduce possible
        // TES locations for relations to clone
        if ( !m_relationsBaseName.empty() ) { m_relationLocations.insert( relationsLocation( ( *it )->parent() ) ); }
        // If the CLID is defined, use this to search for objects of the correct
        // type in the TES at the same level as the particles
        if ( m_useClassID ) { this->findByCLID( *it ); }
        // Recursively find the relations for the daughters
        this->particleLoop( ( *it )->daughters().begin(), ( *it )->daughters().end() );
      }
    }

    void findByCLID( const LHCb::Particle* particle ) {
      // Get particle location
      std::string pLoc = objectLocation( particle->parent() );
      // Strip the trailing "/Particles"
      boost::erase_all( pLoc, "/Particles" );
      // Load the resulting data node
      const DataObject* node = this->template getIfExists<DataObject>( pLoc );
      if ( node ) {
        // Search for objects with the requested CLID below this location
        this->selectContainers( node, m_relationLocations, TABLE::classID() );
      }
    }

    std::string relationsLocation( const DataObject* pObj ) const {
      // Get the parent container TES location
      std::string pLoc = objectLocation( pObj );

      // Form the relations TES location for this location
      boost::replace_all( pLoc, "/Particles", m_relationsBaseName );

      // return the new location
      return pLoc;
    }

    //===========================================================================

  private:
    bool m_removeOriginals;

    std::string m_relationsBaseName;
    bool        m_useClassID;

    std::set<std::string> m_relationLocations;
  };

} // namespace MicroDST

typedef LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double> PP2MCPTable;
//=============================================================================
template <>
struct BindType2Cloner<PP2MCPTable> {
  typedef LHCb::MCParticle ToType;
  typedef ICloneMCParticle ToCloner;
};
//=============================================================================
template <>
struct Defaults<PP2MCPTable> {
  const static std::string clonerType;
  const static std::string relationsName;
};
const std::string Defaults<PP2MCPTable>::clonerType = "MCParticleCloner";
//=============================================================================
template <>
struct Location<PP2MCPTable> {
  const static std::string Default;
};
const std::string Location<PP2MCPTable>::Default = "NO DEFAULT LOCATION";
//=============================================================================
typedef MicroDST::RelationsClonerAlg<PP2MCPTable> CopyProtoParticle2MCRelations;
DECLARE_COMPONENT_WITH_ID( CopyProtoParticle2MCRelations, "CopyProtoParticle2MCRelations" )
