/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ICloneMCParticle.h"
#include "ICloneMCVertex.h"
#include <Event/MCVertex.h>
#include <MicroDST/MicroDSTTool.h>

//-----------------------------------------------------------------------------
// Implementation file for class : MCVertexCloner
//
// 2007-11-30 : Juan PALACIOS
//-----------------------------------------------------------------------------

/** @class MCVertexCloner MCVertexCloner.h
 *
 *  MicroDSTTool to clone an LHCb::MCVertex and place it in a TES location
 *  parallel to that of the parent. It clones and stores the decay products
 *  of the MCVertex using an using an ICloneMCParticle.
 *  All LHCb::MCParticle cloning is performed by the ICloneMCParticle
 *  implementation, which is set via the ICloneMCParticle property.
 *
 *  @author Juan PALACIOS
 *  @date   2007-11-30
 */
class MCVertexCloner final : public extends<MicroDSTTool, ICloneMCVertex> {

public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;

  LHCb::MCVertex* operator()( const LHCb::MCVertex* mcVertex ) const override;

public:
  typedef MicroDST::BasicItemCloner<LHCb::MCVertex> BasicCloner;

private:
  LHCb::MCVertex* clone( const LHCb::MCVertex* mcVertex ) const;

  void cloneDecayProducts( const SmartRefVector<LHCb::MCParticle>& products, LHCb::MCVertex* clonedVertex ) const;

private:
  ICloneMCParticle*            m_particleCloner = nullptr;
  Gaudi::Property<std::string> m_particleClonerName{this, "ICloneMCParticle", "MCParticleCloner"};
};

//=============================================================================

StatusCode MCVertexCloner::initialize() {
  return extends::initialize().andThen(
      [&] { m_particleCloner = tool<ICloneMCParticle>( m_particleClonerName, this->parent() ); } );
}

//=============================================================================

LHCb::MCVertex* MCVertexCloner::operator()( const LHCb::MCVertex* vertex ) const {
  if ( !vertex ) return nullptr;

  LHCb::MCVertex* clone = getStoredClone<LHCb::MCVertex>( vertex );

  const auto nProd      = vertex->products().size();
  const auto nCloneProd = ( clone ? clone->products().size() : 0 );

  return ( clone && ( nProd == nCloneProd ) ? clone : this->clone( vertex ) );
}

//=============================================================================

LHCb::MCVertex* MCVertexCloner::clone( const LHCb::MCVertex* vertex ) const {
  LHCb::MCVertex* clone = nullptr;
  if ( vertex ) {
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Cloning MCVertex key=" << vertex->key() << " " << objectLocation( vertex ) << endmsg;
    clone = cloneKeyedContainerItem<BasicCloner>( vertex );
    if ( clone ) {
      // Clone and set the mother for this vertex
      clone->setMother( ( *m_particleCloner )( vertex->mother() ) );
      // clone daugthers
      cloneDecayProducts( vertex->products(), clone );
    }
  }
  return clone;
}

//=============================================================================

void MCVertexCloner::cloneDecayProducts( const SmartRefVector<LHCb::MCParticle>& products,
                                         LHCb::MCVertex*                         clonedVertex ) const {
  // Clear the current products
  clonedVertex->clearProducts();

  // loop over the products and clone
  for ( const auto& prod : products ) {
    if ( msgLevel( MSG::DEBUG ) )
      debug() << " -> Cloning MCParticle key=" << prod->key() << " " << objectLocation( prod ) << endmsg;

    LHCb::MCParticle* productClone = ( *m_particleCloner )( prod );
    if ( productClone ) {
      // set origin vertexfor the cloned product particle
      productClone->setOriginVertex( clonedVertex );
      // if not already present, add to vertex products list
      const bool found = std::any_of(
          clonedVertex->products().begin(), clonedVertex->products().end(),
          [&productClone]( const SmartRef<LHCb::MCParticle>& mcP ) { return mcP.target() == productClone; } );
      if ( !found ) { clonedVertex->addToProducts( productClone ); }
      // else
      //{ warning() << "Attempt add a duplicate MCParticle product SmartRef" << endmsg; }
    }
  }
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCVertexCloner )

//=============================================================================
