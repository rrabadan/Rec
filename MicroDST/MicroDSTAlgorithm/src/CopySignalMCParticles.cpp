/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCParticle.h"
#include "ICloneMCParticle.h"
#include "Kernel/Particle2MCLinker.h"
#include "MicroDST/MicroDSTAlgorithm.h"
#include <memory>
#include <string>
#include <vector>
//-----------------------------------------------------------------------------
// Implementation file for class : CopySignalMCParticles
//
// 2015-03-24 : Chris Jones
//-----------------------------------------------------------------------------

/** @class CopySignalMCParticles CopySignalMCParticles.h
 *
 *  Clones all 'signal' MCParticles.
 *
 *  @author Chris Jones
 *  @date   2015-03-24
 */

class CopySignalMCParticles final : public MicroDSTAlgorithm {

public:
  /// Standard constructor
  using MicroDSTAlgorithm::MicroDSTAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  /// Type of MCParticle cloner
  Gaudi::Property<std::string> m_mcpClonerName{this, "ICloneMCParticle", "MCParticleCloner"};

  /// MCParticle Cloner
  ICloneMCParticle* m_cloner = nullptr;

  /// Location of MCParticles to clone
  Gaudi::Property<std::string> m_mcPsLoc{this, "MCParticlesLocation", LHCb::MCParticleLocation::Default};
};

//=============================================================================
// Initialization
//=============================================================================
StatusCode CopySignalMCParticles::initialize() {
  return MicroDSTAlgorithm::initialize().andThen( [&] { m_cloner = tool<ICloneMCParticle>( m_mcpClonerName, this ); } );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CopySignalMCParticles::execute() {
  struct Asct {
    CopySignalMCParticles const*          self      = nullptr;
    std::optional<ProtoParticle2MCLinker> m_charged = {};
    std::optional<ProtoParticle2MCLinker> m_neutral = {};

    auto operator()( const LHCb::MCParticle* p ) {
      // Pick the correct association for charge/neutral
      if ( p->particleID().threeCharge() == 0 ) {
        if ( !m_neutral )
          m_neutral.emplace( self, Particle2MCMethod::NeutralPP, LHCb::ProtoParticleLocation::Neutrals );
        return m_neutral->range( p );
      } else {
        if ( !m_charged ) m_charged.emplace( self, Particle2MCMethod::ChargedPP, LHCb::ProtoParticleLocation::Charged );
        return m_charged->range( p );
      }
    }
  };

  Asct asct{this};

  // load the primary MCParticles
  const auto* mcPs = getIfExists<LHCb::MCParticles>( m_mcPsLoc );
  if ( mcPs ) {
    // Loop over them and look for signal
    for ( const auto* mcP : *mcPs ) {
      if ( mcP->fromSignal() ) ( *m_cloner )( mcP );
    }
  }
  return StatusCode::SUCCESS;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CopySignalMCParticles )

//=============================================================================
